import datetime
import time
import schedule
import os

def job():
    os.system('/home/ts_ryohei_a_yasuda/.pyenv/shims/python3 /var/app/total/src/rakuten/FGS41-google-trends/google_scrape.py')

schedule.every().sunday.at("09:00").do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
