import argparse
import sys
import MeCab

# set perser
parser = argparse.ArgumentParser(
    prog='initMeCab',
    usage='Use initial setting for MeCab',
    description='Please read Read.me. There are 2 check samples when you init MeCab.',
    epilog='end',
    add_help=True,
)

# add param
parser.add_argument('-c', '--check', help='1: check 1, 2: check 2. ... see Read.me.', action='store')
args = parser.parse_args()

# each test
if args.check and args.check == '2' :
    # -c 2 : 'check 2' initial test
    m = MeCab.Tagger("-d /usr/lib/mecab/dic/mecab-ipadic-neologd")
    print(m.parse("安倍晋三首相が、カシオの時計を買っていた。"))
else :
    # -c 1 : 'check 1' initial test
    m = MeCab.Tagger("-Ochasen")
    print(m.parse("安倍晋三首相は、国会で施政方針演説を行った。"))
