'use strict';

const clipboardy = require('clipboardy');
const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const today = moment().format('YYYYMMDD');
const shell = require('shelljs');


const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const image = new(require('../modules/image.js'));
const util = new(require('../modules/util.js'));
const robot = new(require('../modules/robot.js'));
const screenshot = new(require('../modules/screenshot.js'));
const selenium = new(require('../modules/selenium.js'));
const watson = new(require('../modules/watson.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'screen', alias: 's', type: String }
]);

// variable
let driver = null;
const basename = 'trySimilarBot';


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let from1 = { x: 1120, y: 500, z: 100 };
            let to1 = { x: 1120, y: 800, z: 100 };
            let from2 = { x: 700, y: 180, z: 100 };
            let to2 = { x: 700, y: 540, z: 100 };

            // other screen setting
            switch (cli['screen']) {
                case 'mypc':
                    from1 = { x: 1460, y: 800, z: 100 };
                    to1 = { x: 1460, y: 1700, z: 100 };
                    from2 = { x: 800, y: 480, z: 100 };
                    to2 = { x: 740, y: 830, z: 100 };
                    break;
            }

            screenshot.init();
            driver = yield selenium.init(cli['browser']);
            driver.get('https://www.similarweb.com/website/view.co');

            driver.wait(until.titleIs('Pardon Our Interruption'), 30000);
            yield util.sleep(15000);

            // move and scroll
            yield robot.move(from1, to1);
            yield robot.clickScroll(4);

            // move to btn valify
            robot.move(from2, to2);
            robot.click();

            resolve();
        });
    });
}


// init action
const login = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            clipboardy.writeSync('SDFASDFASDFASFASFASDFAF');
            let sss = clipboardy.readSync();



            console.log(888, sss);


            resolve();
        });
    });
}



// take picture of bot screen shot
const takeScreenShot = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let screen = { x: 628, y: 418 }

            // other screen setting
            switch (cli['screen']) {
                case 'mypc':
                    screen = { x: 692, y: 707 }
                    break;
            }

            yield util.mkdir(`./${conf.dirPath_out}`);
            yield util.mkdir(`./${conf.dirPath_out}/${today}`);
            yield util.mkdir(`./${conf.dirPath_out}/${today}/${basename}`);
            yield util.sleep(2000);

            // screen shot
            yield screenshot.do(screen.x, screen.y, 100, 100, 0, `./${conf.dirPath_out}/${today}/${basename}/screenShot.png`);
            yield util.sleep(4000);

            // remove arc line
            let { canvas, context, images } = yield image.getCanvasContext(100, 100, 'rgb(255, 255, 255)', [`${conf.dirPath_out}/${today}/${basename}/screenShot.png`]);
            context.drawImage(images[0], 0, 0, 100, 100);
            context.lineWidth = 6;
            context.strokeStyle = 'rgb(255, 255, 255)';
            context.beginPath();
            context.arc(50, 50, 55, 0, 2 * Math.PI);
            context.stroke();
            yield image.setCanvasContext(canvas, `${conf.dirPath_out}/${today}/${basename}/screenShot.png`);
            resolve();
        });
    });
}

// roteto the image icon
const rotetoImage = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create roteto images
            yield util.sleep(5000);
            shell.exec(`node src/tools/getRotateImages.js -e png -R 40 -r 1 -i ./${conf.dirPath_out}/${today}/${basename}/screenShot.png -o ./${conf.dirPath_out}/${today}/${basename}/`);
            resolve();
        });
    });
}

// get image data from visual recognition
const getVisualRecognition = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let resArr = [];
            let filename = fs.readdirSync(`./${conf.dirPath_out}/${today}/${basename}`);
            for (let i = 0; i < filename.length; i++) {

                if (filename[i].match('.json') || filename[i] === 'screenShot.png') {
                    continue;
                }

                let img_url = `${conf.dirPath_out}/${today}/${basename}/${filename[i]}`;
                let classifier_ids = [`animalxdirectionx12_957563722`, `animalxdirectionx4_1518359035`, `symbol_830737325`];
                let threshold = 0.3;
                yield watson.init();
                let res = yield watson.getClassifier(img_url, classifier_ids, threshold);
                let image = res.images[0].image;
                let classifiers = res.images[0].classifiers;
                let name = res.images[0].image;

                //get direct4Max
                let direction4 = getClassifier(classifiers, 'animal-direction-4');
                let direction12 = getClassifier(classifiers, 'animal-direction-12');

                resArr.push({
                    name,
                    direction4,
                    direction12,
                    highScoreClass4: direction4.highScoreClass,
                    highScore4: direction4.highScore,
                    highScoreClass12: direction12.highScoreClass,
                    highScore12: direction12.highScore
                });
            }

            resArr.sort(function (a, b) {
                if (a.highScoreClass4 < b.highScoreClass4) return -1;
                if (a.highScoreClass4 > b.highScoreClass4) return 1;
                if (a.highScore4 < b.highScore4) return 1;
                if (a.highScore4 > b.highScore4) return -1;
                return 0;
            });

            fs.writeFileSync(`${conf.dirPath_out}/${today}/${basename}/image.json`, JSON.stringify(resArr), 'utf-8');

            resolve();
        });
    });
}

// get symbol data from visual recognition
const isSymbol = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(1500);
            let img_url = `${conf.dirPath_out}/${today}/${basename}/screenShot.png`;
            let classifier_ids = [`animalxdirectionx12_957563722`, `animalxdirectionx4_1518359035`, `symbol_830737325`];
            let threshold = 0.6;
            yield watson.init();
            let res = yield watson.getClassifier(img_url, classifier_ids, threshold);
            let image = res.images[0].image;
            let classifiers = res.images[0].classifiers;
            let name = res.images[0].image;
            let flag = false;

            for (let i = 0; i < classifiers.length; i++) {
                if (classifiers[i].classifier_id === 'symbol_830737325') {
                    if (classifiers[i].classes[0].class === 'check') {
                        flag = true;
                    }
                }
            }

            resolve(flag);
        });
    });
}

// get image data from visual recognition
const getClassifier = function (classifiers, name) {

    for (let i = 0; i < classifiers.length; i++) {
        if (classifiers[i].name === name) {

            // get high score
            let classes = classifiers[i].classes;
            let highScore = 0;
            let highScoreClass = '';
            for (let j = 0; j < classes.length; j++) {
                if (classes[j].score > highScore) {
                    highScore = classes[j].score;
                    highScoreClass = classes[j].class;
                }
            }

            classifiers[i].highScore = highScore;
            classifiers[i].highScoreClass = highScoreClass;
            return classifiers[i];
        }
    }

    return {
        classifier_id: '',
        name: '',
        classes: [],
        highScoreClass: 'XXX',
        highScore: 0
    }
}

// get click num
const getClickNum = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let json = fs.readFileSync(`${conf.dirPath_out}/${today}/${basename}/image.json`);
            let image = JSON.parse(json);

            // similar bot is 40/360°changing, so we culc as this ::  screenShot_80.png > 80 / 40 = 2
            let clicknum = parseInt(image[0].name.replace('screenShot_', '').replace('.png', ''), 10) / 40;

            resolve(clicknum);
        });
    });
}

// get image data from visual recognition
const valifyBot = function (click_num) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // move to btn valify
            let from1 = { x: 570, y: 180, z: 100 };
            let to1 = { x: 570, y: 485, z: 100 };
            robot.move(from1, to1);

            for (let i = 0; i < click_num; i++) {
                robot.click();
                yield util.sleep(1000);
            }

            // move to btn valify
            let from2 = { x: 570, y: 485, z: 100 };
            let to2 = { x: 674, y: 560, z: 100 };
            robot.move(from2, to2);
            robot.click();

            resolve();
        });
    });
}

// get image data from visual recognition
const completeBot = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // move to btn valify
            let from1 = { x: 570, y: 180, z: 100 };
            let to1 = { x: 570, y: 675, z: 100 };
            robot.move(from1, to1);

            robot.click();
            yield util.sleep(1000);

            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        let click_num = 0;

        switch (cli['run']) {
            case 3: // test action
                yield login();
                break;
            case 2: // test action
                yield completeBot();
                break;
            case 1: // usual action
            default:
                yield init();

                let isEnd = false;
                while (!isEnd) {
                    yield takeScreenShot();
                    isEnd = yield isSymbol();
                    if (!isEnd) {
                        yield rotetoImage();
                        yield getVisualRecognition();
                        click_num = yield getClickNum();
                        yield valifyBot(click_num);
                    }
                }

                yield completeBot();
                break;
        }
    });
}

run();