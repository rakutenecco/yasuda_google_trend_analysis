'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const tsv = new(require('../modules/tsv.js'));
const util = new(require('../modules/util.js'));
const cheerio = new(require('../modules/cheerio-httpcli.js'));
const selenium = new(require('../modules/selenium.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String }
]);

// variable
let driver = null;


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            // create table "proxy_list"
            yield db.connect(conf.filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS proxy_list(id TEXT primary key, ip TEXT, port INTEGER, tokumei TEXT, resIP TEXT, connect INTEGER, decision TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql, {});

            // init selenium driver
            if (cli['run'] === 1) {
                driver = yield selenium.init(cli['browser']);
            }

            resolve();
        });
    });
}

// scrape proxy data, and set db as proxy_list
const scrapeProxy = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let i = 1;
            let max = 2;

            // scrape each page
            while (i < max) {

                let url = conf.proxy_url_cybersyndrome.replace('{0}', i);

                // proxy setting
                let pa = yield getProxyArray(url);
                max = pa.max ? Math.ceil(pa.max / 500) : 1;
                for (let j = 0; j < pa.list.length; j++) {
                    const sql = `INSERT OR REPLACE INTO proxy_list(id, ip, port, tokumei, resIP, connect, decision, reg_date) VALUES ("${pa.list[j].id}", "${pa.list[j].ip}", "${pa.list[j].port}", "${pa.list[j].tokumei}", "", "", "", "");`;
                    yield db.do(`run`, sql, {});
                };
                i++;
            }

            resolve();
        });
    });
}

// check proxy data
const chkProxy = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let id = '';
            let ip = '';
            let port = '';
            let resIP = '';
            let decision = '';
            let connect = 0;
            let reg_date = moment().format('YYYYMMDD');

            // get proxy data
            let sql = `SELECT * FROM proxy_list where tokumei = 'A' and decision = ''`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                id = res[i].id;
                ip = res[i].ip;
                port = res[i].port;
                resIP = '';
                decision = 'X';
                connect = 0;

                yield cheerio.setProxy(ip, port);

                let $ = yield cheerio.get(conf.proxy_url_shindankun);
                if ($ && $('body').text().match(/総合評価/i)) {

                    let result_date = $('tr').eq(17).text();
                    if (result_date) {
                        let row = result_date.split('\n');
                        resIP = row[1].split(' ')[0];
                        decision = row[6].replace('総合評価：', '');
                        connect = 1;
                    }
                }

                // get proxy data
                sql = `UPDATE proxy_list SET resIP = '${resIP}', connect = '${connect}', decision = '${decision}' WHERE id = '${id}';`;
                yield db.do(`run`, sql, {});
                console.log(`[Update] ${id}`);
            }
        });
    });
}

// drop table
const reset = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // init
            yield init();

            // drop table proxy_list
            let sql = `DROP TABLE IF EXISTS proxy_list;`;
            yield db.do(`run`, sql, {});

            // create table proxy_list
            sql = `CREATE TABLE IF NOT EXISTS proxy_list(id TEXT primary key, ip TEXT, port INTEGER, tokumei TEXT, resIP TEXT, connect INTEGER, decision TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql, {});

            yield db.close();
            resolve();
        });
    });
}

// robot similar web
const test = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();
            let sql = `SELECT * FROM proxy_list where tokumei = 'A' and decision = 'A+'`;
            let res = yield db.do(`all`, sql, {});
            fs.writeFileSync(`./${conf.dirPath_out_test}/getProxyList.txt`, JSON.stringify(res));
            yield db.close();
            resolve();
        });
    });
}

// robot similar web
const getProxyArray = function (url) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // show similar on google search
            let list = [];
            driver.get(url);
            driver.wait(until.titleIs('CyberSyndrome : Search Results'), 1000);

            // get data
            let $ = yield selenium.parseBody(driver);
            let max = 0;

            let matched = $('#div_result').text().match(/検索結果：(.*?)件/i);
            if (matched) {
                max = parseInt(matched[0].replace(/検索結果：/i, '').replace(/件/i, '').trim(), 10);
            }

            $('tr').each(function (e, i) {

                let id = $(this).find('td').eq(1).text();
                if (id) {
                    let ids = id.split(':');
                    let ip = ids[0];
                    let port = ids[1];
                    let tokumei = $(this).find('td').eq(3).text();
                    list.push({ id: id, ip: ip, port: port, tokumei: tokumei });
                }
            });

            resolve({ max, list });
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 3: // reset table action
                yield reset();
                break;
            case 2: // test action
                yield test();
                break;
            case 1: // usual action
            default:
                /*****
                 * you have to build server
                 * $ npm run server
                 * then you can run with choosing browser
                 * $ node src/scraping/proxy.js -B phantom -r 1
                 * or
                 * $ npm run proxy
                 *****/
                yield reset();
                yield init();
                yield scrapeProxy();
                yield chkProxy();

                break;
        }
    });
}

run();