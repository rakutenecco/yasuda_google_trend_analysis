'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const robot = require('robotjs');
const shell = require('shelljs');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const tsv = new(require('../modules/tsv.js'));
const util = new(require('../modules/util.js'));
const selenium = new(require('../modules/selenium.js'));

// parse args
const cli = commandArgs([
    { name: 'browser', alias: 'B', type: String }
]);

// variable
let driver = null;



// Speed up the mouse.
robot.setMouseDelay(2);

var twoPI = Math.PI * 2.0;
var screenSize = robot.getScreenSize();
var height = (screenSize.height / 2) - 10;
var width = screenSize.width;



function intoSimilar(to) {

    const r = Math.floor(Math.random() * 5);

    const pos0 = { x: 0, y: 0, z: 0 };
    const pos1 = { x: 200, y: 298, z: 200 };

    // move search box
    move(pos0, to);
    robot.mouseClick("left");
    util.sleep(500);

    // click typing
    robot.typeString("similarweb");
    robot.keyTap("enter");
    util.sleep(500);

    // move first search list
    move(to, pos1);
    util.sleep(3000);
    robot.mouseClick("left");
    util.sleep(5000 + 200 * r);
}



function scroll(from) {

    return new Promise((resolve, reject) => {
        co(function* () {

            const pos1 = { x: 1116, y: 800, z: 470 };
            const pos2 = { x: 1116, y: 120, z: 470 };
            const r = Math.floor(Math.random() * 15) + 5;

            move(from, pos1);
            yield clickScroll(r);
            move(pos1, pos2);
            yield clickScroll();

            resolve();
        });
    });
}

function chkFile(fileName) {

    const files = fs.readdirSync('./out');
    let flag = false;
    for (let i = 0; i < files.length; i++) {
        if (files[i].match(fileName)) {
            flag = true;
        }
    }

    return flag;
}


function randomMove(from) {

    //
    if (0 === Math.floor(Math.random() * 1)) {

        const pos1 = { x: 1046, y: 820, z: 470 };
        const pos2 = [
            { x: 340, y: 395, z: 100 },
            { x: 340, y: 425, z: 100 },
            { x: 340, y: 470, z: 100 },
            { x: 340, y: 500, z: 100 },
            { x: 340, y: 530, z: 100 }
        ]
        move(from, pos1);
        clickScroll();

        move(pos1, pos2[0]);
        util.sleep(100);
        robot.mouseClick("left");
        util.sleep(8000);

        move(from, pos1);
        clickScroll();

        move(pos1, pos2[1]);
        util.sleep(100);
        robot.mouseClick("left");
        util.sleep(8000);

    }
}



// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // driver = getBuilder();
            // driver = yield selenium.init();



            // connect to table "proxy_list"
            yield db.connect(conf.filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS proxy_list(id TEXT primary key, ip TEXT, port INTEGER, tokumei TEXT, resIP TEXT, connect INTEGER, decision TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql, {});

            // open browser
            let ip = '114.215.102.168';
            let port = '';
            yield openBrowser(ip, port);

            resolve();
        });
    });
}

//  action
const hi = function () {

    return new Promise((resolve, reject) => {
        co(function* () {


            // connect to table "proxy_list"
            yield db.connect(conf.filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS proxy_list(id TEXT primary key, ip TEXT, port INTEGER, tokumei TEXT, resIP TEXT, connect INTEGER, decision TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql, {});

            // open browser
            let ip = '114.215.102.168';
            let port = '';
            yield openBrowser(ip, port);

            resolve();
        });
    });
}



// open browser
const openBrowser = function (ip, port) {

    return new Promise((resolve, reject) => {
        co(function* () {

            shell.exec(`chromium-browser --proxy-server="${ip}:${port}"`);
            // select * from proxy_list where tokumei = 'A' and (decision = 'A+' or decision = 'A')

            // get proxy data
            let sql = `SELECT * FROM proxy_list where tokumei = 'A' and decision = ''`;
            let res = yield db.do(`all`, sql, {});



            resolve();
        });
    });
}



let pos = [
    { x: 200, y: 150, z: 200 },
    { x: 200, y: 290, z: 200 },
    { x: 450, y: 210, z: 200 },
    { x: 350, y: 140, z: 140 }, // 3
    { x: 380, y: 141, z: 240 }, // 4
    { x: 320, y: 100, z: 180 },
    { x: 344, y: 220, z: 210 },
    { x: 362, y: 220, z: 80 }, // 7
    { x: 1046, y: 800, z: 470 },
    { x: 1046, y: 140, z: 470 },
    { x: 1086, y: 230, z: 170 },
    { x: 1100, y: 330, z: 100 },
    { x: 1300, y: 330, z: 100 },
    { x: 980, y: 330, z: 100 },
];


// robot similar web
const robotSimilar = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // go to similar
            // intoSimilar(pos[0]);

            // 1st search on silimar
            move(pos[0], pos[3]);
            search(pos[3], pos[4], conf.url[0].url);

            // select target web site
            move(pos[4], pos[7]);
            robot.mouseClick("left");
            yield util.sleep(10000);

            // scroll down up
            scroll(pos[7]);

            resolve();
        });
    });
}



// run action
const run = function () {

    co(function* () {
        yield init();
        // yield showSimilar();
        // yield robotSimilar();
    });
}

run();