'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const _ = require('underscore');

const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const selenium = new(require('../modules/selenium.js'));
const cheerio = new(require('../modules/cheerio-httpcli.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'post_target_day', alias: 't', type: String }, // instagram post day (ex. 2018-05-01)
    { name: 'update_target_day', alias: 'T', type: String }, // update day (ex.2018-05-19)
    { name: 'category', alias: 'c', type: String },
    { name: 'hash', alias: 'h', type: Boolean }, // scrape tag_hash as web page
]);

let driver = null;
let driver2 = null;
let today_haifun = moment().add(0, 'days').format('YYYY-MM-DD');
let yesterday_haifun = moment().add(-1, 'days').format('YYYY-MM-DD');
let update_target_day = cli['update_target_day'] ? moment(cli['update_target_day']).format('YYYY-MM-DD') : today_haifun;
let post_target_day = cli['post_target_day'] ? moment(cli['post_target_day']).format('YYYY-MM-DD') : yesterday_haifun;
let category = cli['category'] ? cli['category'] : 'test';


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(conf.filePath_db);
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();
            resolve();
        });
    });
}

// get explore count
function getExploreCount(page_id) {
    return new Promise((resolve, reject) => {
        co(function* () {

            page_id = util.encodeUrl(page_id);
            let url = `https://www.instagram.com/explore/tags/${page_id}/`;
            let $ = yield cheerio.get(url);
            if ($) {
                // bodyから、page_info手前の数値を参照
                let body = $('body').html();
                let date1 = body.match(/(...............)page_info/i);
                if (date1) {
                    let date2 = date1[0].replace(/[page_infocount, ":\{]/g, '');
                    resolve(date2);
                }
            } else {
                resolve(0);
            }
        });
    });
}

// scrape posts for each page
const scrapePostsEach = function (page_id) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get link url lists
            let $ = yield selenium.parseBody(driver);
            let urls = _.map($('._mck9w'), function (e, i) {
                return $(e).find('a').attr('href');
            });

            // 重複したURLをスクレイピングを取得し、ｕｒｌｓから除外する
            let up_date = moment().format('YYYY-MM-DD');
            let url_str = urls.join('","');
            let sql2 = `SELECT url FROM scraping_instagram_targets_result a where a.url in ("${url_str}")`;
            let targets2 = yield db.do(`all`, sql2, {});
            let duplicates = _.map(targets2, function (e, i) { // get duplicates url
                return e.url;
            });
            urls = _.difference(urls, duplicates); // remove duplicates

            // open each url and set scraped data
            for (let j = 0; j < urls.length; j++) {

                let url = urls[j];
                driver2.get(`https://www.instagram.com${url}/`);

                // get post data
                let $ = yield selenium.parseBody(driver2);

                // post_date
                let post_date = $('._p29ma').attr('datetime').substr(0, 10);

                // tags
                let opts = [];
                $('._ezgzd').each(function (i, e) {

                    // scrape only thrower
                    let tags = [];
                    let tag_page_id = $(e).find('._2g7d5').text();
                    let tag_a = $(e).find('a').text();

                    // a name text
                    let a_txt = _.map($(e).find('a'), function (e, i) {
                        return $(e).text();
                    });

                    // set data when page_id is equal
                    if (page_id === a_txt[0]) {
                        for (let k = 0; k < a_txt.length; k++) {

                            // page_id TEXT, tag_none TEXT, tag_hash TEXT, tag_at TEXT, post_date TEXT, url TEXT, up_date TEXT
                            let opt = [];
                            switch (a_txt[k].substr(0, 1)) {
                                case '#': // hash
                                    opt = [page_id, '', a_txt[k], '', post_date, url, up_date];
                                    break;
                                case '@': // at mark
                                    opt = [page_id, '', '', a_txt[k], post_date, url, up_date];
                                    break;
                                default: // none
                                    opt = [page_id, a_txt[k], '', '', post_date, url, up_date];
                                    break;
                            }

                            // set option
                            opts.push(opt);
                        }
                    }
                });

                // insert data
                for (let k = 0; k < opts.length; k++) {
                    let sql = `INSERT OR REPLACE INTO scraping_instagram_targets_result(page_id, tag_none, tag_hash, tag_at, post_date, url, up_date) VALUES (?, ?, ?, ?, ?, ?, ?);`;
                    yield db.do(`run`, sql, opts[k]);
                }

                yield util.sleep(1000);
            }

            // return isEachPage
            let isEachPage = urls.length > 0 ? true : false;
            resolve(isEachPage);
        });
    });
}

// scrape posts
const scrapePosts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // init selenium
            driver = yield selenium.init(cli['browser']);
            driver2 = yield selenium.init(cli['browser']);

            // get targets page_id
            let sql = `SELECT page_id FROM scraping_instagram_targets a where a.cat = '${category}' and a.del = 0`;
            let targets = yield db.do(`all`, sql, {});

            // each target pages
            for (let i = 0; i < targets.length; i++) {

                let cnt = 0;
                let page_id = util.encodeUrl(targets[i].page_id);
                driver.get(`https://www.instagram.com/${page_id}/`);

                // data check after scrolling (10 times)
                while (cnt < 10) {

                    // scrape targets each
                    if (yield scrapePostsEach(page_id)) {
                        cnt = 0;
                    } else {
                        cnt++;
                    }

                    // scroll down
                    yield selenium.scroll(driver, 'footer');
                    yield util.sleep(300);
                    console.log(`check cnt : ${page_id} = ${cnt}/10`);
                }
            }

            // finish selenium, close browser
            driver.quit();
            driver2.quit();
            resolve();
        });
    });
}

// scrape web page
const scrapePage = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // init selenium
            driver = yield selenium.init(cli['browser']);
            driver2 = yield selenium.init(cli['browser']);
            yield cheerio.init(10000);

            // get targets tags from 2 ... post_target_day or update_target_day.
            // default post_target_day is yesterday, update_target_day is today (ex. today = 2018-05-02, yesterday = 2018-05-01)
            let sql = `SELECT tag_hash, tag_at FROM scraping_instagram_targets_result a where (a.post_date > '${post_target_day}' or a.up_date = '${update_target_day}') and tag_none = ''`;
            let targets = yield db.do(`all`, sql, {});

            // get tag list
            let [tag_at_list, tag_hash_list] = _.reduce(targets, function ([s1, s2], e, i) {
                // set exist tag_hash or tag_at where not stocked by s1 or s2
                if (e.tag_at && !_.contains(s1, e.tag_at)) s1.push(e.tag_at.substr(1));
                if (e.tag_hash && !_.contains(s2, e.tag_hash)) s2.push(e.tag_hash.substr(1));
                return [s1, s2];
            }, new Array([], []));

            // [hash option] if you want to scrape tag_hash as instgram page, use command option -h
            if (cli['hash']) {
                tag_at_list = _.union(tag_at_list, tag_hash_list);
            }

            // each target pages
            for (let i = 0; i < tag_at_list.length; i++) {

                let page_id = util.encodeUrl(tag_at_list[i]);
                driver.get(`https://www.instagram.com/${page_id}/`);
                console.log(i, page_id);

                // get post data
                let $ = yield selenium.parseBody(driver);

                // get each num
                let [post_num, followed_num, following_num] = _.reduce($('._h9luf').find('li'), function (s, e, i) {
                    let num1 = util.replace($(e).find('._fd86t').text(), { num: true });
                    let num2 = util.replace($(e).find('._fd86t').attr('title'), { num: true });
                    s.push(num2 ? num2 : num1);
                    return s;
                }, []);

                // explore_num
                let explore_num = yield getExploreCount(page_id);

                // auth batch
                let authentication = $('.coreSpriteVerifiedBadge') && $('.coreSpriteVerifiedBadge').length > 0 ? 1 : 0;

                // web_site
                let web_site = '';
                let web_site_pre = $('._tb97a') && $('._tb97a').find('a') ? $('._tb97a').find('a').attr('href') : '';
                // only for links to other page
                // true > https://l.instagram.com/?u=https, false > /explore/tags/proudshibas/
                if (web_site_pre && web_site_pre.substr(0, 1) !== '/') {
                    driver2.get(web_site_pre);
                    web_site = yield selenium.getCurrentUrl(driver2);
                }

                // update
                let up_date = today_haifun;

                // tags
                let opt = [page_id, post_num, followed_num, following_num, explore_num, authentication, web_site, up_date];

                // set data
                if (post_num) {
                    let sql = `INSERT OR REPLACE INTO scraping_instagram_targets_page(page_id, post_num, followed_num, following_num, explore_num, authentication, web_site, up_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;
                    yield db.do(`run`, sql, opt);
                }
            }

            driver.quit();
            driver2.quit();
            resolve();
        });
    });
}

// drop table
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql1 = `DROP TABLE IF EXISTS scraping_instagram_targets;`;
            yield db.do(`run`, sql1, {});

            let sql2 = `DROP TABLE IF EXISTS scraping_instagram_targets_result;`;
            yield db.do(`run`, sql2, {});

            let sql3 = `DROP TABLE IF EXISTS scraping_instagram_targets_page;`;
            yield db.do(`run`, sql3, {});

            resolve();
        });
    });
}

// set list for target instagram web page
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table
            const sqlA = `CREATE TABLE IF NOT EXISTS scraping_instagram_targets(num INTEGER primary key, page_id TEXT, cat TEXT, del INTEGER);`;
            const sqlB = `CREATE TABLE IF NOT EXISTS scraping_instagram_targets_result(num INTEGER primary key, page_id TEXT, tag_none TEXT, tag_hash TEXT, tag_at TEXT, post_date TEXT, url TEXT, up_date TEXT);`;
            const sqlC = `CREATE TABLE IF NOT EXISTS scraping_instagram_targets_page(page_id TEXT primary key, post_num INTEGER, followed_num INTEGER, following_num INTEGER, explore_num INTEGER, authentication INTEGER, web_site TEXT, up_date TEXT);`;
            yield db.do(`run`, sqlA, {});
            yield db.do(`run`, sqlB, {});
            yield db.do(`run`, sqlC, {});

            // set targets
            const sql1 = `INSERT OR REPLACE INTO scraping_instagram_targets(page_id, cat, del) VALUES ("mensfashionpost", "sample", 0);`;
            const sql2 = `INSERT OR REPLACE INTO scraping_instagram_targets(page_id, cat, del) VALUES ("halno", "test", 1);`;
            const sql3 = `INSERT OR REPLACE INTO scraping_instagram_targets(page_id, cat, del) VALUES ("shige.shibainu", "test", 0);`;
            const sql4 = `INSERT OR REPLACE INTO scraping_instagram_targets(page_id, cat, del) VALUES ("iamyoshee", "test", 0);`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.do(`run`, sql3, {});
            yield db.do(`run`, sql4, {});

            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            // node src/scraping/instagram.js -r 3 -B chrome
            case 3: // reset list for target instagram web page
                yield init();
                yield drop();
                yield setList();
                yield end();
                break;

                // node src/scraping/instagram.js -r 2 -c test -B chromium -t 2018-04-01 -T 2018-05-20 -h
                // -B chrome, -B phantom, -B firefox
                // -t post_target_day, -T update_target_day, -h update at_mark and hash as page_id
            case 2: // set list for target instagram web page
                yield init();
                yield scrapePage();
                yield end();
                break;

                // node src/scraping/instagram.js -r 1 -B chrome
                // -B chrome, -B phantom, -B firefox
            case 1: // get post from instagram targets
            default:
                yield init();
                yield scrapePosts();
                yield end();
                break;
        }
    });
}

run();