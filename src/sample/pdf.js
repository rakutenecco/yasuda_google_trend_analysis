'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const PDFDocument = require('pdfkit');

const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const pdf = new(require('../modules/pdf.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            pdf.init(conf.dirPath_pdf);
            resolve();
        });
    });
}

// make PDF
const makePDF = function (filepath) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let context = yield pdf.getPdfKitContext();
            context.fontSize(22);
            context.text('Hi, 日本語 / English is O.K.', 50, 100);
            context.image(`${conf.dirPath_img_sample}/sample01.jpg`, 50, 130, { fit: [200, 200], 'page-break-inside': 'avoid' })
            yield pdf.setPdfKitContext(context, filepath);
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // make pdf
            default:
                yield init();
                let filepath = `${conf.dirPath_pdf}/sample.pdf`;
                yield makePDF(filepath);
        }
    });
}

run();