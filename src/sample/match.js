'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const match = new(require('../modules/match.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


let sample1 = `
<head>
<title>正規表現：正しいＵＲＬかどうか調べる - phpspot</title>
<meta http-equiv="Content-Type" content="text/html; charset=Shift-JIS">
<meta name="KEYWORDS" content="フリー,PHP,Editor,ＰＨＰ,エディタ,Ｕｎｉｃｏｄｅ,UTF,入門">
<meta name="description" content="Well organized and easy to understand Web building tutorials with lots of examples.">
<link rel="stylesheet" href="/php/def.css?200904">
</head>
<body><p>
URLを解析して、特定パターンに合致する文字列を取り出したかったのです。
http://hoge.com/hoge/fuga/goods/
ここから、スラッシュで囲まれた最初のブロックの文字列…
今回の場合は hoge だけ取り出したい。
最初はこう書いてましたが。
参考はhttp://d.hatena.ne.jp/cloned/20061110#cです。
</p></body>`;



// do sample
const doSample = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let res = '';
            res = yield match.getUrl(sample1);
            console.log('match.getUrl', res);

            res = yield match.getMetaKeyword(sample1);
            console.log('match.getMetaKeyword', res);

            res = yield match.getMetaDescription(sample1);
            console.log('match.getMetaDescription', res);

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // do sample
            default:
                yield doSample();
        }
    });
}

run();