'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
require('chromedriver');
const fs = require('fs');
const webdriver = require('selenium-webdriver');

const conf = new(require('../conf.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

let driver = null;
let chromeCapabilities = null;

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set chrome option
            chromeCapabilities = webdriver.Capabilities.chrome();
            chromeCapabilities.set('chromeOptions', {
                'args': [
                    'disable-infobars',
                    '--proxy-server=socks5://127.0.0.1:9050',
                ]
            });

            resolve();
        });
    });
}

// do sample
const doSample = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            console.log('This is a test for chrome-driver via socks5 proxy to tor');

            driver = new webdriver.Builder()
                .withCapabilities(chromeCapabilities)
                .forBrowser('chrome')
                .build();

            driver.get('https://browserleaks.com/ip');
            yield util.sleep(10000);
            driver.quit();

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // doSample
            default:
                yield init();
                yield doSample();
        }
    });
}

run();