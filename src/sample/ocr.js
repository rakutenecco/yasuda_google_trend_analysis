'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const db = new(require('../modules/db.js'));
const ocr = new(require('../modules/ocr.js'));
const ocrGoogle = new(require('../modules/ocr_google.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            ocr.init();
            yield ocrGoogle.init();
            resolve();
        });
    });
}

// end
const end = function (isTesseract) {

    if (isTesseract) {
        process.exit(0);
    }
}

// download and ocr
const doDownload = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
/*
            const url_img = 'http://www.cabanedezucca-watch.com/wp/wp-content/uploads/2017/09/buttersable_lineup.jpg';
            const dirpath_out = './img/ocr';
            const filepath_img = yield ocr.downloadImage(url_img, dirpath_out);
*/
            let filepath_img = './img/ocr/buttersable_lineup.jpg';
            const lang = 'jpn';
            let res = yield ocr.getText(filepath_img, lang);
            console.log(res);
            resolve();
        });
    });
}

// test1
const doTest1 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const filepath_img = './img/sample/newspaper01.jpg';
            const lang = 'jpn';
            let res = yield ocr.getText(filepath_img, lang);
            console.log(res);
            resolve();
        });
    });
}

// test2
const doTest2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const filepath_img = './img/sample/newspaper02.jpg';
            const lang = 'eng';
            let res = yield ocr.getText(filepath_img, lang);
            console.log(res);
            resolve();
        });
    });
}

// test3
const doTest3 = function (filepath) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // english
            const filepath_img2 = './img/sample/newspaper02.jpg';
            const res2 = yield ocrGoogle.getText(filepath_img2);
            console.log(res2.text);
            console.log(res2.all);

            // japanese
            const filepath_img1 = './img/sample/newspaper01.jpg';
            const res1 = yield ocrGoogle.getText(filepath_img1);
            console.log(res1.text);
            console.log(res1.all);

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            case 4: // download and ocr
                yield init();
                yield doDownload();
                end();
                break;

            case 3: // google cloud api
                yield init();
                yield doTest3();
                end();
                break;

            case 2: // tesseract
                yield init();
                yield doTest2();
                end(true);
                break;

            case 1: // tesseract
            default:
                yield init();
                yield doTest1();
                end(true);
        }
    });
}

run();