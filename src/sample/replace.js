'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// doSample
const doSample1 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let str = '   先頭がスペース、全角スペース（　）半角スペース（ ）タブ（	\t）、改行↓\nスペース（ ）、最後がスペース   ';
            console.log('sample string ===', str);
            console.log('replace >', util.replace(str));
            console.log('----------');

            str = ' 999 010,230.234cm';
            console.log('sample string ===', str);
            console.log('replace {num: true} >', util.replace(str, { num: true }));
            console.log('replace {num: true, match:\'cm\'} >', util.replace(str, { num: true, match: 'cm' }));
            console.log('replace {num: true, match:\'cm\', toNum: true} >', util.replace(str, { num: true, match: 'cm', toNum: true }));
            console.log('replace {num: true, match:\'cm\', removeMatch: true} >', util.replace(str, { num: true, match: 'cm', removeMatch: true }));
            console.log('----------');

            str = '50%OFF!!\n  13,105円（税込み）';
            console.log('sample string ===', str);
            console.log('replace >', util.replace(str));
            console.log('replace {price: true} >', util.replace(str, { price: true }));
            console.log('replace {price: true, match:\'円\'} >', util.replace(str, { price: true, match: '円' }));
            console.log('replace {price: true, match:\'円\', toNum: true} >', util.replace(str, { price: true, match: '円', toNum: true }));
            console.log('replace {price: true, match:\'円\', removeMatch: true} >', util.replace(str, { price: true, match: '円', removeMatch: true }));
            console.log('replace {price: true, match:\'円\', removeComma: true} >', util.replace(str, { price: true, match: '円', removeComma: true }));
            console.log('----------');

            str = ' 13,105円（税込み）';
            console.log('sample string ===', str);
            console.log('replace {price: true} >', util.replace(str, { price: true }));
            console.log('----------');

            str = ' 999 $12.345,05';
            console.log('sample string ===', str);
            console.log('replace {price: true} >', util.replace(str, { price: true }));
            console.log('replace {price: true, match:\'$\'} >', util.replace(str, { price: true, match: '$' }));
            console.log('replace {price: true, match:\'$\', toNum: true} >', util.replace(str, { price: true, match: '$', toNum: true }));
            console.log('replace {price: true, match:\'$\', removeMatch: true} >', util.replace(str, { price: true, match: '$', removeMatch: true }));
            console.log('----------');

            str = '型番はW-96H-1AJFです!';
            console.log('sample string ===', str);
            console.log('replace {price: true} >', util.replace(str, { mpn: true }));
            console.log('----------');

            str = ' seikoの型番はW-96H-1AJFです!';
            console.log('sample string ===', str);
            console.log('replace {price: true} >', util.replace(str, { mpn: true }));
            console.log('----------');

            resolve();
        });
    });
}

// doSample
const doSample2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let str = 'テストﾃｽﾄ testTESTtＥｓＴ Ťëšŧ ２０１８/10/01';
            console.log('sample string ===', str);
            console.log('replaceToLower >', util.replaceToLower(str));
            console.log('replaceToUpper >', util.replaceToUpper(str));
            console.log('replaceFullToHalf >', util.replaceFullToHalf(str));
            console.log('replaceHalfToFull >', util.replaceHalfToFull(str));
            console.log('replaceKanaFullToHalf >', util.replaceKanaFullToHalf(str));
            console.log('replaceKanaHalfToFull >', util.replaceKanaHalfToFull(str));
            console.log('replaceSpecialCharactersToAlphabet >', util.replaceSpecialCharactersToAlphabet(str));
            console.log('----------');

            str = '⑨ appΛle or+ang?e & ba$na\'na';
            console.log('sample string ===', str);
            console.log('replaceSymbol >', util.replaceSymbol(str, { ' ': true, '&': true }));
            console.log('replaceGroupBy >', util.replaceGroupBy(str));
            console.log('----------');

            resolve();
        });
    });
}

// doSample
const doSample3 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let str = '<p class="ref-no"><span style="text-align: center; background-color: #FFDBCD; padding: 3px 5px; color:#FF867E; display:block; margin:5px auto; width:150px;">クリスマス限定 400本</span>NJAM701<br>22,000円＋税<br></p>';
            console.log('sample string ===', str);
            console.log('replaceSpan default      >', util.replaceSpan(str));
            console.log('replaceSpan isText=true  >', util.replaceSpan(str, true));
            console.log('replaceSpan isText=false >', util.replaceSpan(str, false));
            console.log('----------');

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // do sample
            default:
                yield init();
                yield doSample1();
                yield doSample2();
                yield doSample3();
                yield end();
        }
    });
}

run();