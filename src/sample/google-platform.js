'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const googlePlatform = new(require('../modules/google-platform.js'));
const util = new(require('../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // pdf.init(conf.dirPath_pdf);
            resolve();
        });
    });
}

// do ocr sample
const doOcr = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let filename1 = `./img/sample/newspaper02.jpg`;
            let filename2 = `./img/sample/newspaper03.jpg`;
            console.log(`-----------------`);
            console.log(`[text_detection]: simple image text: ${filename1}`);
            console.log(`[text_detection]: simple image text: ${filename2}`);
            console.log(`-----------------`);
            let res = yield googlePlatform.text_detection([filename1, filename2]);
            console.log('text >>> ', res.responses[0].fullTextAnnotation.text);
            console.log('text >>> ', res.responses[1].fullTextAnnotation.text);

            resolve();
        });
    });
}

// do ocr2 sample
const doOcr2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let filename1 = `./img/sample/newspaper04.jpg`;
            console.log(`-----------------`);
            console.log(`[document_text_detection]: web page image text: ${filename1}`);
            console.log(`-----------------`);
            let res = yield googlePlatform.document_text_detection([filename1]);
            // console.log('text >>> ', res.responses[0].fullTextAnnotation.text);

            console.log(res);



            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // 
                yield init();
                yield doOcr2();
                break;
            case 1: // 
            default:
                yield init();
                yield doOcr();
        }
    });
}

run();