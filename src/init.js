'use strict';

// copy myconf
const shell = require('shelljs');
console.log('myconf setting ...');
shell.exec('node src/tools/setMyconf.js');
shell.exec('sleep 4s;');

// modules
const co = require('co');
const commandArgs = require('command-line-args');

const conf = new(require('./conf.js'));
const log = new(require('./modules/log.js'));
const util = new(require('./modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // usual action
            default:

                console.log('dir setting ...');
                util.mkdir(`./${conf.dirPath_in}`);
                util.mkdir(`./${conf.dirPath_in_img}`);
                util.mkdir(`./${conf.dirPath_out}`);
                util.mkdir(`./${conf.dirPath_out_test}`);

                //log
                log.init();

                console.log('database setting ...');
                shell.exec('node src/tools/setDB.js');
                break;
        }
    });
}

run();