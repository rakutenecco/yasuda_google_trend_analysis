'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');
const moment = require('moment');
const fs = require('fs');
const conf = new (require('../../conf.js'));
const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: String },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },
    { name: 'getdate', alias: 'g', type: String },
]);

let runtype;
if(!cli['run'] || cli['run'] == 1){
    //mode = 'new';
    runtype = 1;
} else if (cli['run'] == 2){
    //mode = 'continue';
    runtype = 2;
} else if (cli['run'] == 9){
    //mode = 'output';
    runtype = 9;
}

const maxretry = 10;
const cron = cli['cron'] ? cli['cron'] : 'cron10'; // cron10
let getdate = cli['getdate'] ? cli['getdate'] : moment().day(0).format('YYYYMMDD');

const category = cli['fname'].replace('targets_','').replace('.txt','');// fashion_02_01
let outfile;
// ex) fname:targets_fashion.txt -> FGS239_wear_posts_fashion_20190304.txt
// ex) fname:targets_fashion_02_01.txt -> FGS239_wear_posts_fashion_20190304_02_01.txt
// ex) fname:targets_new_20190301.txt -> FGS239_wear_posts_new_20190301_20190304.txt
let tfnm = category.split(/_/g);
if(tfnm.length == 1) {
    outfile = `${conf.dirPath_share_vb}/FGS239_wear_posts_${tfnm[0]}_${getdate}.txt`;

} else if ( tfnm.length == 3 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS239_wear_posts_${tfnm[0]}_${getdate}_${tfnm[1]}_${tfnm[2]}.txt`;

} else if ( tfnm.length == 2 && tfnm[0] == 'new' && isFinite(tfnm[1]) ) {
    outfile = `${conf.dirPath_share_vb}/FGS239_wear_posts_${tfnm[0]}_${tfnm[1]}_${getdate}.txt`;

} else {
    runtype = '';
}

console.log('/// FGS239 wear shell.js start ///');
console.log(`  runtype: ${runtype}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  getdate: ${getdate}`);
console.log(`  outfile: ${outfile}`);


// scrape wear
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let lflg = true; // loop continue?
            let lcnt = 0;
            do{
            // mode and loop
                if (fs.existsSync(outfile)) {
                    console.log(` outfile exists: ${outfile}`);
                    if ( tfnm.length == 3 ) {
                        shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js -f ${outfile}`);
                    }
                    lflg = false;

                }else{
                    lcnt++;
                    if( lcnt > maxretry ){                        
                        lflg = false;

                    }else{
                        if( lcnt > 1 ) runtype = 2;
                        console.log(` FGS239-wear-posts/shell.js scraper.js CALL lcnt: ${lcnt} runtype: ${runtype} getdate: ${getdate} ${cron} ${cli['fname']}`);
                        shell.exec(`node src/rakuten/FGS239-wear-posts/scraper.js -r ${runtype} -n ${cron} -f ${cli['fname']} -g ${getdate}` );
                    }
                }

            }while(lflg)

            resolve();

        }).catch((e) => {
            console.log(e);
            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');

        switch (runtype) {

            case 1://'new':
            case 2://'continue':
                yield scrape_main();
                break;
            case 9://'output':
                shell.exec(`node src/rakuten/FGS239-wear-posts/scraper.js -r ${runtype} -n ${cron} -f ${cli['fname']} -g ${getdate}` );
                break;

            default:
                break;
        }

        console.log(`/// FGS239-wear-posts shell.js ${cron} end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();