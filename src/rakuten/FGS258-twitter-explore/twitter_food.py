import ast
import time
import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup as bs4
from dateutil import relativedelta
from retrying import retry, RetryError

reg_date = str(datetime.date.today())
up_date = str(datetime.date.today() + relativedelta.relativedelta(days=1))
wordlist = ['sweets','スイーツ','グルメ','ケーキ','アイス','お節','おせち料理','おせち','バレンタイン','お鍋','鍋','鍋パ','チョコレート','パフェ','料理','ランチ','ディナー','lunch','chocolate','cake','healthyfood','breakfast']

# 本日日付設定
today = datetime.date.today()

# ファイル名設定
yyyymmdd = str(today).replace('-','')

# jsonファイル読み込み
with open(r'/var/app/total/myconf.json') as f:
    conf = f.read()

myconf_json = ast.literal_eval(conf)

# ファイル作成のディレクトリパス
path = "/var/app/total/total-data/FGS258-twitter-explore/{0}-{1}/".format(yyyymmdd, myconf_json['pc_id'])
file_name = path + "/fgs258-twitter-food.txt"

# ファイル書き込み開始
with open(file_name , 'w') as f:
    f.write('row_id' + "\t" + 'page_id' + "\t" + 'tweet_id' + "\t" + 'keyword' + "\t" + 'tag1' + "\t" + 'reg_date' + "\t" + 'up_date' + "\n")
    f.write('row_id' + "\t" + 'page_id' + "\t" + 'tweet_id' + "\t" + 'keyword' + "\t" + 'tag1' + "\t" + 'reg_date' + "\t" + 'up_date' + "\n")
    f.write('VARCHAR(100)' + "\t" + 'VARCHAR(50)' + "\t" + 'VARCHAR(20)' + "\t" + 'VARCHAR(500)' + "\t" + 'VARCHAR(500)' + "\t" + 'VARCHAR(20)' + "\t" + 'VARCHAR(20)' + "\n")

#optionsインスタンスを生成
options = Options()

#オプションを設定(ブラウザを立ち上げない)
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-gpu')

# スクレイピングしたデータをparseする
result = []
def parse(page):
    lst = []
    soup = bs4(page, 'html.parser')
    data = soup.find_all("div")[1].find_all("a")
    for x in data:
        if x.text.startswith('#'):
            print(x.text)
            lst.append(x.text)
    return lst

# スクレイピング処理
@retry(stop_max_attempt_number=1000, wait_fixed=1000)
def scrape(word, reg_date, up_date):
    driver = webdriver.Chrome('/var/app/total/chromedriver', options=options)
    driver.get('https://twitter.com/search?q=%23{0}%20since%3A{1}_00:00:00_JST%20until%3A{2}_00:00:00_JST'.format(word, reg_date, up_date))
    cnt = 0
    while True:
        cnt = cnt + 1000
        time.sleep(3)
        driver.execute_script("window.scrollTo(0, {0});".format(cnt))
        lst = parse(driver.page_source)
        if cnt > 100000:
            break
    driver.close()
            
    WORD = list(set(lst))
    return WORD

# ファイル書き込み
for x in wordlist:
    try:
        WORDLST = scrape(x, reg_date, up_date)
        print(WORDLST)
    except:
        WORDLST = scrape(x, reg_date, up_date)
        print(WORDLST)
    for y in range(len(WORDLST)):
        result.append('null' + "\t" + x + "\t" + 'null' + "\t" +  WORDLST[y].replace("#", "") + "\t" + WORDLST[y] + "\t" + reg_date + "\t" + up_date + "\n")
    with open(file_name , 'a') as f:
        for i in range(len(result)):
            f.write(result[i])
        result = []
