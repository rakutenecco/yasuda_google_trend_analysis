'use strict';

const co = require('co');
const shell = require('shelljs');
const commandArgs = require('command-line-args');
const moment = require('moment');

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'addDay', alias: 'a', type: Number },
]);

const addDay = cli['addDay'] || 0;
const yesterday = moment().add(addDay - 1,'days').format('YYYYMMDD');
const yesterday_haifun = moment().add(addDay - 1,'days').format('YYYY-MM-DD');


// run action
const run = function () {

    co(function* () {

        // 
        switch (cli['run']) {

            case 90: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs242'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;



            case 13: // life cron5
                // shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron5`);
                for (let i = 0; i < 2; i++) {
                    shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron5`);
                }
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron5`);
                shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);
                break;

            case 12: // fashion cron4
                // shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron4`);
                for (let i = 0; i < 2; i++) {
                    shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron4`);
                }
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron4`);
                shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);
                break;

            case 11: // food cron3
                // shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron3`);
                for (let i = 0; i < 2; i++) {
                    shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron3`);
                }
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron3`);
                shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);
                break;



            case 3: // life cron5
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron5`);
                for (let i = 0; i < 2; i++) {
                    // shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron5`);
                    shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron5 -t ${yesterday}`);
                }
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron5 -t ${yesterday}`);
                // shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);
                break;

            case 2: // fashion cron4
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron4`);
                for (let i = 0; i < 2; i++) {
                    // shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron4`);
                    shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron4 -t ${yesterday}`);
                }
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron4 -t ${yesterday}`);
                // shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);
                break;

            case 1: // food cron3
            default:
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 99 -C cron3`);
                for (let i = 0; i < 2; i++) {
                    // shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron3`);
                    shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 2 -C cron3 -t ${yesterday}`);
                }
                shell.exec(`node src/rakuten/FGS258-twitter-explore/scraper.js -r 90 -C cron3 -t ${yesterday}`);
                // shell.exec(`node src/rakuten/FGS258-twitter-explore/shell.js -r 90`);
                break;
        }
    });
}

run();