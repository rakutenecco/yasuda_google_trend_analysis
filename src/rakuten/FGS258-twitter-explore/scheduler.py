import datetime
import time
import schedule
import os

def job1():
    os.system('/home/ts_ryohei_a_yasuda/.pyenv/shims/python3 /var/app/total/src/rakuten/FGS258-twitter-explore/twitter_life.py')

def job2():
    os.system('/home/ts_ryohei_a_yasuda/.pyenv/shims/python3 /var/app/total/src/rakuten/FGS258-twitter-explore/twitter_fashion.py')

def job3():
    os.system('/home/ts_ryohei_a_yasuda/.pyenv/shims/python3 /var/app/total/src/rakuten/FGS258-twitter-explore/twitter_food.py')

schedule.every().day.at("09:05").do(job1)
schedule.every().day.at("09:05").do(job2)
schedule.every().day.at("09:05").do(job3)
while True:
    schedule.run_pending()
    time.sleep(1)
