'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'runtype', alias: 'r', type: Number },   // 0:all 1:scrape 2:output
    { name: 'cron', alias: 'n', type: String },      // cron2 (weekly)
    { name: 'startdate', alias: 's', type: String }, // yyyymmdd
]);

let runtype = cli['runtype'] || 0;
const cron = cli['cron'] || 'cron2';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const today = moment().format('YYYYMMDD');
const startdate = cli['startdate'] || moment().add(-1, 'd').format('YYYYMMDD');
const startdate_hiphen = startdate.substr(0, 4) + '-' + startdate.substr(4, 2) + '-' + startdate.substr(6, 2);
const start_site = 'https://www.gqjapan.jp/fashion/runway';
//https://www.gqjapan.jp/fashion/runway?page=2
let arrUrl = [];

// init action
const init_table = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // FGS203_url
            // sqlstr = `DROP TABLE IF EXISTS FGS203_url;`;
            // yield db.do(`run`, sqlstr, {});

            sqlstr = 'CREATE TABLE IF NOT EXISTS FGS203_url ' +
            '(c_num TEXT, c_getdate TEXT, c_URL TEXT, c_title TEXT, c_publish TEXT, primary key(c_getdate,c_num));';
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // FGS203_article
            // sqlstr = `DROP TABLE IF EXISTS FGS203_article;`;
            // yield db.do(`run`, sqlstr, {});

            sqlstr = 'CREATE TABLE IF NOT EXISTS FGS203_article ' +
                '(c_num TEXT, c_getdate TEXT, c_art_publish TEXT, c_category TEXT, c_brand TEXT, c_art_title TEXT, c_article TEXT, primary key(c_getdate,c_num));';
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // old and today delete
            const deldate = moment(startdate_hiphen).add(-30, 'd').format('YYYYMMDD');
            //console.log(` FGS203_url deldate: ${deldate}`);
            sqlstr = `DELETE FROM FGS203_url WHERE c_publish < '${deldate}' and c_getdate = '${today}';`;
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            //console.log(` FGS203_article deldate: ${deldate}`);
            sqlstr = `DELETE FROM FGS203_article WHERE c_art_publish < '${deldate}' and c_getdate = '${today}';`;
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});


            resolve();
        });
    });
}


// scrape url data
const scrape_url = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            //const stopday = moment().subtract(2,'days').format('YYYYMMDD');
            let up_date = today;
            let p = 1;  // page
            let n = 0;  // today article counter
            let site;

            while (up_date >= startdate) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + '?page=' + p;
                }
                // console.log(' p = ' + p);
                let $ = yield cheerio.get(site);
                if ($){// && $('body').html().match(/js-load-more-container/i)) {
                let x = $('.TeaserLayoutBase-j4d1l0-8');
                    for (let i = 0; i < 7; i++) {
                        n = n + 1;
                        let e = x[i];
                        let url = 'https://www.gqjapan.jp' + $(e).find('a').attr('href');
                        //console.log('url = ' + url);

                        let title = $(e).find('H3').text().replace(/[\n\r\t\(\) ,]/g, '');
                        //console.log('title = ' + title);

                        let publish = $(e).find('time').text().replace(/[\n\r\t\s\(\) ,]/g, '');
                        //console.log('publish = [' + publish + ']');

                        let arr_date = publish.split(/\D/);
                        let new_up_date = arr_date[0] + ('0' + arr_date[1]).slice(-2) + ('0' + arr_date[2]).slice(-2);
                        if (up_date >= new_up_date) {
                            up_date = new_up_date;
                        }
                        //console.log('up_date = ' + up_date);
                        //console.log(startdate)

                        //if( up_date <= startdate )  break;

                        const sql = `INSERT OR REPLACE INTO FGS203_url ` +
                            `(c_num,c_getdate,c_URL,c_title,c_publish) VALUES (?, ?, ?, ?, ?);`;
                        let num = ('000' + n).slice(-4)
                        //console.log('num = ' + num);
                        let opt = [num, today, url, title, new_up_date];
                        //console.log(num, today, url, title, new_up_date)
                        yield db.do(`run`, sql, opt);
                        //console.log('ok');

                        let str = { c_num: num, c_URL: url }
                        arrUrl.push(str);

                    };
                }
                p = p + 1;

            }

            resolve();
        });
    });
}


// scrape article data
const scrape_article = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < arrUrl.length; i++) {

                let num = arrUrl[i].c_num;
                let getdate = today;
                let URL = arrUrl[i].c_URL;
                //console.log('num : ' + num + '  URL : ' + URL );

                let $ = yield cheerio.get(URL);
                if ($){

                    let e = $('.NewsletterSliceStickyContainer-p18ox4-2.fnbkZM');
                    if(e.length == 0){
                        e = $('.Wrapper-bizjVT');
                    }
                    let tmpctgry = $(e).find('.Wrapper-sc-1o1q527-0.yighD.sc-1lew042-0.kRmImZ').find('p').text();
                    //console.log(tmpctgry);

                    let arr_category = tmpctgry.split(": ");
                    let category = arr_category[0];
                    let brand = arr_category[1];

                    let art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) ,]/g, '');
                    //console.log(art_title);

                    let arr_pub = $(e).find('time').text().split(/\D/);
                    let art_publish = arr_pub[0] + ('0' + arr_pub[1]).slice(-2) + ('0' + arr_pub[2]).slice(-2);
                    //console.log(art_publish);

                    let article = $(e).find('.Wrapper-abvmkk-0.DqBMn').find('p').text().replace(/[\n\r\t\(\) ,]/g, '');
                    //console.log(article);

                    let tmpart = $('.MainContentWrapper-s89gjf-14.bHhLUT').text().replace(/[\n\r\t\(\) ,]/g, '');
                    if(tmpart.length == 0){
                        tmpart = $('.DekText-igdbVK').text();
                    }
                    let insta = $('.instagram-media').find('p').text();
                    //console.log(article);
                    //console.log(insta);
                    tmpart = tmpart.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');
                    article = article + tmpart;
                    //console.log(article);

                    //console.log(`  num: ${num}  publish: ${art_publish}  title: ${art_title}`);
                    const sql = `INSERT OR REPLACE INTO FGS203_article ` +
                        `(c_num, c_getdate, c_art_publish, c_category, c_brand, c_art_title, c_article) VALUES (?, ?, ?, ?, ?, ?, ?);`;
                    let opt = [num, getdate, art_publish, category, brand, art_title, article];
                    yield db.do(`run`, sql, opt);

                }
            }

            resolve();
        });
    });
}

// scrape main
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            yield init_table();
            yield scrape_url();
            if(arrUrl.length > 0) yield scrape_article();

            resolve();
        });
    });
}


// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            let sql = `SELECT a.c_getdate, a.min_num, a.c_URL, a.c_title, b.c_art_publish, b.c_category, b.c_brand, b.c_art_title, b.c_article ` +
                `FROM ( SELECT MAX(c_getdate) c_getdate, MIN(c_num) min_num, c_publish, c_URL, MAX(c_title) c_title ` + 
                `  FROM FGS203_url WHERE c_getdate = '${today}' GROUP BY c_publish, c_URL ) a ,FGS203_article b ` +
                `WHERE a.c_getdate = b.c_getdate AND a.min_num = b.c_num ` +
                `AND b.c_art_publish >= '${startdate}' AND b.c_art_publish < '${today}' ` +
                `ORDER BY a.min_num;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS203-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', 'URL', 'タイトル', '記事内更新日', 'カテゴリー', 'ブランド', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_URL', 'c_title', 'c_art_publish', 'c_category', 'c_brand', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(200)', 'VARCHAR(1000)', 'VARCHAR(8)', 'VARCHAR(200)', 'VARCHAR(50)', 'VARCHAR(1000)', 'VARCHAR(29000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].min_num, res[i].c_URL, res[i].c_title, res[i].c_art_publish, res[i].c_category, res[i].c_brand, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            resolve();
        });
    });
}



// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        yield db.connect(filePath_db);

        if(runtype == 0 || runtype == 1) {            
            yield scrape_main();
        }

        if(runtype == 0 || runtype == 2) {
            yield setOutFile();
        }

        yield db.close();
        console.log(`/// FGS203-gqjapan scraper.js end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    });
}

run();