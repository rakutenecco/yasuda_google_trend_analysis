'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

// --------------------------------------
// monthly
// --------------------------------------

// google trend monthly watch 1
new cronJob(`00 00 16 1 * *`, function () {
        shell.exec('npm run gtm');
    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend monthly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

// google trend monthly skirt 2
new cronJob(`00 00 16 2 * *`, function () {
        shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 40');
    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop google-trend monthly cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

// --------------------------------------
// weekly
// --------------------------------------

// casio Tuesday
new cronJob(`00 5 9 * * 2`, function () {
    shell.exec('pkill chromium');
    shell.exec('sleep 90');
    shell.exec('npm run casio');
    shell.exec('pkill chromium');
    shell.exec('sleep 90');
    shell.exec('npm run seiko');
}, function () {
    // error
    now = moment().format('YYYYMMDD HH:mm:ss');
    fs.appendFileSync(path_log, `Error ${now} : stop casio cron \n`, 'utf8');
},
true, 'Asia/Tokyo');

// google trend weekly watch Wed
new cronJob(`00 5 9 * * 3`, function () {
    now = moment().format('YYYYMMDD HH:mm:ss');
    console.log('watch week cron start ',now)
    shell.exec('pkill chromium');
    shell.exec('sleep 90');
    shell.exec('npm run gtw-d');
    shell.exec('npm run gtw');
    now = moment().format('YYYYMMDD HH:mm:ss');
    console.log('watch week cron end ',now)
}, function () {
    // error
    now = moment().format('YYYYMMDD HH:mm:ss');
    fs.appendFileSync(path_log, `Error ${now} : stop google-trend weekly cron \n`, 'utf8');
},
true, 'Asia/Tokyo');

// google trend weekly skirt Thu
new cronJob(`00 5 9 * * 4`, function () {
    now = moment().format('YYYYMMDD HH:mm:ss');
    console.log('skirt week cron start ',now)
    shell.exec('pkill chromium');
    shell.exec('sleep 90');
    shell.exec('node src/rakuten/FGS41-google-trends/shell.js -r 30');
    now = moment().format('YYYYMMDD HH:mm:ss');
    console.log('skirt week cron end ',now)
}, function () {
    // error
    now = moment().format('YYYYMMDD HH:mm:ss');
    fs.appendFileSync(path_log, `Error ${now} : stop google-trend weekly cron \n`, 'utf8');
},
true, 'Asia/Tokyo');

console.log('clone setting');
