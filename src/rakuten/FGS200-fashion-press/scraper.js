'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'startday', alias: 's', type: String },
]);

const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron0.db`;
const startday = cli['startday'];

// drop table for concern tables
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql = `DROP TABLE IF EXISTS FGS200_url;`;
            yield db.do(`run`, sql, {});

            sql = `DROP TABLE IF EXISTS FGS200_article;`;
            yield db.do(`run`, sql, {});
            yield db.close();

            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS FGS200_url ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_URL TEXT, c_title TEXT, c_publish TEXT, c_category TEXT);`;
            //console.log('sql = ' + sql);
            yield db.do(`run`, sql, {});

            let up_date = today;
            let p = 1;
            let n = 0;
            let start_site = 'https://www.fashion-press.net/news/search/collection';
            let site;

            //while (p <= 6) {
            while (up_date >= startday) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + '?page=' + p;
                }
                //console.log(site);
                //console.log(p);

                let $ = yield cheerio.get(site);
                //console.log($('body').html());
                if ($ && $('body').html().match(/fp_media_tile news_media col_3/i)) {
                    $('.fp_media_tile.news_media.col_3').each(function (i, e) {
                        co(function* () {

                            n = n + 1;
                            //console.log(n);

                            let url = 'https://www.fashion-press.net' + $(e).find('a').attr('href');
                            //console.log(url);

                            let title = $(e).find('a div').text();
                            //console.log(title);

                            let category = $(e).find('div').eq(1).find('a').eq(0).text().replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
                            //console.log(category);

                            let publish = $(e).find('div span').eq(0).text().replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, '');
                            //console.log(publish);

                            let arr_pub = publish.split('.');
                            //console.log(arr_pub[0] + ',' + arr_pub[1] + ',' + arr_pub[2]);
                            if (arr_pub[1] != undefined) {
                                let m0 = ('00' + arr_pub[1]).slice(-2);
                                let d0 = ('00' + arr_pub[2]).slice(-2);
                                up_date = arr_pub[0] + m0 + d0;
                            }
                            //console.log(up_date);

                            const sql = `INSERT OR REPLACE INTO FGS200_url ` +
                                `(c_num, c_getdate,c_URL,c_title,c_publish,c_category) VALUES (?, ?, ?, ?, ?, ?);`;
                            //console.log('sql = ' + sql);
                            let num = ('000' + n).slice(-4)
                            //console.log('num = ' + num);
                            let opt = [num, today, url, title, publish, category];
                            yield db.do(`run`, sql, opt);
                        });
                    });
                }
                p = p + 1;

            }

            yield db.close();

            resolve();
        });
    });
}

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS FGS200_article ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_art_publish TEXT, c_hashtag TEXT, c_art_title TEXT, c_article TEXT);`;
            //console.log('sql = ' + sql);
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}

// get url list
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            let sql = `SELECT * FROM FGS200_url`;
            let res = yield db.do(`all`, sql, {});

            resolve(res);
        });
    });
}

// scrape proxy data, and set db as proxy_list
const scrape = function (urls) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            for (let i = 0; i < urls.length; i++) {

                let number = urls[i].c_num;
                //console.log(number);
                let getdate = urls[i].c_getdate;
                let URL = urls[i].c_URL;
                let art_title = '';
                let art_publish = '';
                let hashtag = '';
                let article = '';
                let alist = '';

                let $ = yield cheerio.get(urls[i].c_URL);
                //if ($ && $('body').html().match(/fp_content/i)) {
                if ($ && $('body').html().match(/fp_content_left/i)) {

                    let e = $('.fp_content_left');
                    art_title = $(e).find('div').eq(0).find('h1').text();
                    //console.log(art_title);

                    let tmp_pub = $(e).find('.footer_caption').find('span').eq(0).text();
                    //console.log(tmp_pub);
                    let tmp_pub2 = tmp_pub.match(/20(\d{2})\D(0[1-9]|1[0-2])\D(0[1-9]|[12][0-9]|3[01])/g);
                    art_publish = tmp_pub2[0].split(/\D/).join('');
                    //console.log(art_publish);

                    let white_tag = $(e).find('.white_tag');
                    for (let j = 0; j < white_tag.length; j++) {
                        let wtag = white_tag.eq(j).text()

                        if (wtag.substr(0, 1) == '#') {
                            if (hashtag.length > 0) {
                                hashtag = hashtag + ',#' + wtag.slice(2);
                            } else {
                                hashtag = '#' + wtag.slice(2);
                            }
                        }
                    }
                    //console.log(hashtag);

                    article = $(e).find('div').eq(3).text().replace(/[\n\r\t ,]/g, '');
                    //console.log(article);

                    if ($(e).html().match(/news_pager_top/i)) {
                        let m = 0;
                        $(e).find('.news_pager_top').find('li').each(function (i, p) {
                            co(function* () {

                                if (m > 0) {
                                    //console.log($(p).find('a').attr('href'));
                                    let url2 = 'https://www.fashion-press.net' + $(p).find('a').attr('href');
                                    if (alist.length > 0) {
                                        alist = alist + ',' + url2;
                                    } else {
                                        alist = url2;
                                    }
                                    //console.log(alist);                                    
                                }
                                m++;
                            });
                        });
                    }
                }

                if (alist.length > 0) {
                    let url2 = alist.split(',');
                    for (let p = 0; p < url2.length; p++) {
                        let $2 = yield cheerio.get(url2[p]);
                        //console.log($2('.fp_content_left').find('div').eq(3).text().replace(/[\n\r\t ,]/g, ''));
                        let q = $2('.fp_content_left').find('div').eq(3).text().replace(/[\n\r\t ,]/g, '');

                        article = article + q;
                        //console.log(article);
                    }
                }

                const sql = `INSERT OR REPLACE INTO FGS200_article ` +
                    `(c_num,c_getdate,c_art_publish,c_hashtag,c_art_title,c_article) ` +
                    `VALUES (?, ?, ?, ?, ?, ?);`;
                //console.log('sql = ' + sql);
                let opt = [number, getdate, art_publish, hashtag, art_title, article];
                yield db.do(`run`, sql, opt);

            }

            resolve();
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            yield db.connect(filePath_db);
            let sql = `SELECT a.c_getdate, a.min_num, a.c_URL, a.c_title, b.c_art_publish, b.c_hashtag, a.c_category, b.c_art_title, b.c_article ` +
                `FROM ( SELECT MAX(c_getdate) c_getdate, MIN(c_num) min_num, c_publish, c_URL, MAX(c_category) c_category, MAX(c_title) c_title FROM FGS200_url GROUP BY c_publish, c_URL ) a ,FGS200_article b ` +
                `WHERE a.c_getdate = b.c_getdate AND a.min_num = b.c_num ` +
                `AND b.c_art_publish >= '${startday}' AND b.c_art_publish < '${today}' ` +
                `ORDER BY a.min_num;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS200-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', 'URL', 'タイトル', '記事内更新日', 'ハッシュタグ', 'カテゴリー', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_URL', 'c_title', 'c_art_publish', 'c_hashtag', 'c_category', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(200)', 'VARCHAR(1000)', 'VARCHAR(8)', 'VARCHAR(1000)', 'VARCHAR(50)', 'VARCHAR(1000)', 'VARCHAR(15000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].min_num, res[i].c_URL, res[i].c_title, res[i].c_art_publish, res[i].c_hashtag, res[i].c_category, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            yield db.close();
            resolve(res);
        });
    });
}



// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 1: // drop table
                yield drop();
                break;
            case 2: // set url list
                yield setList();
                break;
            case 3: // scrape data
                yield init();
                const list = yield getList();
                yield scrape(list);
                yield end();
                break;
            case 4: // out data to share folder
                yield setOutFile();
                break;
            default:
                break;
        }
    });
}

run();