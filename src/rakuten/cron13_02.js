'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS286 yahoo all weekly
new cronJob(`00 30 00 * * 0`, function () {
//new cronJob(`00 16 17 * * 3`, function () {
        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        console.log(` yahoo cron13_02 start ${st}`);

        let startdate = moment().day(0).add(-7, "days").format('YYYYMMDD');    
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_01 -f targets_all_06_04.txt -s ${startdate}`);
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_02 -f targets_all_06_05.txt -s ${startdate}`);
        shell.exec(`node src/rakuten/FGS286-yahoo/shell.js -r 1 -n cron13_03 -f targets_all_06_06.txt -s ${startdate}`);
        
        console.log(` yahoo cron13_02 end   ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    
}, function () {
    // error
    console.log(`Error ${today} : FGS283 recipe FGS286 yahoo scrape cron13_02`);
},
true, 'Asia/Tokyo');

console.log('cron13_02 setting');
