require('shelljs/global');

const co = require('co');
const shell = require('shelljs');
const commandArgs = require('command-line-args');
const moment = require('moment');


// parse args
const cli = commandArgs([
    //    { name: 'brand_id', alias: 'b', type: Number },
    //    { name: 'nums', alias: 'n', type: Number, multiple: true },
    //    { name: 'critical', alias: 'c', type: Boolean },
    //    { name: 'skip', alias: 's', type: Boolean },
    { name: 'task', alias: 't', type: String },
    { name: 'run', alias: 'r', type: Number }
]);



let opt_t = cli['task'] ? cli['task'] : "";


let ymd = 'a' + moment().format('YYYYMMDD') + '.csv';



// mount share folder
// shell.exec(`echo 'bebe1973?1' | sudo -S mount -t vboxsf share /mnt/share/`);


// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {

            case 2: // usual action
                // run daily action
                shell.exec(`cd ../amazon/ ; python3 amazon2.py ${opt_t}`);

                // tsv > txt on share folder
                shell.exec(`cp ../amazon/output2/${ymd} /mnt/share/amazon_ranking_merge_2.csv`);
                shell.exec(`cp ../amazon/output/store.csv /mnt/share/store.csv`);
                shell.exec(`cp ../amazon/output/store_mst.csv /mnt/share/store_mst.csv`);
                break;

            case 1: // usual action
                // run daily action
                shell.exec(`cd ../amazon/ ; python3 amazon1.py ${opt_t}`);

                // tsv > txt on share folder
                shell.exec(`cp ../amazon/output1/${ymd} /mnt/share/amazon_ranking_merge.csv`);
                shell.exec(`cp ../amazon/output/store.csv /mnt/share/store.csv`);
                shell.exec(`cp ../amazon/output/store_mst.csv /mnt/share/store_mst.csv`);
                break;

            default:
                // run daily action
                shell.exec(`cd ../amazon/ ; python3 amazon.py ${opt_t}`);

                // tsv > txt on share folder
                shell.exec(`cp ../amazon/output/${ymd} /mnt/share/amazon_ranking_merge.csv`);
                shell.exec(`cp ../amazon/output/store.csv /mnt/share/store.csv`);
                shell.exec(`cp ../amazon/output/store_mst.csv /mnt/share/store_mst.csv`);
                break;
        }
    });
}

run();