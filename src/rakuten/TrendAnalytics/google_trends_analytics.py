import math 
import pandas as pd
import numpy as np
from datetime import datetime
import statsmodels.api as sm
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from matplotlib.backends.backend_pdf import PdfPages
import pymannkendall as mk
%matplotlib inline

data = pd.read_csv(r'C:\Users\ts-ryohei.a.yasuda\Desktop\folder\fgs41-google-trends.txt',sep='\t',encoding ='utf-8')

dt = data.groupby('name').groups

LST = list(dt.keys())

print(LST)

plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
plt.gca().get_yaxis().get_major_formatter().set_useOffset(False)


def plot(path,index,volume,y2,y3,y4,name,yoy):
    ax = plt.subplot()
    ax.get_xaxis().get_major_formatter().set_scientific(False)
    ax.get_yaxis().get_major_formatter().set_scientific(False)
    plt.setp(ax.get_xticklabels(), rotation=80)
    plt.title('{0}      {1}'.format(name,yoy), fontname="MS Gothic")
    ticks = 5
    plt.xticks(range(0, len(index), ticks), index[::ticks])
    plt.plot(index, volume, color='gray', label='original')
    plt.plot(index,y2, color='turquoise', label='moving average month')
    plt.plot(index,y3, color='tomato', label='moving average three month')
    plt.plot(index,y4, color='green', label='moving average half-year')
    plt.legend()
    plt.ylabel('Trend')
    plt.xlabel('year')
    plt.tight_layout()
    plt.savefig(path)
    plt.show()

    
for i in range(len(LST)):
    
    separated_data = data.groupby('name').get_group(LST[i])
    vol = pd.Series(separated_data["volume"], dtype='float')
    vol.index = pd.Series(separated_data['year_month'], dtype='str')


    y2 = vol.rolling(window=4).mean()
    y3 = vol.rolling(window=12).mean()
    y4 = vol.rolling(window=24).mean()
    
    
    try:
        decomp = sm.tsa.seasonal_decompose(vol,freq = 23)
        tdata = decomp.trend
        
        yoy = vol["20200524"] / vol["20190526"] * 100
        print(yoy)
        print(vol)
    
        result = mk.original_test(tdata)
        print(result)
        inc_dec_flg = result[0]
        print("増減")
        print(inc_dec_flg)
        p_value = result[2]
        print("P値")
        print(float(p_value))
        
        #トレンドが上昇傾向にあり、p値が0.05以下であり、昨年対比が100%以上、かつゴールデンクロスが直近に見られるものをグラフに出力する。
        if inc_dec_flg == "increasing" and p_value < 0.05 and yoy > 100 and golden[len(golden) -1]:
            plot(r'C:\Users\ts-ryohei.a.yasuda\Desktop\folder\beauty\google1\{0}.png'.format(LST[i]), vol.index, vol,y2,y3,y4,LST[i],yoy)

    except Exception as e:
        pass