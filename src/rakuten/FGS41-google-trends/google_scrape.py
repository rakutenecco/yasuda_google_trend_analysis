#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import ast
import time
import datetime
import pandas as pd
from time import sleep
from dateutil import relativedelta
from pytrends.request import TrendReq
from retrying import retry, RetryError

from stem import Signal
from stem.control import Controller


# 本日日付設定
today = datetime.date.today()

# 3年前の日付
#three_years_ago = today - relativedelta.relativedelta(years=3)
three_years_ago = today - relativedelta.relativedelta(years=5)


# ファイル名設定
yyyymmdd = str(today).replace('-','')

# jsonファイル読み込み
with open(r'/var/app/total/myconf.json') as f:
    conf = f.read()

myconf_json = ast.literal_eval(conf)

# ファイル作成のディレクトリパス
path = "/var/app/total/total-data/FGS41-google-trends/{0}-{1}/".format(yyyymmdd, myconf_json['pc_id'])
file_name = path + "/fgs41-google-trends_food_analytics.txt"

# 出力ディレクトリ作成
os.system("mkdir -p {0}".format(path))

# 抽出キーワード読み込み
csv_input = pd.read_csv(r"/var/app/total/src/rakuten/FGS41-google-trends/wordlist.csv",encoding="utf-8")

# プロキシ設定
proxies = {
        'http':'socks5://127.0.0.1:9050',
        'https':'socks5://127.0.0.1:9050'
        }

# ファイル書き込み開始
with open(file_name , 'w') as f:
    f.write('name' + "\t" + 'year_month' + "\t" + 'volume' + "\t" + 'times' + "\t" + 'yyyymmdd' + "\n")
    f.write('name' + "\t" + 'year_month' + "\t" + 'volume' + "\t" + 'times' + "\t" + 'yyyymmdd' + "\n")
    f.write('VARCHAR(1000)' + "\t" + 'VARCHAR(8)' + "\t" + 'VARCHAR(5)' + "\t" + 'VARCHAR(10)' + "\t" + 'VARCHAR(8)' + "\n")

# torの起動
os.system('killall tor')
os.system('sudo service tor restart')

def reset_tor():
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)

# 配列初期化
result = []


# スクレイピング関数
def scrape(x):
    #pytrends = TrendReq(hl='ja-JP')
    pytrends = TrendReq(hl='ja-JP', tz=360, timeout=(10,25), proxies=proxies, retries=2, backoff_factor=0.1)
    pytrends.build_payload(kw_list=[csv_input.header[x]], cat=0, timeframe='{0} {1}'.format(three_years_ago, today), geo='JP', gprop='')

    interest_over_time_df = pytrends.interest_over_time()
    print(interest_over_time_df)

    for y in range(len(interest_over_time_df.values)):
        result.append(csv_input.header[x] + "\t" + str(interest_over_time_df.index[y]).replace('-','').replace(' 00:00:00', '') + "\t" + str(interest_over_time_df.values[y][0]) + "\t" + 'week' + "\t" + str(today).replace('-','') + "\n")

# 再実行用スクレイピング関数
@retry(stop_max_attempt_number=100, wait_fixed=1000)
def retry_scrape(x):
    #os.system('killall tor')
    #os.system('sudo service tor restart')
    reset_tor()
    #os.system('sudo service tor restart')
    scrape(x)

# スクレイピング実行
for x in range(len(csv_input.values)):
    try:
        scrape(x)

    except Exception as e:
        print(e)
        retry_scrape(x)

# 書き込み
with open(file_name , 'a') as f:
    for y in range(len(result)):
        f.write(result[y])
