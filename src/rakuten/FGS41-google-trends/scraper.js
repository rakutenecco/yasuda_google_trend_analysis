'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const shell = require('shelljs');
const url = require('url');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const targets = require('./targets.js').targets;
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
// const match = new(require('../../modules/match.js'));
// const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));
const phantom = new(require('../../modules/phantom.js'));

let now = '';

now = moment().format('YYYYMMDD HH:mm:ss');
console.log('google trend scraper start ',now)

// parse args
const cli = commandArgs([
    { name: 'category', alias: 'c', type: String },
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'time', alias: 't', type: String },
    { name: 'ip', alias: 'I', type: String },
    { name: 'port', alias: 'P', type: Number },
    { name: 'options', alias: 'o', multiple: true, type: String },
]);

// variable
const time = cli['time'] || 'week';
const category = cli['category'] || 'watch';
const today = moment().format('YYYYMMDD');
const ip = cli['ip'] || 'localhost';
const port = cli['port'] || 8091;
let opt = {};
opt.tor = cli['options'] && cli['options'][0] === 'tor' ? true : false;

let filePath_html = `./${conf.dirPath_local}/FGS41/watch_{0}.html`;
let html_tmp = `
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>HTMLの書き方</title>
</head>
<body>
    <h1>HTMLの書き方</h1>
    <table>
        <tr id="end1"><td>{100}</td><td>{0}</td></tr>
        <tr id="end2"><td>{101}</td><td>{1}</td></tr>
        <tr id="end3"><td>{102}</td><td>{2}</td></tr>
        <tr id="end4"><td>{103}</td><td>{3}</td></tr>
        <tr id="end5"><td>{104}</td><td>{4}</td></tr>
        <tr id="end6"><td>{105}</td><td>{5}</td></tr>
        <tr id="end7"><td>{106}</td><td>{6}</td></tr>
        <tr id="end8"><td>{107}</td><td>{7}</td></tr>
        <tr id="end9"><td>{108}</td><td>{8}</td></tr>
        <tr id="end10"><td>{109}</td><td>{9}</td></tr>
    </table>
</body>
</html>`;
let html_tmp2 = `
<span id="cat_{4}" class="names">{0}</span><span class="nameGTs">{5}</span>
<script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/1040_RC04/embed_loader.js"></script> <script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"{0}","geo":"","time":"{2} {3}"}],"category":0,"property":""}, {"exploreQuery":"date={2}%20{3}&q={1}","guestPath":"https://trends.google.co.jp:443/trends/embed/"}); </script>`;
let html_tmp3 = `
<span id="cat_{4}" class="names">{0}</span><span class="nameGTs">{5}</span>
<script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/1087_RC02/embed_loader.js"></script> <script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"{0}","geo":"","time":"today 5-y"}],"category":0,"property":""}, {"exploreQuery":"date=today 5-y&q={1}","guestPath":"https://trends.google.co.jp:443/trends/embed/"}); </script> `;

let driver = null;
let dirPath_txt = `./src/rakuten/FGS41-google-trends/result-${today}`;
let idMax = 0;

// filepath cron
let filePath_db = `${conf.dirPath_db}/cron6.db`;
switch (category) {
    case 'skirt': // fashion cron6
        filePath_db = `${conf.dirPath_db}/cron6.db`;
        break;
    case 'watch': // life cron6
    default:
        filePath_db = `${conf.dirPath_db}/cron6.db`;
        break;
}

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set selenium with browser
            yield util.mkdir(`./${conf.dirPath_local}`);
            yield util.mkdir(`./${conf.dirPath_local}/FGS41`);
            yield util.mkdir(dirPath_txt);
            yield util.mkdir(`${conf.dirPath_data}\/FGS41-google-trends`);
            yield util.mkdir(`${conf.dirPath_data}\/FGS41-google-trends\/${today}-${conf.pc_id}`);

            driver = yield selenium.init(cli['browser'], opt);

            // create table "proxy_list"
            yield db.connect(filePath_db);

            resolve();
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.quit();
            yield db.close();

            resolve();
        });
    });
}

// drop table
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql1 = `DROP TABLE IF EXISTS fashion_fgs41_google_trends_result;`;
            yield db.do(`run`, sql1, {});

            let sql2 = `DROP TABLE IF EXISTS fashion_fgs41_google_trends_list;`;
            yield db.do(`run`, sql2, {});

            yield db.close();
            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sqlA = `CREATE TABLE IF NOT EXISTS fashion_fgs41_google_trends_result(id INTEGER primary key, brand_id INTEGER, name TEXT, name2 TEXT, year_month TEXT, volume INTEGER, category TEXT, times TEXT, yyyymmdd TEXT, repeat INTEGER);`;
            const sqlB = `CREATE TABLE IF NOT EXISTS fashion_fgs41_google_trends_list(id TEXT primary key, name TEXT, nameGT TEXT, category TEXT, delflag INTEGER, isdata INTEGER);`;
            yield db.do(`run`, sqlA, {});
            yield db.do(`run`, sqlB, {});

            for (let i = 0; i < targets.length; i++) {
                let sql1 = `INSERT OR REPLACE INTO fashion_fgs41_google_trends_list(id, name, nameGT, category, delflag, isdata) VALUES ("${targets[i].id}", "${targets[i].name}", "${targets[i].nameGT}", "${targets[i].category}", "${targets[i].delflag}", "${targets[i].isdata}");`;
                yield db.do(`run`, sql1, {});
            }
            yield db.close();

            resolve();
        });
    });
}

// scrape google trends
const scrape = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let $1 = null;
            let $2 = null;

            let sql = `SELECT id, name, nameGT, category, delflag, isdata, id as brand_id, -1 as repeat FROM fashion_fgs41_google_trends_list where id not in (
                select brand_id from fashion_fgs41_google_trends_result where yyyymmdd = '${today}' and times = '${time}' and category = '${category}' group by brand_id
              ) and delflag = 0  and category = '${category}'
              
              union all
              
              SELECT * FROM fashion_fgs41_google_trends_list a
              inner join
              (
              select brand_id, repeat from (select brand_id, sum(volume) as volume, repeat from fashion_fgs41_google_trends_result where yyyymmdd = '${today}' and times = '${time}' and category = '${category}' and repeat < 5 group by brand_id) where volume = 0
              ) b
              on 
              a.id = b.brand_id
              order by id`; // LIMIT 30

            let categories = yield db.do(`all`, sql, {});

            let urls = yield setHtml(categories, time);

            // 各ブランドを実行する
            for (let i = 0; i < urls.length; i++) {

                let name = urls[i];
                let id_cat = [];
                let names = [];
                let nameGTs = [];

                // open url
                driver.get(urls[i]);
                yield util.sleep(5000);
                yield selenium.reload(driver);
                yield util.sleep(40000);
                selenium.scroll(driver, '#end2');
                yield util.sleep(800);
                selenium.scroll(driver, '#end4');
                yield util.sleep(800);
                selenium.scroll(driver, '#end6');
                yield util.sleep(800);
                selenium.scroll(driver, '#end8');
                yield util.sleep(800);
                selenium.scroll(driver, '#end10');
                yield util.sleep(8000);

                // get from table
                $1 = yield selenium.parseBody(driver);
                $1('tr').each(function (j, e) {
                    id_cat.push($1(e).find('td').eq(0).text());
                    names.push($1(e).find('.names').eq(0).text());
                    nameGTs.push($1(e).find('.nameGTs').eq(0).text());
                });

                // get from iframe
                let iframes = yield selenium.getElements(driver, 'iframe');
                for (let j = 0; j < iframes.length; j++) {

                    driver.switchTo().frame(iframes[j]);
                    $2 = yield selenium.parseBody(driver);

                    // file name >> / is _
                    let cat = util.replace($2('.fe-line-chart-legend-text').text().replace(/\//g, '_'));
                    let fileName = `multiTimeline_${cat}_${(new Date()).getTime()}.csv`;

                    // set header
                    let vals = []; // [ [ '‪2013', '03', '01‬' ], '64' ]   >   [yyyy, mm, dd], value

                    // scrape each value
                    $2('.line-chart table tr').each(function (k, e) {
                        if (k === 0) return;
                        let data = [];
                        let ymd = $2(e).find('td').eq(0).text();
                        data.push(ymd.replace(/\//g, '-'));
                        data.push($2(e).find('td').eq(1).text());
                        vals.push(data);
                    });

                    let id_cat_split = id_cat[j].split('-');

                    let repeat = id_cat_split[2] != 99 ? parseInt(id_cat_split[2], 10) + 1 : 0;
                    if (id_cat_split[3] == 0) {
                        repeat = 5; // isdata == 0 >> never repeat
                    }

                    // if data
                    if (vals.length > 0) {

                        // reset data
                        let sql1 = `delete from fashion_fgs41_google_trends_result where name = '${cat}' and yyyymmdd = '${today}' and repeat < ${repeat}`;
                        yield db.do(`run`, sql1, {});

                        // append file
                        for (let k = 0; k < vals.length; k++) {
                            // dirPath_txtへ出力する
                            let data = (vals[k][0] ? vals[k][0] : '') + ',' + (vals[k][1] ? vals[k][1] : '');
                            fs.appendFileSync(dirPath_txt + '/' + fileName, data + '\n', 'utf8');

                            // insert data
                            let sql2 = `INSERT OR REPLACE INTO fashion_fgs41_google_trends_result(brand_id, name, name2, year_month, volume, category, times, yyyymmdd, repeat) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                            let opt2 = [id_cat_split[0], cat, nameGTs[j], vals[k][0], vals[k][1], id_cat_split[1], time, today, repeat];
                            yield db.do(`run`, sql2, opt2);
                        }

                    } else {

                        // get yyyymm
                        let sql = `select * from fashion_fgs41_google_trends_result where category = '${category}' and times = '${time}' and yyyymmdd = '${today}' and brand_id = ${id_cat_split[0]}`;
                        let record = yield db.do(`all`, sql, {});

                        if (record.length > 0) {
                            // update record
                            for (let l = 0; l < record.length; l++) {
                                sql = `update fashion_fgs41_google_trends_result set repeat = ${repeat} where brand_id = ${id_cat_split[0]}`;
                                yield db.do(`run`, sql, {});
                            }

                        } else {
                            // insert new record
                            // get yyyymm
                            let sql = `select * from fashion_fgs41_google_trends_result where category = '${category}' and times = '${time}' and yyyymmdd = '${today}' and brand_id in (select brand_id from fashion_fgs41_google_trends_result where category = '${category}' and times = '${time}' and yyyymmdd = '${today}' order by brand_id LIMIT 1) order by id`; // LIMIT 30
                            let base = yield db.do(`all`, sql, {});

                            for (let k = 0; k < base.length; k++) {
                                let sql = `INSERT OR REPLACE INTO fashion_fgs41_google_trends_result(brand_id, name, name2, year_month, volume, category, times, yyyymmdd, repeat) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                                let opt = [id_cat_split[0], names[j], nameGTs[j], base[k]['year_month'], 0, id_cat_split[1], time, today, repeat];
                                yield db.do(`run`, sql, opt);
                            }
                        }
                        // get dd

                    }

                    // driver swith to parent
                    driver.switchTo().defaultContent();
                }
                yield util.sleep(4000);
            }

            resolve();
        });
    });
}

// call api
// this api is no more available
const callAPI = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let name = 'seiko';
            const MAX_MONTH_COUNT = 149;
            const termToMax = moment().subtract(1, 'month').format('YYYYMM');
            const termTo = cli['term-to'] && (cli['term-to'] < termToMax) ? cli['term-to'] : termToMax;
            const termFromMin = moment().subtract(MAX_MONTH_COUNT, 'month').format('YYYYMM');
            const termFrom = cli['term-from'] && (cli['term-from'] > termFromMin) ? cli['term-from'] : termFromMin;
            const termGap = moment(termTo, 'YYYYMM').diff(moment(termFrom, 'YYYYMM'), 'months') + 1;
            const urlFormat = {
                protocol: 'http',
                host: 'www.google.com',
                pathname: 'trends/fetchComponent',
                query: {
                    q: name,
                    cid: 'TIMESERIES_GRAPH_0',
                    'export': 3,
                    date: `${moment(termFrom, 'YYYYMM').format('M/YYYY')} ${termGap}m`,
                    cmpt: 'q',
                    tz: 'Etc/GMT-9'
                }
            };

            let url_api = url.format(urlFormat);

            // this api is no more available
            console.log(url_api);

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            // let sql = `select id, brand_id, name, year_month, name2, volume, category, times, yyyymmdd from fashion_fgs41_google_trends_result where category = '${category}' order by  brand_id, year_month;`;            
            let sql = `select a.id, a.brand_id, b.name, a.year_month, b.nameGT as name2, a.volume, a.category, a.times, a.yyyymmdd from fashion_fgs41_google_trends_result a inner join fashion_fgs41_google_trends_list b on a.brand_id = b.id and a.category = '${category}' and a.yyyymmdd = '${today}' order by a.brand_id;`;
            let res = yield db.do(`all`, sql, {});
            res = res || [];

            // write header
            yield util.mkdir(`${conf.dirPath_data}\/FGS41-google-trends`);
            yield util.mkdir(`${conf.dirPath_data}\/FGS41-google-trends\/${today}-${conf.pc_id}`);
            let filepath = `${conf.dirPath_share_vb}/FGS41-google-trends-${today}-${time}.txt`;
            let filepath2 = `${conf.dirPath_data}\/FGS41-google-trends\/${today}-${conf.pc_id}\/fgs41-google-trends_${category}-${time}.txt`;

            // append
            fs.writeFileSync(filepath, '', 'utf-8');
            fs.writeFileSync(filepath2, '', 'utf-8');
            fs.appendFileSync(filepath, (['id', 'brand_id', 'name', 'year_month', 'name2', 'volume', 'category', 'times', 'yyyymmdd'].join('\t') + '\n'), 'utf8');
            fs.appendFileSync(filepath, (['id', 'brand_id', 'name', 'year_month', 'name2', 'volume', 'category', 'times', 'yyyymmdd'].join('\t') + '\n'), 'utf8');
            fs.appendFileSync(filepath, (['INTEGER', 'INTEGER', 'VARCHAR(1000)', 'CHAR(8)', 'VARCHAR(1000)', 'INTEGER', 'VARCHAR(20)', 'VARCHAR(20)', 'VARCHAR(8)'].join('\t') + '\n'), 'utf8');

            for (let i = 0; i < res.length; i++) {
                let name2 = res[i].name2 ? res[i].name2 : util.replaceGroupBy(res[i].name);
                fs.appendFileSync(filepath, ([res[i].id, res[i].brand_id, res[i].name, res[i].year_month.replace(/-/g, '').substr(1, 8), name2, res[i].volume, res[i].category, res[i].times, res[i].yyyymmdd].join('\t') + '\n'), 'utf8');
            }

            // copy data
            let data = fs.readFileSync(filepath, 'utf-8');
            fs.writeFileSync(filepath2, data, 'utf-8');

            // remove files
            for (let i = 1; i <= 21; i++) {
                for (var key in conf.target_pc_id) {
                    if (conf.target_pc_id.hasOwnProperty(key)) {
                        let pc_id = conf.target_pc_id[key];
                        let past = moment().add(-1 * i - 3, 'days').format('YYYYMMDD');
                        let filepath_past = `${conf.dirPath_data}\/FGS41-google-trends\/${past}-${pc_id}\/`;
                        yield util.remove(filepath_past, true);
                    }
                }
            }

            resolve();
        });
    });
}

// set html tag
const setHtml = function (newCategories, time) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let cnt = 0;
            let data = html_tmp;

            let dir = `${conf.dirPath_local}/FGS41`;
            let filenames = fs.readdirSync(dir, 'utf-8');
            let filenames2 = '';

            // remove html
            for (let i = 0; i < filenames.length; i++) {
                fs.unlinkSync(dir + '/' + filenames[i], 'utf-8');
            }

            // week or month
            switch (time) {
                case 'week':

                    // make html
                    for (let i = 0; i < newCategories.length; i++) {

                        let _data = html_tmp3.replace(/\{0\}/g, newCategories[i].name).replace('{1}', encodeURIComponent(newCategories[i].name)).replace(/\{4\}/g, i % 10).replace(/\{5\}/g, newCategories[i].nameGT);
                        let repeat = newCategories[i].repeat != -1 ? newCategories[i].repeat : 99;
                        data = data.replace('{' + i % 10 + '}', _data);
                        data = data.replace('{10' + i % 10 + '}', `${newCategories[i].id}-${newCategories[i].category}-${repeat}-${newCategories[i].isdata}`);

                        if (i % 10 === 9 || i === newCategories.length - 1) {
                            fs.writeFileSync(filePath_html.replace('{0}', cnt), data, 'utf-8');
                            cnt++;
                            data = html_tmp;
                        }
                    }

                    break;
                case 'month':

                    // make html
                    const termToMin = moment().subtract(149, 'month').format('YYYY-MM-01');
                    const termToMax = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD');

                    for (let i = 0; i < newCategories.length; i++) {

                        let _data = html_tmp2.replace(/\{0\}/g, newCategories[i].name).replace('{1}', encodeURIComponent(newCategories[i].name)).replace(/\{2\}/g, termToMin).replace(/\{3\}/g, termToMax).replace(/\{4\}/g, i % 10).replace(/\{5\}/g, newCategories[i].nameGT);
                        let repeat = newCategories[i].repeat != -1 ? newCategories[i].repeat : 99;
                        data = data.replace('{' + i % 10 + '}', _data);
                        data = data.replace('{10' + i % 10 + '}', `${newCategories[i].id}-${newCategories[i].category}-${repeat}-${newCategories[i].isdata}`);

                        if (i % 10 === 9 || i === newCategories.length - 1) {
                            fs.writeFileSync(filePath_html.replace('{0}', cnt), data, 'utf-8');
                            cnt++;
                            data = html_tmp;
                        }
                    }

                    break;
            }

            // return localhost urls
            filenames2 = fs.readdirSync(dir, 'utf-8');
            let urls = filenames2.map(function (e, i) {
                return `http://${ip}:${port}/FGS41/` + e;
            });

            resolve(urls);
        });
    });
}

// clean
const clean = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            shell.exec('bleachbit --clean google_chrome.cache');
            shell.exec('bleachbit --clean google_chrome.cookies');
            shell.exec('bleachbit --clean google_chrome.dom');
            shell.exec('bleachbit --clean google_chrome.form_history');
            shell.exec('bleachbit --clean google_chrome.history');
            shell.exec('bleachbit --clean google_chrome.passwords');
            shell.exec('bleachbit --clean google_chrome.search_engines');
            shell.exec('bleachbit --clean google_chrome.session');
            shell.exec('bleachbit --clean google_chrome.vacuum');

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // login
                yield phantom.loginNoneIntra();
                break;

            case 6: // out data to share folder
                yield init();
                yield wait();
                break;
            case 5: // out data to share folder
                yield db.connect(filePath_db);
                yield setOutFile();
                yield db.close();
                break;
            case 4: // reset table
                yield init();
                yield callAPI();
                yield end();
                break;
            case 3: // check each data
                yield showList();
                break;
            case 2: // reset table
                yield drop();
                yield setList();
                break;
            case 1: // scrape data
            default:
                yield clean();
                // scrape data
                for (let i = 0; i < 20; i++) {
                    yield init();
                    yield scrape();
                    yield end();
                    yield clean();
                }
                // yield setOutFile();
                break;
        }

        now = moment().format('YYYYMMDD HH:mm:ss'); 
        console.log('google trend scraper end ',now)

    });
}

run();