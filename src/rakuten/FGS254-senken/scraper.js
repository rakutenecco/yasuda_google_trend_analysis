'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'runtype', alias: 'r', type: Number },   // 0:all 1:scrape 2:output
    { name: 'cron', alias: 'n', type: String },      // cron2 (weekly)
    { name: 'startdate', alias: 's', type: String }, // yyyymmdd
]);

let runtype = cli['runtype'] || 0;
const cron = cli['cron'] || 'cron2';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const today = moment().format('YYYYMMDD');
const startdate = cli['startdate'] || moment().add(-1, 'd').format('YYYYMMDD');
const startdate_hiphen = startdate.substr(0, 4) + '-' + startdate.substr(4, 2) + '-' + startdate.substr(6, 2);
const start_site = ['https://senken.co.jp/categories/trend'];

console.log(`   startdate:${startdate} cron:${cron}`);

let arrUrl = [];
let n = 0;

// init action
const init_table = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // FGS254_url
            // sqlstr = `DROP TABLE IF EXISTS FGS254_url;`;
            // yield db.do(`run`, sqlstr, {});

            sqlstr = 'CREATE TABLE IF NOT EXISTS FGS254_url ' +
            '(c_num TEXT, c_getdate TEXT, c_publish TEXT, c_URL TEXT, c_title TEXT, primary key(c_getdate,c_num));';
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // FGS254_article
            // sqlstr = `DROP TABLE IF EXISTS FGS254_article;`;
            // yield db.do(`run`, sqlstr, {});

            sqlstr = 'CREATE TABLE IF NOT EXISTS FGS254_article ' +
                '(c_num TEXT, c_getdate TEXT, c_URL TEXT, c_art_publish TEXT, c_art_title TEXT, c_article TEXT, primary key(c_getdate,c_num));';
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // old and today delete
            const deldate = moment(startdate_hiphen).add(-30, 'd').format('YYYYMMDD');
            sqlstr = `DELETE FROM FGS254_url WHERE c_publish < '${deldate}' or c_getdate = '${today}';`;
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `DELETE FROM FGS254_article WHERE c_art_publish < '${deldate}' or c_getdate = '${today}';`;
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});


            resolve();
        });
    });
}

// scrape Url data
const scrape_url = function (start_site) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let up_date = today;
            let p = 1;
            let site;

            while (up_date >= startdate) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + '?page=' + p;
                }
                //console.log('p = ' + p + ' ' + up_date);

                let $ = yield cheerio.get(site);
                if ($ && $('body').html().match(/post-large/i)) {
                    $('.post-large').each(function (i, e) {
                        co(function* () {

                            let url = 'https://senken.co.jp' + $(e).find('.m-b-0').find('a').attr('href');
                            let title = $(e).find('.m-b-0').eq(0).text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                            let publish = $(e).find('.m-b-h').text().replace(/[\n\r\t\(\) ,]/g, '').replace(/[^0-9]/g, '');
                            if (up_date >= publish) {
                                up_date = publish;
                            }
                            const sql = `INSERT OR REPLACE INTO FGS254_url ` +
                                `(c_num, c_getdate, c_publish, c_URL, c_title) VALUES (?, ?, ?, ?, ?);`;
                            n = n + 1;
                            let num = ('000' + n).slice(-4)
                            let opt = [num, today, publish, url, title];
                            //console.log(opt);
                            yield db.do(`run`, sql, opt);

                            let str = { c_num: num, c_URL: url }
                            arrUrl.push(str);

                        });
                    });
                }
                if ($ && $('body').html().match(/post-small/i)) {
                    $('.post-small').find('.content').each(function (i, e) {
                        co(function* () {

                            let url = 'https://senken.co.jp' + $(e).find('a').attr('href');
                            let title = $(e).find('a').text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                            let publish = $(e).find('small').text().replace(/[\n\r\t\(\) ,]/g, '').replace(/[^0-9]/g, '');
                            if (up_date >= publish) {
                                up_date = publish;
                            }
                            const sql = `INSERT OR REPLACE INTO FGS254_url ` +
                                `(c_num, c_getdate, c_publish, c_URL, c_title) VALUES (?, ?, ?, ?, ?);`;
                            n = n + 1;
                            let num = ('000' + n).slice(-4)
                            let opt = [num, today, publish, url, title];
                            //console.log(opt);
                            yield db.do(`run`, sql, opt);

                            let str = { c_num: num, c_URL: url }
                            arrUrl.push(str);

                        });
                    });
                }
                p++;

            }
            console.log(`    ${start_site}  ${n} 件`);
            resolve();
        });
    });
}

// scrape article data
const scrape_article = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < arrUrl.length; i++) {

                let num = arrUrl[i].c_num;
                let getdate = today;
                let URL = arrUrl[i].c_URL;
                //console.log('num : ' + num + '  URL : ' + URL );

                let $ = yield cheerio.get(URL);
                if ($ && $('body').html().match(/article/i)) {
                    let e = $('article');

                    let art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                    //console.log(art_title);

                    let apwk = $(e).find('.m-r-h').text().split(' ');
                    let art_publish = apwk[0].replace(/[^0-9]/g, '');
                    //console.log('publish = [' + publish + ']');

                    let article = $(e).find('.article-content.m-y-2').text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                    //console.log(article);

                    const sql = `INSERT OR REPLACE INTO FGS254_article ` +
                        `(c_num, c_getdate, c_URL, c_art_publish, c_art_title, c_article) VALUES (?, ?, ?, ?, ?, ?);`;
                    let opt = [num, getdate, URL, art_publish, art_title, article];
                    //console.log(opt);
                    yield db.do(`run`, sql, opt);

                }
            }

            resolve();
        });
    });
}

// scrape main
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            yield init_table();

            yield cheerio.init(10000);
            for (let i = 0; i < start_site.length; i++) {
                yield scrape_url(start_site[i]);
            }
            if(arrUrl.length > 0) yield scrape_article();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            //yield db.connect(filePath_db);
            let sql = `SELECT u.c_getdate, u.c_num, u.c_publish, u.c_URL, u.c_title, a.c_art_title, a.c_article ` +
                `FROM FGS254_url u ,FGS254_article a ` +
                `WHERE u.c_getdate = '${today}' ` +
                `AND u.c_getdate = a.c_getdate AND u.c_num = a.c_num ` +
                `AND u.c_publish >= '${startdate}' AND u.c_publish < '${today}' ` +
                `ORDER BY u.c_num;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS254-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', '更新日', 'URL', 'タイトル', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_publish', 'c_URL', 'c_title', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(8)', 'VARCHAR(100)', 'VARCHAR(1000)', 'VARCHAR(1000)', 'VARCHAR(20000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].c_num, res[i].c_publish, res[i].c_URL, res[i].c_title, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            //yield db.close();
            resolve(res);
        });
    });
}

// run action
const run = function () {

    co(function* () {
        
        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        console.log(`/// FGS254-senken scraper.js start /// ${st}`);
        yield db.connect(filePath_db);

        if(runtype == 0 || runtype == 1) {            
            yield scrape_main();
        }

        if(runtype == 0 || runtype == 2) {
            yield setOutFile();
        }

        yield db.close();
        console.log(`/// FGS254-senken scraper.js   end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();