'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const shell = require('shelljs');
const _ = require('underscore');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const selenium = new(require('../../modules/selenium.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));
const util = new(require('../../modules/util.js'));
const instagram = new(require('../../modules/instagram.js'));
const phantom = new(require('../../modules/phantom.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'post_target_day', alias: 't', type: String }, // instagram post day (ex. 2018-05-01)
    { name: 'update_target_day', alias: 'T', type: String }, // update day (ex.2018-05-19)
    { name: 'category', alias: 'c', type: String },
    { name: 'cron', alias: 'C', type: String },
    { name: 'hash', alias: 'h', type: Boolean }, // scrape tag_hash as web page
    { name: 'target_page_id', alias: 'i', type: String }, // target_page_id
    { name: 'options', alias: 'o', multiple: true, type: String }, // tor
]);

let driver = null;
let driver2 = null;
let task_name = 'fgs242';
let today = moment().format('YYYYMMDD');
let today_haifun = moment().add(0, 'days').format('YYYY-MM-DD');
let yesterday = moment().add(-1, 'days').format('YYYYMMDD');
let yesterday_haifun = moment().add(-1, 'days').format('YYYY-MM-DD');
let yesterday_slash = moment().add(-1, 'days').format('YYYY/MM/DD');
let update_target_day = cli['update_target_day'] ? moment(cli['update_target_day']).format('YYYY-MM-DD') : today_haifun;
let post_target_day = cli['post_target_day'] ? moment(cli['post_target_day']).format('YYYY-MM-DD') : yesterday_haifun;
let target_page_id = cli['target_page_id'] ? cli['target_page_id'] : 'sweets';
let category = cli['category'] ? cli['category'] : 'food';
let cron = cli['cron'] ? cli['cron'] : 'cron3';
let browser = cli['browser'] || 'chromium';
let checkHash = cli['hash'] || false;
let tor = cli['options'] && cli['options'][0] === 'tor' ? true : false;

// filepath cron
let filePath_db = `${conf.dirPath_db}/cron3.db`;
let dirPath_fastload2_pre = `${conf.dirPath_data}\/FGS242-instagram-explore`;
let dirPath_fastload2 = `${conf.dirPath_data}\/FGS242-instagram-explore\/${today}-${conf.pc_id}`;

// cron
switch (cron) {
    case 'cron3':
        filePath_db = `${conf.dirPath_db}/cron3.db`;
        break;
    case 'cron4':
        filePath_db = `${conf.dirPath_db}/cron4.db`;
        break;
    case 'cron5':
        filePath_db = `${conf.dirPath_db}/cron5.db`;
        break;
}

console.log('post_target_day', post_target_day, ', update_target_day', update_target_day);


// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const inVisible = true;
            yield util.mkdir(dirPath_fastload2_pre);
            yield util.mkdir(dirPath_fastload2);
            yield instagram.init(task_name, update_target_day, post_target_day, category, cron, browser, checkHash, inVisible, filePath_db);
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// set fastload
const setFastload = function (genre, filePath_dbs, filepath_fastload, filepath_fastload2) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set filepath
            fs.writeFileSync(filepath_fastload, '', 'utf-8');
            fs.writeFileSync(filepath_fastload2, '', 'utf-8');
            yield util.sleep(500);

            // header check
            fs.appendFileSync(filepath_fastload, ['row_id', 'page_id', 'url', 'keyword', 'tag1', 'tag2', 'detailurl', 'linkurl', 'explore_id', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['row_id', 'page_id', 'url', 'keyword', 'tag1', 'tag2', 'detailurl', 'linkurl', 'explore_id', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['VARCHAR(20)', 'VARCHAR(20)', 'VARCHAR(2000)', 'VARCHAR(500)', 'VARCHAR(500)', 'VARCHAR(500)', 'VARCHAR(2000)', 'VARCHAR(2000)', 'VARCHAR(500)', 'VARCHAR(20)'].join('\t') + '\n', 'utf-8');
            yield util.sleep(500);

            for (let h = 0; h < filePath_dbs.length; h++) {

                yield db.connect(filePath_dbs[h]);
                console.log(genre, filePath_dbs[h], 200);

                // get page data
                let sql = `SELECT * FROM fashion_${task_name}_library_instagram_targets_result a where cat = '${genre}' and up_date = '${today_haifun}'`;
                let targets = yield db.do(`all`, sql, {});

                // each target pages
                for (let i = 0; i < targets.length; i++) {

                    let id2 = `${today}_${i}`;
                    let id_pre = targets[i].url.split('/');
                    let id = id_pre[2];
                    let url = `https://www.instagram.com${targets[i].url}`;
                    let tag1 = targets[i].tag_hash;
                    let tag2 = targets[i].tag_at;
                    let keyword = targets[i].tag_key;
                    let detailurl = targets[i].tag_at ? `https://www.instagram.com/${keyword}/` : '';
                    let linkurl = targets[i].web_site;
                    let page_id = targets[i].page_id;
                    let reg_date = targets[i].post_date;

                    if (keyword.length > 250) continue;

                    // append data
                    fs.appendFileSync(filepath_fastload, [id2, id, url, keyword, tag1, tag2, detailurl, linkurl, page_id, reg_date].join('\t') + '\n', 'utf-8');
                }

                // copy data
                let data = fs.readFileSync(filepath_fastload, 'utf-8');
                fs.writeFileSync(filepath_fastload2, data, 'utf-8');

                yield db.close();
                yield util.sleep(1000);
            }

            resolve();
        });
    });
}

// set out fastload data
const outFastload = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let retry = 0;
            let filePath_dbs = [`${conf.dirPath_db}/cron3.db`, `${conf.dirPath_db}/cron4.db`, `${conf.dirPath_db}/cron5.db`]
            let categories = ['food', 'fashion', 'life'];

            // set fastload
            for (let g = 0; g < categories.length; g++) {

                let filepath_fastload = `${conf.dirPath_share_vb}\/${task_name}-instagram-${categories[g]}-${today}.txt`;
                let filepath_fastload2 = `${dirPath_fastload2}\/${task_name}-instagram-${categories[g]}.txt`;
                yield setFastload(categories[g], filePath_dbs, filepath_fastload, filepath_fastload2);

                // check filesize
                for (let i = 0; i < 3; i++) {
                    let isInFileSize = util.isInFileSize(filepath_fastload2, 3000);
                    if(!isInFileSize && retry < 3) {
                        retry++;
                        console.log('outFastload retry', retry);
                        yield setFastload(categories[g], filePath_dbs, filepath_fastload, filepath_fastload2);
                    }
                }
            }

            // remove files
            for (let i = 1; i <= 21; i++) {
                for (var key in conf.target_pc_id) {
                    if (conf.target_pc_id.hasOwnProperty(key)) {
                        let pc_id = conf.target_pc_id[key];
                        let past = moment().add(-1 * i - 5,'days').format('YYYYMMDD');
                        let filepath_past = `${conf.dirPath_data}\/FGS242-instagram-explore\/${past}-${pc_id}\/`;
                        yield util.remove(filepath_past, true);
                    }
                }
            }

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        let post_date_min = moment().add(-10, 'days').format('YYYY-MM-DD');
        let post_cnt_max = 1550;
        let endHour = '23' // 23:00 finish
        let targets = [
            { page_id: 'sweets', cat: 'food', cron: 'cron3' },
            { page_id: 'スイーツ', cat: 'food', cron: 'cron3' },
            { page_id: 'グルメ', cat: 'food', cron: 'cron3' },
            { page_id: 'ケーキ', cat: 'food', cron: 'cron3' },
            { page_id: 'アイス', cat: 'food', cron: 'cron3' },
            { page_id: 'お節', cat: 'food', cron: 'cron3' },
            { page_id: 'おせち料理', cat: 'food', cron: 'cron3' },
            { page_id: 'おせち', cat: 'food', cron: 'cron3' },
            { page_id: 'バレンタイン', cat: 'food', cron: 'cron3' },
            { page_id: 'お鍋', cat: 'food', cron: 'cron3' },
            { page_id: '鍋', cat: 'food', cron: 'cron3' },
            { page_id: '鍋パ', cat: 'food', cron: 'cron3' },
            { page_id: 'food', cat: 'food', cron: 'cron3' },
            { page_id: 'gourmet', cat: 'food', cron: 'cron3' },

            { page_id: 'チョコレート', cat: 'food', cron: 'cron4' },
            { page_id: 'パフェ', cat: 'food', cron: 'cron4' },
            { page_id: '料理', cat: 'food', cron: 'cron4' },
            { page_id: 'ランチ', cat: 'food', cron: 'cron4' },
            { page_id: 'ハロウィン', cat: 'fashion', cron: 'cron4' },
            { page_id: '腕時計', cat: 'fashion', cron: 'cron4' },
            { page_id: 'ファッション', cat: 'fashion', cron: 'cron4' },
            { page_id: 'メンズファッション', cat: 'fashion', cron: 'cron4' },
            { page_id: 'ファッションコーデ', cat: 'fashion', cron: 'cron4' },
            { page_id: 'ファッションスナップ', cat: 'fashion', cron: 'cron4' },
            { page_id: 'バッグ', cat: 'fashion', cron: 'cron4' },
            { page_id: 'アクセサリー', cat: 'fashion', cron: 'cron4' },
            { page_id: 'コスプレ', cat: 'fashion', cron: 'cron4' },
            { page_id: 'ワイン', cat: 'food', cron: 'cron4' },
            { page_id: 'wine', cat: 'food', cron: 'cron4' },
            { page_id: '日本酒', cat: 'food', cron: 'cron4' },
            { page_id: 'ウイスキー', cat: 'food', cron: 'cron4' },
            { page_id: '焼酎', cat: 'food', cron: 'cron4' },

            { page_id: 'ディナー', cat: 'food', cron: 'cron5' },
            { page_id: 'lunch', cat: 'food', cron: 'cron5' },
            { page_id: 'chocolate', cat: 'food', cron: 'cron5' },
            { page_id: 'cake', cat: 'food', cron: 'cron5' },
            { page_id: 'healthyfood', cat: 'food', cron: 'cron5' },
            { page_id: 'breakfast', cat: 'food', cron: 'cron5' },
            { page_id: 'ダイエット', cat: 'life', cron: 'cron5' },
            { page_id: 'インテリア', cat: 'life', cron: 'cron5' },
            { page_id: 'コスメ', cat: 'life', cron: 'cron5' },
            { page_id: '雑貨', cat: 'life', cron: 'cron5' },
            { page_id: 'ネイル', cat: 'life', cron: 'cron5' },
            { page_id: 'キッチン', cat: 'life', cron: 'cron5' },
            { page_id: '収納', cat: 'life', cron: 'cron5' },
            { page_id: 'kitchen', cat: 'life', cron: 'cron5' },
            { page_id: 'interiordesign', cat: 'life', cron: 'cron5' },
            { page_id: 'interior', cat: 'life', cron: 'cron5' }
        ];

        switch (cli['run']) {

            // node src/rakuten/FGS156-library-instagram/scraper.js -r 99
            case 99: // reset list for target instagram web page
                yield init();
                let tables = { targets: true, targets_result: true, targets_page: true, targets_match: true };
                yield instagram.resetDB(tables, targets);
                yield end();
                break;

            // node src/rakuten/FGS156-library-instagram/scraper.js -r 98
            case 98: // reset list for target instagram web page
                yield init();
                yield instagram.deleteDB();
                yield end();
                break;

                // node src/rakuten/FGS242-instagram-explore/scraper.js -r 3
            case 90: // set out fastload data
                yield init();
                yield outFastload();
                yield end();
                break;

                // node src/rakuten/FGS242-instagram-explore/scraper.js -r 2 -C cron3
                // -B chrome, -B phantom, -B firefox
            case 2: // set out fastload data
                for (let i = 0; i < targets.length; i++) {
                    if (targets[i].cron != cron) continue;
                    let strTor = tor ? 'tor': '';
                    shell.exec(`node src/rakuten/FGS242-instagram-explore/scraper.js -r 1 -C ${cron} -i ${targets[i].page_id} -o ${strTor}`);
                }
                break;

                // node src/rakuten/FGS242-instagram-explore/scraper.js -r 1
                // -B chrome, -B phantom, -B firefox
            case 1: // get post from instagram targets
            default:
                yield init();
                yield phantom.loginNoneIntra();
                yield instagram.scrapeExplore2(post_date_min, post_cnt_max, endHour, target_page_id, tor);
                yield end();
                break;

        }
    });
}

run();