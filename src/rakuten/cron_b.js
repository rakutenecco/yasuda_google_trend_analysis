'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const path_log = `../log/${date}.txt`;
let now = '';

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

// instagram daily 00:05
new cronJob(`00 05 00 * * 0-6`, function () {
        shell.exec('npm run amazon1');
    }, function () {
        // error
        now = moment().format('YYYYMMDD HH:mm:ss');
        fs.appendFileSync(path_log, `Error ${now} : stop instagram cron \n`, 'utf8');
    },
    true, 'Asia/Tokyo');

console.log('clone setting');