'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// CasioUsa 関数生成
let CasioUsa = function () {

    this.title = 'CasioUsa';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
CasioUsa.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
CasioUsa.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs42_casio_category where page_id in ('12-1','12-2','12-3','12-4','12-5','12-6','12-7','12-8','12-9','12-10','12-11');`;
            const sql2 = `delete from fashion_fgs42_casio_product where page_id in ('12-1','12-2','12-3','12-4','12-5','12-6','12-7','12-8','12-9','12-10','12-11');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape category
CasioUsa.prototype.scrapeCategory = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'casio USA';
            let sql = `SELECT * FROM fashion_fgs42_casio_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let id = res[i].id;
                let cat_url = res[i].cat_url;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let page_id = res[i].page_id;
                let manufacturer_name = res[i].name;
                let baseUrl = util.getBaseUrl(cat_url);

                // open url
                driver.get(cat_url);
                yield util.sleep(2000);

                // scrape list
                $ = yield selenium.parseBody(driver);
                $('.info.bg-white').each(function (i, e) {
                    co(function* () {
                        let text = $(e).find('.t-size-x-small').text();
                        let url = baseUrl + $(e).find('a').attr('href');
                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs42_casio_category(category_name, category_url, page_id) VALUES (?, ?, ?);`;
                        let opt = [text, url, page_id];
                        yield db.do(`run`, sql, opt);
                    });
                });

                yield util.sleep(200);
            }

            yield end();

            resolve();
        });
    });
}

// scrape product
CasioUsa.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let sql1 = `SELECT * FROM fashion_fgs42_casio_category a INNER JOIN fashion_fgs42_casio_list b ON a.page_id = b.page_id and a.page_id in ('12-1','12-2','12-3','12-4','12-5','12-6','12-7','12-8','12-9','12-10','12-11') WHERE NOT EXISTS (SELECT * FROM fashion_fgs42_casio_product c WHERE c.url = a.category_url);`;
            let res = yield db.do(`all`, sql1, {});

            // each product
            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let category_name = res[i].category_name;
                let category_url = res[i].category_url;
                let id = res[i].id;
                let page_id = res[i].page_id;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let manufacturer_name = res[i].name;
                let brand_name = '';
                let insert_date = moment().format('YYYY-MM-DD');
                console.log(category_url, shop_category1);

                // open url
                driver.get(category_url);
                yield util.sleep(2000);

                // set product lists
                $ = yield selenium.parseBody(driver);

                let mpn_1 = util.replace($('.js-toggle-body .t-size-x-large').text());
                let mpn_2 = mpn_1;
                let color = '';
                let product_name = mpn_1;
                let price = util.replace($('.price').text(), { price: true, match: '$', removeMatch: true, removeComma: true });
                let price_tax = util.getPriceTax(price);
                let price_unit = 'ドル';
                let size = '';
                let size_value = yield match.getSizeUnit(size);
                let url = category_url;

                // shop情報を取得する
                let category_url_2 = util.replace($('.txt-center').find('a').attr('href'));
                driver.get(category_url_2);
                yield util.sleep(2000);

                // scrape list
                $ = yield selenium.parseBody(driver);
                $('#content_block #prod_title').each(function (i, e) {
                    co(function* () {
                        // let prod_title = $(e).html();
                        // let mpn_1_2 = util.replace(prod_title.replace('<br />', '<br>').split('<br>')[0]);
                        color = $(e).find('#eitemNo').html();
                        let price_2 = util.replace($(e).find('#price').text(), { price: true, match: '$', removeMatch: true, removeComma: true });
                        let price_tax_2 = util.getPriceTax(price_2);
                        price = price || price_2;
                        price_tax = price_tax || price_tax_2;
                    });
                });

                // insert data
                let sql = `INSERT OR REPLACE INTO fashion_fgs42_casio_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                yield db.do(`run`, sql, opt);
                console.log(opt);
            }

            yield end();

            resolve();
        });
    });
}

module.exports = CasioUsa;