'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs42'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;

            case 1: // usual action
            default:
                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 100');
                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 99');

                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 9908');
                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 8');

                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 9907');
                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 7');

                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 9906');
                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 6');

                shell.exec('node src/rakuten/FGS42-casio/scraper.js -r 5 -B chromium');
                shell.exec('node src/rakuten/FGS42-casio/shell.js -r 100');
                break;
        }
    });
}

run();