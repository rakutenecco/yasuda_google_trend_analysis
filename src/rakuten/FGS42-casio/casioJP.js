'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// CasioJP 関数生成
let CasioJP = function () {

    this.title = 'CasioJP';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
CasioJP.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
CasioJP.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs42_casio_category where page_id in ('12-13');`;
            const sql2 = `delete from fashion_fgs42_casio_product where page_id in ('12-13');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape category
CasioJP.prototype.scrapeCategory = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'casio JP';
            let sql = `SELECT * FROM fashion_fgs42_casio_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let id = res[i].id;
                let cat_url = res[i].cat_url;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let page_id = res[i].page_id;
                let manufacturer_name = res[i].name;
                let baseUrl = util.getBaseUrl(cat_url);
                let isNext = true;
                let page = 1;
                let eleNextNum = 0;

                driver.get(cat_url);
                yield util.sleep(3000);

                // 
                while (isNext) {

                    // open url
                    isNext = false;
                    selenium.scroll(driver, 'footer');
                    yield util.sleep(1000);

                    // scrape list
                    $ = yield selenium.parseBody(driver);
                    $('#result-list .column').each(function (i, e) {
                        co(function* () {
                            let text = $(e).find('h3.font-x-small').text();
                            let url = baseUrl + $(e).find('a').attr('href');
                            // insert data
                            let sql = `INSERT OR REPLACE INTO fashion_fgs42_casio_category(category_name, category_url, page_id) VALUES (?, ?, ?);`;
                            let opt = [text, url, page_id];
                            yield db.do(`run`, sql, opt);
                        });
                    });

                    // paging
                    let ele1 = yield selenium.getElements(driver, '.pagination');
                    let ele2 = yield selenium.getElements(ele1[0], 'a');
                    for (let j = 0; j < ele2.length; j++) {
                        let page_this = yield selenium.getText(ele2[j]);
                        // console.log('page_this > ', page_this, page, page < page_this, parseInt(page, 10) < parseInt(page_this, 10));

                        // flag next
                        if (parseInt(page, 10) < parseInt(page_this, 10) && page_this.match(/^[0-9]+$/i) && !isNext) {
                            page = page_this;
                            isNext = true;
                            eleNextNum = j;
                        }
                    }

                    if (isNext) {
                        ele2[eleNextNum].click();
                    }

                    yield util.sleep(2000);
                }
            }

            yield end();

            resolve();
        });
    });
}

// scrape product
CasioJP.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let sql1 = `SELECT * FROM fashion_fgs42_casio_category a INNER JOIN fashion_fgs42_casio_list b ON a.page_id = b.page_id and a.page_id in ('12-13') WHERE NOT EXISTS (SELECT * FROM fashion_fgs42_casio_product c WHERE c.url = a.category_url);`;
            let res = yield db.do(`all`, sql1, {});

            // each product
            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let category_name = res[i].category_name;
                let category_url = res[i].category_url;
                let id = res[i].id;
                let page_id = res[i].page_id;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let manufacturer_name = res[i].name;
                let brand_name = '';
                let insert_date = moment().format('YYYY-MM-DD');
                console.log(category_url, shop_category1);

                // open url
                driver.get(category_url);
                yield util.sleep(2000);

                // set product lists
                $ = yield selenium.parseBody(driver);

                let mpn_1 = util.replace($('.toggle-body h2.t-size-x-large').text());
                let mpn_2 = mpn_1;
                let color = '';
                let product_name = mpn_1;
                let price = util.replace($('.price').text(), { price: true, match: '¥', removeMatch: true, removeComma: true });
                let price_tax = util.getPriceTax(price);
                let price_unit = '円';
                let url = category_url;

                // size
                let sizes = [];
                $ = yield selenium.parseBody(driver);
                $('.outer li').each(function (i, e) {
                    co(function* () {
                        let size_pre = util.replace($(e).text());
                        if (size_pre.match('サイズ')) {
                            sizes.push(size_pre);
                        }
                    });
                });
                let size = sizes.join(' ');
                let size_value = yield match.getSizeUnit(size);

                // insert data
                let sql = `INSERT OR REPLACE INTO fashion_fgs42_casio_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                yield db.do(`run`, sql, opt);
            }

            yield end();

            resolve();
        });
    });
}

module.exports = CasioJP;