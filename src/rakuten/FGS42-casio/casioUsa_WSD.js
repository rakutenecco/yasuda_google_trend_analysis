'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// CasioUsaWSD 関数生成
let CasioUsaWSD = function () {

    this.title = 'CasioUsaWSD';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: true });

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
CasioUsaWSD.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
CasioUsaWSD.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs42_casio_category where page_id in ('12-12');`;
            const sql2 = `delete from fashion_fgs42_casio_product where page_id in ('12-12');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();

            resolve();
        });
    });
}

// scrape product
CasioUsaWSD.prototype.scrapeProduct = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield init();

            let $ = null;
            let shop_category1 = 'casio wsd-f20';
            let sql = `SELECT * FROM fashion_fgs42_casio_list where shop_category1 = '${shop_category1}'`;
            let res = yield db.do(`all`, sql, {});

            // each product
            for (let i = 0; i < res.length; i++) {

                let isEndPage = false;
                let category_name = res[i].shop_category1;
                let category_url = res[i].cat_url;
                let id = res[i].id;
                let page_id = res[i].page_id;
                let shop_category1 = res[i].shop_category1;
                let shop_category2 = res[i].shop_category2;
                let manufacturer_name = res[i].name;
                let brand_name = '';
                let insert_date = moment().format('YYYY-MM-DD');
                console.log(category_url, shop_category1);

                // open url
                driver.get(category_url);
                yield util.sleep(6000);

                // set product lists
                $ = yield selenium.parseBody(driver);
                $('.wat-label').each(function (i, e) {
                    co(function* () {
                        let mpn_1 = util.replace($(e).find('.title-4').text());
                        let mpn_2 = mpn_1;
                        let color = util.replace($(e).find('.txt-p').text());
                        let product_name = mpn_1;
                        let price = 0;
                        let price_tax = util.getPriceTax(price);
                        let price_unit = 'ドル';
                        let size = '';
                        let size_value = yield match.getSizeUnit(size);
                        let url = category_url;

                        // insert data
                        let sql = `INSERT OR REPLACE INTO fashion_fgs42_casio_product(id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, unit_price, category_name, insert_date, url) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`;
                        let opt = [id, page_id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, price_tax, price, price_unit, category_name, insert_date, url];
                        yield db.do(`run`, sql, opt);
                    });
                });
            }

            yield end();

            resolve();
        });
    });
}

module.exports = CasioUsaWSD;