'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
new cronJob(`00 15,45 22-23 * * *`, function () {
    
    shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js`);
    
}, function () {
    // error
    console.log(`Error ${today} : FGS239 make_outfile`);
},
true, 'Asia/Tokyo');

console.log('cron12 setting');
