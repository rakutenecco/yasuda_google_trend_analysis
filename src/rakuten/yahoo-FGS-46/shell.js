require('shelljs/global');

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const shell = require('shelljs');
const conf = new(require('../../conf.js'));
const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    //    { name: 'brand_id', alias: 'b', type: Number },
    //    { name: 'nums', alias: 'n', type: Number, multiple: true },
    //    { name: 'critical', alias: 'c', type: Boolean },
    //    { name: 'skip', alias: 's', type: Boolean },
    { name: 'ch', alias: 'c', type: Number } //,
    //    { name: 'init', alias: 'i', type: Boolean }
]);

// let opt_c = cli['ch'] ? cli['ch'] : 0;
let opt_c = conf.pc_id=='P51392' ? '-c 1' : '-c 0';
let ymd = 'y' + moment().format('YYYYMMDD') + '.csv';

// mount share folder
// shell.exec(`echo 'bebe1973?1' | sudo -S mount -t vboxsf share /mnt/share/`);

// run daily action
shell.exec(`cd ../yahoo/ ; python3 yahoo.py ${opt_c}`);
shell.exec(`cd ../yahoo/ ; python3 yahoo.py ${opt_c}`);
shell.exec(`cd ../yahoo/ ; python3 yahoo.py ${opt_c}`);
shell.exec(`cd ../yahoo/ ; python3 yahoo.py ${opt_c}`);

// tsv > txt on share folder
// shell.exec(`cp ../yahoo/output/${ymd} /mnt/share/yahoo_ranking_merge.csv`);
shell.exec(`cp ../yahoo/input/shop.csv /mnt/share/shop.csv`);
