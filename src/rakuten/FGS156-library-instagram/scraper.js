'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const _ = require('underscore');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const selenium = new(require('../../modules/selenium.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));
const util = new(require('../../modules/util.js'));
const instagram = new(require('../../modules/instagram.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'post_target_day', alias: 't', type: String }, // instagram post day (ex. 2018-05-01)
    { name: 'update_target_day', alias: 'T', type: String }, // update day (ex.2018-05-19)
    { name: 'category', alias: 'c', type: String },
    { name: 'hash', alias: 'h', type: Boolean }, // scrape tag_hash as web page
]);

let driver = null;
let driver2 = null;
let task_name = 'fgs156';
let cron = 'cron2';
let filePath_db = `${conf.dirPath_db}/cron2.db`;
let today = moment().format('YYYYMMDD');
let today_haifun = moment().add(0, 'days').format('YYYY-MM-DD');
let yesterday_haifun = moment().add(-1, 'days').format('YYYY-MM-DD');
let update_target_day = cli['update_target_day'] ? moment(cli['update_target_day']).format('YYYY-MM-DD') : today_haifun;
let post_target_day = cli['post_target_day'] ? moment(cli['post_target_day']).format('YYYY-MM-DD') : yesterday_haifun;
let category = cli['category'] ? cli['category'] : 'basic';
let browser = cli['browser'] || 'chromium';
let checkHash = cli['hash'] || false;
console.log('post_target_day', post_target_day, ', update_target_day', update_target_day);

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield instagram.init(task_name, update_target_day, post_target_day, category, cron, browser, checkHash, false, filePath_db);
            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// set out fastload data
const outFastload = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(filePath_db);

            // set filepath
            let filepath_fastload = `${conf.dirPath_share_vb}\/${task_name}-instagram-library.txt`;
            let filepath_fastload2 = `${conf.dirPath_share_vb}\/${task_name}-instagram-library_${today}.txt`;

            // header check
            fs.writeFileSync(filepath_fastload, '', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['ページ名', 'ページ元', 'ページURL', 'リンクTITLE', 'リンクURL', '検索投稿数', '取得タイプ', 'ページ状態', '公式認証', 'accessories', 'bag', 'fashiongoods', 'food', 'inner', 'kids', 'ladiesfashion', 'liquor_japan', 'liquor_western', 'mensfashion', 'shoes', 'sweet', 'watch', '参照元'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['tag_name', 'tag_origin', 'tag_url', 'link_name', 'link_url', 'postCount', 'tag_type', 'page_status', 'badge_auth', 'ge_accessories', 'ge_bag', 'ge_fashiongoods', 'ge_food', 'ge_inner', 'ge_kids', 'ge_ladiesfashion', 'ge_liquor_japan', 'ge_liquor_western', 'ge_mensfashion', 'ge_shoes', 'ge_sweet', 'ge_watch', 'ref_url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath_fastload, ['VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(1000)', 'VARCHAR(300)', 'VARCHAR(1000)', 'BIGINT', 'VARCHAR(10)', 'VARCHAR(10)', 'VARCHAR(10)', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'INTEGER', 'VARCHAR(1000)'].join('\t') + '\n', 'utf-8');

            // get page data
            let sql = `SELECT page_id, page_id_ref, cat, web_site, post_num, followed_num, following_num, explore_num, authentication  FROM fashion_${task_name}_library_instagram_targets_page a WHERE NOT EXISTS(SELECT * FROM fashion_${task_name}_library_instagram_targets_match b WHERE a.page_id = b.page_id and cat = '${category}' and del = 0 and up_date <> '${today_haifun}');`;
            let targets = yield db.do(`all`, sql, {});

            // each target pages
            for (let i = 0; i < targets.length; i++) {
                let tag_name = targets[i].page_id;
                let tag_origin = targets[i].page_id_ref;
                let tag_url = `https://www.instagram.com/${targets[i].page_id}`;
                let link_name = ``;
                let link_url = targets[i].web_site;
                let postCount = targets[i].explore_num;
                let tag_type = ``;
                let page_status = targets[i].post_num == 0 && targets[i].followed_num == 0 && targets[i].following_num == 0 ? `no page` : ``;
                let badge_auth = targets[i].authentication == 1 ? `Verified` : ``;

                let mb = yield util.getMetaBrands(link_url);
                let ge_accessories = mb['accessories'] || 0;
                let ge_bag = mb['bag'] || 0;
                let ge_fashiongoods = mb['fashiongoods'] || 0;
                let ge_food = mb['food'] || 0;
                let ge_inner = mb['inner'] || 0;
                let ge_kids = mb['kids'] || 0;
                let ge_ladiesfashion = mb['ladiesfashion'] || 0;
                let ge_liquor_japan = mb['liquor_japan'] || 0;
                let ge_liquor_western = mb['liquor_western'] || 0;
                let ge_mensfashion = mb['mensfashion'] || 0;
                let ge_shoes = mb['shoes'] || 0;
                let ge_sweet = mb['sweet'] || 0;
                let ge_watch = mb['watch'] || 0;
                let ref_url = '';

                // append data
                fs.appendFileSync(filepath_fastload, [tag_name, tag_origin, tag_url, link_name, link_url, postCount, tag_type, page_status, badge_auth, ge_accessories, ge_bag, ge_fashiongoods, ge_food, ge_inner, ge_kids, ge_ladiesfashion, ge_liquor_japan, ge_liquor_western, ge_mensfashion, ge_shoes, ge_sweet, ge_watch, ref_url].join('\t') + '\n', 'utf-8');

                // set data
                const sql1 = `INSERT OR REPLACE INTO fashion_${task_name}_library_instagram_targets_match(match, page_id, page_id_ref, cat, up_date, del) VALUES (?, ?, ?, ?, ?, ?);`;
                let opt1 = [targets[i].page_id, targets[i].page_id, targets[i].page_id_ref, targets[i].cat, today_haifun, 0];
                yield db.do(`run`, sql1, opt1);
            }

            // copy data
            let data = fs.readFileSync(filepath_fastload, 'utf-8');
            fs.writeFileSync(filepath_fastload2, data, 'utf-8');

            yield db.close();
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {

            // node src/rakuten/FGS156-library-instagram/scraper.js -r 3
            case 99: // reset list for target instagram web page
                yield init();
                let tables = { targets: true, targets_result: true, targets_page: true, targets_match: true };
                let targets = [
                    { page_id: 'gigihadid', cat: category },
                    { page_id: 'kendalljenner', cat: category },
                    { page_id: 'kaiagerber', cat: category },
                    { page_id: 'alexachung', cat: category },
                    { page_id: 'buyma_us', cat: category }
                ];
                yield instagram.resetDB(tables, targets);
                yield end();
                break;

                // node src/rakuten/FGS156-library-instagram/scraper.js -r 5
            case 98: // set match data set list in input
                yield init();
                yield instagram.setMatch();
                yield end();
                break;

                // node src/rakuten/FGS156-library-instagram/scraper.js -r 4
            case 3: // set out fastload data
                yield init();
                yield outFastload();
                yield end();
                break;

                // node src/rakuten/FGS156-library-instagram/scraper.js -r 2 -t 2018-04-01 -T 2018-05-20 -h
                // -B chrome, -B phantom, -B firefox
                // -t post_target_day, -T update_target_day, -h update at_mark and hash as page_id
            case 2: // set list for target instagram web page
                yield init();
                yield instagram.scrapePage();
                yield end();
                break;

                // node src/rakuten/FGS156-library-instagram/scraper.js -r 1
                // -B chrome, -B phantom, -B firefox
            case 1: // get post from instagram targets
            default:
                yield init();
                let post_date_min = moment().add(-1, 'months').format('YYYY-MM-DD');
                yield instagram.scrapePosts(post_date_min);
                yield end();
                break;
        }
    });
}

run();
