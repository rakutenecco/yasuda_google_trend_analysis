'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'startday', alias: 's', type: String },
    { name: 'cron', alias: 'n', type: String },      // cron2 (weekly)
]);

const today = moment().format('YYYYMMDD');
const startday = cli['startday'];

const cron = cli['cron'] || 'cron2';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

// drop table for concern tables
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            //yield db.connect(filePath_db);

            let sql = `DROP TABLE IF EXISTS FGS252_url;`;
            yield db.do(`run`, sql, {});

            sql = `DROP TABLE IF EXISTS FGS252_article;`;
            yield db.do(`run`, sql, {});
            //yield db.close();

            resolve();
        });
    });
}


// url data Scrape
const urlScrape = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield db.connect(filePath_db);

            const sql1 = `DROP TABLE IF EXISTS FGS252_url;`;
            yield db.do(`run`, sql1, {});

            const sql2 = `CREATE TABLE IF NOT EXISTS FGS252_url ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_category TEXT, c_sectors TEXT, c_publish TEXT, c_URL TEXT, c_title TEXT);`;
            yield db.do(`run`, sql2, {});

            let site = 'https://jp.fashionnetwork.com/news/';

            yield setUrlScrape(site);

            //yield db.close();

            resolve();
        });
    });
}

let n = 0;
// scrape Url data, and set db as FGS252_url
const setUrlScrape = function (start_site) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);


            let up_date = today;
            let p = 1;
            //let n = 0;
            let site;

            while (up_date >= startday) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + p + '.html';
                }
                //console.log('p = ' + p + ' ' + up_date);

                let $ = yield cheerio.get(site, 10);
                if ($ && $('body').html().match(/list-ui list-news/i)) {
                    $('.media__body.list-ui__faux-block-link').each(function (i, e) {
                        co(function* () {

                            n = n + 1;
                            //console.log('n = ' + n);

                            let url = $(e).find('.list-ui__title').attr('href');
                            //console.log('url = ' + url);

                            let title = $(e).find('.list-ui__title').text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                            //console.log('title = ' + title);

                            //console.log('publish = [' + $(e).find('.media__misc').find('time').text().replace(/[\n\r\t\(\) ,]/g, '') + ']');
                            let pbwk = $(e).find('.media__misc').find('time').text().replace(/[\n\r\t\(\) ,]/g, '').split('/');
                            let month = ('0' + pbwk[0]).slice(-2);
                            let day = ('0' + pbwk[1]).slice(-2);
                            let year = '20' + pbwk[2];
                            let publish = year + month + day;
                            //console.log('publish = [' + publish + ']');

                            let sectors = $(e).find('.media__misc').find('a').eq(0).text().replace(/[\n\r\t\s\(\) .,-]/g, '');

                            let category = $(e).find('.media__misc').find('a').eq(1).text().replace(/[\n\r\t\s\(\) .,-]/g, '');

                            if (up_date >= publish) {
                                up_date = publish;
                            }
                            //console.log('up_date = ' + up_date);

                            const sql = `INSERT OR REPLACE INTO FGS252_url ` +
                                `(c_num, c_getdate, c_category, c_sectors, c_publish, c_URL, c_title) VALUES (?, ?, ?, ?, ?, ?, ?);`;
                            let num = ('000' + n).slice(-4)
                            //console.log(num + ' ' + title );
                            let opt = [num, today, category, sectors, publish, url, title];
                            yield db.do(`run`, sql, opt);
                            //console.log('ok');

                        });
                    });
                }
                p++;

            }

            resolve();
        });
    });
}

// article data Scrape
const artScrape = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield db.connect(filePath_db);

            const sql1 = `DROP TABLE IF EXISTS FGS252_article;`;
            yield db.do(`run`, sql1, {});

            const sql2 = `CREATE TABLE IF NOT EXISTS FGS252_article ` +
                `(c_num TEXT primary key, c_getdate TEXT, c_URL TEXT, c_art_publish TEXT, c_art_title TEXT, c_article TEXT);`;
            yield db.do(`run`, sql2, {});

            let lst = 0;
            do {
                const list = yield getList();
                if (list.length > 0) {
                    yield setArtScrape(list);
                }
                lst = list.length;
            } while (lst > 0)

            //yield db.close();

            resolve();
        });
    });
}

// get url list
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `SELECT min(u.c_num) c_num, min(u.c_getdate) c_getdate, u.c_URL FROM FGS252_url u
                        where not exists ( select * from FGS252_article a where a.c_URL = u.c_URL )
                        group by c_URL order by 1`;
            let res = yield db.do(`all`, sql, {});

            resolve(res);
        });
    });
}

// scrape article data, and set db as FGS252_article
const setArtScrape = function (urls) {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            for (let i = 0; i < urls.length; i++) {

                let num = urls[i].c_num;
                let getdate = urls[i].c_getdate;
                let URL = urls[i].c_URL;
                //console.log('num = ' + num);
                //console.log('URL = ' + URL);

                let tmp0 = URL.replace('http://jp.fashionnetwork.com/news/', '').split(',');
                let tmp1 = util.encodeUrl(tmp0[0]);
                let tmp2 = 'http://jp.fashionnetwork.com/news/' + tmp1 + ',' + tmp0[1];
                //console.log(tmp2);
                let $ = yield cheerio.get2(tmp2);

                if ($ && $('body').html().match(/articleBody/i)) {
                    let e = $('article');

                    let art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                    //console.log(art_title);

                    let art_publish = $(e).find('.time-ago__text').text().replace(/[^0-9]/g, '');
                    //console.log('art_publish = [' + art_publish + ']');

                    let article = $(e).find('.newsContent').text().replace(/[\n\r\t\(\) 　 ,]/g, '');
                    //console.log(article);

                    const sql = `INSERT OR REPLACE INTO FGS252_article ` +
                        `(c_num, c_getdate, c_URL, c_art_publish, c_art_title, c_article) VALUES (?, ?, ?, ?, ?, ?);`;
                    let opt = [num, getdate, URL, art_publish, art_title, article];
                    yield db.do(`run`, sql, opt);

                }
            }

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            //yield db.connect(filePath_db);
            let sql = `SELECT u.c_getdate, u.c_num, u.c_category, u.c_sectors, u.c_publish, u.c_URL, u.c_title, a.c_art_title, a.c_article ` +
                `FROM FGS252_url u ,FGS252_article a ` +
                `WHERE u.c_getdate = '${today}' ` +
                `AND u.c_getdate = a.c_getdate AND u.c_num = a.c_num ` +
                `AND u.c_publish >= '${startday}' AND u.c_publish < '${today}' ` +
                `ORDER BY u.c_num;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS252-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', 'カテゴリー', 'セクター', '更新日', 'URL', 'タイトル', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_category', 'c_sectors', 'c_publish', 'c_URL', 'c_title', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(50)', 'VARCHAR(50)', 'VARCHAR(8)', 'VARCHAR(500)', 'VARCHAR(500)', 'VARCHAR(500)', 'VARCHAR(29000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].c_num, res[i].c_category, res[i].c_sectors, res[i].c_publish, res[i].c_URL, res[i].c_title, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            //yield db.close();
            resolve(res);
        });
    });
}



// run action
const run = function () {

    co(function* () {
        
        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        console.log(`/// FGS252-FashionNetwork scraper.js start /// ${st}`);

        yield db.connect(filePath_db);

        switch (cli['run']) {
            case 1: // scrape
                yield urlScrape();
                yield artScrape();
                yield setOutFile();
                break;
            case 2: // drop
                yield drop();
                break;
            case 3: // url data Scrape
                yield urlScrape();
                break;
            case 4: // article data Scrape
                yield artScrape();
                break;
            case 5: // out data to share folder
                yield setOutFile();
                break;
            default:
                break;
        }

        yield db.close();
        console.log(`/// FGS252-FashionNetwork scraper.js   end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    });
}

run();