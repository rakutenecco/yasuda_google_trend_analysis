'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
//const moment = require('moment');
const moment = require('moment-timezone');
//const url = require('url');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');
const selenium = new (require('../../modules/selenium.js'));
const cheerio = new (require('../../modules/cheerio-httpcli.js'));

// new modules
const conf = new (require('../../conf.js'));
const db = new (require('../../modules/db.js'));
const util = new (require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },// target file name
    { name: 'startdate', alias: 's', type: String },// yyyymmdd Sunday only
//    { name: 'getProc', alias: 'p', type: Number },// 1:cheerio1 2:cheerio2 3:chromium
]);
let mode;
if(cli['run'] == 1){
    mode = 'new';
} else if (cli['run'] == 2){
    mode = 'continue';
} else if (cli['run'] == 9){
    mode = 'output';
}
const cron = cli['cron'] ;
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const infile = `${conf.dirPath_share_vb}/targets/${cli['fname']}`;

//const runtype = cli['run'];
const category = cli['fname'].replace('targets_','').replace('.txt','');
const tname = `FGS258_twitter_posts_${category}`;
const logtable = 'FGS258_log';

//const yesterday = moment().subtract(1, 'days').format('YYYYMMDD');
//const targetday = cli['targetday'] ? cli['targetday'] : yesterday;


let startdate; //output filename
let startdate_slash; //result data, log data
if(cli['startdate']){
    startdate = cli['startdate'];
    startdate_slash = cli['startdate'].substr(0, 4) + '/' + cli['startdate'].substr(4, 2) + '/' + cli['startdate'].substr(6, 2);
}else{
    startdate = moment().day(0).add(-7, "days").format('YYYYMMDD');
    startdate_slash = moment(startdate).format('YYYY/MM/DD');
}
const startdate_hiphen = startdate_slash.replace(/\//g, '-'); //ng check

let outfile;
let outlogfile;
// ex) fname:targets_all.txt -> FGS258_twitter_allposts_week_all_20190609.txt
// ex) fname:targets_all_03_01.txt -> FGS258_twitter_allposts_week_all_20190609_03_01.txt
// ex) fname:targets_fashion.txt -> FGS258_twitter_allposts_week_fashion_20190609.txt
// ex) fname:targets_new_20190614.txt -> FGS258_twitter_allposts_week_new20190614_20190609.txt
let tfnm = category.split(/_/g);
if(tfnm.length == 1) {
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}.log`;

} else if ( tfnm.length == 3 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}_${tfnm[1]}_${tfnm[2]}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}_${tfnm[1]}_${tfnm[2]}.log`;

} else if ( tfnm.length == 2 && tfnm[0] == 'new' && isFinite(tfnm[1]) ) {
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}${tfnm[1]}_${startdate}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}${tfnm[1]}.log`;

} else {
    console.log(` cli['fname']: ${cli['fname']} check the file name.`);
    return;
}

//average file
const avrfile = `${conf.dirPath_share_vb}/targets/FGS258_twitter_targets_allposts_week_average.txt`;
const avrtable = `FGS258_twitter_targets_allposts_week_average`;

//NG file
const ngfile = `${conf.dirPath_share_vb}/targets/FGS258_twitter_targets_${category}_ngchk.txt`;

// get process
let getProc = 1;
// if(getProc > 3){
//     console.log(` cli['getProc']: ${cli['getProc']} error.`);
//     return;
// }
const sttime = moment().format('YYYY/MM/DD HH:mm:ss');
console.log(`/// FGS258-twitter-posts scraper.js start  ${sttime} ///`);
console.log(`  mode: ${mode}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  args  category: ${category}`);
console.log(`  startdate: ${startdate_slash}`);
console.log(`  outfile: ${outfile}`);
//console.log(`  getProc: ${getProc}`);

const maxloop = 100;//20;
const minnum = 10;

// variable
let keywords = [];
//let notes;


//target date
//const target_date = targetday.substr(0, 4) + '/' + targetday.substr(4, 2) + '/' + targetday.substr(6, 2);

const since = startdate_slash.replace(/\//g, '-'); //URL:start(defalt)
//console.log(since,'test');
//const since_unix = moment(since).tz("Asia/Tokyo").unix();
//console.log(since_unix);
const untilstr = moment(startdate_slash.replace(/\//g, '-')).add(7, 'days').format('YYYY-MM-DD_00:00:00');
const lastdate = moment(startdate_slash.replace(/\//g, '-')).add(6, 'days').format('YYYY-MM-DD');
let until_date; //URL:end

let tgtnum; // targets table word number
//let getkwd = [];
let getkwds = 0;
let lcnt = 1;
let zcnt = 0;
let starttime;

let posts = 0;
let d_posts = 0;
let d_ngflg = -1;

let keyword;
let driver;

let Lgflg = false;
let Lgstr = '';
let dfscnds = 0;

let scrlcnt = 0;

//to check the number of url hit
let urlHit = 0;

let stopflg = false;

let lcomp = 10; // 10:undefined error 20:3keyword continuous error 30:time get error 100:access restriction error 1:complete

let ttl_scrlcnt = 0;

let ng2arr = [];
let ncnt = 0;

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// insert Log
const insertLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            starttime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `INSERT OR REPLACE INTO ${logtable} ` +
                `(tbname, startdate, loop_count, remainkwd, starttime, getProc) ` +
                `VALUES (?, ?, ?, ?, ?, ?);`;
            let opt = [tname, startdate_slash, lcnt, keywords.length, starttime, getProc];
            console.log(` insert log : tname:${tname} startdate:${startdate_slash} lcnt:${lcnt} kwds:${keywords.length} cron:${cron}  ${starttime}`);
            yield db.do(`run`, sql, opt);
            resolve();
        });
    });
}

// update Log
const updateLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            const curtime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `UPDATE ${logtable} SET getkwd = ${getkwds}, endtime = '${curtime}', complete = ${lcomp}, scrlcnt = ${ttl_scrlcnt} ` +
            `WHERE tbname = '${tname}' and startdate = '${startdate_slash}' and loop_count = ${lcnt} and starttime = '${starttime}';`;
            //console.log(sql);
            console.log(` update log : tname:${tname} startdate:${startdate_slash} lcnt:${lcnt} getkwds:${getkwds} lcomp:${lcomp} scrlcnt:${ttl_scrlcnt} cron:${cron}  ${starttime}-${curtime}`);
            yield db.do(`run`, sql, {});
            resolve();
        });
    });
}

// output Log
const outLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select tbname, startdate, loop_count, remainkwd, getkwd, starttime, endtime, complete, scrlcnt, getProc from ${logtable} ` +
                      `where tbname = '${tname}' and startdate = '${startdate_slash}' order by starttime;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            console.log(` output log: ${outlogfile}`);

            // append
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outlogfile, ([res[i].tbname, res[i].startdate, res[i].loop_count, res[i].remainkwd, res[i].getkwd, res[i].starttime, res[i].endtime, res[i].complete, res[i].scrlcnt, res[i].getProc].join('\t') + '\n'), 'utf8');
            }

            resolve();

        });
    });
}

// output NG file
const outNGfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select a.keyword keyword, '${startdate_slash}' startdate, a.postdate postdate, a.posts posts, a.average average, a.ngflg ngflg, a.lstuntil lstuntil, a.regtime regtime
            from ${tname} a
            where not exists (select * from ${tname} c where c.startdate = '${startdate_slash}' and c.ngflg = 0 and c.keyword = a.keyword ) 
            and a.startdate = '${startdate_slash}' order by 1,8;`;

            //console.log(`outNG sql:${sql}`);
            let res = yield db.do(`all`, sql, {});            

            if(res.length > 0){
                console.log(` output NG file: ${ngfile}`);

                // append
                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(ngfile, ([res[i].keyword, res[i].startdate, res[i].postdate, res[i].posts, res[i].average, res[i].ngflg, res[i].lstuntil, res[i].regtime].join('\t') + '\n'), 'utf8');
                }

            }else{
                console.log(` not exists NG words`);

            }

            resolve();

        });
    });
}

// init action
const init_table = function (result_table, targets_table, avr_table) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // targets table drop
            sqlstr = `DROP TABLE IF EXISTS ${targets_table};`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // targets table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${targets_table} ` +
                `(keyword TEXT PRIMARY KEY);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            
            // average table drop
            sqlstr = `DROP TABLE IF EXISTS ${avr_table};`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // average table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${avr_table} ` +
                `(keyword TEXT PRIMARY KEY, avrposts bigint);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});


            // result table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${result_table} ` +
                `(keyword TEXT, startdate TEXT, postdate TEXT, posts bigint, average bigint, ngflg integer, lstuntil TEXT, regtime TEXT);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i01_${result_table} on ${result_table} (keyword, startdate, ngflg, postdate);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});


            // old result delete
            const deldate = moment(startdate_slash.replace(/\//g, '-')).subtract(35, 'days').format('YYYY/MM/DD');
            console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM ${result_table} WHERE startdate < '${deldate}';`;// or startdate = '${startdate_slash}';`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // log table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${logtable} ` +
                `(tbname TEXT, startdate TEXT, loop_count integer, remainkwd bigint, getkwd bigint, starttime TEXT, endtime TEXT, complete integer, scrlcnt bigint, getProc integer );`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${logtable} on ${logtable} (tbname, startdate);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // old log delete
            const deldate2  = moment(startdate_slash.replace(/\//g, '-')).subtract(35, 'days').format('YYYY/MM/DD');
            console.log(` log deldate: ${deldate2}`);
            sqlstr = `DELETE FROM ${logtable} WHERE tbname = '${result_table}' and startdate < '${deldate2}';`;// or startdate = '${startdate_slash}';`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            resolve();
        });
    });
}

// store targets
const store_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            //infile = `${conf.dirPath_share_vb}/targets_${category}.txt`;
            //console.log('infile',infile);
            if (fs.existsSync(infile)) {
                console.log(` infile: ${infile} read start`);
                let arr = fs.readFileSync(infile).toString().split("\n");
                //console.log(kywds.length);
                if(arr[arr.length -1].length == 0) arr.pop();
                console.log(` targets file: ${arr.length}`);
                
                //Twitter hashtag check!! utf-8
                let kywds = [];
                let exkywds = [];
                for(let i = 0; i < arr.length; i++) {
                    
                    let str = arr[i];
                    let testResult = str.match(/[^ー・々〆ﾟ･αβφゞ﨑0-9_A-Za-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                    let numtest =  str.match(/[^0-9]/g);
                    if(testResult || !numtest){
                        exkywds.push(str);
                    }else{
                        kywds.push(str);
                    }
                }
                let twitter_except_file = `${conf.dirPath_share_vb}/targets/targets_except/targets_twitter_except_${category}_${startdate}.txt`;

                if(exkywds.length > 0){
                    for (let i = 0; i < exkywds.length; i++) {
                        fs.appendFileSync(twitter_except_file, exkywds[i] + '\n', 'utf8');
                    }
                }
                console.log(` except  keyword: ${exkywds.length}`);
                console.log(` targets keyword: ${kywds.length}`);

                let sql = `insert into FGS258_targets_${category} values `;
                if (kywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(kywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == kywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = kywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = kywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        //console.log(sql + '("' + exc.join('"),("') + '");');
                        yield db.do(`run`, sql + '("' + exc.join('"),("') + '");', {});
                        i++;
                    } while (times >= i);
                }
                result = true;

            }else{
                console.log('  not exists in file');
            
            }

            //average file
            if (fs.existsSync(avrfile) && result) {
                result = false;

                console.log(` average file: ${avrfile} read start`);
                let tgtsavrfile = fs.readFileSync(avrfile).toString().split("\n");
                //console.log(kywds.length);
                if(tgtsavrfile[tgtsavrfile.length -1].length == 0) tgtsavrfile.pop();
                //console.log(` targets average file: ${tgtsavrfile.length}`);

                let avkywds = tgtsavrfile;
                console.log(` targets average keyword: ${avkywds.length}`);

                let sql = `insert into ${avrtable} values `;
                if (avkywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(avkywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == avkywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = avkywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = avkywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        let arr = [];
                        for ( let iarr = 0, len = exc.length; iarr < len; ++iarr) {
                            //console.log( exc[iarr].replace('///','",'));
                            arr.push(exc[iarr].replace('///','",'));
                        }
                        //console.log(sql + '("' + arr.join('),("') + ');');
                        yield db.do(`run`, sql + '("' + arr.join('),("') + ');', {});
                        i++;
                    } while (times >= i);
                }                
                result = true;

            }else{
                console.log('  not exists averagefile');
                result = false;

            }
            resolve(result);
        });
    });
}

// get targets list
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS258_targets_${category};`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword, ifnull(m.avrposts, 0) avrposts, 0 ngflg
             from FGS258_targets_${category} a 
             left join ${avrtable} m on a.keyword = m.keyword
            where not exists (select * from ${tname} b where b.startdate = '${startdate_slash}' 
                and a.keyword = b.keyword )
            order by 1;`;
            //console.log(`get1 sql:${sql}`);
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            keywords = yield db.do(`all`, sql, {});
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            console.log('  get_targets  remain keywords: ' + keywords.length);

            if ( keywords.length == 0 ) {
                console.log(`    end once scraping`);
                yield get_targets2();
                if ( keywords.length == 0 ) {
                    console.log(`    end cheerio scraping`);
                    yield get_targets3();
                    if ( keywords.length == 0 ) {
                        console.log(`    end chromium scraping`);
                    }else{
                        getProc = 3;
                        console.log(`    getProc : 3`);
                    }
                }else{
                    getProc = 2;
                    console.log(`    getProc : 2`);
                }
            }else{
                //selenium default
                getProc = 3;
                console.log(`    getProc : 3`);
            }

            resolve();

        });
    });
}

// get targets 2
const get_targets2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql = `select a.keyword keyword, a.average avrposts, a.ngflg ngflg
             from ( select keyword, max(average) average, max(ngflg) ngflg from ${tname}
             where startdate = '${startdate_slash}' and ngflg != 0 and ngflg != 9 and postdate is NULL 
             group by 1 ) a
            where not exists (select * from ${tname} c where c.startdate = '${startdate_slash}' 
                and (c.ngflg = 0 or c.ngflg = 9) and postdate is NULL and a.keyword = c.keyword )
              and not exists (select * from (
                  select keyword,count(*) from ${tname} 
                   where startdate = '${startdate_slash}' and ngflg != 0 and ngflg != 9 and postdate is NULL 
                   group by 1 having count(*) > 1
                ) tt where a.keyword = tt.keyword )
            order by 3,2,1;`;
            //console.log(`get2 sql:${sql}`);
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            keywords = yield db.do(`all`, sql, {});
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            console.log('  get_targets2 remain keywords: ' + keywords.length);

            resolve();
        });
    });
}

// get targets 3
const get_targets3 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select a.keyword keyword, a.average avrposts, a.ngflg ngflg
            from ( select keyword, max(average) average, max(ngflg) ngflg from ${tname}
            where startdate = '${startdate_slash}' and ngflg != 0 and ngflg != 9 and postdate is NULL 
            group by 1 ) a
            where not exists (select * from ${tname} c where c.startdate = '${startdate_slash}' 
                and (c.ngflg = 0 or c.ngflg = 9) and postdate is NULL and a.keyword = c.keyword )
              and not exists (select * from (
                  select keyword,posts,count(*) from ${tname} 
                   where startdate = '${startdate_slash}' and (ngflg = 1 or ngflg = 2) and postdate is NULL 
                   group by 1,2 having count(*) > 1
                ) tt where a.keyword = tt.keyword )
            order by 3,2,1;`;
            //console.log(`get3 sql:${sql}`);
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            keywords = yield db.do(`all`, sql, {});
            //console.log(`${moment().format('YYYY/MM/DD HH:mm:ss.SSS')}`);
            console.log('  get_targets3 remain keywords: ' + keywords.length);

            if ( keywords.length > 0 ) {
                
                //loop count            
                sql = `select count(distinct loop_count) lct from ${logtable} 
                where startdate = '${startdate_slash}' and tbname = '${tname}' and getProc = 3;`;
                //console.log(sql);
                let num = yield db.do(`all`, sql, {});
                //console.log('    getProc : 3 loop_count:' + num[0].lct);
                if ( num[0].lct >= 13 ) {
                    console.log('    loop_count >= 13 end scraping');
                    keywords.length = 0
                }
            }

            resolve();
        });
    });
}

// get loop count
const get_loop_count = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //loop count
            
            const sql = `select max(loop_count) lct from ${logtable} where startdate = '${startdate_slash}' and tbname = '${tname}' and complete = 1;`;
            //console.log(sql);
            let num = yield db.do(`all`, sql, {});
            //console.log(' max loop_count: ' + num[0].lct);
            if ( num[0].lct > 0 ) {
                lcnt = num[0].lct + 1;
            }
            console.log(' loop_count: ' + lcnt);

            resolve();
        });
    });
}

// getElements
const getElements = function (driver_ele, selector) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!driver_ele || !selector) {
                console.log('    getElements arg null err');
                resolve('');
            }
            driver_ele.findElements(byElement(selector)).then(function (ele) {
                resolve(ele);
            }, function (err) {
                //console.log('    getElements err');
                resolve('');
            });
        }).catch((e) => {
            console.log('    catch getElements');
            resolve('');
        });
    });
}

// getAttrbute
const getAttrbute = function (driver_ele, attr) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!driver_ele || !attr) {
                //console.log('    getElements arg null err');
                resolve('');
            }
            driver_ele.getAttribute(attr).then(function (ele) {
                //console.log(`    ele:${ele}`); 
                resolve(ele);
            }, function (err) {
                console.log('    getAttribute err');
                resolve('');
            });
        }).catch((e) => {
            console.log('    catch getAttrbute');
            resolve('');

        });
    });
}


// Selenium 関数生成
let byElement = function (selector) {

    let tag = '';
    switch (selector.substr(0, 1)) {
        case '#':
            return By.id(selector.substr(1));
        case '.':
            return By.className(selector.substr(1));
        case '/':
            return By.xpath(selector);
        default:
            return By.css(selector);
    }
};

// get data from url
const getText = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
            }

            ele.getText().then(function (text) {
                resolve(text);
            }, function (err) {
                //console.log(err);
                resolve('');
            });
        }).catch((e) => {
            console.log('    catch getText');
            resolve('');
        });
    });
}

// scroll
const getScroll = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
            }

            driver.executeScript("arguments[0].scrollIntoView();", ele).then(null, function(err) {
                //
            }, function (err) {
                //console.log(err);
                resolve('');
            });
            //console.log("success");
            yield util.sleep(500);//200
            resolve('');
        }).catch((e) => {
            console.log('    catch getScroll');
            resolve('');
        });
    });
}


// open getlist
const get_empty = function (since_date) {

    return new Promise((resolve, reject) => {
        co(function* () {
            
            let result = false;

            //let url = `https://twitter.com/search?f=tweets&q=%23${keyword}%20since%3A${since_date}_00:00:00_JST%20until%3A${until_date}_JST&f=live`;
            let url = `https://twitter.com/search?q=%23${keyword}%20since%3A${since_date}_00:00:00_JST%20until%3A${until_date}_JST&f=live`;
            //console.log(url);
            if(!driver) {
                
               // console.log('     driver null');
                if(getProc == 3){
                    driver = yield selenium.init('chromium', { inVisible:true });
                }else{
                    driver = yield selenium.init('', { inVisible:true });
                }
                driver.manage().window().setSize(930,350);
            }
            driver.get(url).then(null, function (err) {
                
                //resolve(text);
            }, function (err) {
                console.log('      driver get err');
                resolve('err');
            });

            yield util.sleep(2000);
            if(driver){
                //let ele_empty = yield getElements(driver, '.SearchEmptyTimeline-emptyTitle');
                // div containing no result text  
                //ield util.sleep(2000);

                urlHit = urlHit + 1;
               
                //let limiter = yield(driver, 'span.css-901oao.css-16my406.r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0')
                let ele_empty = yield getElements(driver, 'div.css-901oao.r-hkyrab.r-1qd0xha.r-1b6yd1w');
                if (ele_empty.length > 0) {
                    let txt = yield getText(ele_empty[0]);
                    //console.log(txt);
                    let rt = txt.search( 'No results' );
                    if(rt > -1) {
                        result = true;
                    } else {
                        result = 'err';
                    }
                } else {
                    //let ele_grid = yield getElements(driver, '.u-lg-size2of3');
                    //div containing posts
                    let ele_grid = yield getElements(driver, 'div.r-13qz1uu.r-184en5c');
                    if(ele_grid.length == 1) {
                        let ele_time = yield getElements(ele_grid[0], 'time');
                        if(ele_time.length == 0) {
                            result = true;
                        }
                    } else {
                        result = 'err';
                    }
                    
                }
                if(urlHit > 1000){
                    result = 'limitReach';
                }
            }

            resolve(result);
        }).catch((e) => {
           console.log('    catch get_empty');
           resolve('err');
        });
    });
}

// page_scroll
const page_scroll = function (l_num, kwd, st) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let last_num = l_num;
            let result = '';
            const maxcnt = 2;
            let retry = false;  //fnnction error
            let rcnt = 0;       //retryカウンタ

            const maxpage = 5; // max scroll
            let p = 1;

            let ele_fs;
            let ele_fl;
            //ele_fs = yield getElements(driver, '.Footer-item');
            // Footer item list 
            //yield util.sleep(100);
            ele_fs = yield getElements(driver, 'a.r-bcqeeo.r-1qfoi16.r-qvutc0');         
            if (!ele_fs) ele_fs = yield getElements(driver, 'a.r-bcqeeo.r-1qfoi16.r-qvutc0');
            if(!ele_fs) {
                console.log(`       page_scroll  getElements Footer-item err  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                resolve('err');
            }
            //ele_fl = yield getElements(driver, '.stream-footer');
            //scroll element
            ele_fl = yield getElements(driver, 'div.css-1dbjc4n.r-1bxhq7s');
            if (!ele_fl) ele_fl = yield getElements(driver, 'div.css-1dbjc4n.r-1bxhq7s');
            if(!ele_fl) {
                console.log(`       page_scroll  getElements stream-footer err  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                resolve('err');
            }
            
            do {
                if(retry) rcnt = rcnt + 1;
                        
                if(rcnt >= maxcnt){
                    console.log(`       scroll redo: ${rcnt} max  kwd:${kwd}`);
                    eol = true;
                    result = 'failure';

                } else {
                    let ele_f;
                    if ( last_num < 3 ){                
                        ele_f = ele_fs[ele_fs.length - 1];
                    } else {
                        ele_f = ele_fl[0];
                    }
                    yield getScroll(ele_f);
                    
                    yield util.sleep(300);

                    scrlcnt = scrlcnt + 1;
                    ttl_scrlcnt = ttl_scrlcnt + 1;
                    //console.log(scrlcnt,'scrlnt');
                    let ele3 = yield getElements(driver, 'time');
            
                    if(!ele3) {
                        console.log(`       scroll .time get err  ttl_scrlcnt:${ttl_scrlcnt}  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                        lcomp = 30;
                        result = 'timegeterr';
                        break;
                    }
                    let this_num = ele3.length;
                    //console.log(`        page_scroll p:${p}  last_num:${last_num} ele3.length:${ele3.length}  kwd:${kwd}`);

                    if ( last_num == this_num ){
                        
                        let maxct = 2;
                        let scct = 1;
                        do {
                            
                            let rt = yield updown_scroll(ele_f);
                            yield util.sleep(300);
                            if(scct == maxct) {
                                //console.log(`      *** updown_scroll  last_num == this_num  ${kwd}  until_date:${until_date}  scct:${scct} max ***`); 
                                yield util.sleep(500);
                            }

                            if ( rt ) {
                                ele3 = yield getElements(driver, 'time');
                                if(!ele3) {
                                    console.log(`       page_scroll .time get err  ttl_scrlcnt:${ttl_scrlcnt}  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                                    continue;
                                }
                                scct = scct + 1;
                                //console.log(`          page_scroll updown_scroll  scct:${scct}  last_num:${last_num} ele3.length:${ele3.length}  kwd:${kwd}`);
                                if(last_num < ele3.length){
                                    this_num = ele3.length;
                                    //console.log(`          page_scroll updown_scroll kwd:${kwd} p:${p} scct:${scct}  last_num:${last_num} ele3.length:${ele3.length}  until_date:${until_date}`);
                                    break;
                                }
                            }
                        }while (scct <= maxct)
                    }

                    if ( last_num == this_num && last_num < 10 ){
                        
                        // 1second add
                        //console.log(`      ### get_empty_add  last_num < 10  ${kwd}  last_num:${last_num}  until_date:${until_date} ###`); 
                        
                        
                        let ele_empty_add = yield get_empty_add(st);
                        if (!ele_empty_add) {
                            console.log(`    ${kwd} page_scroll ele_empty_add err`); 
                            result = 'err'
                            break;
                        }
                        if(this_num < ele_empty_add) {
                            let dif = ele_empty_add - this_num;
                            d_posts = d_posts - dif;
                        }
                        this_num = ele_empty_add;
                        last_num = this_num;

                        //ele_fs = yield getElements(driver, '.Footer-item');
                      
                        ele_fs = yield getElements(driver, 'a.r-bcqeeo.r-1qfoi16.r-qvutc0'); 
                        if (!ele_fs)  ele_fs = yield getElements(driver, 'a.r-bcqeeo.r-1qfoi16.r-qvutc0'); 
                        if(!ele_fs) {
                            console.log(`       page_scroll  getElements Footer-item err  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                            result = 'err'
                            break;
                        }
                        ele_fl = yield getElements(driver, 'div.css-1dbjc4n.r-1bxhq7s');
                        if (!ele_fl) ele_fl = yield getElements(driver, 'div.css-1dbjc4n.r-1bxhq7s');
                        if(!ele_fl) {
                            console.log(`       page_scroll  getElements stream-footer err  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                            result = 'err'
                            break;
                        }

                        let maxct = 4;
                        let scct = 1;
                        do {

                            let ele_f2;
                            if((scct)%2 == 0) {
                                ele_f2 = ele_fs[ele_fs.length - 1];
                            }else{
                                ele_f2 = ele_fl[0];
                            }
                            let rt = yield updown_scroll(ele_f2);
                            
                            if(scct == maxct) {
                                //console.log(`      *** updown_scroll  last_num < 10  ${kwd}  until_date:${until_date}  scct:${scct} max ***`); 
                                yield util.sleep(500);
                            }

                            if ( rt ) {
                                ele3 = yield getElements(driver, 'time');
                                if(!ele3) {
                                    console.log(`       page_scroll .time get err  ttl_scrlcnt:${ttl_scrlcnt}  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                                    continue;
                                }
                                scct = scct + 1;
                                //console.log(`          page_scroll updown_scroll  scct:${scct}  last_num:${last_num} ele3.length:${ele3.length}  kwd:${kwd}`);
                                if(last_num < ele3.length){
                                    this_num = ele3.length;
                                    //console.log(`          page_scroll updown_scroll success  kwd:${kwd} p:${p} scct:${scct}  last_num:${last_num} ele3.length:${ele3.length}  until_date:${until_date}`);
                                    break;
                                }
                            }
                        }while (scct <= maxct)
                        
                    }
                    
                    if ( last_num == this_num || p == maxpage ){
                        //console.log("maxpage equals p");
                        //let elespn = yield getElements(ele3[ele3.length - 1], 'span');
                        
                        let elespn = ele3[ele3.length - 1];
                        //console.log(`   elespn.length:${elespn.length}`);
                        if(!elespn) {
                            console.log(`     scroll span get err`);
                            retry = true;
                            continue;
                            //break;
                        }
                        //console.log(elespn.length);
                        //let utime = yield getAttrbute(elespn, 'data-time');
                        let utime = yield getAttrbute(elespn, 'datetime');
                        //let utime = moment(isotime).format("YYYY-MM-DD_HH:mm:ss");    
                         
                        if(!utime) {
                            console.log(`     scroll data-time err kwd:${kwd} p:${p} eletime:${eletime.length} elespn:${elespn.length} this_num:${this_num} until_date:${until_date} posts:${posts}`);
                            break;
                        }
                        
                        until_date = moment(utime).tz("Asia/Tokyo").format("YYYY-MM-DD_HH:mm:ss");
                        //console.log(until_date,'until'); 
                        d_posts = d_posts + this_num;

                        if ( last_num == this_num ) {
                            result = 'end';
                            break;
                        }else{
                            result = 'continue';
                            break;
                        }                        

                    }else{
                        retry = false;
                        last_num = this_num;
                        p = p + 1;
                    }
                }
            } while (true)

            resolve(result);

        }).catch((e) => {
            console.log('    catch page_scroll');
            //console.log(e);
            //resolve();
            resolve('failure');
        });
    });
}

// updown_scroll
const updown_scroll = function (ele_f) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let ele_h;
            //let ele_f;
            //ele_h = yield getElements(driver, '.SearchNavigation');
            ele_h = yield getElements(driver, 'div.r-1pi2tsx.r-1777fci');
            if (!ele_h) ele_h = yield getElements(driver, 'div.r-1pi2tsx.r-1777fci');
            if(!ele_h) {
                console.log(`       updown_scroll  getElements SearchNavigation err  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                resolve('err');
            }
            yield getScroll(ele_h[0]);
            yield getScroll(ele_f);

            resolve(true);

        }).catch((e) => {
            console.log('    catch updown_scroll');
            //console.log(e);
            //resolve();
            return false;
        });
    });
}

// get_empty_add
const get_empty_add = function (st) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = 0;
            let tmp = until_date.split('_');
            let tmptime = moment(`${tmp[0]} ${tmp[1]}`).add(1, 'seconds').format("YYYY-MM-DD_HH:mm:ss");
            //console.log(`          get_empty_add 1second  until_date:${until_date} -> tmptime:${tmptime}`);
            until_date = tmptime;
            //console.log(`  until_date:${until_date}    tmptime:${tmptime}`);

            let ele_empty = yield get_empty(st);
            if (ele_empty == 'err') {
                console.log(`      get_empty err   get_empty_add  ${st}`);
                result = false;

            } else if (ele_empty) {
                console.log(`      get_empty err   get_empty_add  ${st}`);
                result = false;

            } else {
                let ele1 = yield getElements(driver, 'time');
                if(!ele1) {
                    console.log(`      getElements   get_empty_add  time get err`); 
                    result = false;
                } else {                               
                    result = ele1.length;
                    //d_posts = d_posts - result;
                    //console.log(`          get_empty_add 1second  result:${result} d_posts:${d_posts}  until_date:${until_date}`);
                }
            }           

            resolve(result);

        }).catch((e) => {
            console.log('    catch get_empty_add');
            //console.log(e);
            //resolve();
            resolve('');
        });
    });
}

// reg_daily_posts
const reg_daily_posts = function (idx, st) {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql = `INSERT OR REPLACE INTO ${tname} ` +
                        `(keyword, startdate, postdate, posts, average, ngflg, lstuntil, regtime) ` +
                        `VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;

            let l_ngflg = 0;
            let jdgtime = `${st}_01:00:00`;
            let avrg = Math.floor(keywords[idx].avrposts * 0.143);
            let criteria = Math.floor(keywords[idx].avrposts * 0.143 * 0.2);
            if(avrg >= 285 || d_posts >= 285){

                if( jdgtime < until_date ) {
                    l_ngflg = 2;
                    //console.log(`     ngflg:${ngflg} ${idx}: ${keywords[idx].keyword}  d_posts:${d_posts} avr:${avrg} st:${st} lstuntil:${until_date}`);

                }else if(d_posts < criteria) {
                    l_ngflg = 1;
                    //console.log(`     ngflg:${ngflg} ${idx}: ${keywords[idx].keyword}  d_posts:${d_posts} < ${criteria} avr:${avrg} st:${st} lstuntil:${until_date}`);
                }
            }

            if( d_ngflg < l_ngflg)  d_ngflg = l_ngflg;
            //console.log(`      ${idx}: ${keywords[idx].keyword}  d_posts:${d_posts} criteria:${criteria} avr:${avrg} st:${st} lstuntil:${until_date} l_ngflg:${l_ngflg}`);
            let regtime = moment().format('YYYY/MM/DD HH:mm:ss');
            let opt = [keywords[idx].keyword, startdate_slash, st, d_posts, avrg, l_ngflg, until_date, regtime];
            yield db.do(`run`, sql, opt);

            resolve(true);

        }).catch((e) => {
            console.log('    catch reg_daily_posts');
            //console.log(e);
            //resolve();
            resolve('');
        });
    });
}

// scrape_posts_selenium chroium
const scrape_posts_selenium = function (idx, kwd, st, ed) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = '';
            const maxcnt = 2;
            let retry = false;  //fnnction error
            let rcnt = 0;       //retryカウンタ

            do {
                if(retry) rcnt = rcnt + 1;
                    
                if(rcnt >= maxcnt){
                    result = 'err'
                    break;

                } else {
                    yield util.sleep(500);
                    let ele_empty = yield get_empty(st);

                    if(ele_empty ==  'limitReach'){
                        result = 'end';
                        urlHit = 0;
                        driver.quit();
                        driver = null;
                        yield util.sleep(5000);
                        break;
                    }
                    if (ele_empty == 'err') {
                        console.log(`      ${idx}:${kwd}  scrape_posts_selenium get_empty err`);
                        retry = true;
                        continue;

                    } else if ( until_date == ed && ele_empty ) {
                        result = 'end';
                        break;

                    } else {
                        let last_num;

                        if ( until_date != ed && ele_empty ){
                            // 1second add
                            //console.log(`      ### get_empty_add  ele_empty  ${kwd}  until_date:${until_date} ###`);
                            yield util.sleep(500);

                            let ele_empty_add = yield get_empty_add(st);
                            if (!ele_empty_add) {
                                console.log(`    ${kwd} scrape_posts_selenium ele_empty_add err`); 
                                result = 'err'
                                break;
                            }
                            last_num = ele_empty_add;
                            d_posts = d_posts - last_num;
                            //console.log(`          get_empty_add 1second  result:${ele_empty_add} d_posts:${d_posts}  until_date:${until_date}`);
                            
                        } else {

                            let ele1 = yield getElements(driver, 'time');
                            if(!ele1) {
                                console.log(`    ${kwd} scrape_posts_selenium time get err`); 
                                retry = true;
                                continue;
                            }                                
                            last_num = ele1.length;
                        }

                        if(last_num == 0) {
                            console.log(`    ${kwd} scrape_posts_selenium last_num == 0 err`); 
                            result = 'err';
                            break;
                        }

                        //scroll
                        let scrlrt = yield page_scroll(last_num, kwd, st); // 5page scroll

                        if(scrlrt == 'timegeterr') {
                            console.log(`    ${kwd} scrape_posts_selenium scroll timeget err`); 
                            result = 'err';
                            break;
                        }

                        if(scrlrt == 'continue'){
                            retry = false;
                            rcnt = 0;
                        }else if(scrlrt == 'end'){
                            result = 'end';
                            break;
                        }else{
                            result = 'err';
                            break;
                        }
                        
                        if((scrlcnt > 250 && driver)) {
                            //console.log(`        scrlcnt:${scrlcnt} ${idx}:${kwd} until_date:${until_date} posts:${posts}   ${moment().format('YYYY/MM/DD HH:mm:ss')}`); 
                            scrlcnt = 0;
                            driver.quit();
                            driver = null;
                            yield util.sleep(5000);
                        }
                    }
                }
                //console.log(`       ${idx}:${kwd} ng:${keywords[idx].ngflg}  scrlcnt:${scrlcnt} st:${st} until_date:${until_date} d_posts:${d_posts} posts:${posts}`);
            }while (true)

            resolve(result);

        }).catch((e) => {
            console.log('    catch getPsts2');
            //console.log(e);
            //resolve();
            resolve('failure');
        });
    });
}

// scrape Cheerio
const scrape_posts_cheerio = function (idx, kwd) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(!process.env.NODE_TLS_REJECT_UNAUTHORIZED){
                yield cheerio.init();
            }
            let result = '';
            const maxcnt = 3;
            let retry = false;
            let rcnt = 0;       //retryカウンタ
            let ctflg = false;  //Continuous

            const maxpage = 5; // max scroll
            let p = 0;

            Lgflg = false;
            Lgstr = '';
            dfscnds = 0;

            until_date = untilstr;
            let bf = until_date.split('_');
            let bfposts = posts;
            
            do {
                if(retry) {
                    if(ctflg){
                        rcnt = rcnt + 1;
                    }
                    ctflg = true;
                } else {
                    retry = false;
                    ctflg = false;
                } 
                        
                if(rcnt >= maxcnt){
                    console.log(`      scrape_posts_cheerio retrymax  ${idx}:${kwd} p:${p} ttlcnt:${ttl_scrlcnt} until:${until_date}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    result = 'err';
                    break;

                } else {

                    //let url = `https://twitter.com/search?f=tweets&q=%23${keyword}%20since%3A${since}_00:00:00_JST%20until%3A${until_date}_JST`;
                    let url = `https://twitter.com/search?q=%23${keyword}%20since%3A${since}_00:00:00_JST%20until%3A${until_date}_JST&f=live`;
                    //console.log(url);

                    let $ = yield cheerio.get2(url);
                    if(!$) {
                        yield util.sleep(2000);
                        if(!$) {
                            console.log(`       scrape_posts_cheerio get2(url) err ${idx}:${kwd} p:${p} ttl_scrlcnt:${ttl_scrlcnt} since:${since} until:${until_date}`);
                            retry = true;
                            continue;
                        }
                    }
                    yield util.sleep(100);
                    let h = $('body').html();
                    if(!h || h.length == 0) {
                        yield util.sleep(1000);
                        if(!h || h.length == 0) {
                            console.log(`       scrape_posts_cheerio get2(html) err ${idx}:${kwd} p:${p} ttl_scrlcnt:${ttl_scrlcnt} since:${since} until:${until_date}`);
                            retry = true;
                            continue;
                        }
                    }

                    let ctime;
                    let utime;
                    
                    //if ( h.match(/Grid-cell u-size2of3 u-lg-size3of4/i) ) {
                        yield util.sleep(300);
                    if ( h.match(/ r-13qz1uu r-184en5c/i) ) {
                        console.log("hgere");
                        ctime = $('time');

                        if(ctime.length >= 1){
                            posts = posts + ctime.length;
                            //utime = $(ctime).eq(ctime.length -1).find('span').attr('data-time');
                            utime = $(ctime).eq(ctime.length -1).attr('datetime');
                            if(!utime) {
                                console.log(`       scrape_posts_cheerio data-time err ${idx}:${kwd} p:${p} ttl_scrlcnt:${ttl_scrlcnt} ctime:${ctime.length} since:${since} until:${until_date} posts:${posts}`);
                                retry = true;
                                continue;
                            }                            
                            //until_date = moment.unix(utime).tz("Asia/Tokyo").format("YYYY-MM-DD_HH:mm:ss");   
                            until_date = moment(utime).tz("Asia/Tokyo").format("YYYY-MM-DD_HH:mm:ss");                          
                            //console.log(`       ${idx}:${kwd} p:${p} posts:${posts}`); 

                            if(p > 0){
                                scrlcnt = scrlcnt + 1;
                                ttl_scrlcnt = ttl_scrlcnt + 1;
                                
                                if((ttl_scrlcnt)%500 == 0){
                                    console.log(`       ttlcnt:${ttl_scrlcnt}  ${idx}:${kwd}  p:${p}  posts:${posts}  until_date:${until_date}  bf:${bf[0]}_${bf[1]}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                                }
                            }
                            
                            

                            p = p + 1;
                            if(p > maxpage){

                                let af = until_date.split('_');

                                let bfdate = moment(`${bf[0]} ${bf[1]}`);
                                let afdate = moment(`${af[0]} ${af[1]}`);

                                dfscnds = bfdate.diff(afdate, 'seconds');

                                if ( dfscnds < 180 && getProc < 2 ) {
                                    if(bf[0] == lastdate){
                                        let psdiff = posts - bfposts;
                                        Lgstr = `${af[0]}_${af[1]} - ${bf[0]}_${bf[1]} :${psdiff}`;
                                        result = 'Lgposts';
                                        break;
                                    }
                                }
                                
                                p = 0;
                                bf = until_date.split('_');
                                bfposts = posts;
                                
                            }

                        }else{ // ctime.length == 0 
                            result = 'end';
                            break;
                        }

                    } else {
                        //console.log('    scrape_posts_cheerio  get error');
                        retry = true;
                        continue;
                    }
                }
            } while (true)

            resolve(result);
        }).catch((e) => {
           console.log('    catch scrape_posts_cheerio');
           console.log(e);
           resolve('catch');
        });
    });
}

// scrape_twitter
const scrape_twitter_posts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
                    
            let kIDX = 0;

            let next = false;
            let result = false; //db insert

            let ccnt = 0; //continuous count Restart browser
            let scnt = 0; //stop count Emergency stop
            let pcnt = 0; //stop count Emergency stop2
            let rt = true;

            while(kIDX < keywords.length) {
                next = false;
                result = false;
                posts = 0;
                Lgflg = false;
                keyword = util.encodeUrl(keywords[kIDX].keyword);
                scrlcnt = 0;

                if(getProc < 3){ //cheerio
                    do {                            
                        let gprt = yield scrape_posts_cheerio(kIDX, keywords[kIDX].keyword);
                        
                        if(gprt == 'err') {
                            next = true;
                            result = false;
                            ccnt = ccnt + 1;
                        }else if(gprt == 'end'){
                            result = true;
                            next = true;
                            ccnt = 0;
                        }else if(gprt == 'Lgposts'){
                            Lgflg = true;
                            until_date = Lgstr;
                            result = true;
                            next = true;
                            ccnt = 0;
                        }else{
                            result = false;
                            rt = false;
                            break;
                        }

                    }while (!next)

                } else { //chromium

                    let stime = moment().format('YYYY/MM/DD HH:mm:ss');
                    d_ngflg = -1;
                    for (let i = 6; i >= 0; i--) {

                        next = false;
                        result = false;
                        let st = moment(since).add(i, 'days').format('YYYY-MM-DD');
                        until_date = moment(since).add(i+1, 'days').format('YYYY-MM-DD_00:00:00');
                        d_posts = 0;
                     
                        //console.log(kIDX, keywords[kIDX].keyword, st, until_date);
                        let gprt2 = yield scrape_posts_selenium(kIDX, keywords[kIDX].keyword, st, until_date);
                       
                        if(gprt2 == 'err') {
                            next = true;
                            result = false;
                            ccnt = ccnt + 1;
                        }else if(gprt2 == 'end'){
                            result = true;
                            next = true;
                            ccnt = 0;
                        }else{
                            result = false;
                            rt = false;
                            break;
                        }

                        if(!result) break;
                        //if( i == 0 ) 
                        //console.log(`    ${kIDX}: ${keywords[kIDX].keyword}  ng:${keywords[kIDX].ngflg}  d_posts:${d_posts}  avrposts:${keywords[kIDX].avrposts} st:${st} until_date:${until_date}  scrlcnt:${scrlcnt} ttl_scrlcnt:${ttl_scrlcnt}   ${stime} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                        let rprt = yield reg_daily_posts(kIDX, st);
                        if(!rprt){
                            result = false;
                            rt = false;
                            break;
                        }
                        posts = posts + d_posts;                        
                    }
                }

                if(rt == false) break;

                //log
                if((kIDX)%500 == 0){
                    console.log(`  ${kIDX} : ${keywords[kIDX].keyword} ng:${keywords[kIDX].ngflg} posts:${posts} avrposts:${keywords[kIDX].avrposts} ttl_scrlcnt:${ttl_scrlcnt} ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                }

                if(result){
                    const sql = `INSERT OR REPLACE INTO ${tname} ` +
                        `(keyword, startdate, posts, average, ngflg, lstuntil, regtime) ` +
                        `VALUES (?, ?, ?, ?, ?, ?, ?);`;

                    let ngflg = 0;
                    if(Lgflg){
                        ngflg = 9;
                        console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);

                    }else{
                        if(keywords[kIDX].avrposts >= 70 || posts >= 70){

                            const jdgtime = moment(startdate_hiphen).add(1,'d').format('YYYY-MM-DD_00:00:00');
                            const jdgtime2 = moment(startdate_hiphen).format('YYYY-MM-DD_01:00:00');
                            const jdgtime3 = moment(startdate_hiphen).add(1,'d').format('YYYY-MM-DD_21:00:00');
                            //let chk_date = until_date.split('_')[0];
                            let criteria = Math.floor(keywords[kIDX].avrposts * 0.2);

                            if( jdgtime < until_date ) {
                                ngflg = 2;
                                console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);

                            }else if( posts > 2000 && jdgtime2 < until_date ) {
                                ngflg = 2;
                                console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);

                            }else if(posts < criteria) {
                                ngflg = 1;
                                console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} < ${criteria} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);
                            }

                            if(getProc == 2){
                                if( jdgtime3 < until_date ) {
                                    ngflg = 3;
                                    console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);
    
                                }else if( posts > 500 && jdgtime2 < until_date ) {
                                    ngflg = 3;
                                    console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);
    
                                }

                            }

                        }
                    }

                    let regtime = moment().format('YYYY/MM/DD HH:mm:ss');
                    if(posts == 0) until_date = '';
                    if(d_ngflg >= 0) ngflg = d_ngflg;

                    let opt = [keywords[kIDX].keyword, startdate_slash, posts, keywords[kIDX].avrposts, ngflg, until_date, regtime];
                    yield db.do(`run`, sql, opt);

                    //scnt
                    if(getProc == 3){
                        if( keywords[kIDX].avrposts > 100 && posts == 0){
                            scnt = scnt + 1;
                            if(scnt >= 3) stopflg = true;
                        }else{
                            scnt = 0;
                        }
                    }

                }

                if(ccnt >= 3){
                    rt = false;
                    lcomp = 20;
                    if( driver ){
                        driver.quit();
                        driver = null;
                    }
                    break;
                }

                if( stopflg ){
                    rt = true;
                    lcomp = 100;
                    if( driver ){
                        driver.quit();
                        driver = null;
                    }
                    break;
                }

                kIDX = kIDX + 1;
            }

            resolve(rt);
        }).catch((e) => {
        //    console.log(e);
            console.log('    catch scrape_twitter_posts');
            resolve(false);
        });
    });
}

// scrape_twitter
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            let lflg = true; // loop continue?
            
            if (mode == 'new') {
                yield init_table(tname,`FGS258_targets_${category}`, avrtable);
                let st = yield store_targets();
                if(!st){
                    console.log(`   store targets err`);
                    lflg = false;
                }
            }else{
                yield get_loop_count();
            }

            yield get_targets();
            if ( keywords.length == 0 ) {
                lflg = false;
                result = true;
            }

            while(lflg){

                if (lcnt > maxloop) {
                    console.log(`   end  maxloop`);
                    result = true;
                    break;
                }

                lcomp = 10;
                getkwds = 0;
                yield insertLog();
                let rt = yield scrape_twitter_posts();
                if(rt && !stopflg) {
                    lcomp = 1;
                }

                let bflen = keywords.length;
                yield get_targets();

                if ( minnum >= keywords.length ) {
                    console.log(`   end  min keywords`);
                    lflg = false;
                    result = true;
                }

                getkwds = bflen - keywords.length;
                if (getkwds == 0){
                    zcnt++;
                    console.log(` get keywords: zero count: ${zcnt}`);
                    if(zcnt > 3) {
                        console.log(`   end scraping  file output`);
                        lflg = false;
                        result = true;
                    }

                }else{
                    zcnt = 0;
                }

                yield updateLog();
                lcnt++;

                if(!rt) {
                    console.log(`   exit scraping`);
                    lflg = false;
                    result = false;
                }

                if(stopflg) {
                    console.log(`   !! stop scraping 2  file output`);
                    lflg = false;
                    result = true;
                }

            }

            if( driver ){
                driver.quit();
                driver = null;
            }

            if(result) yield setOutFile();

            resolve();
        }).catch((e) => {
            console.log('    catch scrape_main');
            //console.log(e);
            resolve('');
        //    return null;
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(!stopflg){
                let sql = `select keyword, max(posts) posts
                from ${tname} where startdate = '${startdate_slash}' and postdate is NULL and ngflg = 0
                group by 1
                union all
                select keyword, max(posts) posts
                from (
                select keyword, posts, count(*) cnt
                from ${tname} t
                where startdate = '${startdate_slash}' and (ngflg = 1 or ngflg = 2) and postdate is NULL
                and not exists (select * from ${tname} x where x.ngflg = 0 and x.postdate is NULL and x.keyword = t.keyword)
                group by 1, 2
                having cnt > 1
                ) a
                group by 1
                order by 1;`;
                let res = yield db.do(`all`, sql, {});

                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(outfile, ([res[i].keyword, '', startdate_slash, res[i].posts].join('\t') + '\n'), 'utf8');
                }

                yield outNGfile();
                yield outLog();

            }else{
                fs.writeFileSync(outfile, "" );
                let stat = fs.statSync(outfile);
                console.log(` stopflg! output filesize:${stat.size}`);
            }
            
            yield end();

            resolve();
        //}).catch((e) => {
        //    console.log(e);
        });
    });
}

// run action
const run = function () {

    co(function* () {

        yield db.connect(filePath_db);

        switch (mode) {

            case 'new':
            case 'continue':
                yield scrape_main();
                break;
            
            case 'output':
                yield setOutFile();
                break;

            default:
                break;
        }
        console.log(`/// FGS258-twitter scraper.js end /// ${sttime} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();