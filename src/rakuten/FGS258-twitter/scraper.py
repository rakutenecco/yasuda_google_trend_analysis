# -*- encoding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import datetime
from dateutil import relativedelta

regdate = str(datetime.date.today())
up_date = str(datetime.date.today() + relativedelta.relativedelta(days=1))
word = 'チョコレート'

print(regdate)
print(up_date)
driver = webdriver.Chrome("./chromedriver")
lst = []

def ittan():
    from bs4 import BeautifulSoup as bs4
    soup = bs4(driver.page_source, 'html.parser')
    DD = soup.find_all("div")[1].find_all("a")
    for x in DD:
        if x.text.startswith('#'):
            print(x.text)
            lst.append(x.text)

#optionsインスタンスを生成
options = Options()
#オプションを設定
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-gpu')
 
#ドライバインスタンスを生成(Chromeの立ち上げ)
driver = webdriver.Chrome(options=options)
driver.get('https://twitter.com/search?q=%23{0}%20since%3A{1}_00:00:00_JST%20until%3A{2}_00:00:00_JST'.format(word, regdate, up_date))

cnt = 0
while True:
    cnt = cnt + 1000
    time.sleep(5)
    driver.execute_script("window.scrollTo(0, {0});".format(cnt))
    ittan()
    if cnt > 100000:
        break
    
list(set(lst))