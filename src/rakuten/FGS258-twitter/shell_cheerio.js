'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');
const moment = require('moment');
const fs = require('fs');
const conf = new (require('../../conf.js'));
const util = new (require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: String },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },// target file name
    { name: 'startdate', alias: 's', type: String },// yyyymmdd Sunday only
    { name: 'getProc', alias: 'p', type: Number },// 1:cheerio1 2:cheerio2 3:chromium
]);

let runtype;
if(!cli['run'] || cli['run'] == 1){
    //mode = 'new';
    runtype = 1;
} else if (cli['run'] == 2){
    //mode = 'continue';
    runtype = 2;
} else if (cli['run'] == 9){
    //mode = 'output';
    runtype = 9;
}
let startdate = moment().day(0).add(-7, "days").format('YYYYMMDD');//the last Sunday 7 days or more ago
if( cli['startdate'] ){
    let d = moment(cli['startdate']).day();
    if(d > 0) {
        console.log(` -s startdate[${cli['startdate']}] not Sunday`);
        return;
    }else if (startdate < cli['startdate']){
        console.log(` -s startdate[${cli['startdate']}] Specify Sunday more than 7 days ago`);
        return;
    }else{
        startdate = cli['startdate'];
    }
}

const maxretry = 10;
const cron = cli['cron'] ? cli['cron'] : 'cron11';
const category = cli['fname'].replace('targets_','').replace('.txt','');// fashion_02_01
let outfile;
// ex) fname:targets_all.txt -> FGS258_twitter_allposts_week_all_20190609.txt
// ex) fname:targets_all_03_01.txt -> FGS258_twitter_allposts_week_all_20190609_03_01.txt
// ex) fname:targets_fashion.txt -> FGS258_twitter_allposts_week_fashion_20190609.txt
// ex) fname:targets_new_20190614.txt -> FGS258_twitter_allposts_week_new20190614_20190609.txt
let tfnm = category.split(/_/g);
if(tfnm.length == 1) {
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}.txt`;

} else if ( tfnm.length == 3 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}_${tfnm[1]}_${tfnm[2]}.txt`;

} else if ( tfnm.length == 2 && tfnm[0] == 'new' && isFinite(tfnm[1]) ) {
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}${tfnm[1]}_${startdate}.txt`;

} else {    
    console.log(` -f fname[${cli['fname']}] error`);
    return;
}

// get process
let getProc = cli['getProc'] || 1;
if(getProc > 3){
    console.log(` cli['getProc']: ${cli['getProc']} error.`);
    return;
}


console.log('/// FGS258 titter shell.js start ///');
console.log(`  runtype: ${runtype}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  startdate: ${startdate}`);
console.log(`  outfile: ${outfile}`);
console.log(`  getProc: ${getProc}`);


// scrape insta posts
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let lflg = true; // loop continue?
            let lcnt = 0;

            if(getProc == 3 && fs.existsSync(outfile)){
                let tmp = moment().format('YYYYMMDD_HHmmss');
                fs.renameSync(outfile, `${outfile}_${tmp}bak`);
            }

            do{
                if (fs.existsSync(outfile)) {
                    console.log(` outfile exists: ${outfile}`);
                    lflg = false;

                }else{
                    lcnt++;
                    if( lcnt > maxretry ){
                        console.log(` maxretry over`);
                        lflg = false;

                    }else{
                        if( lcnt > 1 ) runtype = 2;
                        console.log(` FGS258-twitter/shell.js scraper.js CALL lcnt: ${lcnt} runtype: ${runtype} startdate: ${startdate} ${cron} ${cli['fname']}`);
                        shell.exec(`node src/rakuten/FGS258-twitter/scraper_cheerio.js -r ${runtype} -n ${cron} -f ${cli['fname']} -s ${startdate} -p ${getProc}` );
                        yield util.sleep(30000);
                    }
                }

            }while(lflg)

            resolve();
        }).catch((e) => {
            console.log('  scraper.js CALL err ');
            //console.log(e);
            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {        
                 
        let st = moment().format('YYYY/MM/DD HH:mm:ss');

        switch (runtype) {

            case 1://'new':
            case 2://'continue':
                yield scrape_main();
                break;
            case 9://'output':
                shell.exec(`node src/rakuten/FGS258-twitter/scraper_cheerio.js -r ${runtype} -n ${cron} -f ${cli['fname']} -s ${startdate}` );
                break;

            default:
                break;
        }

        console.log(`/// FGS258-twitter shell.js ${cron} end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        
    });
}

run();