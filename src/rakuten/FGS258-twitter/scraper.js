'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment-timezone');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');
const selenium = new (require('../../modules/selenium.js'));


// new modules
const conf = new (require('../../conf.js'));
const db = new (require('../../modules/db.js'));
const util = new (require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },// target file name
    { name: 'startdate', alias: 's', type: String },// yyyymmdd Sunday only
//    { name: 'getProc', alias: 'p', type: Number },// 1:cheerio1 2:cheerio2 3:chromium
]);
let mode;
if(cli['run'] == 1){
    mode = 'new';
} else if (cli['run'] == 2){
    mode = 'continue';
} else if (cli['run'] == 9){
    mode = 'output';
}
const cron = cli['cron'] ;
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const infile = `${conf.dirPath_share_vb}/targets/${cli['fname']}`;


const category = cli['fname'].replace('targets_','').replace('.txt','');
const tname = `FGS258_twitter_posts_${category}`;
const logtable = 'FGS258_log';


let startdate; //output filename
let startdate_slash; //result data, log data
if(cli['startdate']){
    startdate = cli['startdate'];
    startdate_slash = cli['startdate'].substr(0, 4) + '/' + cli['startdate'].substr(4, 2) + '/' + cli['startdate'].substr(6, 2);
}else{
    startdate = moment().day(0).add(-7, "days").format('YYYYMMDD');
    startdate_slash = moment(startdate).format('YYYY/MM/DD');
}
const startdate_hiphen = startdate_slash.replace(/\//g, '-'); //ng check

let outfile;
let outlogfile;

let postList = [];
// ex) fname:targets_all.txt -> FGS258_twitter_allposts_week_all_20190609.txt
// ex) fname:targets_all_03_01.txt -> FGS258_twitter_allposts_week_all_20190609_03_01.txt
// ex) fname:targets_fashion.txt -> FGS258_twitter_allposts_week_fashion_20190609.txt
// ex) fname:targets_new_20190614.txt -> FGS258_twitter_allposts_week_new20190614_20190609.txt
let tfnm = category.split(/_/g);
if(tfnm.length == 1) {
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}.log`;

} else if ( tfnm.length == 3 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}_${tfnm[1]}_${tfnm[2]}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}_${tfnm[1]}_${tfnm[2]}.log`;

} else if ( tfnm.length == 2 && tfnm[0] == 'new' && isFinite(tfnm[1]) ) {
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}${tfnm[1]}_${startdate}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}${tfnm[1]}.log`;

} else if ( tfnm.length == 4 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS258_twitter_allposts_week_${tfnm[0]}_${startdate}_${tfnm[1]}_${tfnm[2]}_${tfnm[3]}.txt`;
    outlogfile = `${conf.dirPath_share_vb}/log/FGS258_twitter_allposts_week_${tfnm[0]}_${tfnm[1]}_${tfnm[2]}_${tfnm[3]}.log`;

} else {
    console.log(` cli['fname']: ${cli['fname']} check the file name.`);
    return;
}

//average file
const avrfile = `${conf.dirPath_share_vb}/targets/FGS258_twitter_targets_allposts_week_average.txt`;
const avrtable = `FGS258_twitter_targets_allposts_week_average`;

//NG file
const ngfile = `${conf.dirPath_share_vb}/targets/FGS258_twitter_targets_${category}_ngchk.txt`;

// get process
let getProc = 1;

const sttime = moment().format('YYYY/MM/DD HH:mm:ss');
console.log(`/// FGS258-twitter-posts scraper.js start  ${sttime} ///`);
console.log(`  mode: ${mode}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  args  category: ${category}`);
console.log(`  startdate: ${startdate_slash}`);
console.log(`  outfile: ${outfile}`);


const maxloop = 100;
const minnum = 10;

// variable
let keywords = [];

const since = startdate_slash.replace(/\//g, '-'); //URL:start(defalt)

const untilstr = moment(startdate_slash.replace(/\//g, '-')).add(7, 'days').format('YYYY-MM-DD_00:00:00');
const lastdate = moment(startdate_slash.replace(/\//g, '-')).add(6, 'days').format('YYYY-MM-DD');
let until_date; //URL:end

let tgtnum; // targets table word number

let getkwds = 0;
let lcnt = 1;
let zcnt = 0;
let starttime;

let posts = 0;
let d_posts = 0;
let d_ngflg = -1;

let keyword;
let driver;

let Lgflg = false;


let scrlcnt = 0;

let stopflg = false;

let lcomp = 10; // 10:undefined error 20:3keyword continuous error 30:time get error 100:access restriction error 1:complete

let ttl_scrlcnt = 0;

// Twitter API Rate Limits 170 calls/15 min
const MAX_REQUEST_RATE = 140;
let requestedNumber = 0;

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// insert Log
const insertLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            starttime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `INSERT OR REPLACE INTO ${logtable} ` +
                `(tbname, startdate, loop_count, remainkwd, starttime, getProc) ` +
                `VALUES (?, ?, ?, ?, ?, ?);`;
            let opt = [tname, startdate_slash, lcnt, keywords.length, starttime, getProc];
            console.log(` insert log : tname:${tname} startdate:${startdate_slash} lcnt:${lcnt} kwds:${keywords.length} cron:${cron}  ${starttime}`);
            yield db.do(`run`, sql, opt);
            resolve();
        });
    });
}

// update Log
const updateLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            const curtime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `UPDATE ${logtable} SET getkwd = ${getkwds}, endtime = '${curtime}', complete = ${lcomp}, scrlcnt = ${ttl_scrlcnt} ` +
            `WHERE tbname = '${tname}' and startdate = '${startdate_slash}' and loop_count = ${lcnt} and starttime = '${starttime}';`;
            //console.log(sql);
            console.log(` update log : tname:${tname} startdate:${startdate_slash} lcnt:${lcnt} getkwds:${getkwds} lcomp:${lcomp} scrlcnt:${ttl_scrlcnt} cron:${cron}  ${starttime}-${curtime}`);
            yield db.do(`run`, sql, {});
            resolve();
        });
    });
}

// output Log
const outLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select tbname, startdate, loop_count, remainkwd, getkwd, starttime, endtime, complete, scrlcnt, getProc from ${logtable} ` +
                      `where tbname = '${tname}' and startdate = '${startdate_slash}' order by starttime;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            console.log(` output log: ${outlogfile}`);

            // append
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outlogfile, ([res[i].tbname, res[i].startdate, res[i].loop_count, res[i].remainkwd, res[i].getkwd, res[i].starttime, res[i].endtime, res[i].complete, res[i].scrlcnt, res[i].getProc].join('\t') + '\n'), 'utf8');
            }

            resolve();

        });
    });
}

// output NG file
const outNGfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select a.keyword keyword, '${startdate_slash}' startdate, a.postdate postdate, a.posts posts, a.average average, a.ngflg ngflg, a.lstuntil lstuntil, a.regtime regtime
            from ${tname} a
            where not exists (select * from ${tname} c where c.startdate = '${startdate_slash}' and c.ngflg = 0 and c.keyword = a.keyword ) 
            and a.startdate = '${startdate_slash}' order by 1,8;`;

            //console.log(`outNG sql:${sql}`);
            let res = yield db.do(`all`, sql, {});            

            if(res.length > 0){
                console.log(` output NG file: ${ngfile}`);

                // append
                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(ngfile, ([res[i].keyword, res[i].startdate, res[i].postdate, res[i].posts, res[i].average, res[i].ngflg, res[i].lstuntil, res[i].regtime].join('\t') + '\n'), 'utf8');
                }

            }else{
                console.log(` not exists NG words`);

            }

            resolve();

        });
    });
}

// init action
const init_table = function (result_table, targets_table, avr_table) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // targets table drop
            sqlstr = `DROP TABLE IF EXISTS ${targets_table};`;
            yield db.do(`run`, sqlstr, {});

            // targets table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${targets_table} ` +
                `(keyword TEXT PRIMARY KEY);`;
            yield db.do(`run`, sqlstr, {});

            
            // average table drop
            sqlstr = `DROP TABLE IF EXISTS ${avr_table};`;
            yield db.do(`run`, sqlstr, {});

            // average table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${avr_table} ` +
                `(keyword TEXT PRIMARY KEY, avrposts bigint);`;
            yield db.do(`run`, sqlstr, {});


            // result table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${result_table} ` +
                `(keyword TEXT, startdate TEXT, postdate TEXT, posts bigint, average bigint, ngflg integer, lstuntil TEXT, regtime TEXT);`;
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i01_${result_table} on ${result_table} (keyword, startdate, ngflg, postdate);`;
            yield db.do(`run`, sqlstr, {});


            // old result delete
            const deldate = moment(startdate_slash.replace(/\//g, '-')).subtract(35, 'days').format('YYYY/MM/DD');
            console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM ${result_table} WHERE startdate < '${deldate}';`;// or startdate = '${startdate_slash}';`;
            yield db.do(`run`, sqlstr, {});

            // log table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${logtable} ` +
                `(tbname TEXT, startdate TEXT, loop_count integer, remainkwd bigint, getkwd bigint, starttime TEXT, endtime TEXT, complete integer, scrlcnt bigint, getProc integer );`;
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${logtable} on ${logtable} (tbname, startdate);`;
            yield db.do(`run`, sqlstr, {});

            // old log delete
            const deldate2  = moment(startdate_slash.replace(/\//g, '-')).subtract(35, 'days').format('YYYY/MM/DD');
            console.log(` log deldate: ${deldate2}`);
            sqlstr = `DELETE FROM ${logtable} WHERE tbname = '${result_table}' and startdate < '${deldate2}';`;
            yield db.do(`run`, sqlstr, {});

            resolve();
        });
    });
}

// store targets
const store_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;

            if (fs.existsSync(infile)) {
                console.log(` infile: ${infile} read start`);
                let arr = fs.readFileSync(infile).toString().split("\n");
                //console.log(kywds.length);
                if(arr[arr.length -1].length == 0) arr.pop();
                console.log(` targets file: ${arr.length}`);
                
                //Twitter hashtag check!! utf-8
                let kywds = [];
                let exkywds = [];
                for(let i = 0; i < arr.length; i++) {
                    
                    let str = arr[i];
                    let testResult = str.match(/[^ー・々〆ﾟ･αβφゞ﨑0-9_A-Za-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                    let numtest =  str.match(/[^0-9]/g);
                    if(testResult || !numtest){
                        exkywds.push(str);
                    }else{
                        kywds.push(str);
                    }
                }
                let twitter_except_file = `${conf.dirPath_share_vb}/targets/targets_except/targets_twitter_except_${category}_${startdate}.txt`;

                if(exkywds.length > 0){
                    for (let i = 0; i < exkywds.length; i++) {
                        fs.appendFileSync(twitter_except_file, exkywds[i] + '\n', 'utf8');
                    }
                }
                console.log(` except  keyword: ${exkywds.length}`);
                console.log(` targets keyword: ${kywds.length}`);

                let sql = `insert into FGS258_targets_${category} values `;
                if (kywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(kywds.length / n);
                    if (times * n == kywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = kywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        let exc = kywds.slice(st, end);

                        yield db.do(`run`, sql + '("' + exc.join('"),("') + '");', {});
                        i++;
                    } while (times >= i);
                }
                result = true;

            }else{
                console.log('  not exists in file');
            
            }

            //average file
            if (fs.existsSync(avrfile) && result) {
                result = false;

                console.log(` average file: ${avrfile} read start`);
                let tgtsavrfile = fs.readFileSync(avrfile).toString().split("\n");

                if(tgtsavrfile[tgtsavrfile.length -1].length == 0) tgtsavrfile.pop();

                let avkywds = tgtsavrfile;
                console.log(` targets average keyword: ${avkywds.length}`);

                let sql = `insert into ${avrtable} values `;
                if (avkywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(avkywds.length / n);

                    if (times * n == avkywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = avkywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        let exc = avkywds.slice(st, end);
                        let arr = [];
                        for ( let iarr = 0, len = exc.length; iarr < len; ++iarr) {
                            arr.push(exc[iarr].replace('///','",'));
                        }
                        yield db.do(`run`, sql + '("' + arr.join('),("') + ');', {});
                        i++;
                    } while (times >= i);
                }                
                result = true;

            }else{
                console.log('  not exists averagefile');
                result = false;

            }
            resolve(result);
        });
    });
}

// get targets list
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS258_targets_${category};`;
            let num = yield db.do(`all`, sql1, {});

            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword, ifnull(m.avrposts, 0) avrposts, 0 ngflg
             from FGS258_targets_${category} a 
             left join ${avrtable} m on a.keyword = m.keyword
            where not exists (select * from ${tname} b where b.startdate = '${startdate_slash}' 
                and a.keyword = b.keyword )
            order by 1;`;

            keywords = yield db.do(`all`, sql, {});       
            console.log('  get_targets  remain keywords: ' + keywords.length);

            if ( keywords.length == 0 ) {
                console.log(`    end once scraping`);
                yield get_targets2();
                if ( keywords.length == 0 ) {
                    console.log(`    end cheerio scraping`);
                    yield get_targets3();
                    if ( keywords.length == 0 ) {
                        console.log(`    end chromium scraping`);
                    }else{
                        getProc = 3;
                        console.log(`    getProc : 3`);
                    }
                }else{
                    getProc = 2;
                    console.log(`    getProc : 2`);
                }
            }else{
                //selenium default
                getProc = 3;
                console.log(`    getProc : 3`);
            }

            resolve();

        });
    });
}

// get targets 2
const get_targets2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql = `select a.keyword keyword, a.average avrposts, a.ngflg ngflg
             from ( select keyword, max(average) average, max(ngflg) ngflg from ${tname}
             where startdate = '${startdate_slash}' and ngflg != 0 and ngflg != 9 and postdate is NULL 
             group by 1 ) a
            where not exists (select * from ${tname} c where c.startdate = '${startdate_slash}' 
                and (c.ngflg = 0 or c.ngflg = 9) and postdate is NULL and a.keyword = c.keyword )
              and not exists (select * from (
                  select keyword,count(*) from ${tname} 
                   where startdate = '${startdate_slash}' and ngflg != 0 and ngflg != 9 and postdate is NULL 
                   group by 1 having count(*) > 1
                ) tt where a.keyword = tt.keyword )
            order by 3,2,1;`;

            keywords = yield db.do(`all`, sql, {});
            console.log('  get_targets2 remain keywords: ' + keywords.length);

            resolve();
        });
    });
}

// get targets 3
const get_targets3 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select a.keyword keyword, a.average avrposts, a.ngflg ngflg
            from ( select keyword, max(average) average, max(ngflg) ngflg from ${tname}
            where startdate = '${startdate_slash}' and ngflg != 0 and ngflg != 9 and postdate is NULL 
            group by 1 ) a
            where not exists (select * from ${tname} c where c.startdate = '${startdate_slash}' 
                and (c.ngflg = 0 or c.ngflg = 9) and postdate is NULL and a.keyword = c.keyword )
              and not exists (select * from (
                  select keyword,posts,count(*) from ${tname} 
                   where startdate = '${startdate_slash}' and (ngflg = 1 or ngflg = 2) and postdate is NULL 
                   group by 1,2 having count(*) > 1
                ) tt where a.keyword = tt.keyword )
            order by 3,2,1;`;
            keywords = yield db.do(`all`, sql, {});
            console.log('  get_targets3 remain keywords: ' + keywords.length);

            if ( keywords.length > 0 ) {
                
                //loop count            
                sql = `select count(distinct loop_count) lct from ${logtable} 
                where startdate = '${startdate_slash}' and tbname = '${tname}' and getProc = 3;`;

                let num = yield db.do(`all`, sql, {});

                if ( num[0].lct >= 13 ) {
                    console.log('    loop_count >= 13 end scraping');
                    keywords.length = 0
                }
            }

            resolve();
        });
    });
}

// get loop count
const get_loop_count = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //loop count
            
            const sql = `select max(loop_count) lct from ${logtable} where startdate = '${startdate_slash}' and tbname = '${tname}' and complete = 1;`;

            let num = yield db.do(`all`, sql, {});

            if ( num[0].lct > 0 ) {
                lcnt = num[0].lct + 1;
            }
            console.log(' loop_count: ' + lcnt);

            resolve();
        });
    });
}

// getElements
const getElements = function (driver_ele, selector) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!driver_ele || !selector) {
                console.log('    getElements arg null err');
                resolve('');
            }
            driver_ele.findElements(byElement(selector)).then(function (ele) {
                resolve(ele);
            }, function (err) {

                resolve('');
            });
        }).catch((e) => {
            console.log('    catch getElements');
            resolve('');
        });
    });
}

// getAttrbute
const getAttrbute = function (driver_ele, attr) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!driver_ele || !attr) {
                //console.log('    getElements arg null err');
                resolve('');
            }
            yield util.sleep(50);
            driver_ele.getAttribute(attr).then(function (ele) {
                //console.log(`    ele:${ele}`); 
                resolve(ele);
            }, function (err) {
                console.log('    getAttribute err');
                resolve('');
            });
        }).catch((e) => {
            console.log('    catch getAttrbute');
            resolve('');

        });
    });
}


// Selenium 関数生成
let byElement = function (selector) {

    let tag = '';
    switch (selector.substr(0, 1)) {
        case '#':
            return By.id(selector.substr(1));
        case '.':
            return By.className(selector.substr(1));
        case '/':
            return By.xpath(selector);
        default:
            return By.css(selector);
    }
};

// get data from url
const getText = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
            }

            ele.getText().then(function (text) {
                resolve(text);
            }, function (err) {
                resolve('');
            });
        }).catch((e) => {
            console.log('    catch getText');
            resolve('');
        });
    });
}

// scroll
const getScroll = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
            }
            driver.executeScript("arguments[0].scrollIntoView();", ele).then(null, function(err) {
                //
            }, function (err) {

                resolve('');
            });
            yield util.sleep(500);//200
            resolve('');
        }).catch((e) => {
            console.log('    catch getScroll');
            resolve('');
        });
    });
}


// open getlist
const get_empty = function (since_date) {

    return new Promise((resolve, reject) => {
        co(function* () {
            let isOldLayout = false;
            let retryCount = 0;
            const MAX_LOOP_GET_NEW_LAYOUT = 3;
            let result = false;
            let url = `https://twitter.com/search?q=%23${keyword}%20since%3A${since_date}_00:00:00_JST%20until%3A${until_date}_JST&f=live`;
            //console.log(url);
            do {
                if(!driver) {
                
                    if(getProc == 3){
                        driver = yield selenium.init('chromium', { inVisible:true });
                    }else{
                        driver = yield selenium.init('', { inVisible:true });
                    }
                    yield util.sleep(200);
                }

                driver.get(url).then(null, function (err) {
                
                }, function (err) {
                    console.log('      driver get err');
                    resolve('err');
                });

                yield util.sleep(600);
                let oldLayoutElement = yield getElements(driver, 'div.u-size2of3.u-lg-size3of4');
                if (oldLayoutElement.length > 0 && retryCount < MAX_LOOP_GET_NEW_LAYOUT) {
                    console.log('Diver got the old layout. Retry new diver.', url);
                    isOldLayout = true;
                    retryCount++;
                    requestedNumber = 0;
                    driver.quit();
                    yield util.sleep(300);
                    driver = null;
                    continue;
                }

                isOldLayout = false;
                requestedNumber++;
                let ele_empty = yield getElements(driver, 'div.r-15d164r.r-bcqeeo.r-q4m81j.r-qvutc0');
                if (ele_empty.length > 0) {
                    let txt = yield getText(ele_empty[0]);
                    const noResultRegex = /結果なし|No results/g;
                    let rt = txt.search(noResultRegex);
                    if(rt > -1) {
                        result = true;
                    } else {
                        console.log('error in find No results.', txt);
                        result = 'err';
                    }
                } else {
                    //div containing posts
                    yield util.sleep(800);
                    let ele_grid = yield getElements(driver, 'div.r-13qz1uu.r-184en5c');
                    
                    if(ele_grid.length == 1) {
                        let ele_time = yield getElements(ele_grid[0], 'time');
                        if(ele_time.length == 0) {
                            result = true;
                        }
                    } else {
                        console.log('err2');
                        result = 'err';
                    }  
                }
            } while (isOldLayout);
            resolve(result);
        }).catch((e) => {
           console.log('    catch get_empty', e);
           resolve('err');
        });
    });
}

// page_scroll
const page_scroll = function (l_post,kwd, st) {

    return new Promise((resolve, reject) => {
        co(function* () {
            let result = '';
            const maxpage = 180; // max scroll
            let p = 1;
            let utime = postList[postList.length -1];
            let lastPost = l_post;
            //scroll element        
            do {
                let prev = String(yield getAttrbute(lastPost, 'datetime'));
                yield getScroll(lastPost);
                scrlcnt = scrlcnt + 1;
                ttl_scrlcnt = ttl_scrlcnt + 1;
                requestedNumber++;

                yield util.sleep(1000);
                let eleTime = yield getElements(driver, 'time');
                            
                if(!eleTime) {
                    console.log(`  scroll time get err  ttl_scrlcnt:${ttl_scrlcnt}  kwd:${kwd}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                    lcomp = 30;
                    result = 'timegeterr';
                    break;
                }
                
                //checking no new data loaded
                let now = String(yield getAttrbute(eleTime[eleTime.length-1], 'datetime'));
                if (p == maxpage || prev === now){    
                    until_date = moment(utime).tz("Asia/Tokyo").format("YYYY-MM-DD_HH:mm:ss");
                    d_posts =  new Set(postList).size;
                    postList = [];             
                    result = 'end';
                    break;
                }
                if(eleTime.length > 0){
                    yield getPostListDateAsArray(eleTime);
                    utime = postList[postList.length -1];
                    lastPost = eleTime[eleTime.length-1];

                    if(!utime) {
                        console.log(`    scroll data-time err kwd:${kwd} p:${p} eleAttDatetime:${eleTime.length - 1} this_num:${this_num} until_date:${until_date} posts:${posts}`);
                        break;
                    }                     
                }
                p = p + 1;
            } while (p <= maxpage)

            resolve(result);

        }).catch((e) => {
            console.log('    catch page_scroll');
            resolve('failure');
        });
    });
}


// get_empty_add
const get_empty_add = function (st) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = 0;
            let tmp = until_date.split('_');
            let tmptime = moment(`${tmp[0]} ${tmp[1]}`).add(1, 'seconds').format("YYYY-MM-DD_HH:mm:ss");

            until_date = tmptime;

            let ele_empty = yield get_empty(st);
            if (ele_empty == 'err') {
                console.log(`      get_empty err   get_empty_add  ${st}`);
                result = false;

            } else if (ele_empty) {
                console.log(`      get_empty err   get_empty_add  ${st}`);
                result = false;

            } else {
                let ele1 = yield getElements(driver, 'time');
                if(!ele1) {
                    console.log(`      getElements   get_empty_add  time get err`); 
                    result = false;
                } else {                               
                    result = ele1.length;
                }
            }           

            resolve(result);

        }).catch((e) => {
            console.log('    catch get_empty_add');
            resolve('');
        });
    });
}

// reg_daily_posts
const reg_daily_posts = function (idx, st) {

    return new Promise((resolve, reject) => {
        co(function* () {

            const sql = `INSERT OR REPLACE INTO ${tname} ` +
                        `(keyword, startdate, postdate, posts, average, ngflg, lstuntil, regtime) ` +
                        `VALUES (?, ?, ?, ?, ?, ?, ?, ?);`;

            let l_ngflg = 0;
            let jdgtime = `${st}_01:00:00`;
            let avrg = Math.floor(keywords[idx].avrposts * 0.143);
            let criteria = Math.floor(keywords[idx].avrposts * 0.143 * 0.2);
            if(avrg >= 285 || d_posts >= 285){

                if( jdgtime < until_date ) {
                    l_ngflg = 2;
                }else if(d_posts < criteria) {
                    l_ngflg = 1;
                }
            }

            if( d_ngflg < l_ngflg)  d_ngflg = l_ngflg;
            console.log(`      ${idx}: ${keywords[idx].keyword}  d_posts:${d_posts} criteria:${criteria} avr:${avrg} st:${st} lstuntil:${until_date} l_ngflg:${l_ngflg} create_date: ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            let regtime = moment().format('YYYY/MM/DD HH:mm:ss');
            let opt = [keywords[idx].keyword, startdate_slash, st, d_posts, avrg, l_ngflg, until_date, regtime];
            yield db.do(`run`, sql, opt);

            resolve(true);

        }).catch((e) => {
            console.log('    catch reg_daily_posts');
            resolve('');
        });
    });
}

//Post Date get as Array List
const getPostListDateAsArray = function(elem) {
    return new Promise((resolve, reject) => {
        co(function* () {
            if(elem.length > 0){
                for(let i = 0; i< elem.length;i++){
                    postList.push(String(yield getAttrbute(elem[i], 'datetime')));
                }
            }
            resolve(true);
        }). catch((e) => {
            console.log(' Catch could not get PostList Date');
            resolve('failure');
        });
    });
}

// scrape_posts_selenium chroium
const scrape_posts_selenium = function (idx, kwd, st, ed) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = '';
            const maxcnt = 2;
            let retry = false;  //fnnction error
            let rcnt = 0;       //retryカウンタ
            let l_post;
            if(requestedNumber > MAX_REQUEST_RATE) {
                console.log('Rate limit exceeded: 140 calls every 15 minutes. Called requests = ', requestedNumber);
                requestedNumber = 0;
                driver.quit();
                yield util.sleep(200);
                driver = null;
            }

            do {
                if(retry) rcnt = rcnt + 1;
                if(rcnt >= maxcnt){
                    result = 'err'
                    break;

                } else {
                    let ele_empty = yield get_empty(st);
                    if (ele_empty == 'err') {
                        console.log(`      ${idx}:${kwd}  scrape_posts_selenium get_empty err`);
                        retry = true;
                        continue;
                    
                    } else if ( until_date == ed && ele_empty ) {
                        result = 'end';
                        break;

                    } else {
                        let last_num;

                        if ( until_date != ed && ele_empty ){
                            yield util.sleep(300);
                            
                            let ele_empty_add = yield get_empty_add(st);
                            if (!ele_empty_add) {
                                console.log(`    ${kwd} scrape_posts_selenium ele_empty_add err`); 
                                result = 'err'
                                break;
                            }
                            last_num = ele_empty_add;
                            d_posts = d_posts - last_num;
                            
                        } else {
                            yield util.sleep(300);
                            let ele1 = yield getElements(driver, 'time');
                            if(!ele1) {
                                console.log(`    ${kwd} scrape_posts_selenium time get err`); 
                                retry = true;
                                continue;
                            }                                
                            last_num = ele1.length;
                            l_post = ele1[ele1.length-1];
                            //push Postdate Time
                            yield getPostListDateAsArray(ele1);
                            
                        }

                        if(last_num == 0) {
                            console.log(`    ${kwd} scrape_posts_selenium last_num == 0 err`); 
                            result = 'err';
                            break;
                        }

                        //scroll
                        let scrlrt = yield page_scroll(l_post,kwd, st);

                        if(scrlrt == 'timegeterr') {
                            console.log(`    ${kwd} scrape_posts_selenium scroll timeget err`); 
                            result = 'err';
                            break;
                        }

                        if(scrlrt == 'continue'){
                            retry = false;
                            rcnt = 0;
                        }else if(scrlrt == 'end'){
                            result = 'end';
                            break;
                        }else{
                            result = 'err';
                            break;
                        }

                        if(scrlcnt > 250 && driver) {
                            console.log(`        scrlcnt:${scrlcnt} ${idx}:${kwd} until_date:${until_date} posts:${posts}   ${moment().format('YYYY/MM/DD HH:mm:ss')}`); 
                            scrlcnt = 0;
                            driver.quit();
                            driver = null;
                            yield util.sleep(10000);
                        }
                    }
                }

            }while (true)

            resolve(result);

        }).catch((e) => {
            console.log('    catch getPsts2');
            resolve('failure');
        });
    });
}

// scrape_twitter
const scrape_twitter_posts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
                    
            let kIDX = 0;

            let next = false;
            let result = false; //db insert

            let ccnt = 0; //continuous count Restart browser
            let scnt = 0; //stop count Emergency stop
            let rt = true;

            while(kIDX < keywords.length) {
                next = false;
                result = false;
                posts = 0;
                Lgflg = false;
                keyword = util.encodeUrl(keywords[kIDX].keyword);
                scrlcnt = 0;

                if(getProc < 3){ 

                } else { //chromium

                    let stime = moment().format('YYYY/MM/DD HH:mm:ss');
                    d_ngflg = -1;
                    for (let i = 6; i >= 0; i--) {

                        next = false;
                        result = false;
                        let st = moment(since).add(i, 'days').format('YYYY-MM-DD');
                        until_date = moment(since).add(i+1, 'days').format('YYYY-MM-DD_00:00:00');
                        d_posts = 0;
                     
                        let gprt2 = yield scrape_posts_selenium(kIDX, keywords[kIDX].keyword, st, until_date);
                       
                        if(gprt2 == 'err') {
                            next = true;
                            result = false;
                            ccnt = ccnt + 1;
                        }else if(gprt2 == 'end'){
                            result = true;
                            next = true;
                            ccnt = 0;
                        }else{
                            result = false;
                            rt = false;
                            break;
                        }

                        if(!result) break;
                       
                        let rprt = yield reg_daily_posts(kIDX, st);
                        if(!rprt){
                            result = false;
                            rt = false;
                            break;
                        }
                        posts = posts + d_posts;                        
                    }
                }

                if(rt == false) break;

                //log
                if((kIDX)%500 == 0){
                    console.log(`  ${kIDX} : ${keywords[kIDX].keyword} ng:${keywords[kIDX].ngflg} posts:${posts} avrposts:${keywords[kIDX].avrposts} ttl_scrlcnt:${ttl_scrlcnt} ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                }

                if(result){
                    const sql = `INSERT OR REPLACE INTO ${tname} ` +
                        `(keyword, startdate, posts, average, ngflg, lstuntil, regtime) ` +
                        `VALUES (?, ?, ?, ?, ?, ?, ?);`;

                    let ngflg = 0;
                    if(Lgflg){
                        ngflg = 9;
                        console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);

                    }else{
                        if(keywords[kIDX].avrposts >= 70 || posts >= 70){

                            const jdgtime = moment(startdate_hiphen).add(1,'d').format('YYYY-MM-DD_00:00:00');
                            const jdgtime2 = moment(startdate_hiphen).format('YYYY-MM-DD_01:00:00');
                            const jdgtime3 = moment(startdate_hiphen).add(1,'d').format('YYYY-MM-DD_21:00:00');
                          
                            let criteria = Math.floor(keywords[kIDX].avrposts * 0.2);

                            if( jdgtime < until_date ) {
                                ngflg = 2;
                                console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);

                            }else if( posts > 2000 && jdgtime2 < until_date ) {
                                ngflg = 2;
                                console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);

                            }else if(posts < criteria) {
                                ngflg = 1;
                                console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} < ${criteria} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);
                            }

                            if(getProc == 2){
                                if( jdgtime3 < until_date ) {
                                    ngflg = 3;
                                    console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);
    
                                }else if( posts > 500 && jdgtime2 < until_date ) {
                                    ngflg = 3;
                                    console.log(`     ngflg:${ngflg} ${kIDX}: ${keywords[kIDX].keyword}  posts:${posts} avr:${keywords[kIDX].avrposts} lstuntil:${until_date}`);
    
                                }

                            }

                        }
                    }

                    let regtime = moment().format('YYYY/MM/DD HH:mm:ss');
                    if(posts == 0) until_date = '';
                    if(d_ngflg >= 0) ngflg = d_ngflg;

                    let opt = [keywords[kIDX].keyword, startdate_slash, posts, keywords[kIDX].avrposts, ngflg, until_date, regtime];
                    yield db.do(`run`, sql, opt);

                    //scnt
                    if(getProc == 3){
                        if( keywords[kIDX].avrposts > 100 && posts == 0){
                            scnt = scnt + 1;
                            if(scnt >= 5) stopflg = true;
                        }else{
                            scnt = 0;
                        }
                    }

                }

                if(ccnt >= 5){
                    rt = false;
                    lcomp = 20;
                    if( driver ){
                        driver.quit();
                        driver = null;
                    }
                    break;
                }

                if( stopflg ){
                    rt = true;
                    lcomp = 100;
                    if( driver ){
                        driver.quit();
                        driver = null;
                    }
                    break;
                }

                kIDX = kIDX + 1;
            }

            resolve(rt);
        }).catch((e) => {
            console.log('    catch scrape_twitter_posts');
            resolve(false);
        });
    });
}

// scrape_twitter
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            let lflg = true; // loop continue?
            
            if (mode == 'new') {
                yield init_table(tname,`FGS258_targets_${category}`, avrtable);
                let st = yield store_targets();
                if(!st){
                    console.log(`   store targets err`);
                    lflg = false;
                }
            }else{
                yield get_loop_count();
            }

            yield get_targets();
            if ( keywords.length == 0 ) {
                lflg = false;
                result = true;
            }

            while(lflg){

                if (lcnt > maxloop) {
                    console.log(`   end  maxloop`);
                    result = true;
                    break;
                }

                lcomp = 10;
                getkwds = 0;
                yield insertLog();
                let rt = yield scrape_twitter_posts();
                if(rt && !stopflg) {
                    lcomp = 1;
                }

                let bflen = keywords.length;
                yield get_targets();

                if ( minnum >= keywords.length ) {
                    console.log(`   end  min keywords`);
                    lflg = false;
                    result = true;
                }

                getkwds = bflen - keywords.length;
                if (getkwds == 0){
                    zcnt++;
                    console.log(` get keywords: zero count: ${zcnt}`);
                    if(zcnt > 3) {
                        console.log(`   end scraping  file output`);
                        lflg = false;
                        result = true;
                    }

                }else{
                    zcnt = 0;
                }

                yield updateLog();
                lcnt++;

                if(!rt) {
                    console.log(`   exit scraping`);
                    lflg = false;
                    result = false;
                }

                if(stopflg) {
                    console.log(`   !! stop scraping 2  file output`);
                    lflg = false;
                    result = true;
                }

            }

            if( driver ){
                driver.quit();
                driver = null;
            }

            if(result) yield setOutFile();

            resolve();
        }).catch((e) => {
            console.log('    catch scrape_main');
            resolve('');
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(!stopflg){
                let sql = `select keyword, max(posts) posts
                from ${tname} where startdate = '${startdate_slash}' and postdate is NULL and ngflg = 0
                group by 1
                union all
                select keyword, max(posts) posts
                from (
                select keyword, posts, count(*) cnt
                from ${tname} t
                where startdate = '${startdate_slash}' and (ngflg = 1 or ngflg = 2) and postdate is NULL
                and not exists (select * from ${tname} x where x.ngflg = 0 and x.postdate is NULL and x.keyword = t.keyword)
                group by 1, 2
                having cnt > 1
                ) a
                group by 1
                order by 1;`;
                let res = yield db.do(`all`, sql, {});

                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(outfile, ([res[i].keyword, '', startdate_slash, res[i].posts].join('\t') + '\n'), 'utf8');
                }

                yield outNGfile();
                yield outLog();

            }else{
                fs.writeFileSync(outfile, "" );
                let stat = fs.statSync(outfile);
                console.log(` stopflg! output filesize:${stat.size}`);
            }
            
            yield end();

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        yield db.connect(filePath_db);

        switch (mode) {

            case 'new':
            case 'continue':
                yield scrape_main();
                break;
            
            case 'output':
                yield setOutFile();
                break;

            default:
                break;
        }
        console.log(`/// FGS258-twitter scraper.js end /// ${sttime} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();