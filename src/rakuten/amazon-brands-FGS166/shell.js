'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 2: // db clean
                shell.exec('node src/rakuten/amazon-brands-FGS166/scraper.js -r 3'); // drop table
                break;
            case 1: // usual action
            default:
                shell.exec('node src/rakuten/amazon-brands-FGS166/scraper.js -r 2'); // create url list
                shell.exec('node src/rakuten/amazon-brands-FGS166/scraper.js -r 1'); // scrape
                yield util.sleep(300000);
                shell.exec('node src/rakuten/amazon-brands-FGS166/scraper.js -r 1'); // scrape
                yield util.sleep(300000);
                shell.exec('node src/rakuten/amazon-brands-FGS166/scraper.js -r 1'); // scrape
                shell.exec('node src/rakuten/amazon-brands-FGS166/scraper.js -r 4'); // out file
                break;
        }
    });
}

run();