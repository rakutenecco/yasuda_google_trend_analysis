'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron0.db`;

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS fashion_amazon_topbrands(FAT_id TEXT primary key, brand TEXT, brand_kana TEXT, product_num INTEGER, FATU_id TEXT, reg_date TEXT);`;
            yield db.do(`run`, sql, {});

            resolve();
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// drop table for concern tables
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql = `DROP TABLE IF EXISTS fashion_amazon_topbrands;`;
            yield db.do(`run`, sql, {});

            sql = `DROP TABLE IF EXISTS fashion_amazon_topbrands_urls;`;
            yield db.do(`run`, sql, {});
            yield db.close();

            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sql = `CREATE TABLE IF NOT EXISTS fashion_amazon_topbrands_urls(FATU_id TEXT primary key, url TEXT, site TEXT, page TEXT, genre TEXT, category TEXT, category2 TEXT);`;
            yield db.do(`run`, sql, {});

            const sql1 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-watch_ladies', 'https://www.amazon.com/gp/search/other/ref=lp_6358544011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A6358543011%2Cn%3A6358544011&bbn=6358544011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513150068', 'amazon.com', 'Top Brands', 'watch', 'ladies', '');`;
            const sql2 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-watch_mens', 'https://www.amazon.com/gp/search/other/ref=lp_6358540011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147441011%2Cn%3A6358539011%2Cn%3A6358540011&bbn=6358540011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513150477', 'amazon.com', 'Top Brands', 'watch', 'mens', '');`;
            const sql3 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.de-Top-watch_ladies', 'https://www.amazon.de/gp/search/other/ref=lp_10084749031_sa_p_89?rh=n%3A193707031%2Cn%3A%21193708031%2Cn%3A10084749031&bbn=10084749031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513750912', 'amazon.de', 'Top Brands', 'watch', 'ladies', '');`;
            const sql4 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.de-Top-watch_mens', 'https://www.amazon.de/gp/search/other/ref=lp_10084739031_sa_p_89?rh=n%3A193707031%2Cn%3A%21193708031%2Cn%3A10084739031&bbn=10084739031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751074', 'amazon.de', 'Top Brands', 'watch', 'mens', '');`;
            const sql5 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.fr-Top-watch_ladies', 'https://www.amazon.fr/gp/search/other/ref=lp_10143466031_sa_p_89?rh=n%3A60649031%2Cn%3A%2160937031%2Cn%3A10143466031&bbn=10143466031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751524', 'amazon.fr', 'Top Brands', 'watch', 'ladies', '');`;
            const sql6 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.fr-Top-watch_mens', 'https://www.amazon.fr/gp/search/other/ref=lp_10143456031_sa_p_89?rh=n%3A60649031%2Cn%3A%2160937031%2Cn%3A10143456031&bbn=10143456031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751451', 'amazon.fr', 'Top Brands', 'watch', 'mens', '');`;
            const sql7 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-watch_ladies', 'https://www.amazon.co.uk/gp/search/other/ref=lp_10103527031_sa_p_89?rh=n%3A328228011%2Cn%3A%21328229011%2Cn%3A10103527031&bbn=10103527031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751647', 'amazon.co.uk', 'Top Brands', 'watch', 'ladies', '');`;
            const sql8 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-watch_mens', 'https://www.amazon.co.uk/gp/search/other/ref=lp_10103528031_sa_p_89?rh=n%3A328228011%2Cn%3A%21328229011%2Cn%3A10103528031&bbn=10103528031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751715', 'amazon.co.uk', 'Top Brands', 'watch', 'mens', '');`;
            const sql9 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.jp-Top-watch_ladies', 'https://www.amazon.co.jp/gp/search/other/ref=lp_333010011_sa_p_89?rh=n%3A2229202051%2Cn%3A%212229203051%2Cn%3A2230006051%2Cn%3A333010011&bbn=333010011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513751952', 'amazon.co.jp', 'Top Brands', 'watch', 'ladies', '');`;
            const sql10 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.jp-Top-watch_mens', 'https://www.amazon.co.jp/gp/search/other/ref=lp_333009011_sa_p_89?rh=n%3A2229202051%2Cn%3A%212229203051%2Cn%3A2230005051%2Cn%3A333009011&bbn=333009011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1513752004', 'amazon.co.jp', 'Top Brands', 'watch', 'mens', '');`;
            const sql11 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-shoes_mens', 'https://www.amazon.com/gp/search/other/ref=lp_679255011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147441011%2Cn%3A679255011&bbn=679255011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1516080160', 'amazon.com', 'Top Brands', 'shoes', 'mens', '');`;
            const sql12 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-shoes_ladies', 'https://www.amazon.com/gp/search/other/ref=lp_679380011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A679337011&pickerToList=lbr_brands_browse-bin&ie=UTF8', 'amazon.com', 'Top Brands', 'shoes', 'ladies', '');`;
            const sql13 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-shoes_mens', 'https://www.amazon.co.uk/gp/search/other/ref=sr_sa_p_89?rh=n%3A11961407031%2Cn%3A12728621031%2Cn%3A%2111961408031%2Cn%3A12422026031%2Cn%3A1769738031&bbn=12728621031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1516080371', 'amazon.co.uk', 'Top Brands', 'shoes', 'mens', '');`;
            const sql14 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-shoes_ladies', 'https://www.amazon.co.uk/gp/search/other/ref=sr_sa_p_89?rh=n%3A11961407031%2Cn%3A12728621031%2Cn%3A%2111961408031%2Cn%3A12422025031%2Cn%3A1769798031&bbn=12728621031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1516080330', 'amazon.co.uk', 'Top Brands', 'shoes', 'ladies', '');`;
            const sql15 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-sandals_ladies', 'https://www.amazon.com/gp/search/other/ref=lp_679425011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147440011%2Cn%3A679337011%2Cn%3A679425011&bbn=679425011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108539', 'amazon.com', 'Top Brands', 'sandals', 'ladies', '');`;
            const sql16 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.com-Top-sandals_mens', 'https://www.amazon.com/gp/search/other/ref=lp_679320011_sa_p_89?rh=n%3A7141123011%2Cn%3A7147441011%2Cn%3A679255011%2Cn%3A679320011&bbn=679320011&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108590', 'amazon.com', 'Top Brands', 'sandals', 'mens', '');`;
            const sql17 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-sandals_ladies', 'https://www.amazon.co.uk/gp/search/other/ref=lp_1769853031_sa_p_89?rh=n%3A355005011%2Cn%3A%21362350011%2Cn%3A1769609031%2Cn%3A1769798031%2Cn%3A1769853031&bbn=1769853031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108643', 'amazon.co.uk', 'Top Brands', 'sandals', 'ladies', '');`;
            const sql18 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.co.uk-Top-sandals_mens', 'https://www.amazon.co.uk/gp/search/other/ref=lp_1769791031_sa_p_89?rh=n%3A355005011%2Cn%3A%21362350011%2Cn%3A1769609031%2Cn%3A1769738031%2Cn%3A1769791031&bbn=1769791031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108682', 'amazon.co.uk', 'Top Brands', 'sandals', 'mens', '');`;
            const sql19 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.de-Top-sandals_ladies', 'https://www.amazon.de/gp/search/other/ref=lp_1760312031_sa_p_89?rh=n%3A355006011%2Cn%3A%21361139011%2Cn%3A1760296031%2Cn%3A1760304031%2Cn%3A1760312031&bbn=1760312031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108738', 'amazon.de', 'Top Brands', 'sandals', 'ladies', '');`;
            const sql20 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.de-Top-sandals_mens', 'https://www.amazon.de/gp/search/other/ref=lp_1760372031_sa_p_89?rh=n%3A355006011%2Cn%3A%21361139011%2Cn%3A1760296031%2Cn%3A1760367031%2Cn%3A1760372031&bbn=1760372031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108807', 'amazon.de', 'Top Brands', 'sandals', 'mens', '');`;
            const sql21 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.fr-Top-sandals_ladies', 'https://www.amazon.fr/gp/search/other/ref=lp_1765117031_sa_p_89?rh=n%3A215934031%2Cn%3A%21215935031%2Cn%3A1765042031%2Cn%3A1765056031%2Cn%3A1765117031&bbn=1765117031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108979', 'amazon.fr', 'Top Brands', 'sandals', 'ladies', '');`;
            const sql22 = `INSERT OR REPLACE INTO fashion_amazon_topbrands_urls(FATU_id, url, site, page, genre, category, category2) VALUES ('amazon.fr-Top-sandals_mens', 'https://www.amazon.fr/gp/search/other/ref=lp_1765293031_sa_p_89?rh=n%3A215934031%2Cn%3A%21215935031%2Cn%3A1765042031%2Cn%3A1765241031%2Cn%3A1765293031&bbn=1765293031&pickerToList=lbr_brands_browse-bin&ie=UTF8&qid=1519108959', 'amazon.fr', 'Top Brands', 'sandals', 'mens', '');`;

            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.do(`run`, sql3, {});
            yield db.do(`run`, sql4, {});
            yield db.do(`run`, sql5, {});
            yield db.do(`run`, sql6, {});
            yield db.do(`run`, sql7, {});
            yield db.do(`run`, sql8, {});
            yield db.do(`run`, sql9, {});
            yield db.do(`run`, sql10, {});
            yield db.do(`run`, sql11, {});
            yield db.do(`run`, sql12, {});
            yield db.do(`run`, sql13, {});
            yield db.do(`run`, sql14, {});
            yield db.do(`run`, sql15, {});
            yield db.do(`run`, sql16, {});
            yield db.do(`run`, sql17, {});
            yield db.do(`run`, sql18, {});
            yield db.do(`run`, sql19, {});
            yield db.do(`run`, sql20, {});
            yield db.do(`run`, sql21, {});
            yield db.do(`run`, sql22, {});
            yield db.close();

            resolve();
        });
    });
}

// get url list
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // cheerio timeout setting
            yield cheerio.init(10000);

            // create table "proxy_list"
            let sql = `SELECT * FROM fashion_amazon_topbrands_urls`;
            let res = yield db.do(`all`, sql, {});

            resolve(res);
        });
    });
}

// scrape proxy data, and set db as proxy_list
const scrape = function (urls) {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urls.length; i++) {

                let FATU_id = urls[i].FATU_id;
                let site = urls[i].site;
                let page = urls[i].page;
                let reg_date = today;

                let $ = yield cheerio.get(urls[i].url);
                if ($ && $('body').html().match(/a-list-item/i)) {
                    $('.a-list-item').each(function (i, e) {
                        co(function* () {
                            let brand_pre = $(e).find('.refinementLink').text().replace(/[\n\r\t"]/g, '');
                            let brand_splits = brand_pre.replace(')', '').split('(');
                            let brand = brand_splits[0].trim();
                            let brand_kana = brand_splits[1] ? brand_splits[1].trim() : '';
                            let product_num = $(e).find('.narrowValue').text().replace(/[\n\r\t\(\) ,]/g, '');
                            let FAT_id = `${FATU_id}-${brand}-${reg_date}`;
                            const sql = `INSERT OR REPLACE INTO fashion_amazon_topbrands(FAT_id, brand, brand_kana, product_num, FATU_id, reg_date) VALUES ("${FAT_id}", "${brand}", "${brand_kana}", "${product_num}", "${FATU_id}", "${reg_date}");`;
                            yield db.do(`run`, sql, {});
                        });
                    });
                }
            }

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            yield db.connect(filePath_db);
            // let sql = `SELECT b.FAT_id, b.FATU_id, b.brand, b.brand_kana, b.product_num, u.site, u.page, u.ge_accessories, u.ge_bag, u.ge_fashiongoods, u.ge_food, u.ge_inner, u.ge_kids, u.ge_ladiesfashion, u.ge_liquor_japan, u.ge_liquor_western, u.ge_mensfashion, u.ge_shoes, u.ge_sweet, u.ge_watch, reg_date FROM fashion_amazon_topbrands b inner join fashion_amazon_topbrands_urls u where b.FATU_id = u.FATU_id and reg_date = '${today}'`;
            let sql = `SELECT b.FAT_id, b.FATU_id, b.brand, b.brand_kana, b.product_num, u.site, u.page, u.genre, u.category, u.category2, reg_date FROM fashion_amazon_topbrands b inner join fashion_amazon_topbrands_urls u where b.FATU_id = u.FATU_id and reg_date = '${today}'`;
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/amazon-brands-FGS166-${today}.txt`;
            fs.appendFileSync(filepath, ['FAT_id', 'FATU_id', 'ブランド名', 'ブランド名カナ', '登録商品数', 'サイト名', 'ページ名', 'ジャンル', 'タイプ', 'タイプ(予備)', '取得日'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['FAT_id', 'FATU_id', 'brand', 'brand_kana', 'product_num', 'site', 'page', 'genre', 'category', 'category2', 'reg_date'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(100)', 'VARCHAR(100)', 'INTEGER', 'VARCHAR(50)', 'VARCHAR(20)', 'VARCHAR(50)', 'VARCHAR(50)', 'VARCHAR(50)', 'VARCHAR(8)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                // fs.appendFileSync(filepath, [res[i].FAT_id, res[i].FATU_id, res[i].brand, res[i].brand_kana, res[i].product_num, res[i].site, res[i].page, res[i].ge_accessories, res[i].ge_bag, res[i].ge_fashiongoods, res[i].ge_food, res[i].ge_inner, res[i].ge_kids, res[i].ge_ladiesfashion, res[i].ge_liquor_japan, res[i].ge_liquor_western, res[i].ge_mensfashion, res[i].ge_shoes, res[i].ge_sweet, res[i].ge_watch, res[i].reg_date].join('\t') + '\n', 'utf-8');
                fs.appendFileSync(filepath, [res[i].FAT_id, res[i].FATU_id, res[i].brand, res[i].brand_kana, res[i].product_num, res[i].site, res[i].page, res[i].genre, res[i].category, res[i].category2, res[i].reg_date].join('\t') + '\n', 'utf-8');
            }

            yield db.close();
            resolve(res);
        });
    });
}



// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 4: // out data to share folder
                yield setOutFile();
                break;
            case 3: // drop table
                yield drop();
                break;
            case 2: // set url list
                yield setList();
                break;
            case 1: // scrape data
            default:
                yield init();
                const list = yield getList();
                yield scrape(list);
                yield end();
                break;
        }
    });
}

run();