'use strict';

const shell = require('shelljs');
const moment = require('moment');
const today = moment().format('YYYYMMDD');
const conf = new(require('../conf.js'));

console.log(`cp db/main.db ${conf.dirPath_share_vb}/main-${today}.db`);
shell.exec(`cp db/main.db ${conf.dirPath_share_vb}/main-${today}.db`);