'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const tsv = new(require('../../modules/tsv.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));
const phantom = new(require('../../modules/phantom.js'));

// each scrape modules
const seikoWatch = new(require('./seikoWatch.js'));
const credor = new(require('./credor.js'));
const galante = new(require('./galante.js'));
const wired = new(require('./wired.js'));
const alba = new(require('./alba.js'));
const soma = new(require('./soma.js'));
const agnesb = new(require('./agnesb.js'));
const mackintoshPhilosophy = new(require('./mackintosh-philosophy.js'));
const cabaneDeZucca = new(require('./cabaneDeZucca.js'));
const michelklein = new(require('./michelklein.js'));
const isseyMiyake = new(require('./isseyMiyake.js'));
const tsumoriChisato = new(require('./tsumoriChisato.js'));
const jillStuart = new(require('./jillStuart.js'));
const asicsWatch = new(require('./asicsWatch.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
]);

// variable
let driver = null;
const today = moment().format('YYYYMMDD');
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // set selenium with browser
            driver = yield selenium.init(cli['browser'], { inVisible: true });

            yield util.mkdir(`${conf.dirPath_data}\/FGS44-seiko`);

            // create table "proxy_list"
            yield db.connect(filePath_db);

            resolve();
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            driver.quit();
            yield db.close();

            resolve();
        });
    });
}

// drop table
const drop = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // drop table proxy_list
            yield db.connect(filePath_db);

            let sql1 = `DROP TABLE IF EXISTS fashion_fgs44_seiko_product;`;
            yield db.do(`run`, sql1, {});

            let sql2 = `DROP TABLE IF EXISTS fashion_fgs44_seiko_category;`;
            yield db.do(`run`, sql2, {});

            let sql3 = `DROP TABLE IF EXISTS fashion_fgs44_seiko_list;`;
            yield db.do(`run`, sql3, {});

            yield db.close();
            resolve();
        });
    });
}

// set url list
const setList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // create table "proxy_list"
            yield db.connect(filePath_db);
            const sqlA = `CREATE TABLE IF NOT EXISTS fashion_fgs44_seiko_product(num INTEGER primary key, id INTEGER, page_id TEXT, mpn_1 TEXT, mpn_2 TEXT, manufacturer_name TEXT, brand_name TEXT, product_name TEXT, color TEXT, size TEXT, size_value TEXT, exc_price NUMERIC, inc_price NUMERIC, unit_price TEXT, category_name TEXT, insert_date TEXT, url TEXT);`;
            const sqlB = `CREATE TABLE IF NOT EXISTS fashion_fgs44_seiko_category(Num INTEGER primary key, category_name TEXT, category_url TEXT, page_id TEXT);`;
            const sqlC = `CREATE TABLE IF NOT EXISTS fashion_fgs44_seiko_list(page_id TEXT primary key, id TEXT, num INTEGER, category TEXT, name TEXT, delflag INTEGER, shop_category1 TEXT, shop_category2 TEXT, cat_url TEXT, cat_url2 TEXT, cat_url3 TEXT, stage TEXT);`;
            yield db.do(`run`, sqlA, {});
            yield db.do(`run`, sqlB, {});
            yield db.do(`run`, sqlC, {});

            const sql1 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-1", "5932", 1, "totalScraper", "SEIKO", 0, "セイコーウオッチ株式会社 製品検索", "", "https://www.seikowatches.com/jp-ja/watchfinder", "none", "none", "productList");`;
            const sql2 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-2", "5932", 2, "totalScraper", "SEIKO", 0, "クレドール", "Men's", "http://www.credor.com/lineup/mens/", "http://www.credor.com/", "none", "productList");`;
            const sql3 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-3", "5932", 3, "totalScraper", "SEIKO", 0, "クレドール", "Ladies", "http://www.credor.com/lineup/ladies/", "http://www.credor.com/", "none", "productList");`;
            const sql4 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-4", "5932", 4, "totalScraper", "SEIKO", 0, "クレドール", "Couple Watches", "http://www.credor.com/lineup/couple_watches/", "http://www.credor.com/", "none", "productList");`;
            const sql5 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-5", "5932", 5, "totalScraper", "SEIKO", 0, "クレドール", "Masterpieces", "http://www.credor.com/lineup/masterpieces/", "http://www.credor.com/", "none", "productList");`;
            const sql6 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-6", "5932", 6, "totalScraper", "SEIKO", 0, "クレドール", "Others", "http://www.credor.com/lineup/others/", "http://www.credor.com/", "none", "productList");`;
            const sql7 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-7", "5932", 7, "totalScraper", "SEIKO", 0, "ガランテ", "ALL MODEL", "http://www.galante.jp/", "http://www.galante.jp/", "none", "productList");`;
            const sql8 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-8", "5932", 8, "totalScraper", "SEIKO", 0, "ワイアード", "wired", "https://w-wired.com/product/wired/", "https://w-wired.com/", "none", "productList");`;
            const sql9 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-9", "5932", 9, "totalScraper", "SEIKO", 0, "ワイアード", "wired-f", "https://w-wired.com/product/wired-f/", "https://w-wired.com/", "none", "productList");`;
            const sql10 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-10", "5932", 10, "totalScraper", "SEIKO", 0, "ワイアード", "pair-style", "https://w-wired.com/product/pair-style/", "https://w-wired.com/", "none", "productList");`;
            const sql11 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-11", "5932", 11, "totalScraper", "SEIKO", 0, "アルバ", "アルバ 製品検索", "https://www.alba.jp/p_search/result.php", "https://www.alba.jp/", "none", "productList");`;
            const sql12 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-12", "5932", 12, "totalScraper", "SEIKO", 0, "ソーマ", "ソーマ", "http://www.soma-sportswatch.com/", "http://www.soma-sportswatch.com/", "none", "productList");`;
            const sql13 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-13", "5932", 13, "totalScraper", "SEIKO", 0, "アニエスベー", "marcello", "https://www.agnesb-watch.jp/p_search/index.php?series=marcello", "https://www.agnesb-watch.jp/", "none", "productList");`;
            const sql14 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-14", "5932", 14, "totalScraper", "SEIKO", 0, "マッキントッシュ フィロソフィー", "WATCH COLLECTION", "http://license.seiko-watch.co.jp/mackintosh-philosophy/watches.html", "http://license.seiko-watch.co.jp/", "none", "productList");`;
            const sql15 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-15", "5932", 15, "totalScraper", "SEIKO", 0, "カバンドズッカ", "history", "http://www.cabanedezucca-watch.com/history/", "http://www.cabanedezucca-watch.com/", "none", "productList");`;
            const sql16 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-16", "5932", 16, "totalScraper", "SEIKO", 0, "ミッシェル クラン", "p_search", "http://license.seiko-watch.co.jp/p_search/index.php?P=0&bid=97&cid=2", "http://license.seiko-watch.co.jp/michelklein/", "none", "productList");`;
            const sql17 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-17", "5932", 17, "totalScraper", "SEIKO", 0, "イッセイ ミヤケ", "history", "https://www.isseymiyake-watch.com/watches/index.html", "https://www.isseymiyake-watch.com/", "none", "productList");`;
            const sql18 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-18", "5932", 18, "totalScraper", "SEIKO", 0, "ツモリ チサト", "history", "http://tsumorichisato-watch.jp/collection.html", "http://tsumorichisato-watch.jp/", "none", "productList");`;
            const sql19 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-19", "5932", 19, "totalScraper", "SEIKO", 0, "ジル スチュアート", "2017", "http://www.jillstuarttime.com/news.html", "http://www.jillstuarttime.com/", "none", "productList");`;
            const sql20 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-20", "5932", 20, "totalScraper", "SEIKO", 0, "ジル スチュアート", "2016", "http://www.jillstuarttime.com/news/2016.html", "http://www.jillstuarttime.com/", "none", "productList");`;
            const sql21 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-21", "5932", 21, "totalScraper", "SEIKO", 0, "ジル スチュアート", "2015", "http://www.jillstuarttime.com/news/2015.html", "http://www.jillstuarttime.com/", "none", "productList");`;
            const sql22 = `INSERT OR REPLACE INTO fashion_fgs44_seiko_list(page_id, id, num, category, name, delflag, shop_category1, shop_category2, cat_url, cat_url2, cat_url3, stage) VALUES ("5932-22", "5932", 22, "totalScraper", "SEIKO", 0, "アシックス", "2017", "http://asics-watch.com/products/products.html", "http://asics-watch.com/", "none", "productList");`;

            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.do(`run`, sql3, {});
            yield db.do(`run`, sql4, {});
            yield db.do(`run`, sql5, {});
            yield db.do(`run`, sql6, {});
            yield db.do(`run`, sql7, {});
            yield db.do(`run`, sql8, {});
            yield db.do(`run`, sql9, {});
            yield db.do(`run`, sql10, {});
            yield db.do(`run`, sql11, {});
            yield db.do(`run`, sql12, {});
            yield db.do(`run`, sql13, {});
            yield db.do(`run`, sql14, {});
            yield db.do(`run`, sql15, {});
            yield db.do(`run`, sql16, {});
            yield db.do(`run`, sql17, {});
            yield db.do(`run`, sql18, {});
            yield db.do(`run`, sql19, {});
            yield db.do(`run`, sql20, {});
            yield db.do(`run`, sql21, {});
            yield db.do(`run`, sql22, {});
            yield db.close();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            let sql = `SELECT mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, min(insert_date) insert_date, url
            FROM fashion_fgs44_seiko_product
            group by mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, url
            order by insert_date;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});

            yield util.mkdir(`${conf.dirPath_data}\/FGS44-seiko`);
            yield util.mkdir(`${conf.dirPath_data}\/FGS44-seiko\/${today}-${conf.pc_id}`);
            let filepath = `${conf.dirPath_share_vb}/FGS44-seiko-${today}.txt`;
            let filepath2 = `${conf.dirPath_data}\/FGS44-seiko\/${today}-${conf.pc_id}\/FGS44-seiko.txt`;

            // write header
            fs.writeFileSync(filepath, '', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['BIGINT', 'BIGINT', 'VARCHAR(100)', 'VARCHAR(100)', 'VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(300)', 'VARCHAR(100)', 'VARCHAR(300)', 'VARCHAR(100)', 'DECIMAL(15,2)', 'DECIMAL(15,2)', 'VARCHAR(200)', 'VARCHAR(8)', 'VARCHAR(300)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [i + 1, '5932', res[i].mpn_1, res[i].mpn_2, res[i].manufacturer_name, res[i].brand_name, res[i].product_name, res[i].color, res[i].size, res[i].size_value, res[i].exc_price, res[i].inc_price, (res[i].category_name ? res[i].category_name.substr(0, 50) : ''), res[i].insert_date.replace(/-/g, ''), res[i].url].join('\t') + '\n', 'utf-8');
            }

            // copy data
            let data = fs.readFileSync(filepath, 'utf-8');
            fs.writeFileSync(filepath2, data, 'utf-8');

            resolve(res);
        });
    });
}

// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // login
                yield phantom.loginNoneIntra();
                break;

            case 99: // reset table
                yield drop();
                yield setList();
                break;

            // case 9919: // アシックス 削除
            //     yield asicsWatch.delete();
            //     break;

            // case 19: // アシックス
            //     yield asicsWatch.init(cli['browser']);
            //     yield asicsWatch.scrapeCategory();
            //     yield asicsWatch.scrapeProduct();
            //     break;

            case 9918: // ジル スチュアート 削除
                yield jillStuart.delete();
                break;

            case 18: // ジル スチュアート
                yield jillStuart.init(cli['browser']);
                yield jillStuart.scrapeCategory();
                yield jillStuart.scrapeProduct();
                break;

            // case 9917: // ツモリ チサト 削除
            //     yield tsumoriChisato.delete();
            //     break;

            // case 17: // ツモリ チサト
            //     yield tsumoriChisato.init(cli['browser']);
            //     yield tsumoriChisato.scrapeCategory();
            //     yield tsumoriChisato.scrapeProduct();
            //     break;

            case 9916: // イッセイ ミヤケ 削除
                yield isseyMiyake.delete();
                break;

            case 16: // イッセイ ミヤケ
                yield isseyMiyake.init(cli['browser']);
                yield isseyMiyake.scrapeCategory();
                yield isseyMiyake.scrapeProduct();
                break;

            case 9915: // ミッシェル クラン 削除
                yield michelklein.delete();
                break;

            case 15: // ミッシェル クラン
                yield michelklein.init(cli['browser']);
                yield michelklein.scrapeCategory();
                yield michelklein.scrapeProduct();
                break;

            // case 9914: // カバンドズッカ 削除
            //     yield cabaneDeZucca.delete();
            //     break;

            // case 14: // カバンドズッカ
            //     // https://www.seiko-watch.co.jp/products/
            //     yield cabaneDeZucca.init(cli['browser']);
            //     yield cabaneDeZucca.scrapeCategory();
            //     yield cabaneDeZucca.scrapeProduct();
            //     break;

            case 9913: // マッキントッシュ フィロソフィー 削除
                yield mackintoshPhilosophy.delete();
                break;

            case 13: // マッキントッシュ フィロソフィー
                yield mackintoshPhilosophy.init(cli['browser']);
                yield mackintoshPhilosophy.scrapeCategory();
                yield mackintoshPhilosophy.scrapeProduct();
                break;

            case 9912: // アニエスベー 削除
                yield agnesb.delete();
                break;

            case 12: // アニエスベー
                yield agnesb.init(cli['browser']);
                yield agnesb.scrapeCategory();
                yield agnesb.scrapeProduct();
                break;

            case 9911: // ソーマ 削除
                yield soma.delete();
                break;

            case 11: // ソーマ
                yield soma.init(cli['browser']);
                yield soma.scrapeCategory();
                yield soma.scrapeProduct();
                break;

            case 9910: // アルバ 削除
                yield alba.delete();
                break;

            case 10: // アルバ
                yield alba.init(cli['browser']);
                yield alba.scrapeCategory();
                yield alba.scrapeProduct();
                break;

            case 9909: // ワイアード 削除
                yield wired.delete();
                break;

            case 9: // ワイアード
                yield wired.init(cli['browser']);
                yield wired.scrapeProduct();
                break;

            case 9908: // ガランテ 削除
                yield galante.delete();
                break;

            case 8: // ガランテ
                yield galante.init(cli['browser']);
                yield galante.scrapeProduct();
                break;

            case 9907: // クレドール 削除
                yield credor.delete();
                break;

            case 7: // クレドール
                yield credor.init(cli['browser']);
                yield credor.scrapeCategory();
                yield credor.scrapeProduct();
                break;

            case 9906: // セイコーウオッチ株式会社 製品検索 削除
                yield seikoWatch.delete();
                break;

            case 6: // セイコーウオッチ株式会社 製品検索
                yield seikoWatch.init(cli['browser']);
                yield seikoWatch.scrapeCategory();
                yield seikoWatch.scrapeProduct();
                break;

            case 5: // scrape product only
                yield init();
                yield setOutFile();
                yield end();
                break;
            case 4: // scrape product only
                yield init();
                yield scrapeProduct();
                yield setOutFile();
                yield end();
                break;
                // case 3: // check each data
                //     yield showList();
                //     break;
            case 1: // scrape data
            default:
                yield init();
                yield scrapeCategory();
                yield scrapeProduct();
                yield scrapeProduct();
                yield scrapeProduct();
                yield setOutFile();
                yield end();
                break;
        }
    });
}

run();