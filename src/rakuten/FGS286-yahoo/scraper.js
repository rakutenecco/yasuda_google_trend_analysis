'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');
const selenium = new (require('../../modules/selenium.js'));

// new modules
const conf = new (require('../../conf.js'));
const db = new (require('../../modules/db.js'));
const util = new (require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },
    { name: 'startday', alias: 's', type: String },
    { name: 'getdate', alias: 'g', type: String },
]);

let mode;
if(cli['run'] == 1){
    mode = 'new';
} else if (cli['run'] == 2){
    mode = 'continue';
} else if (cli['run'] == 9){
    mode = 'output';
}

const cron = cli['cron'] ;//cron13,cron13_01,cron13_02,cron13_03
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const infile = `${conf.dirPath_share_vb}/targets/${cli['fname']}`;//`${conf.dirPath_share_vb}/targets/targets_${category}.txt`;
const category = cli['fname'].replace('targets_','').replace('.txt','');// flower, new_20190305, all_03_01, fashion_03_03

const tname = `FGS286_yahoo_${category}`;
const logtable = 'FGS286_log';

const istsql = `INSERT OR REPLACE INTO ${tname} VALUES `;

let target_date = moment().add(-29,'d').format('YYYY/MM/DD'); //log
if(cli['startday']){
    let start_date = cli['startday'].substr(0, 4) + '/' + cli['startday'].substr(4, 2) + '/' + cli['startday'].substr(6, 2);
    if( start_date > target_date ) target_date = start_date;
    if( target_date >= moment().format('YYYY/MM/DD') ) {
        console.log(` cli['startday']: ${cli['startday']} err`);
        return;
    }
}

let getdate; //output filename
let getdate_slash; //result data, log data
if(cli['getdate']){
    getdate = cli['getdate'];
    getdate_slash = cli['getdate'].substr(0, 4) + '/' + cli['getdate'].substr(4, 2) + '/' + cli['getdate'].substr(6, 2);
}else{
    getdate = moment().format('YYYYMMDD');
    getdate_slash = moment().format('YYYY/MM/DD');
}

let outfile;
// ex) fname:targets_life.txt -> FGS286_yahoo_life_20190304.txt
// ex) fname:targets_new_20190322.txt -> FGS286_yahoo_new_20190322_20190322.txt
// ex) fname:targets_all_02_01.txt -> FGS286_yahoo_all_20190304_02_01.txt
let tfnm = category.split(/_/g);
if(tfnm.length == 1) {
    outfile = `${conf.dirPath_share_vb}/FGS286_yahoo_${tfnm[0]}_${getdate}.txt`;

} else if ( tfnm.length == 2 && tfnm[1].length == 8 && isFinite(tfnm[1]) ) {
    outfile = `${conf.dirPath_share_vb}/FGS286_yahoo_${tfnm[0]}_${tfnm[1]}_${getdate}.txt`;

} else if ( tfnm.length == 3 && tfnm[1].length == 2 && isFinite(tfnm[1]) && tfnm[2].length == 2 && isFinite(tfnm[2]) ){
    outfile = `${conf.dirPath_share_vb}/FGS286_yahoo_${tfnm[0]}_${getdate}_${tfnm[1]}_${tfnm[2]}.txt`;

} else {
    mode = '';
}

//average file
const avrfile = `${conf.dirPath_share_vb}/targets/FGS286_yahoo_targets_all_average.txt`;
const avrtable = `FGS286_yahoo_targets_all_average`;

//NG file
const ngfile = `${conf.dirPath_share_vb}/targets/FGS286_yahoo_targets_${category}_ngchk.txt`;

console.log('/// FGS286 scraper.js start ///');
console.log(`  mode: ${mode}`);
console.log(`  args  cron: ${cron}`);
console.log(`  args  fname: ${cli['fname']}`);
console.log(`  args  category: ${category}`);
console.log(`  target_date: ${target_date}`);
console.log(`  getdate: ${getdate_slash}`);
console.log(`  outfile: ${outfile}`);

const maxloop = 20;
const minnum = 0;

// variable
let keywords = [];
let kwdRD = [];

let tgtnum; // targets table word number
let getkwd = [];
let lcnt = 1;
let zcnt = 0;
let starttime;
//let prcstype;

let driver;
let keyword;
let twts;

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.close();

            resolve();
        });
    });
}

// insert Log
const insertLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            starttime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `INSERT OR REPLACE INTO FGS286_log ` +
                //`(tbname, getdate, loop_count, remainkwd, getkwd, starttime, endtime) ` +
                //`VALUES (?, ?, ?, ?, ?, ?, ?);`;
                `(tbname, getdate, target_date, loop_count, remainkwd, starttime) ` +
                `VALUES (?, ?, ?, ?, ?, ?);`;
            let opt = [tname, getdate_slash, target_date, lcnt, keywords.length, starttime];
            console.log('', tname, getdate_slash, target_date, lcnt, keywords.length, '', starttime, '', cron);
            yield db.do(`run`, sql, opt);
            resolve();
        });
    });
}

// update Log
const updateLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            const curtime  = moment().format('YYYY/MM/DD HH:mm:ss');
            const sql = `UPDATE FGS286_log SET getkwd = ${getkwd.length}, endtime = '${curtime}' ` +
            `WHERE tbname = '${tname}' and getdate = '${getdate_slash}' and target_date = '${target_date}' and loop_count = ${lcnt} and starttime = '${starttime}';`;
            //console.log(sql);
            console.log(tname, getdate_slash, target_date, lcnt, keywords.length, getkwd.length, starttime, curtime, cron);
            yield db.do(`run`, sql, {});
            resolve();
        });
    });
}

// output Log
const outLog = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let sql = `select tbname, getdate, target_date, loop_count, remainkwd, getkwd, starttime, endtime from FGS286_log ` +
                      `where tbname = '${tname}' and getdate = '${getdate_slash}' and target_date = '${target_date}' order by starttime;`;
            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            let filepath = `${conf.dirPath_share_vb}/log/${tname}.log`;
            console.log(`output log: ${filepath}`);

            // append
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, ([res[i].tbname, res[i].getdate, res[i].target_date, res[i].loop_count, res[i].remainkwd, res[i].getkwd, res[i].starttime, res[i].endtime].join('\t') + '\n'), 'utf8');
            }

            resolve();

        });
    });
}

// output NG file
const outNGfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select a.keyword keyword, a.getdate getdate, '${target_date}' target_date, a.tweets tweets, a.avrtwts avrtwts
            from ${tname} a 
            where not exists (select * from ${tname} c where c.getdate = '${getdate_slash}' and c.tweets >= 0 and c.keyword = a.keyword ) 
            and a.avrtwts > 0 and a.getdate = '${getdate_slash}';`;

            //console.log(sql);
            let res = yield db.do(`all`, sql, {});            

            if(res.length > 0){
                console.log(` output NG file: ${ngfile}`);

                // append
                for (let i = 0; i < res.length; i++) {
                    fs.appendFileSync(ngfile, ([res[i].keyword, res[i].getdate, res[i].target_date, res[i].tweets, res[i].avrtwts].join('\t') + '\n'), 'utf8');
                }

            }else{
                console.log(` not exists NG words`);

            }

            resolve();

        });
    });
}

// getText
const getText = function (ele) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!ele) {
                resolve('');
                return;
            }

            ele.getText().then(function (text) {
                resolve(text);
            }, function (err) {
                console.log('      getText err');
                resolve('');
            });
        //}).catch((e) => {
        //    console.log(e);
        //    resolve('');
        });
    });
}

// Selenium 関数生成
let byElement = function (selector) {

    let tag = '';
    switch (selector.substr(0, 1)) {
        case '#':
            return By.id(selector.substr(1));
        case '.':
            return By.className(selector.substr(1));
        case '/':
            return By.xpath(selector);
        default:
            return By.css(selector);
    }
};

// getElements
const getElements = function (driver_ele, selector) {

    return new Promise((resolve, reject) => {
        co(function* () {

            if (!driver_ele || !selector) {
                //console.log('    getElements arg null err');
                resolve('');
                return;
            }
            driver_ele.findElements(byElement(selector)).then(function (ele) {
                resolve(ele);
            }, function (err) {
                //console.log('    getElements err');
                resolve('');
            });
            
        // }).catch((e) => {
        //     console.log('    catch getElements');
        //     resolve('');
        });
    });
}

// init action
const init_table = function (result_table,targets_table, avr_table) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // targets table drop
            sqlstr = `DROP TABLE IF EXISTS ${targets_table};`;
            //console.log('sql3 = ' + sql3);
            yield db.do(`run`, sqlstr, {});

            // targets table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${targets_table} ` +
                `(keyword TEXT PRIMARY KEY);`;
            //console.log('sql4 = ' + sql4);
            yield db.do(`run`, sqlstr, {});


            // average table drop
            sqlstr = `DROP TABLE IF EXISTS ${avr_table};`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});

            // average table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${avr_table} ` +
                `(keyword TEXT PRIMARY KEY, avrposts bigint);`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});


            // create table
            sqlstr = `CREATE TABLE IF NOT EXISTS ${result_table} ` +
                `(keyword TEXT, category TEXT, getdate TEXT, postdate TEXT, tweets bigint, badfeel integer, goodfeel integer, avrtwts bigint );`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${result_table} on ${result_table} (getdate, keyword);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            // old result delete
            const deldate = moment(getdate.replace(/\//g, '-')).subtract(17, 'days').format('YYYY/MM/DD');
            //console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM ${result_table} WHERE getdate < '${deldate}' or getdate = '${getdate_slash}';`;
            //const sql5 = `DELETE FROM ${result_table} WHERE getdate < '${deldate}';`;
            //console.log('sql5 = ' + sql5);
            yield db.do(`run`, sqlstr, {});

            // log table create
            sqlstr = `CREATE TABLE IF NOT EXISTS ${logtable} ` +
                `(tbname TEXT, getdate TEXT, target_date TEXT, loop_count integer, remainkwd bigint, getkwd bigint, starttime TEXT, endtime TEXT );`;
            //console.log('sql6 = ' + sql6);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `CREATE INDEX IF NOT EXISTS i_${logtable} on ${logtable} (tbname, getdate);`;
            //console.log('sql2 = ' + sql2);
            yield db.do(`run`, sqlstr, {});

            // old log delete
            const deldate2 = moment(getdate.replace(/\//g, '-')).subtract(16, 'days').format('YYYY/MM/DD');
            //console.log(` log deldate: ${deldate2}`);
            //const sql7 = `DELETE FROM FGS239_log WHERE tbname = '${result_table}' and (getdate < '${deldate2}' or getdate = '${getdate}');`;
            sqlstr = `DELETE FROM ${logtable} WHERE tbname = '${result_table}' and getdate < '${deldate2}';`;
            //console.log('sql7 = ' + sql7);
            yield db.do(`run`, sqlstr, {});


            resolve();
        });
    });
}

// store targets
const store_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            //infile = `${conf.dirPath_share_vb}/targets_${category}.txt`;
            //console.log('infile',infile);
            if (fs.existsSync(infile)) {
                console.log(` infile: ${infile} read start`);
                let arr = fs.readFileSync(infile).toString().split("\n");
                //console.log(kywds.length);
                if(arr[arr.length -1].length == 0) arr.pop();
                console.log(` targets file: ${arr.length}`);
                
                //yahoo search string check utf-8
                let kywds = [];
                let exkywds = [];
                for(let i = 0; i < arr.length; i++) {
                    
                    let str = arr[i];
                    let testResult = str.match(/[^'\-%&\.:=’×°ー・々〆ﾟ･αβφ﨑ゞ0-9_A-Za-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                    if(testResult){
                        exkywds.push(str);
                    }else{
                        kywds.push(str);
                    }
                }
                let yahoo_except_file = `${conf.dirPath_share_vb}/targets/targets_except/targets_yahoo_except_${category}_${getdate}.txt`;

                if(exkywds.length > 0){
                    for (let i = 0; i < exkywds.length; i++) {
                        fs.appendFileSync(yahoo_except_file, exkywds[i] + '\n', 'utf8');
                    }
                }
                console.log(` except  keyword: ${exkywds.length}`);
                console.log(` targets keyword: ${kywds.length}`);

                let sql = `insert into FGS286_targets_${category} values `;
                if (kywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(kywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == kywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = kywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = kywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        //console.log(sql + '("' + exc.join('"),("') + '");');
                        yield db.do(`run`, `${sql} ("${exc.join('"),("')}");`, {});
                        i++;
                    } while (times >= i);
                }
                result = true;

            }else{
                console.log('  not exists in file');

            }
            //average file
            if (fs.existsSync(avrfile) && result) {
                result = false;

                console.log(` average file: ${avrfile} read start`);
                let tgtsavrfile = fs.readFileSync(avrfile).toString().split("\n");
                //console.log(kywds.length);
                if(tgtsavrfile[tgtsavrfile.length -1].length == 0) tgtsavrfile.pop();
                //console.log(` targets average file: ${tgtsavrfile.length}`);

                let avkywds = tgtsavrfile;
                console.log(` targets average keyword: ${avkywds.length}`);

                let sql = `insert into ${avrtable} values `;
                if (avkywds.length > 0) {
                    const n = 3000;
                    let times = Math.floor(avkywds.length / n);
                    //console.log('n: ' + n + '  times: ' + times);
                    if (times * n == avkywds.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = avkywds.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = avkywds.slice(st, end);
                        //console.log(exc[0],exc[exc.length -1]);
                        let arr = [];
                        for ( let iarr = 0, len = exc.length; iarr < len; ++iarr) {
                            //console.log( exc[iarr].replace('///','",'));
                            arr.push(exc[iarr].replace('///','",'));
                        }
                        //console.log(sql + '("' + arr.join('),("') + ');');
                        yield db.do(`run`, sql + '("' + arr.join('),("') + ');', {});
                        i++;
                    } while (times >= i);
                }                
                result = true;

            }else{
                console.log('  not exists averagefile');
                result = false;

            }
            resolve(result);

        });
    });
}

// get targets list
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS286_targets_${category};`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword, ifnull(m.avrposts, 0) avrposts
             from FGS286_targets_${category} a 
             left join ${avrtable} m on a.keyword = m.keyword
            where not exists (select * from ${tname} b where b.getdate = '${getdate_slash}' and 
            b.tweets >= 0  
            and a.keyword = b.keyword ) order by 1;`;
            //console.log(sql);
            keywords = yield db.do(`all`, sql, {});
            console.log(' remain keywords: ' + keywords.length);

            resolve(keywords.length);

        });
    });
}

// get targets 2
const get_targets2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //tgtnum            
            const sql1 = `select count(*) cnt from FGS286_targets_${category};`;
            //console.log(sql1);
            let num = yield db.do(`all`, sql1, {});
            //console.log('num: ' + num);
            tgtnum = num[0].cnt;
            console.log(' targets table: ' + tgtnum);

            const sql = `select a.keyword keyword, ifnull(m.avrposts, 0) avrposts
             from FGS286_targets_${category} a 
             left join ${avrtable} m on a.keyword = m.keyword
            where not exists (select * from ${tname} b where b.getdate = '${getdate_slash}' and 
            ( b.tweets >= 0 or ( b.tweets >= -1 and b.avrtwts = 0 ) ) 
            and a.keyword = b.keyword ) order by 1;`;
            //console.log(sql);
            keywords = yield db.do(`all`, sql, {});
            console.log(' remain keywords: ' + keywords.length);

            resolve();
        });
    });
}

// get loop count
const get_loop_count = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //loop count
            
            const sql = `select max(loop_count) lct from FGS286_log where getdate = '${getdate_slash}' and tbname = '${tname}' and getkwd >= 0;`;
            //console.log(sql);
            let num = yield db.do(`all`, sql, {});
            //console.log(' max loop_count: ' + num[0].lct);
            if ( num[0].lct > 0 ) {
                lcnt = num[0].lct + 1;
            }
            console.log(' loop_count: ' + lcnt);

            resolve();
        });
    });
}

// open get_empty
const get_empty = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;

            let url = `https://search.yahoo.co.jp/realtime/search?p=${keyword}`;

            if(!driver) {
                //console.log('  driver get');
                driver = yield selenium.init('chromium', { inVisible:true });
                driver.manage().window().setSize(700,250);
            }
            //driver.get(url);
            driver.get(url).then(null, function (err) {
                //resolve(text);
            }, function (err) {
                console.log('      driver get err');
                resolve('err');
            });

            //yield util.sleep(5000);
            yield util.sleep(1000);
            if(driver){
                //console.log('     sleep 1000 driver exists');
                let ele_TS2ayNav = yield getElements(driver, '#TS2ayNav');
                if (ele_TS2ayNav.length == 0) {
                    //console.log('     sleep 800 graph tab not exists');
                    let ele_mediaNav = yield getElements(driver, '#mediaNav');
                    if (ele_mediaNav.length > 0) {
                        // analyse graph not exists
                        result = true;
                    }else{
                        result = 'err';
                    }
                } else {
                    let ele_period_m = yield getElements(driver, '#period-m');
                    //console.log('     ele_period_m.length',ele_period_m.length);
                    ele_period_m[0].click();
                    yield util.sleep(800);
                }
            }else{
                console.log('     sleep 1000 driver not exists');
                yield util.sleep(2000);
                let ele_TS2ayNav = yield getElements(driver, '#TS2ayNav');
                if (ele_TS2ayNav.length == 0) {
                    console.log('     sleep 2000 graph tab not exists');
                    result = true;
                } else {
                    let ele_period_m = yield getElements(driver, '#period-m');
                    ele_period_m[0].click();
                    yield util.sleep(800);
                }
            }

            resolve(result);
        }).catch((e) => {
            console.log('    get_empty catch');
            resolve('err');
        });
    });
}

// get_data
const get_data = function (kwd, avr) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            let txt_day_last = '99-99';
            const y = moment().format('YYYY');
            const y_last = moment().add(-1,'y').format('YYYY');
            let days = moment().diff(moment(target_date.replace(/\//g, '-')), "days");
            if ( days > 29 ) days = 29;
            //console.log(kwd, 'target_date:' + target_date, days + 'days');

            // test
            // let ele_day;
            // let txt_day;
            // let ele_data;
            // let txt_data;
            // let txt_data1;
            // let txt_data2;
            // for(let xval = 9; xval >= -1; xval-- ){
                // driver.findElement(By.id('placeholder')).then((elem)=>{
                //     driver.actions().mouseMove(elem,{x:xval,y:50}).perform();
                //     driver.sleep(10);
                // });
                // ele_day = yield getElements(driver, '.flot-plugin-label');
                // txt_day = yield getText(ele_day[1]);
                // ele_data = yield getElements(driver, '.flot-plugin-tooltip');
                // txt_data = yield getText(ele_data[0]);
                // console.log(xval,txt_day,txt_data,txt_day.length,txt_data.length);
            // }
            //
            // for(let xval = 9; xval >= -1; xval-- ){
                // driver.findElement(By.id('placeholder3')).then((elem)=>{
                //     driver.actions().mouseMove(elem,{x:xval,y:50}).perform();
                //     driver.sleep(10);
                // });
                // ele_day = yield getElements(driver, '.flot-plugin-label');
                // txt_day = yield getText(ele_day[3]);
                // ele_data = yield getElements(driver, '.flot-plugin-tooltip');
                // txt_data1 = yield getText(ele_data[1]);
                // txt_data2 = yield getText(ele_data[2]);
                // console.log(xval,txt_day,txt_data1,txt_data2,txt_day.length,txt_data1.length,txt_data2.length);
            // }
            //if(txt_day.length == 0 || txt_data.length == 0 || txt_data1.length == 0 || txt_data2.length == 0) result = false;
            //

            let twarr = [];
            for(let xval = 234; xval >= 0; xval = xval-8 ){
            
                // driver.findElement(By.id('placeholder')).then((elem)=>{
                //     driver.actions().mouseMove(elem,{x:xval,y:50}).perform();
                //     driver.sleep(40);
                // });
                driver.findElement(By.id('placeholder')).then(function (elem) {
                    driver.actions().mouseMove(elem,{x:xval,y:50}).perform();
                    driver.sleep(30);
                }, function (err) {
                    console.log('      getdata mouseMove1 err');
                    resolve(false);
                });

                let ele_day = yield getElements(driver, '.flot-plugin-label');
                let txt_day = yield getText(ele_day[1]);
                let ele_data = yield getElements(driver, '.flot-plugin-tooltip');
                let txt_data = yield getText(ele_data[0]);                    
                if(txt_day.length == 0 || txt_data.length == 0){
                    //console.log(`      ${kwd}: get_data err [tweets]  ${moment().format('YYYY/MM/DD HH:mm:ss')}`); 
                    break;
                }
                let md = txt_day.split('/');
                let txt_day_hyphen = `${('0' + md[0]).slice(-2)}-${('0' + md[1]).slice(-2)}`;

                //if(txt_day_last == txt_day_hyphen) console.log('    twarr days duplicate',txt_day_last);
                if(txt_day_last !== txt_day_hyphen){
                    //console.log(xval,txt_day,txt_data,txt_day_hyphen,txt_day_last);
                    
                    //console.log(`${y}-${txt_day_hyphen}`);
                    let txt_date;
                    if(txt_day_last > txt_day_hyphen){
                        txt_date = moment(`${y}-${txt_day_hyphen}`).format('YYYY/MM/DD');
                    }else{
                        txt_date = moment(`${y_last}-${txt_day_hyphen}`).format('YYYY/MM/DD');
                    }              
                    
                    twarr.push([txt_date,txt_data.replace(/[^0-9]/g, '')]);
                    //console.log('    twarr.length',twarr.length);
                    //console.log('  ',twarr.length,twarr[twarr.length-1][0],twarr[twarr.length-1][1]);

                    if(txt_date == target_date) break;

                    txt_day_last = txt_day_hyphen;

                }
            }
            // if(twarr.length != days) {
            //     console.log('    twarr.length',twarr.length,'days',days);
            // }
            if(twarr.length == days){
            
                txt_day_last = '99-99'
                let flarr = [];
                for(let xval = 235; xval >= 0; xval = xval-8 ){
                
                    // driver.findElement(By.id('placeholder3')).then((elem)=>{
                    //     driver.actions().mouseMove(elem,{x:xval,y:50}).perform();
                    //     driver.sleep(60);
                    // });
                    driver.findElement(By.id('placeholder3')).then(function (elem) {
                        driver.actions().mouseMove(elem,{x:xval,y:50}).perform();
                        driver.sleep(80);
                    }, function (err) {
                        console.log('      getdata mouseMove2 err');
                        resolve(false);
                    });

                    let ele_day = yield getElements(driver, '.flot-plugin-label');
                    let txt_day = yield getText(ele_day[3]);
                    let ele_data = yield getElements(driver, '.flot-plugin-tooltip');
                    let txt_data2 = yield getText(ele_data[1]);
                    let txt_data1 = yield getText(ele_data[2]);
                    if(txt_day.length == 0 || txt_data1.length == 0 || txt_data2.length == 0){
                        //console.log(`      ${kwd}: get_data err [feels]  ${moment().format('YYYY/MM/DD HH:mm:ss')}`); 
                        break;
                    }
                        
                    let md = txt_day.split('/');
                    let txt_day_hyphen = `${('0' + md[0]).slice(-2)}-${('0' + md[1]).slice(-2)}`;
                    
                    //if(txt_day_last == txt_day_hyphen) console.log('     flarr days duplicate',txt_day_last);
                    if(txt_day_last !== txt_day_hyphen){
                        //console.log(xval,txt_day,txt_data1,txt_data2,txt_day_hyphen,txt_day_last);                        

                        let txt_date;
                        if(txt_day_last > txt_day_hyphen){
                            txt_date = moment(`${y}-${txt_day_hyphen}`).format('YYYY/MM/DD');
                        }else{
                            txt_date = moment(`${y_last}-${txt_day_hyphen}`).format('YYYY/MM/DD');
                        }              
                        
                        flarr.push([txt_date,txt_data1.replace(/[^0-9]/g, ''),txt_data2.replace(/[^0-9]/g, '')]);
                        //console.log('     flarr.length',flarr.length);
                        //console.log('  ',flarr.length,flarr[flarr.length-1][0],flarr[flarr.length-1][1],flarr[flarr.length-1][2]);

                        if(txt_date == target_date) break;

                        txt_day_last = txt_day_hyphen;

                    }
                }
                // if(flarr.length != days) {
                //     console.log('     flarr',flarr.length,'days',days);
                // }
                if(flarr.length == days){

                    for(let i = 0; i < days; i++ ){
                        if(twarr[i][0] == flarr[i][0]){
                            let str = `("${kwd}","","${getdate_slash}","${twarr[i][0]}",${twarr[i][1]},${flarr[i][1]},${flarr[i][2]},${avr})`;
                            //console.log('str',str);
                            //console.log('kwdRD.length',kwdRD.length);
                            kwdRD.push(str);
                            //console.log('kwdRD.length',kwdRD.length);
                        }else{
                            break;
                        }
                    }
                    // if(kwdRD.length != days) {
                    //     console.log('      kwdRD',kwdRD.length,'days',days);
                    // }
                    if(kwdRD.length == days) result = true;
                }
                
            }

            resolve(result);

        }).catch((e) => {
            console.log('    get_data catch');
            resolve(false);
        });
    });
}

// scrape yahoo data
const get_yahoo_data = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //if( !driver ) driver = yield selenium.init('chromium', { inVisible:true });
        
            const maxcnt = 2;
            
            let rcnt = 0; //kIDXごとのカウンタ
            let kIDX = 0;
            let next = false;
            let result = false;
            let retry = false;  //fnnction error

            let ccnt = 0; //continuous count
            let rt = true;

            while(kIDX < keywords.length) {
                rcnt = 0;
                next = false;
                result = false;
                retry = false;
                twts = -2;              

                keyword = util.encodeUrl(keywords[kIDX].keyword);

                do {
                    if(retry) rcnt = rcnt + 1;
                    result = false;
                    retry = false;

                    if(rcnt >= maxcnt){
                        next = true;
                        console.log(`     ${kIDX}:${keywords[kIDX].keyword} avpsts:${keywords[kIDX].avrposts} rcnt:${rcnt}  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                        ccnt = ccnt + 1;
                        if(driver) {
                            driver.quit();
                            driver = null;
                        }
                    } else {

                        let ele_empty = yield get_empty();
                        //console.log(ele_empty);
                        if (ele_empty == 'err') {
                            console.log(`      get_empty err  ${kIDX}:${keywords[kIDX].keyword}`);
                            retry = true;
                            continue;

                        }else if(ele_empty) {
                            twts = -1;
                            result = true;
                            next = true;
                            ccnt = 0;

                        } else {
                            let rt = yield get_data(keywords[kIDX].keyword, keywords[kIDX].avrposts);
                            if(rt){
                                twts = 0;
                                result = true;
                                next = true;
                                ccnt = 0;

                            }else{
                                //console.log(`    get_data false  ${kIDX}:${keywords[kIDX].keyword}`);
                                retry = true;
                                continue;
                            }
                        }
                    }
                }while (!next)
                
                //log
                if((kIDX)%500 == 0){
                    console.log(`  ${kIDX}:${keywords[kIDX].keyword} target_date:${target_date} ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                }

                if(result){

                    if(kwdRD.length == 0){

                        let str = `("${keywords[kIDX].keyword}","","${getdate_slash}","${target_date}",${twts},0,0,${keywords[kIDX].avrposts})`;
                        //console.log(`${istsql}${str};`);
                        yield db.do(`run`, `${istsql}${str};`, {});

                    }else{
                        //console.log(`${istsql}${kwdRD.join(',')};`);
                        yield db.do(`run`, `${istsql}${kwdRD.join(',')};`, {});

                    }

                    kwdRD = [];

                    if(twts == 0) getkwd.push(keywords[kIDX].keyword);
                }

                if(ccnt > 5){
                    rt = false;
                    break;
                }

                kIDX = kIDX + 1;
            }
            //if(driver) driver.quit();

            resolve(rt);
        }).catch((e) => {
            //console.log(e);
            console.log('    get_yahoo_data catch');
            resolve(false);
        });
    });
}

// scrape_yahoo
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let result = false;
            let lflg = true; // loop continue?

            if (mode == 'new') {
                yield init_table(tname,`FGS286_targets_${category}`, avrtable);
                let st = yield store_targets();
                if(!st){
                    console.log(`   store targets err`);
                    lflg = false;
                }
            }else{
                yield get_loop_count();
            }
            
            while(lflg){
                
                if (lcnt > maxloop) {
                    console.log(`   end  maxloop`);
                    lflg = false;
                    //result = true;

                }else{
                    if(lcnt < 3){
                        yield get_targets();
                    }else{
                        yield get_targets2();
                    }                    

                    if ( minnum >= keywords.length ) {
                        console.log(`   end  min keywords`);
                        lflg = false;
                        result = true;
                    }
                }

                // loop continue?
                if ( lflg ){
                    //console.log(` remain keywords: ${keywords.length}`);
                    getkwd.length = 0;
                    yield insertLog();
                    let rt = yield get_yahoo_data();
                    if(!rt) break;
                    yield updateLog();
                    lcnt++;

                    if (getkwd.length == 0){
                        zcnt++;
                        console.log(` get keywords: zero count: ${zcnt}`);
                        if(zcnt >= 3) {
                            lflg = false;
                            result = true;
                        }

                    }else{
                        console.log(` get keywords: ${getkwd.length} / ${keywords.length}`);
                        zcnt = 0;
                    }

                    if(keywords.length == getkwd.length) {
                        console.log(`   end scraping`);
                        lflg = false;
                        result = true;
                    }

                }

            }
            if(driver) driver.quit();

            if(result) yield setOutFile();

            resolve();
        }).catch((e) => {
            //console.log(e);
            console.log('scrape_main catch');
            resolve(false);
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select keyword, '' category, getdate, postdate, max(tweets) tweets, max(badfeel) badfeel, max(goodfeel) goodfeel
             from ${tname} where getdate = '${getdate_slash}' and postdate >= '${target_date}' and tweets >= 0 
             group by 1, 2, 3, 4 order by 1, 4;`;
            let res = yield db.do(`all`, sql, {});

            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(outfile, ([res[i].keyword, res[i].category, res[i].getdate, res[i].postdate, res[i].tweets, res[i].badfeel, res[i].goodfeel].join('\t') + '\n'), 'utf8');
            }

            yield outNGfile();
            yield outLog();
            yield end();

            resolve();
        //}).catch((e) => {
        //    console.log(e);
        });
    });
}

// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        yield db.connect(filePath_db);

        switch (mode) {

            case 'new':
            case 'continue':
                yield scrape_main();
                break;

            case 'output':
                yield setOutFile();
                break;

            default:
                break;
        }
        console.log(`/// FGS286-yahoo scraper.js end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    });
}

run();