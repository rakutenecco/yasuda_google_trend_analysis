'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS283 recipe Rakuten Weekly Sunday 0:10
new cronJob(`00 10 00 * * 0`, function () {
//new cronJob(`00 40 09 * * 1`, function () {

    // WEAR
    shell.exec(`node src/rakuten/FGS239-wear-posts/shell.js -r 1 -n cron10 -f targets_fashion.txt`);
//    shell.exec(`node src/rakuten/FGS239-wear-posts/shell.js -r 2 -n cron10 -f targets_fashion.txt -g 20191124`);
    
    // Rakuten
    shell.exec(`node src/rakuten/FGS283-recipe/shell.js -r 2 -b cron14_02`);

}, function () {
    // error
    console.log(`Error ${today} : FGS283 recipe Rakuten scrape error cron14_02`);
},
true, 'Asia/Tokyo');

console.log('cron14_02 setting');
