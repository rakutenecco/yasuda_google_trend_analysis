'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');
const moment = require('moment');
const fs = require('fs');
const conf = new (require('../../conf.js'));

const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number }, // 1:new 2:continue 9:output
    { name: 'site', alias: 's', type: Number }, // 1:cookpad 2:rakuten
    { name: 'cron', alias: 'n', type: String },
    { name: 'fname', alias: 'f', type: String },// target file name
]);

let runtype;
if(!cli['run'] || cli['run'] == 1){
    //mode = 'new';
    runtype = 1;
} else if (cli['run'] == 2){
    //mode = 'continue';
    runtype = 2;
} else if (cli['run'] == 9){
    //mode = 'output';
    runtype = 9;
}

const site = cli['site'] || 0;
if(site == 0){
    console.log(` -r cli['site'] null error`);
    return;
}else if (site != 1 && site != 2){
    console.log(` -r cli['site'] error : ${cli['site']}`);
    return;  
}

const maxretry = 10;
const cron = cli['cron'] || 'cron14';

const lastsunday = moment().day(0).format('YYYYMMDD');
const cookpadfile = `${conf.dirPath_share_vb}/FGS283_recipe_posts_cookpad_total_${lastsunday}.txt`;
const rakutenfile = `${conf.dirPath_share_vb}/FGS283_recipe_posts_rakuten_total_${lastsunday}.txt`;

let outfile;
if(site == 1) {
    outfile = cookpadfile;
} else { // site == 2 
    outfile = rakutenfile;
}

console.log('/// FGS283 recipe shell.js start ///');
console.log(`  runtype    : ${runtype}`);
console.log(`  site       : ${site}`);
console.log(`  DB cron    : ${cron}`);
console.log(`  lastsunday : ${lastsunday}`);
console.log(`  targetfile : ${cli['fname']}`);
console.log(`  outfile    : ${outfile}`);

// scrape recipe posts
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let lflg = true; // loop continue?
            let lcnt = 0;
            do{

                if (fs.existsSync(outfile)) {
                    console.log(`outfile exists: ${outfile}`);
                    lflg = false;

                }else{
                    lcnt++;
                    if( lcnt > maxretry ){                        
                        lflg = false;

                    }else{
                        if( lcnt > 1 ) runtype = 2;
                        console.log(` FGS283-recipe/shell.js scraper.js CALL lcnt: ${lcnt} runtype: ${runtype} site: ${site} ${cron} ${lastsunday} ${cli['fname']}}`);
                        shell.exec(`node src/rakuten/FGS283-recipe/scraper.js -r ${runtype} -s ${site} -n ${cron} -f ${cli['fname']}` );
                    }
                }

            }while(lflg)

            resolve();
        }).catch((e) => {
            console.log(e);
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {        
                 
        let st = moment().format('YYYY/MM/DD HH:mm:ss');

        switch (runtype) {

            case 1://'new':
            case 2://'continue':
                yield scrape_main();
                break;
            case 9://'output':
                shell.exec(`node src/rakuten/FGS283-recipe/scraper.js -r ${runtype} -s ${site} -n ${cron}` );
                break;

            default:
                break;
        }

        console.log(`/// FGS283-recipe shell.js -r ${runtype} -s ${site} ${cron} end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
        
    });
}

run();
