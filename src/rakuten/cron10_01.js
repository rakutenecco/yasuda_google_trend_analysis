'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
let yesterday;
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS239 insta posts weekly prod
new cronJob(`00 05 00 * * 0`, function () {
//new cronJob(`00 05 17 * * 0-2`, function () {

    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_01 -f targets_food_02_01.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_02 -f targets_watch.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_02 -f targets_wine.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_02 -f targets_flower.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_02 -f targets_interior.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_02 -f targets_life.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_03 -f targets_kitchen.txt`);
    shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_03 -f targets_fashion.txt`);

}, function () {
    // error
    console.log(`Error : FGS239 insta posts weekly prod  cron10_01`);
},
true, 'Asia/Tokyo');

console.log('cron10_01 setting');
