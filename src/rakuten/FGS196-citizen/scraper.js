'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');
const { Builder, By, Key, Capabilities, until, promise } = require('selenium-webdriver');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'browser', alias: 'B', type: String },
    { name: 'cron', alias: 'n', type: String },
]);

// variable
let driver = null;
const today = moment().format('YYYYMMDD');
//const insert_date = moment().format('YYYY/MM/DD HH:mm:ss');

const cron = cli['cron'] || 'cron2';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

// URLlist
const list = [

    ['citizen', 'エコ・ドライブ ワン', 'https://citizen.jp/one/index.html'],
    ['citizen', 'ザ・シチズン', 'https://citizen.jp/the-citizen/index.html'],
    ['citizen', 'サテライトウェーブ', 'https://citizen.jp//satellitewave-gps/index.html'],
    ['citizen', 'エクシード', 'https://citizen.jp/exceed/index.html'],
    ['citizen', 'アテッサ', 'https://citizen.jp/attesa/index.html'],
    ['citizen', 'プロマスター', 'https://citizen.jp/promaster/index.html'],
    ['citizen', 'シチズン エル', 'https://citizen.jp/citizen_l/index.html'],
    ['citizen', 'クロスシー', 'https://citizen.jp/xc/index.html'],
    ['citizen', 'キー', 'https://citizen.jp/kii/index.html'],
    ['citizen', 'シチズンコレクション', 'https://citizen.jp/collection/index.html'],
    ['citizen', 'エコ・ドライブ Bluetooth', 'https://citizen.jp/bluetooth/index.html'],

    ['CAMPANOLA', '', 'https://campanola.jp/collection/lineup.html'],
    ['WiCCA', '', 'https://wicca-w.jp/collection/index.html'],
    ['INDEPENDENT', '', 'https://independentwatch.com/collection/innovative_line/'],
    ['CLAB LA MER', '', 'https://club-la-mer.jp/'],

    ['REGUNO', 'メンズソーラー電波', 'https://reguno.jp/lineup/solar_rcw/std.html'],
    ['REGUNO', 'メンズソーラー', 'https://reguno.jp/lineup/solar/std.html'],
    ['REGUNO', 'レディスソーラー', 'https://reguno.jp/lineup/solar/lds.html'],
    ['REGUNO', 'ペアソーラー電波', 'https://reguno.jp/lineup/solar_rcw/pair.html'],
    ['REGUNO', 'ペアソーラー', 'https://reguno.jp/lineup/solar/pair.html'],
    ['REGUNO', 'ディズニーソーラー', 'https://reguno.jp/lineup/disney/index.html'],

    ['PaulSmith', 'メン', 'https://www.paulsmith.co.jp/shop/men/accessories/watches/products'],
    ['PaulSmith', 'ウィメン', 'https://www.paulsmith.co.jp/shop/women/accessories/watches/products'],
    ['MargaretHowell', '', 'https://citizen.jp/license/margarethowellidea/lineup/'],
    ['OutdoorProducts', '', 'https://citizen.jp/license/outdoorproducts/lineup/index.html'],

    ['QQ', '', 'https://qq-watch.jp/lineup/index.html'],

    // ['QQ', '電波コンビ', 'https://qq-watch.jp/lineup/combination_s/index.html'],
    // ['QQ', '電波アナログ', 'https://qq-watch.jp/lineup/analog_s/index.html'],
    // ['QQ', '電波デジタル', 'https://qq-watch.jp/lineup/digital_s/index.html'],
    // ['QQ', 'クロノマルチ', 'https://qq-watch.jp/lineup/chrono_multi_s/index.html'],
    // ['QQ', 'スタンスポ', 'https://qq-watch.jp/lineup/standard_s/index.html'],
    // ['QQ', 'スポーツ', 'https://qq-watch.jp/lineup/sports/index.html'],
    // ['QQ', 'キティ', 'https://qq-watch.jp/lineup/kitty/index.html'],
    // ['QQ', 'カラー', 'https://qq-watch.jp/lineup/color/index.html'],
    // ['QQ', 'ディズニー', 'https://qq-watch.jp/lineup/disney/index.html'],

    ['QQsmilesolar', 'series007', 'https://www.smile-qq.com/series007/index-jp.html'],
    ['QQsmilesolar', 'series006', 'https://www.smile-qq.com/series006/index-jp.html'],
    ['QQsmilesolar', 'series005', 'https://www.smile-qq.com/series005/index-jp.html'],
    ['QQsmilesolar', 'series004', 'https://www.smile-qq.com/series004/index-jp.html'],
    ['QQsmilesolar', 'newdesign2', 'https://www.smile-qq.com/newdesign2/index-jp.html'],
    ['QQsmilesolar', 'newdesign', 'https://www.smile-qq.com/newdesign/index-jp.html'],
    //['QQsmilesolar', 'smilesolar', 'https://www.smile-qq.com/smilesolar/jp/#/page/'],

    ['QQsmilesolar', 'mini-series004', 'https://www.smile-qq.com/mini-series004/index-jp.html'],
    ['QQsmilesolar', 'mini-newdesign2', 'https://www.smile-qq.com/mini-newdesign2/index-jp.html'],
    ['QQsmilesolar', 'mini-newdesign', 'https://www.smile-qq.com/mini-newdesign/index-jp.html'],
    //['QQsmilesolar', 'smilesolarmini', 'https://www.smile-qq.com/smilesolarmini/jp/#/page/'],
    
    ['QQsmilesolar', '20bar-series003', 'https://www.smile-qq.com/20bar-series003/index-jp.html'],
    ['QQsmilesolar', '20bar-series002', 'https://www.smile-qq.com/20bar-series002/index-jp.html'],
    ['QQsmilesolar', '20bar', 'https://www.smile-qq.com/20bar/index-jp.html'],

    ['QQsmilesolar', 'disney', 'https://www.smile-qq.com/disney001/index-jp.html'],

    ['QQsmilesolar', 'matching002', 'https://www.smile-qq.com/matchingstyleseries002/index-jp.html'],
    ['QQsmilesolar', 'matching001', 'https://www.smile-qq.com/matchingstyleseries001/index-jp.html'],

    ['QQsmilesolar', 'peanuts', 'https://www.smile-qq.com/peanutscollection/index-jp.html'],
    ['QQsmilesolar', 'pair-designs', 'https://www.smile-qq.com/pair-designs/index-jp.html'],
    ['QQsmilesolar', 'TheRedList', 'https://www.smile-qq.com/theredlist/jp/'],
    ['QQsmilesolar', 'TheGreatMen', 'https://www.smile-qq.com/thegreatmen/jp/'],
    ['QQsmilesolar', 'leitmotiv', 'https://www.smile-qq.com/leitmotiv/jp/'],
    //['QQsmilesolar', 'smilesolarthespice', 'https://www.smile-qq.com/smilesolarthespice/jp/#/page/'],
];

const insSql = `insert into fashion_fgs196_citizen values `;
//const pdcSql = `insert into fashion_fgs196_citizen_product values `;
const brand_id = 1297;
const maker = 'CITIZEN';

// scrape proxy data, and set db as proxy_list

let category1 = '';
let category2 = '';
let list_url = '';

let detail_url = '';
let model_name = '';

let brand_name = '';
let category_name = '';
let product_name = '';
let color = '';
let size = '';
let size_value = '';
let exc_price = '';
let inc_price = 0;

let num = 0;
let urlRD = [];
let dtlRD = [];
//let pdcRD = [];

let Ncnt = 0;
let stTime;

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // create table
            //sqlstr = `DROP TABLE IF EXISTS fashion_fgs196_citizen_category;`;
            //yield db.do(`run`, sqlstr, {});
            //sqlstr = `DROP TABLE IF EXISTS fashion_fgs196_citizen_product;`;
            //yield db.do(`run`, sqlstr, {});
            
            // sqlstr = `CREATE TABLE IF NOT EXISTS fashion_fgs196_citizen_category ` +
            //     `(Num INTEGER, getdate TEXT, category1 TEXT, category2 TEXT, model_name TEXT, cat_url TEXT, primary key(getdate,Num));`;
            // yield db.do(`run`, sqlstr, {});
            sqlstr = `CREATE TABLE IF NOT EXISTS fashion_fgs196_citizen ` +
                `(Num INTEGER, getdate TEXT, mpn TEXT, brand_name TEXT, product_name TEXT, color TEXT, size TEXT, size_value TEXT, ` + 
                `exc_price NUMERIC, inc_price NUMERIC, category_name TEXT, url TEXT, primary key( getdate, Num ));`;
            yield db.do(`run`, sqlstr, {});

            // old result delete
            const deldate = moment().add(-35, 'days').format('YYYYMMDD');
            console.log(` result deldate: ${deldate}`);
            sqlstr = `DELETE FROM fashion_fgs196_citizen WHERE getdate < '${deldate}' or getdate = '${today}';`;
            //console.log(sqlstr);
            yield db.do(`run`, sqlstr, {});
            // sqlstr = `DELETE FROM fashion_fgs196_citizen_product WHERE getdate < '${deldate}' or getdate = '${today}';`;
            // //console.log(sqlstr);
            // yield db.do(`run`, sqlstr, {});

            resolve();
        });
    });
}

const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < list.length; i++) {
                category1 = list[i][0];
                category2 = list[i][1];
                list_url = list[i][2];
                urlRD.length = 0;
                dtlRD.length = 0;
                //pdcRD.length = 0;
                //console.log(category1 + ' ' + category2);

                switch (category1) {
                    case 'citizen':
                        yield citizen();
                        break;
                    case 'CAMPANOLA':
                        yield CAMPANOLA();
                        break;
                    case 'WiCCA':
                        yield WiCCA();
                        break;
                    case 'INDEPENDENT':
                        yield INDEPENDENT();
                        break;
                    case 'CLAB LA MER':
                        yield ClubLaMer();
                        break;
                    case 'REGUNO':
                        yield REGUNO();
                        break;
                    case 'PaulSmith':
                        yield PaulSmith();
                        break;
                    case 'MargaretHowell':
                        yield MargaretHowell();
                        break;
                    case 'OutdoorProducts':
                        yield OutdoorProducts();
                        break;
                    case 'QQ':
                        yield QQ();
                        break;
                    case 'QQsmilesolar':
                        yield QQsmilesolar();
                        break;
                    default:
                        break;
                }
                if (dtlRD.length > 0) {
                    //console.log('  : category : ' + catRD.length + ' 件');
                    //console.log('sql=[' + catSql + catRD.join(',') + ']');
                    //yield db.do(`run`, catSql + catRD[0], {});
                    yield db.do(`run`, insSql + dtlRD.join(','), {});
                    yield util.sleep(1000);
                }
            }
            resolve();
        });
    });
}

// get fashion_fgs196_citizen_category
const getList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            //yield db.connect(filePath_db);
            let sql1 = `SELECT * FROM fashion_fgs196_citizen_category a ` +
                `WHERE not exists (select * from fashion_fgs196_citizen_product b where a.Num = b.Num and a.getdate = b.getdate ) ` +
                ` and a.getdate = '${today}';`;
            //console.log(sql1);
            let res1 = yield db.do(`all`, sql1, {});

            resolve(res1);
        });
    });
}

const scrapeProduct = function (clist) {
    //yield db.connect(filePath_db);

    return new Promise((resolve, reject) => {
        co(function* () {

            const clist = yield getList();
            if(clist.length > 0 ) {
                pdcRD.length = 0;
                console.log('citizen scrapeProduct : ' + clist.length + ' 件取得中・・・');
                for (let i = 0; i < clist.length; i++) {
                    num = clist[i].Num;
                    category1 = clist[i].category1;
                    category2 = clist[i].category2;
                    model_name = clist[i].model_name;
                    detail_url = clist[i].detail_url;

                    //pdcRD.length = 0;
                    //console.log(num);
                    //console.log(detail_url);
                    switch (category1) {
                        case 'citizen':
                            yield citizen_detail();
                            break;
                        case 'CAMPANOLA':
                            yield CAMPANOLA_detail();
                            break;
                        case 'WiCCA':
                            yield WiCCA_detail();
                            break;
                        case 'INDEPENDENT':
                            yield INDEPENDENT_detail();
                            break;
                        case 'CLAB LA MER':
                            yield ClubLaMer_detail();
                            break;
                        //case 'CARAVELLE':
                        //    yield CARAVELLE_detail();
                        //    break;
                        case 'REGUNO':
                            yield REGUNO_detail();
                            break;
                            // case 'PaulSmith':
                            //     yield PaulSmith_detail();
                            //     break;
                            // case 'MargaretHowell':
                            //     yield MargaretHowell_detail();
                            //     break;
                            // case 'BeautyAndYouth':
                            //     yield BeautyAndYouth_detail();
                            //     break;
                            // case 'OutdoorProducts':
                            //     yield OutdoorProducts_detail();
                            //     break;
                        case 'QQ':
                            yield QQ_detail();
                            break;
                            // case 'QQsmilesolar':
                            //     yield QQsmilesolar_detail();
                            //     break;
                        default:
                            break;
                    }
                }
                console.log('citizen scrapeProduct : ' + pdcRD.length + ' 件取得');
                if (pdcRD.length > 0) {
                    const n = 500;
                    //console.log('n = [' + n + ']');
                    let times = Math.floor(pdcRD.length / n);
                    //console.log('times = [' + times + ']');
                    //console.log('times * n = [' + times * n + ']');
                    if (times * n == pdcRD.length) times--;
                    let i = 0;
                    let end = 0;
                    do {
                        if (times <= i) {
                            end = pdcRD.length;
                        } else {
                            end = (i + 1) * n;
                        }
                        let st = i * n;
                        //console.log('st = [' + st + '] end = [' + end + ']');
                        let exc = pdcRD.slice(st, end);
                        //console.log(exc[0]);
                        //console.log(exc[exc.length -1]);
                        //console.log(pdcSql + exc.join(','));
                        yield db.do(`run`, pdcSql + exc.join(','), {});
                        i++;
                    } while (times >= i);
                }
            }
            resolve();
        });
    });
}

// citizen scrape
const citizen = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/lineup_inner/i)) {
                //console.log($('.main').find('.container').find('.lineup_inner').length);
                $('.main').find('.container').find('.lineup_inner').each(function (i, e) {
                    co(function* () {
                        //console.log(i);
                        //console.log($(e).find('a').attr('href'));
                        detail_url = 'https://citizen.jp' + $(e).find('a').attr('href');
                        model_name = $(e).find('.lineup_number').text();
                        //num++;
                        urlRD.push([category1, category2, model_name, detail_url]);
                        //yield pushListRD();
                    });
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield citizen_detail();
            }
            resolve();
        });
    });
}
// citizen detail scrape
const citizen_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //dtlRD.length = 0;
            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];

                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);
                //console.log($);
                if ($ && $('body').html().match(/main/i)) {
                    let e = $('.main').find('.container').eq(0);

                    brand_name = category2;
                    let pname1 = $(e).find('.product_pdBrand').eq(0).find('img').attr('alt');
                    let pname2 = $(e).find('.product_pdNumber').eq(0).text();
                    category_name = '';
                    size = '';
                    size_value = '';

                    if (!pname1) {
                        product_name = model_name;
                        exc_price = 0;
                        inc_price = 0;
                    } else {
                        product_name = pname1 + ' ' + pname2;
                        exc_price = $(e).find('.product_pdPrice').eq(0).text().replace(/[^0-9]/g, '') || 0;
                        inc_price = util.getPriceTax(exc_price);
                        let f = $('.featureDetail_left');
                        $(f).find('tr').each(function (i, e) {
                            let th = util.replace($(e).find('th').text());
                            let td = util.replace($(e).find('td').text());
                            if (th === '重量' && td.length > 0) {
                                size = '重量:' + td;
                            }
                            if (th === '厚み' && td.length > 0) {
                                if (size.length > 0) {
                                    size = size + ' 厚み:' + td;
                                } else {
                                    size = '厚み:' + td;
                                }
                                if ($(e).find('td').find('span').text().length > 0) {
                                    size = size.replace($(e).find('td').find('span').text(), '');
                                }
                            }
                            if (th === 'ケースサイズ' && td.length > 0) {
                                size = size + ' ケースサイズ:' + util.replace($(e).find('td').find('p').eq(0).text());
                            }
                        });
                        size_value = yield match.getSizeUnit(size);
                    }
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}

// CAMPANOLA scrape
const CAMPANOLA = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/series/i)) {
                //console.log($('#series').find('a').length);
                $('#series').find('a').each(function (i, e) {
                    co(function* () {
                        if ($(e).find('.cledit').length > 0) {
                            detail_url = 'https://campanola.jp/collection/' + $(e).attr('href');
                            model_name = '';
                            urlRD.push([category1, category2, model_name, detail_url]);
                            //num++;
                            //yield pushCatRD();
                        }
                    });
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield CAMPANOLA_detail();
            }
            resolve();
        });
    });
}
// CAMPANOLA detail scrape
const CAMPANOLA_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];
            
                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);
                yield util.sleep(1000);
                //console.log($);
                //if ($ && $('body').html().match(/prodactinner/i)) {
                if ($ && $.html().match(/prodactinner/i)) {
                    //console.log($('.main').find('container'));
                    let e = $('#prodactinner');

                    model_name = $(e).find('h2').text();
                    brand_name = category1;

                    let pname1 = $(e).find('.collectionname').text();
                    product_name = brand_name + ' ' + pname1 + ' ' + model_name;
                    category_name = '';
                    exc_price = $(e).find('.price').text().replace(/[^0-9]/g, '');
                    inc_price = util.getPriceTax(exc_price);

                    size = '';
                    size_value = '';
                    //weight,thickness,case size
                    let spec = $('.spec').text();
                    let st = 0;
                    if (spec.search('重さ') >= 0) {
                        st = spec.search('重さ');
                    } else if (spec.search('ケースサイズ') >= 0) {
                        st = spec.search('ケースサイズ');
                    }
                    let ed = spec.search('ケース素材', st);
                    if (st == 0) {
                        st = ed;
                    }
                    size = spec.substr(st, ed - st).replace(/[\n\r\t\(\) ,]/g, '');
                    //console.log(size);
                    size_value = yield match.getSizeUnit(size);
                    //console.log(size_value);
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}

// WiCCA scrape
const WiCCA = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/item/i)) {
                $('.item').each(function (i, e) {
                    co(function* () {
                        model_name = $(e).find('.number').text();
                        detail_url = 'https://wicca-w.jp' + $(e).find('a').attr('href').slice(2);
                        urlRD.push([category1, category2, model_name, detail_url]);
                        //num++;
                        //yield pushCatRD();
                    });
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield WiCCA_detail();
            }
            resolve();
        });
    });
}
// WiCCA detail scrape
const WiCCA_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];
                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);

                if ($ && $('body').html().match(/content/i)) {
                    let e = $('.content-inner.content-inner-r');

                    brand_name = category1;
                    product_name = brand_name + ' ' + model_name;
                    category_name = '';
                    exc_price = $(e).find('.price').text().replace(/[^0-9]/g, '');
                    inc_price = util.getPriceTax(exc_price);

                    size = '';
                    size_value = '';
                    let dt = [];
                    let dd = [];

                    $('.spec-tit').each(function (i, e) {
                        dt.push(util.replace($(e).text()));
                    });
                    $('.spec-text').each(function (i, e) {
                        dd.push(util.replace($(e).text()));
                    });
                    //console.log(dt);
                    //console.log(dd);
                    for (let i = 0; i < dt.length; i++) {
                        //console.log(dt[i]);
                        //console.log(dd[i]);
                        let t = util.replace(dt[i]);
                        let d = util.replace(dd[i]);
                        if (t === '厚み') {
                            size = '厚み:' + d;
                        }
                        if (t === 'ケースサイズ') {
                            size = size + ' ケースサイズ:' + d;
                        }
                    }
                    //console.log(size);
                    size_value = yield match.getSizeUnit(size);
                    //console.log(size_value);
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}
// INDEPENDENT scrape
const INDEPENDENT = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/collection/i)) {
                //console.log($('.collection').find('li').length);
                $('.collection').find('li').each(function (i, e) {
                    co(function* () {
                        detail_url = 'https://independentwatch.com' + $(e).find('a').eq(0).attr('href');
                        model_name = '';
                        urlRD.push([category1, category2, model_name, detail_url]);
                        //num++;
                        //yield pushCatRD();
                    });
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield INDEPENDENT_detail();
            }
            resolve();
        });
    });
}
// INDEPENDENT detail scrape
const INDEPENDENT_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];
                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);

                if ($ && $('body').html().match(/collectionDetailContentR/i)) {
                    let e = $('.collectionDetailContentR');

                    brand_name = category1;
                    category_name = '';
                    model_name = $(e).find('.serial').text();
                    exc_price = $(e).find('.serial').find('.price').text();
                    model_name = model_name.replace(exc_price, '');

                    let pname1 = $(e).find('.type').text().replace(/[\n\r\t]/g, '');
                    if (pname1.length == 0) {
                        product_name = brand_name + ' ' + model_name;
                    } else {
                        product_name = pname1 + ' ' + model_name;
                    }

                    exc_price = exc_price.replace(/[^0-9]/g, '');
                    inc_price = util.getPriceTax(exc_price);

                    size = '';
                    size_value = '';
                    //weight,thickness,case size
                    //console.log($('.infoBox tr').length);
                    $('.infoBox tr').each(function (i, e) {
                        let th = util.replace($(e).find('td').eq(0).text());
                        let td = util.replace($(e).find('td').eq(1).text());
                        if (th === '厚さ') {
                            size = '厚さ:' + td;
                        }
                        if (th === 'ケースサイズ') {
                            size = size + ' ケースサイズ:' + td;
                        }
                    });

                    //console.log(size);
                    size_value = yield match.getSizeUnit(size);
                    //console.log(size_value);
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}
// ClubLaMer scrape
const ClubLaMer = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/section-collection/i)) {
                //console.log($('#section-collection').find('li').length);
                $('#section-collection').find('li').each(function (i, e) {
                    co(function* () {
                        detail_url = 'https://club-la-mer.jp' + $(e).find('a').attr('href').slice(1);
                        model_name = $(e).find('p').text();
                        urlRD.push([category1, category2, model_name, detail_url]);
                        //num++;
                        //yield pushCatRD();
                    });
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield ClubLaMer_detail();
            }
            resolve();
        });
    });
}
// ClubLaMer detail scrape
const ClubLaMer_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];
                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);

                if ($ && $('body').html().match(/main/i)) {
                    let e = $('#main');

                    brand_name = category1;
                    product_name = brand_name + ' ' + model_name;
                    category_name = '';
                    exc_price = $(e).find('.price').text().replace(/[^0-9]/g, '');
                    inc_price = util.getPriceTax(exc_price);
                    size = '';
                    size_value = '';

                    //weight,thickness,case size
                    let dt = [];
                    let dd = [];

                    $('.spec dt').each(function (i, e) {
                        dt.push(util.replace($(e).text()));
                    });
                    $('.spec dd').each(function (i, e) {
                        dd.push(util.replace($(e).text()));
                    });
                    for (let i = 0; i < dt.length; i++) {
                        if (dt[i] === 'ケース外径') {
                            size = 'ケース外径:' + dd[i];
                        }
                        if (dt[i] === 'ケース') {
                            size = dt[i].replace(/[\n\r\t ]/g, '') + dd[i].replace(/[\n\r\t ]/g, '');
                        }
                    }
                    size_value = yield match.getSizeUnit(size);
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}
// CARAVELLE scrape
const CARAVELLE = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/grid-uniform/i)) {
                //console.log($('.grid-uniform').find('.grid-product__wrapper').length);
                $('.grid-uniform').find('.grid-product__wrapper').each(function (i, e) {
                    co(function* () {
                        detail_url = 'https://www.caravellewatches.com' + $(e).find('a').attr('href');
                        model_name = $(e).find('.grid-product__title').find('span').text().replace(/[\n\r\t\(\) ,]/g, '');
                        //console.log(model_name.search('-'));
                        if (model_name.search('-') >= 0) {
                            model_name = model_name.substr(0, model_name.search('-'));
                        }
                        //console.log(model_name);
                        num++;
                        yield pushCatRD();
                    });
                });
            }
            resolve();
        });
    });
}
// CARAVELLE detail scrape
const CARAVELLE_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(detail_url, 3);

            if ($ && $('body').html().match(/product-single__meta/i)) {
                let e = $('.product-single__meta');

                brand_name = category1;
                product_name = brand_name + ' ' + model_name;
                category_name = category2;
                exc_price = 0;
                inc_price = 0;
                size = '';
                size_value = '';

                //weight,thickness,case size
                //console.log($(e).find('.product-single__description.rte li').length);
                $('.product-single__description.rte li').each(function (i, e) {
                    //console.log($(e).text());
                    if ($(e).text().search('Case Diameter: ') >= 0) {
                        size = 'ケース径:' + $(e).text().substr(15).replace(/[\n\r\t\(\) ,]/g, '');
                    }
                    if ($(e).text().search('Case Thickness: ') >= 0) {
                        size = size + ' ケース厚:' + $(e).text().substr(16).replace(/[\n\r\t\(\) ,]/g, '');
                    }
                });
                //console.log(size);
                size_value = yield match.getSizeUnit(size);
                //console.log(size_value);
                yield pushPdcRD();
            }
            resolve();
        });
    });
}
// REGUNO scrape
const REGUNO = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //console.log($('body').html());
            if ($ && $('body').html().match(/productListBlock01/i)) {
                //console.log($('.grid-uniform').find('.grid-product__wrapper').length);
                $('.productListBlock01').find('.col').each(function (i, e) {
                    co(function* () {
                        detail_url = 'https://reguno.jp' + $(e).find('a').attr('href');
                        model_name = $(e).find('.name').text().replace(/[\n\r\t\(\) ,]/g, '');
                        urlRD.push([category1, category2, model_name, detail_url]);
                        //console.log(model_name);
                        //num++;
                        //yield pushCatRD();
                    });
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield REGUNO_detail();
            }
            resolve();
        });
    });
}
// REGUNO detail scrape
const REGUNO_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];
                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);

                if ($ && $('body').html().match(/productDetailBlock01/i)) {
                    let e = $('.productDetailBlock01');

                    brand_name = category1;
                    category_name = category2;
                    product_name = brand_name + ' ' + category_name + ' ' + model_name;
                    exc_price = $(e).find('.price').text().replace(/[^0-9]/g, '');
                    inc_price = util.getPriceTax(exc_price);
                    size = '';
                    size_value = '';

                    //weight,thickness,case size
                    //console.log($('.table01 tr').length);
                    $('.table01 tr').each(function (i, e) {
                        let th = util.replace($(e).find('th').text());
                        let td = util.replace($(e).find('td').text());
                        if (th === '重さ') {
                            size = '重さ:' + td;
                        }
                        if (th === '厚み') {
                            if (size.length > 0) {
                                size = size + ' 厚み:' + td;
                            } else {
                                size = '厚み:' + td;
                            }
                        }
                        if (th === 'ケースサイズ') {
                            size = size + ' ケースサイズ:' + td;
                        }
                    });
                    //console.log(size);
                    size_value = yield match.getSizeUnit(size);
                    //console.log(size_value);
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}
// PaulSmith scrape
const PaulSmith = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            //let pg = [];
            if ($ && $('body').html().match(/product_list/i)) {
                $('.list_wrap').eq(0).find('.txt_item_name').each(function (i, e) {
                    detail_url = 'https://www.paulsmith.co.jp' + $(e).find('a').attr('href');
                    //model_name = $(e).text().replace(/[\n\r\t ]/g, '');
                    urlRD.push([category1, category2, model_name, detail_url]);
                });
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} ${category2} : ${urlRD.length} 件`);
                yield PaulSmith_detail();
            }

            resolve();
        });
    });
}
// PaulSmith detail scrape
const PaulSmith_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];

                let $ = yield cheerio.get(detail_url, 3);
                //console.log($);

                if ($ && $('body').html().match(/product_detail_list/i)) {
                    let e = $('.product_detail_list');

                    // brand name
                    brand_name = category1;
                    //商品名
                    let pname = $(e).find('.product_name').text();
                    //console.log(`pname:${pname}`);
                    //型番
                    let mname = $(e).find('.acdn_contens').eq(0).text();
                    if (mname.search('番」') > 0) {
                        mname = mname.slice(mname.search('番」') + 2);
                        if (mname.search('※') > 0) {
                            mname = mname.substr(0, mname.search('※')).replace(/[\n\r\t\(\) 　：）,]/g, '');
                        } else {
                            mname = mname.replace(/[\n\r\t\(\) 　：）,]/g, '');
                        }

                        mname = mname.replace('カラー450', 'グリーン').replace('カラー990', 'ブラック');
                        mname = mname.replace('カラー350フローラル', 'ブラウン').replace('カラー750スワール', 'マルチカラー');
                        mname = mname.replace('カラー150', 'ブルー').replace('カラー750', 'オレンジ');
                        mname = mname.replace('BH7-261', 'ホワイトBH7-261パープルBH7-261');
                        mname = mname.replace('シルバーBC5-415-90', 'ホワイトBC5-415-90');
                        mname = mname.replace('バーガンディBC5-423-90', 'バーガンディーBC5-423-90');
                        mname = mname.replace('シルバーBT2-611-94', 'ホワイトBT2-611-94');
                        mname = mname.replace('バーガンディBT2-629-90', 'バーガンディーBT2-629-90');

                    } else {
                        mname = '';
                    }
                    //console.log(`mname:${mname}`);

                    //品番 モデル
                    let note = $(e).find('.note').find('p').eq(0).text().replace(/[ 　]/g, '').replace('品番:', '').replace('モデル:', '');
                    //console.log(`note:${note}`);

                    category_name = category2;

                    // price
                    exc_price = $(e).find('.product_price').text().replace(/[^0-9]/g, '');
                    inc_price = util.getPriceTax(exc_price);
                    size = '';
                    size_value = '';

                    //weight,thickness,case size
                    let dt = [];
                    let dd = [];

                    $(e).find('.odd td').each(function (i, e) {
                        dt.push(util.replace($(e).text()));
                    });
                    $(e).find('.even td').each(function (i, e) {
                        dd.push(util.replace($(e).text()));
                    });
                    //console.log(dt);
                    //console.log(dd);
                    for (let i = 0; i < dt.length; i++) {
                        if (size.length == 0) {
                            size = dt[i] + ':' + dd[i];
                        } else {
                            size = size + ' ' + dt[i] + ':' + dd[i];
                        }
                    }
                    //console.log(size);
                    size_value = yield match.getSizeUnit(size);
                    //console.log(size_value);

                    //color
                    $(e).find('.slider.cfx').find('.product_color').each(function (i, e) {
                        co(function* () {
                            color = $(e).text();
                            let ktban;

                            if (mname.search(color) >= 0) {
                                let st = mname.search(color) + color.length;
                                let ed = mname.slice(st).search(/[^A-Z0-9-]/);
                                if (ed >= 0) {
                                    ktban = mname.substr(st,ed);
                                } else {
                                    ktban = mname.slice(st);
                                }
                            } else {
                                ktban = mname;
                            }
                            model_name = ktban + ' ' + color;
                            //console.log(model_name);
                            product_name = brand_name + ' ' + pname + ' ' + model_name + ' ' + note;
                            //console.log(product_name);
                            //num++;
                            yield push_dtlRD();
                        });
                    });

                }
            }
            resolve();
        });
    });
}

// MargaretHowell scrape
const MargaretHowell = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // open url
            driver = yield selenium.init(cli['browser']);
            driver.manage().window().setSize(1000,600);
            driver.get(list_url);
            yield util.sleep(3000);
            let ele1 = yield selenium.getElements(driver, '.swiper-wrapper');

            //product list
            let pdc_arr = []; //all item
            let ele2 = yield selenium.getElements(ele1[0], '.swiper-slide');
            let ele2_2 = yield selenium.getElements(ele2[0], 'a');
            let st_pdc = yield selenium.getAttr(ele2_2[0], 'data-product');
            pdc_arr.push(st_pdc);
            let i = 0;
            let loop = true;
            let n = 0;
            do {
                i++;
                let ele2_a = yield selenium.getElements(ele2[i], 'a');
                let ele2_pdc = yield selenium.getAttr(ele2_a[0], 'data-product');

                if (st_pdc == ele2_pdc) {
                    loop = false;
                } else {
                    pdc_arr.push(ele2_pdc);
                }
            } while (loop);

            //product open
            let ele2_op = yield selenium.getElements(ele1[0], '.swiper-slide-visible');
            let ele_pd; //#productDetail
            //let j = 1;
            let j = 0;
            while (j < pdc_arr.length) {
                //console.log(j+1 + '個め ');
                if (ele2_op.length <= j) {

                    //leftbtn click
                    let ele3 = yield selenium.getElements(driver, '#leftbtn');
                    ele3[0].click();
                    yield util.sleep(2000);
                    let ele4 = yield selenium.getElements(driver, '.swiper-wrapper');
                    let ele4_op = yield selenium.getElements(ele4[0], '.swiper-slide-visible');

                    //product open
                    ele4_op[0].click();
                    yield util.sleep(2000);
                    let ele4_op_d = yield selenium.getElements(ele4_op[0], '#productDetail');

                    let ele4_op_p = yield selenium.getElements(ele4_op_d[0], 'h2'); //h2
                    let tmp_h2 = yield selenium.getText(ele4_op_p[0]);
                    let tmp = yield selenium.getElements(ele4_op_p[0], 'small'); //small
                    let tmp_small = yield selenium.getText(tmp[0]);

                    let str = tmp_h2.replace(tmp_small, '').split(' ');
                    n = n + 1;
                    yield M_H_pushCatPdcRD(str);

                    ele_pd = ele4_op_d;

                } else {
                    //product open

                    ele2_op[j].click();
                    //ele2_op[j+1].click();
                    yield util.sleep(2000);
                    let ele2_op_d = yield selenium.getElements(ele2_op[j], '#productDetail');

                    let ele2_op_p = yield selenium.getElements(ele2_op_d[0], 'h2'); //h2
                    let tmp_h2 = yield selenium.getText(ele2_op_p[0]);
                    let tmp = yield selenium.getElements(ele2_op_d[0], 'small'); //small
                    let tmp_small = yield selenium.getText(tmp[0]);

                    let str = tmp_h2.replace(tmp_small, '').split(' ');
                    n = n + 1;
                    yield M_H_pushCatPdcRD(str);

                    ele_pd = ele2_op_d;
                }

                //color-variations product open
                let ele_ta = yield selenium.getElements(ele_pd[0], '.thumbarea'); //thumbarea
                let ele_talist = yield selenium.getElements(ele_ta[0], '.thumb-link'); //thumb-link
                for (let k = 0; k < ele_talist.length; k++) {

                    //product open
                    ele_talist[k].click();
                    yield util.sleep(500);
                    let ele_h2 = yield selenium.getElements(ele_pd[0], 'h2'); //h2
                    let h2_txt = yield selenium.getText(ele_h2[0]);
                    let ele_smal = yield selenium.getElements(ele_pd[0], 'small'); //small
                    let smal_txt = yield selenium.getText(ele_smal[0]);
                    let str = h2_txt.replace(smal_txt, '').split(' ');

                    n = n + 1;
                    yield M_H_pushCatPdcRD(str);

                }
                //closebtn click
                let ele_cl = yield selenium.getElements(ele_pd[0], '.closebtnlink'); //closebtnlink
                ele_cl[0].click();
                yield util.sleep(2000);

                j++;
            }
            driver.quit();
            if( n > 0 ) {
                console.log(`  ${category1} : ${n} 件`);
            }
            resolve();
        });
    });
}
// MargaretHowell pushCatPdcRD
const M_H_pushCatPdcRD = function (str) {

    return new Promise((resolve, reject) => {

        co(function* () {
            let mname = str[0].replace(/[\n\r\t\(\) ]/g, '');
            let prc = str[1].replace(/[^0-9]/g, '');
            category_name = '';
            detail_url = list_url;
            model_name = mname;
            brand_name = category1;
            product_name = brand_name + ' ' + model_name;
            exc_price = prc;
            inc_price = util.getPriceTax(exc_price);
            color = '';
            size = '';
            size_value = '';
            //num++;
            //console.log(model_name + ' ' + exc_price );
            yield push_dtlRD();
        });
        resolve();
    });
};


// BeautyAndYouth scrape
const BeautyAndYouth = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let ctgly = [];

            $('.lineupListSeriesClm7.for-pc.clearfix').find('li').each(function (i, e) {
                let c_url = $(e).find('a').attr('href');
                let c_nm = $(e).text().replace(/[\n\r\t\(\)]/g, '');
                ctgly.push([c_nm, 'https://citizen.jp' + c_url]);
            });
            let p = 0;
            do {
                if ($ && $('body').html().match(/container clearfix/i)) {
                    $('.container.clearfix').find('.lineup_inner').each(function (i, e) {

                        co(function* () {
                            category_name = ctgly[p][0];
                            detail_url = 'https://citizen.jp' + $(e).find('a').attr('href');
                            model_name = $(e).find('.lineup_number').text().replace(/[\n\r\t\(\) ]/g, '').replace('限定モデル', '');
                            product_name = category_name + ' ' + model_name;
                            brand_name = category1;
                            //console.log(model_name);
                            exc_price = $(e).find('.lineup_price').text().replace(/[^0-9]/g, '');
                            inc_price = util.getPriceTax(exc_price);
                            size = '';
                            size_value = '';
                            //num++;
                            yield pushCatPdcRD();
                        });
                    });
                }
                p++;
                if (p < ctgly.length) {
                    $ = yield cheerio.get(ctgly[p][1], 3);
                }
            } while (p < ctgly.length);

            resolve();
        });
    });
}

// OutdoorProducts scrape
const OutdoorProducts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/container clearfix/i)) {
                $('.container.clearfix').find('.column').each(function (i, e) {

                    co(function* () {
                        category_name = '';
                        detail_url = 'https://citizen.jp' + $(e).find('a').attr('href');
                        model_name = $(e).find('.lineup_number').text();
                        brand_name = category1;
                        product_name = brand_name + ' ' + model_name;
                        //console.log(model_name);
                        exc_price = $(e).find('.lineup_price').text().replace(/[^0-9]/g, '');
                        inc_price = util.getPriceTax(exc_price);
                        size = '';
                        size_value = '';
                        n = n + 1;
                        yield push_dtlRD();
                    });
                });
            }
            console.log(`  ${category1} : ${n} 件`);
            resolve();
        });
    });
}
// QQ scrape
const QQ = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);

            let ctgly = [];
            //console.log($('#centents').find('a').length);
            $('#centents').find('a').each(function (i, e) {
                let c_url = $(e).attr('href');
                let c_nm = $(e).find('img').attr('alt');
                ctgly.push([c_nm, 'https://qq-watch.jp/lineup/' + c_url]);
            });
            //console.log(ctgly);

            for (let p = 0; p < ctgly.length; p++) {
                $ = yield cheerio.get(ctgly[p][1], 3);
                if ($ && $('body').html().match(/lineup/i)) {
                    //console.log($('#lineup').find('li').length);
                    $('#lineup').find('li').each(function (i, e) {

                        co(function* () {
                            category2 = ctgly[p][0];
                            model_name = $(e).find('.number').text().replace(/[\n\r\t\(\) ]/g, '');

                            let tmp = $(e).find('a').attr('onclick');
                            let st = tmp.indexOf('window.open(') + 13;
                            let ed = tmp.indexOf('.html') + 5;
                            //console.log(tmp.substr(st, ed - st ));
                            detail_url = ctgly[p][1].replace('index.html', '') + tmp.substr(st, ed - st);
                            urlRD.push([category1, category2, model_name, detail_url]);
                            //console.log(detail_url);

                        });
                    });
                }
            }
            if(urlRD.length > 0 ) {
                console.log(`  ${category1} total : ${urlRD.length} 件`);
                yield QQ_detail();
            }
            resolve();
        });
    });
}
// QQ detail scrape
const QQ_detail = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < urlRD.length; i++) {
                //num = i + 1;
                category1 = urlRD[i][0];
                category2 = urlRD[i][1];
                model_name = urlRD[i][2];
                detail_url = urlRD[i][3];
                //yield cheerio.init(10000);
                let $ = yield cheerio.get(detail_url, 3);

                if ($ && $('body').html().match(/popup_contents/i)) {
                    let e = $('#popup_contents');

                    brand_name = category1;
                    category_name = category2;
                    product_name = brand_name + ' ' + category_name + ' ' + model_name;
                    exc_price = $(e).find('.price').text().replace(/[^0-9]/g, '');
                    exc_price = exc_price ? exc_price : 0;
                    inc_price = util.getPriceTax(exc_price);
                    size = '';
                    size_value = '';

                    //weight,thickness,case size
                    //console.log($('.table').eq(0).find('tr').length);
                    $('.table').eq(0).find('tr').each(function (i, e) {
                        let th = util.replace($(e).find('th').text());
                        let td = util.replace($(e).find('td').text());
                        if (th === '重量') {
                            size = '重量:' + td;
                        }
                        if (th === '厚み') {
                            if (size.length > 0) {
                                size = size + ' 厚み:' + td;
                            } else {
                                size = '厚み:' + td;
                            }
                        }
                    });
                    //console.log(size);
                    size_value = yield match.getSizeUnit(size);
                    //console.log(size_value);
                    yield push_dtlRD();
                }
            }
            resolve();
        });
    });
}

// QQsmilesolar scrape
const QQsmilesolar = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            switch (category2) {

                case 'series007':
                    yield series007();
                    break;
                case 'series006':
                    yield series006();
                    break;
                case 'series005':
                    yield series005();
                    break;
                case 'series004':
                    yield series004();
                    break;
                case 'newdesign2':
                    yield newdesign2();
                    break;
                case 'newdesign':
                    yield newdesign();
                    break;

                case 'mini-series004':
                    yield mini_series004();
                    break;
                case 'mini-newdesign2':
                    yield mini_newdesign2();
                    break;
                case 'mini-newdesign':
                    yield mini_newdesign();
                    break;

                case '20bar-series003':
                    yield bar20_series003();
                    break;
                case '20bar-series002':
                    yield bar20_series002();
                    break;
                case '20bar':
                    yield bar20();
                    break;

                case 'disney':
                    yield disney();
                    break;

                case 'matching002':
                    yield matching002();
                    break;
                case 'matching001':
                    yield matching001();
                    break;

                case 'peanuts':
                    yield peanuts();
                    break;
                case 'pair-designs':
                    yield pair_designs();
                    break;
                case 'TheRedList':
                    yield TheRedList();
                    break;
                case 'TheGreatMen':
                    yield TheGreatMen();
                    break;
                case 'leitmotiv':
                    yield leitmotiv();
                    break;
                case 'smilesolarthespice':
                    yield smilesolarthespice();
                    break;

                default:
                    break;
            }
            resolve();
        });
    });
}

// series007 scrape
const series007 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {

                $('.watch').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        
                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        size = tmp.substr(st, ed - st);
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });

                        let st2 = tmp.search('価格：');
                        exc_price = tmp.slice(st2).replace(/[^0-9]/g, '') || 0;
                        inc_price = util.getPriceTax(exc_price);
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// series006 scrape
const series006 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {

                //console.log($('#wrapper').find('.watch.fade-up').length);
                $('#wrapper').find('.watch.fade-up').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.name').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// series005 scrape
const series005 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/manual-inner/i)) {
                //console.log($('article').length);
                co(function* () {
                    let tmp = $('#manual-inner').find('.text').text();
                    let st = tmp.search('ケース直径');
                    let ed = tmp.search('ケース素材');
                    tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                    //console.log(tmp);

                    //console.log($('.wlist').find('div').length);
                    $('.wlist').find('div').each(function (i, e) {

                        co(function* () {
                            model_name = $(e).text().replace(/[\n\r\t\(\) ]/g, '');
                            detail_url = list_url;
                            brand_name = category1;
                            category_name = category2;
                            product_name = brand_name + ' ' + category_name + ' ' + model_name;
                            exc_price = 0;
                            inc_price = 0;
                            size = tmp;
                            co(function* () {
                                size_value = yield match.getSizeUnit(size);
                            });
                            n = n + 1;
                            yield push_dtlRD();

                        });
                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// series004 scrape
const series004 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/list/i)) {

                //console.log($('.list').find('.spec').length);
                $('.list').find('.spec').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t\(\) ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.cledit').text();
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                        //console.log(tmp);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// newdesign2 scrape
const newdesign2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/list/i)) {

                //console.log($('.list').find('.spec').length);
                $('.list').find('.spec').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t\(\) ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.cledit').text();
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                        //console.log(tmp);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// newdesign scrape
const newdesign = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/fullpage/i)) {
                //console.log($('article').length);
                co(function* () {
                    //console.log($('.section').find('.spec').text());
                    let tmp = $('.section').find('.spec').text();
                    let st = tmp.search('ケース直径');
                    let ed = tmp.search('ケース素材');
                    tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                    //console.log(tmp);

                    //console.log($('#fullpage').find('.intro').length);
                    $('#fullpage').find('.intro').each(function (i, e) {

                        co(function* () {
                            model_name = $(e).find('h1').text().replace(/[\n\r\t\(\) ]/g, '');
                            detail_url = list_url;
                            brand_name = category1;
                            category_name = category2;
                            product_name = brand_name + ' ' + category_name + ' ' + model_name;
                            exc_price = 0;
                            inc_price = 0;
                            size = tmp;
                            co(function* () {
                                size_value = yield match.getSizeUnit(size);
                            });
                            n = n + 1;
                            yield push_dtlRD();

                        });
                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// smilesolar scrape no data

// mini-series004 scrape
const mini_series004 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {

                //console.log($('#wrapper').find('.innerinfo').length);
                $('#wrapper').find('.innerinfo').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.name').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// mini_newdesign2 scrape
const mini_newdesign2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/list/i)) {

                //console.log($('.list').find('.spec').length);
                $('.list').find('.spec').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t\(\) ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.cledit').text();
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                        //console.log(tmp);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// mini_newdesign scrape
const mini_newdesign = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/article/i)) {

                //console.log($('.section').find('.texts').length);
                $('.section').find('.texts').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('p').text().replace(/[\n\r\t\(\) ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).text();
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                        //console.log(tmp);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// smilesolarmini scrape no data


// 20bar-series003 scrape
const bar20_series003 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {

                $('.watch').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        
                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        size = tmp.substr(st, ed - st);
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });

                        let st2 = tmp.search('価格：');
                        let ed2 = tmp.search('ケース直径');
                        let tmp2 = tmp.substr(st2, ed2 - st2);
                        exc_price = tmp2.replace(/[^0-9]/g, '') || 0;
                        inc_price = util.getPriceTax(exc_price);
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// 20bar-series002 scrape
const bar20_series002 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/list/i)) {

                //console.log($('.list').find('.spec').length);
                $('.list').find('.spec').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t\(\) ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.cledit').text();
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                        //console.log(tmp);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// bar20 scrape
const bar20 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {
                co(function* () {
                    //weight,thickness,case size
                    let mdl = [];
                    let idx = 0;
                    let spc = [];

                    $('#lineup').find('p').each(function (i, e) {
                        mdl.push($(e).text());
                    });
                    //console.log(mdl);

                    let tmp = $('#lineup').text().replace(/[\n\r\t\(\) ]/g, '');
                    //console.log(tmp);
                    idx = tmp.indexOf('ケース直径');
                    while (idx >= 0) {
                        let str = tmp.substr(idx, tmp.indexOf('ケース素材', idx) - idx);
                        spc.push(str);
                        idx = tmp.indexOf('ケース直径', idx + 1);
                    }
                    //console.log(spc);

                    //console.log($('.ms-section').find('h1').length);
                    $('.ms-section').find('h1').each(function (i, e) {

                        co(function* () {
                            model_name = $(e).text().replace(/[\n\r\t\(\) ]/g, '');
                            detail_url = list_url;
                            brand_name = category1;
                            category_name = category2;
                            product_name = brand_name + ' ' + category_name + ' ' + model_name;
                            exc_price = 0;
                            inc_price = 0;

                            for (let s = 0; s < mdl.length; s++) {
                                if (mdl[s].indexOf(model_name.substr(0, 4)) >= 0) {
                                    size = spc[s];
                                }
                            }
                            //console.log(size);
                            co(function* () {
                                size_value = yield match.getSizeUnit(size);
                            });
                            n = n + 1;
                            yield push_dtlRD();

                        });
                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}

// disney scrape
const disney = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/watchelist/i)) {

                let tmp = $('#text_disney').text().replace(/[\n\r\t ]/g, '');
                let st = tmp.search('価格：');
                let ed = tmp.search('ケース素材');
                let tmp2 = tmp.substr(st, ed - st);
                exc_price = tmp2.replace(/[^0-9]/g, '') || 0;
                inc_price = util.getPriceTax(exc_price);

                detail_url = list_url;
                brand_name = category1;
                category_name = category2;

                //.indexOf(') ');'#watchelist').find('#watch').length);
                $('.ma').each(function (i, e) {

                    co(function* () {
                        let name = $(e).find('.name').text();
                        let number = $(e).find('.number');   //arr
                        let spec = $(e).find('.spec');       //arr
                    
                        let numtxt = number.eq(0).text();
                        let numst = numtxt.indexOf(') ');
                        model_name = numtxt.slice(numst + 2);
                        product_name = brand_name + ' ' + category_name + ' ' + name + ' ' + numtxt;
                        size = spec.eq(0).text();
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        co(function* () {
                            yield push_dtlRD();
                        });

                        let numtxt2 = number.eq(1).text();
                        let numst2 = numtxt2.indexOf(') ');
                        model_name = numtxt2.slice(numst2 + 2);
                        product_name = brand_name + ' ' + category_name + ' ' + name + ' ' + numtxt2;
                        size = spec.eq(1).text();
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        co(function* () {
                            yield push_dtlRD();
                        });

                    });
                });


                //console.log($('#watchelist').find('#watch').length);
                $('#watchelist').find('#watch').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        tmp = tmp.substr(st, ed - st);
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}

// matching002 scrape
const matching002 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/watchelist/i)) {

                $('.textsinner').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        
                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        size = tmp.substr(st, ed - st);
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });

                        let st2 = tmp.search('価格：');
                        let ed2 = tmp.search('ケース直径');
                        let tmp2 = tmp.substr(st2, ed2 - st2);
                        exc_price = tmp2.replace(/[^0-9]/g, '') || 0;
                        inc_price = util.getPriceTax(exc_price);

                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}

// matching001 scrape
const matching001 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/watches/i)) {

                $('.watch').each(function (i, e) {

                    co(function* () {
                        let tmpsize = $(e).find('.size').text().replace(/[\n\r\t ]/g, '');
                        model_name = $(e).find('.number').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + tmpsize + ' ' + model_name;
                        
                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('価格');
                        size = tmp.substr(st, ed - st);
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });

                        let st2 = tmp.search('価格');
                        let tmp2 = tmp.slice(st2);
                        exc_price = tmp2.replace(/[^0-9]/g, '') || 0;
                        inc_price = util.getPriceTax(exc_price);

                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}

// peanuts scrape
const peanuts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/watchelist/i)) {

                $('.textsinner').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('.number').text().replace(/[\n\r\t ]/g, '');
                        detail_url = list_url;
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        
                        let tmp = $(e).find('.spec').text().replace(/[\n\r\t ]/g, '');
                        let st = tmp.search('ケース直径');
                        let ed = tmp.search('ケース素材');
                        size = tmp.substr(st, ed - st);
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });

                        let st2 = tmp.search('価格：');
                        let ed2 = tmp.search('ケース直径');
                        let tmp2 = tmp.substr(st2, ed2 - st2);
                        exc_price = tmp2.replace(/[^0-9]/g, '') || 0;
                        inc_price = util.getPriceTax(exc_price);

                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// pair-designs scrape
const pair_designs = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/list/i)) {

                //console.log($('.wlists').find('.wlist').length);
                $('.wlists').find('.wlist').each(function (i, e) {

                    co(function* () {
                        model_name = $(e).find('p').text().replace(/[\n\r\t ]/g, '');
                        brand_name = category1;
                        category_name = category2;
                        product_name = brand_name + ' ' + category_name + ' ' + model_name;
                        model_name = model_name.substr(0, model_name.length - model_name.indexOf('(') + 1)
                        detail_url = list_url;
                        exc_price = 0;
                        inc_price = 0;

                        let tmp = $(e).text().replace(/[\n\r\t ]/g, '').replace(model_name, '');
                        size = tmp;
                        co(function* () {
                            size_value = yield match.getSizeUnit(size);
                        });
                        n = n + 1;
                        yield push_dtlRD();

                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// TheRedList scrape
const TheRedList = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {
                //console.log($('article').length);
                co(function* () {
                    let tmp = $('.texts').text();
                    let st = tmp.search('ケース直径');
                    let ed = tmp.search('ケース素材');
                    tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                    //console.log(tmp);

                    //console.log($('#contnt').find('.photo').length);
                    $('#contnt').find('.photo').each(function (i, e) {

                        co(function* () {
                            model_name = $(e).text().replace(/[\n\r\t\(\) ]/g, '');
                            detail_url = list_url;
                            brand_name = category1;
                            category_name = category2;
                            product_name = brand_name + ' ' + category_name + ' ' + model_name;
                            exc_price = 0;
                            inc_price = 0;
                            size = tmp;
                            co(function* () {
                                size_value = yield match.getSizeUnit(size);
                            });
                            n = n + 1;
                            yield push_dtlRD();

                        });
                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// TheGreatMen scrape
const TheGreatMen = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/wrapper/i)) {
                //console.log($('article').length);
                co(function* () {
                    let tmp = $('.texts').text();
                    let st = tmp.search('ケース直径');
                    let ed = tmp.search('ケース素材');
                    tmp = tmp.substr(st, ed - st).replace(/[\n\r\t\(\) ]/g, '');
                    //console.log(tmp);

                    //console.log($('#contnt').find('.photo').length);
                    $('#contnt').find('.photo').each(function (i, e) {

                        co(function* () {
                            model_name = $(e).text().replace(/[\n\r\t\(\) ]/g, '');
                            detail_url = list_url;
                            brand_name = category1;
                            category_name = category2;
                            product_name = brand_name + ' ' + category_name + ' ' + model_name;
                            exc_price = 0;
                            inc_price = 0;
                            size = tmp;
                            co(function* () {
                                size_value = yield match.getSizeUnit(size);
                            });
                            n = n + 1;
                            yield push_dtlRD();

                        });
                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// leitmotiv scrape
const leitmotiv = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //yield cheerio.init(10000);
            let $ = yield cheerio.get(list_url, 3);
            let n = 0;
            if ($ && $('body').html().match(/article/i)) {
                //console.log($('article').length);
                $('article').each(function (i, e) {
                    co(function* () {
                        if (i > 0) {
                            model_name = $(e).find('h2').text();
                            detail_url = list_url;
                            brand_name = category1;
                            category_name = category2;
                            product_name = brand_name + ' ' + category_name + ' ' + model_name;
                            exc_price = 0;
                            inc_price = 0;
                            size = '';
                            size_value = '';
                            n = n + 1;
                            yield push_dtlRD();
                        }
                    });
                });
            }
            console.log(`  ${category1} ${category2} : ${n} 件`);
            resolve();
        });
    });
}
// smilesolarthespice scrape no data


// pushCatRD
const pushCatRD = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //num ++;
            let str = `(${num},'${today}','${category1}','${category2}','${model_name}','${detail_url}')`;
            console.log(str);
            catRD.push(str);
            //console.log(catRD.length);
            //console.log(catRD);

            resolve();
        });
    });
}

// push_dtlRD
const push_dtlRD = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            Ncnt = Ncnt + 1;
            let str = `(${Ncnt},'${today}','${model_name}','${brand_name}','${product_name}','${color}',` +
            `'${size}','${size_value}',${exc_price},${inc_price},'${category_name}','${detail_url}')`;
            //console.log(str);
            dtlRD.push(str);

            resolve();
        });
    });
}

// pushPdcRD
const pushPdcRD = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let str = `(${num},'${today}','${model_name}','${brand_name}','${product_name}','${color}',` +
            `'${size}','${size_value}',${exc_price},${inc_price},'${category_name}','${detail_url}')`;
            //console.log(str);
            //console.log(pdcRD.length);
            pdcRD.push(str);
            //console.log(pdcRD.length);
            //console.log(pdcRD);

            resolve();
        });
    });
}
// pushCatRD
const pushCatPdcRD = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield pushCatRD();
            resolve();
        });
        co(function* () {

            yield pushPdcRD();
            resolve();
        });
    });
}
// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //get scraped data
            let sql = `SELECT min(Num) Num, mpn, brand_name, product_name, color, size, size_value, exc_price, inc_price, category_name, min(url) url
            FROM fashion_fgs196_citizen
            WHERE getdate = '${today}' 
            GROUP BY 2,3,4,5,6,7,8,9,10
            ORDER BY 1;`;

            // let sql = `SELECT Num, id, mpn_1, mpn_2, manufacturer_name, brand_name, product_name, color, size,
            // size_value, exc_price, inc_price, category_name, insert_date, url
            // FROM fashion_fgs196_citizen_product
            // ORDER BY Num;`;

            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS196_citizen_${today}.txt`;
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['num', 'id', 'mpn_1', 'mpn_2', 'manufacturer_name', 'brand_name', 'product_name', 'color', 'size', 'size_value', 'exc_price', 'inc_price', 'category_name', 'insert_date', 'url'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['BIGINT', 'BIGINT', 'VARCHAR(300)', 'VARCHAR(300)', 'VARCHAR(200)', 'VARCHAR(200)', 'VARCHAR(500)', 'VARCHAR(100)', 'VARCHAR(300)', 'VARCHAR(100)', 'DECIMAL(15,2)', 'DECIMAL(15,2)', 'VARCHAR(200)', 'TIMESTAMP(0)', 'VARCHAR(300)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].Num, 1297, res[i].mpn, res[i].mpn, 'CITIZEN', res[i].brand_name, res[i].product_name, res[i].color, res[i].size, res[i].size_value, res[i].exc_price, res[i].inc_price, res[i].category_name, stTime, res[i].url].join('\t') + '\n', 'utf-8');
            }

            resolve(res);
        });
    });
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //driver.quit();
            //yield util.sleep(10000);
            yield db.close();

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {
        
        stTime = moment().format('YYYY/MM/DD HH:mm:ss');
        yield db.connect(filePath_db);

        switch (cli['run']) {
            case 2: // OutFile
                yield setOutFile();
                break;
            case 1: // scrape data
            default:
                yield init(); //drop create
                yield scrape_main();
                //console.log(` scrapeCategory end  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                //yield scrapeProduct();
                //yield scrapeProduct();
                //yield scrapeProduct();
                //console.log(` scrapeProduct end  ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
                yield setOutFile();
                break;
        }
        yield end();
        console.log(`/// FGS196-citizen scraper.js end /// ${stTime} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
    });
}

run();