'use strict';

const co = require('co');
const shell = require('shelljs');
const commandArgs = require('command-line-args');
const moment = require('moment');
const today = moment().format('YYYYMMDD');


// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 1: // usual action
            default:
                shell.exec('node src/rakuten/FGS45-instagram-MF/scraper.js -r 1');
                shell.exec('node src/rakuten/FGS45-instagram-MF/scraper.js -r 1');
                shell.exec('node src/rakuten/FGS45-instagram-MF/scraper.js -r 12');
                shell.exec('node src/rakuten/FGS45-instagram-MF/scraper.js -r 3');
                break;
        }
    });
}

run();