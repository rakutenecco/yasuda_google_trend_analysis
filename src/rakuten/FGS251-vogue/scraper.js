'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const moment = require('moment');

// new modules
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// parse args
const cli = commandArgs([
    { name: 'runtype', alias: 'r', type: Number },   // 0:all 1:scrape 2:output
    { name: 'cron', alias: 'n', type: String },      // cron2 (weekly)
    { name: 'startdate', alias: 's', type: String }, // yyyymmdd
]);

let runtype = cli['runtype'] || 0;
const cron = cli['cron'] || 'cron2';
const filePath_db = `${conf.dirPath_db}/${cron}.db`;

const today = moment().format('YYYYMMDD');
const startdate = cli['startdate'] || moment().add(-1, 'd').format('YYYYMMDD');
const startdate_hiphen = startdate.substr(0, 4) + '-' + startdate.substr(4, 2) + '-' + startdate.substr(6, 2);
const start_site = ['https://www.vogue.co.jp/fashion/trend-and-story','https://www.vogue.co.jp/fashion/snap'];
//const start_site = ['https://www.vogue.co.jp/fashion/snap'];

console.log(`   startdate:${startdate} cron:${cron}`);

let arrUrl = [];
let n = 0;

// init action
const init_table = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sqlstr;

            // FGS251_url
            // sqlstr = `DROP TABLE IF EXISTS FGS251_url;`;
            // yield db.do(`run`, sqlstr, {});

            sqlstr = 'CREATE TABLE IF NOT EXISTS FGS251_url ' +
            '(c_num TEXT, c_getdate TEXT, c_category TEXT, c_publish TEXT, c_URL TEXT, c_title TEXT, primary key(c_getdate,c_num));';
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // FGS251_article
            // sqlstr = `DROP TABLE IF EXISTS FGS251_article;`;
            // yield db.do(`run`, sqlstr, {});

            sqlstr = 'CREATE TABLE IF NOT EXISTS FGS251_article ' +
                '(c_num TEXT, c_getdate TEXT, c_art_publish TEXT, c_art_title TEXT, c_article TEXT, primary key(c_getdate,c_num));';
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            // old and today delete
            const deldate = moment(startdate_hiphen).add(-30, 'd').format('YYYYMMDD');
            sqlstr = `DELETE FROM FGS251_url WHERE c_publish < '${deldate}' or c_getdate = '${today}';`;
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});

            sqlstr = `DELETE FROM FGS251_article WHERE c_art_publish < '${deldate}' or c_getdate = '${today}';`;
            //console.log(' sqlstr = ' + sqlstr);
            yield db.do(`run`, sqlstr, {});


            resolve();
        });
    });
}

// scrape Url data
const scrape_url = function (start_site) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let up_date = today;
            let p = 1;
            let site;

            while (up_date >= startdate) {

                if (p == 1) {
                    site = start_site;
                } else {
                    site = start_site + '?page=' + p;
                }
                //console.log('p = ' + p + ' ' + up_date);
                //console.log('site',site);
                let $ = yield cheerio.get(site);
                if ($){//} && $('body').html().match(/js-loadcontents/i)) {
                   // let x = $('.TeaserLayout-ilEPsS');
                    let x = $('.TeaserLayoutBase-j4d1l0-8.TeaserLayout-j4d1l0-9');

                   // console.log(x.length); return;
                    for (let i = 0; i < x.length; i++) {
                        let e = x[i];

                        let url = 'https://www.vogue.co.jp' + $(e).find('a').attr('href');
                        //console.log('url = ' + url);

                        let tmp = $(e).find('p').text().replace(/[\n\r\t\(\) ,]/g, '');
                        let st = tmp.indexOf('/') + 1;
                        if(st < 0) continue;
                        let category = tmp.slice(st);
                        //console.log('category = ' + category);

                        let title = $(e).find('H3').text().replace(/[\n\r\t\(\) ,]/g, '');
                        //console.log('title = ' + title);

                        let publish = $(e).find('time').text().replace(/[\n\r\t\s\(\) ,]/g, '');
                        //console.log('publish = [' + publish + ']');

                        let arr_date = publish.split(/\D/);
                        let new_up_date = arr_date[0] + ('0' + arr_date[1]).slice(-2) + ('0' + arr_date[2]).slice(-2);
                        if (new_up_date == today) continue;
                        if (up_date > new_up_date) {
                            up_date = new_up_date;
                        }
                        //console.log('up_date = ' + up_date);

                        //if( up_date < startdate ) break;

                        const sql = `INSERT OR REPLACE INTO FGS251_url ` +
                            `(c_num, c_getdate, c_category, c_publish, c_URL, c_title) VALUES (?, ?, ?, ?, ?, ?);`;
                        n = n + 1;
                        let num = ('000' + n).slice(-4)
                        //console.log(num + ' ' + title );
                        let opt = [num, today, category, new_up_date, url, title];
                        yield db.do(`run`, sql, opt);
                        //console.log('ok');

                        let str = { c_num: num, c_URL: url }
                        arrUrl.push(str);

                    };
                }
                p = p + 1;

            }
            console.log(`    ${start_site}  ${n} 件`);
            resolve();
        });
    });
}

// scrape article data
const scrape_article = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < arrUrl.length; i++) {

                let num = arrUrl[i].c_num;
                let getdate = today;
                let URL = arrUrl[i].c_URL;
                //console.log('num : ' + num + '  URL : ' + URL );

                let $ = yield cheerio.get(URL);
                if ($){//} && $('body').html().match(/content__main single/i)) {

                    let art_title;
                    let art_publish;
                    let article;
                   
                    let e = $('.Text-iyKjXH');
                    if(e.length > 0) {
                       
                        art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) ,]/g, '');
                        //console.log(art_title);

                        let arr_pub = $(e).find('time').text().split(/\D/);
                        art_publish = arr_pub[0] + ('0' + arr_pub[1]).slice(-2) + ('0' + arr_pub[2]).slice(-2);
                        //console.log(art_publish);

                        article = $(e).find('.Wrapper-iFcbid').find('p').text().replace(/[\n\r\t\(\) ,]/g, '');
                        //console.log(article);

                        let tmpart = $('.MainContentWrapper-kLTFQs').text();
                        let insta = $('.instagram-media').find('p').text();
                        //console.log(article);
                        //console.log(insta);
                        tmpart = tmpart.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');
                        article = article + tmpart;
                        //console.log(article);

                    }else{

                        e = $('.Wrapper-twbv4c-0');
                        if(e.length > 0) {
                           
                            art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) ,]/g, '');
                            //console.log(art_title);
    
                            let arr_pub = $(e).find('time').text().split(/\D/);
                            art_publish = arr_pub[0] + ('0' + arr_pub[1]).slice(-2) + ('0' + arr_pub[2]).slice(-2);
                            //console.log(art_publish);

                            article = $('.Wrapper-abvmkk-0').text().replace(/[\n\r\t\(\) ,]/g, '');
                            //console.log(article);
    
                            let tmpart = $('.ArticleWrapper-s89gjf-0.cuHiVx').text();
                            let insta = $('.instagram-media').find('p').text();
                            //console.log(article);
                            //console.log(insta);
                            tmpart = tmpart.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');
                            article = article + tmpart;
                            //console.log(article);

                        } else {

                            e = $('.article-content');
                            if(e.length > 0) {
                               
                                art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) ,]/g, '');
                                //console.log(art_title);
                                art_publish = '';
                                //console.log($('title').eq(0).text());        
                                article = $('title').eq(0).text().replace(/[\n\r\t\(\) ,]/g, '');
                                //console.log(article);

                            }else {
                                    e = $('.Wrapper-sc-1mkvvba-4');
                                    if(e.length > 0) {
                                        
                                        art_title = $(e).find('h1').text().replace(/[\n\r\t\(\) ,]/g, '');
                                        //console.log(art_title);
                
                                        let arr_pub = $(e).find('time').text().split(/\D/);
                                        art_publish = arr_pub[0] + ('0' + arr_pub[1]).slice(-2) + ('0' + arr_pub[2]).slice(-2);
                                        //console.log(art_publish);

                                        article = $('.CollapsingWrapper-kdkdmt-0').text().replace(/[\n\r\t\(\) ,]/g, '');
                                        //console.log(article);
                
                                        let tmpart = $('.Wrapper-ncqy71-1').text();
                                        let insta = $('.instagram-media').find('p').text();
                                        //console.log(article);
                                        //console.log(insta);
                                        tmpart = tmpart.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');
                                        article = article + tmpart;

                                        let f = $('.Wrapper-ydqdr3-0');
                                        if(f.length > 0) {
                                           ;
                                            let url2 = `${URL}?page=2`;
                                            console.log(url2);
                                            $ = yield cheerio.get(url2);
                                            //console.log($);
                                            
                                            let article2 = $('.Wrapper-ncqy71-1').text().replace(/[\n\r\t\(\) ,]/g, '');
                                            article = article + article2;
                                            
                                        }
                                        
                                        //console.log(article);
                                    }
                            }
                        }
                    }

                    e = $('.Wrapper-clvHKm');
                    //console.log(`  num:${num} e.length:${e.length}`);
                    if(e.length > 0) {
                        console.log(5);
                        let atmp = 'https://www.vogue.co.jp' + $(e).find('a');
                        //console.log(`    nexturl: https://www.vogue.co.jp${$(atmp).eq(2).attr('href')}`);
                        let nexturl = 'https://www.vogue.co.jp' + $(atmp).eq(2).attr('href');
                        let tmp = yield scrape_artnexturl(nexturl);
                        article = article + tmp;
                    }
                    //console.log(e,art_title); return;
                    //console.log(`  num: ${num}  publish: ${art_publish}  title: ${art_title}`);
                    //console.log(`       article: ${article}`);
                    const sql = `INSERT OR REPLACE INTO FGS251_article ` +
                        `(c_num, c_getdate, c_art_publish, c_art_title, c_article) VALUES (?, ?, ?, ?, ?);`;
                    let opt = [num, getdate, art_publish, art_title, article];
                    yield db.do(`run`, sql, opt);

                }
            }

            resolve();
        });
    });
}

// scrape article data
const scrape_artnexturl = function (URL) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let article = '';
            let $ = yield cheerio.get(URL);
            if ($){//} && $('body').html().match(/content__main single/i)) {

                let e = $('.Text-iyKjXH');
                if(e.length > 0) {

                    article = $(e).find('.Wrapper-iFcbid').find('p').text().replace(/[\n\r\t\(\) ,]/g, '');
                    //console.log(article);

                    let tmpart = $('.MainContentWrapper-kLTFQs').text();
                    let insta = $('.instagram-media').find('p').text();
                    //console.log(article);
                    //console.log(insta);
                    tmpart = tmpart.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');
                    article = article + tmpart;
                    //console.log(article);

                }else{

                    e = $('.Wrapper-bizjVT');
                    if(e.length > 0) {

                        //article = $('.CollapsingWrapper-imZosc').text().replace(/[\n\r\t\(\) ,]/g, '');
                        //console.log(article);

                        let tmpart = $('.Wrapper-lcWGFZ').text();
                        let insta = $('.instagram-media').find('p').text();
                        //console.log(article);
                        //console.log(insta);
                        tmpart = tmpart.replace(insta, '').replace(/[\n\r\t\(\) ,]/g, '');
                        article = article + tmpart;
                        //console.log(article);

                    } else {

                        e = $('.article-content');
                        if(e.length > 0) {
    
                            article = $('title').eq(0).text().replace(/[\n\r\t\(\) ,]/g, '');
                            //console.log(article);

                        }
                    }
                }

                e = $('.Wrapper-clvHKm');
                if(e.length > 0) {

                    let atmp = 'https://www.vogue.co.jp' + $(e).find('a');
                    //console.log(`    nexturl: https://www.vogue.co.jp${$(atmp).eq(2).attr('href')}`);
                    let nexturl = 'https://www.vogue.co.jp' + $(atmp).eq(2).attr('href');
                    if(URL != nexturl){
                        let tmp = yield scrape_artnexturl(nexturl);
                        article = article + tmp;
                    }
                }
                
            }

            resolve(article);
        });
    });
}

// scrape main
const scrape_main = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            yield init_table();

            yield cheerio.init(10000);
            for (let i = 0; i < start_site.length; i++) {
                yield scrape_url(start_site[i]);
            }
            if(arrUrl.length > 0) yield scrape_article();

            resolve();
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // get scraped data
            //yield db.connect(filePath_db);
            let sql = `SELECT u.c_getdate c_getdate, u.min_num min_num, u.c_category c_category, u.c_publish c_publish, u.c_URL c_URL, u.c_title c_title, a.c_art_title c_art_title, a.c_article c_article ` +
                `FROM ( SELECT MAX(c_getdate) c_getdate, MIN(c_num) min_num, MAX(c_category) c_category, c_publish, c_URL, MAX(c_title) c_title ` + 
                `  FROM FGS251_url WHERE c_getdate = '${today}' GROUP BY c_publish, c_URL ) u , FGS251_article a ` +
                `WHERE u.c_getdate = a.c_getdate AND u.min_num = a.c_num ` +
                `AND u.c_publish >= '${startdate}' AND u.c_publish < '${today}' ` +
                `ORDER BY 2;`;
            //console.log('sql = ' + sql);
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/FGS251-${today}.txt`;
            fs.appendFileSync(filepath, ['データ取得日', 'number', 'カテゴリー', '更新日', 'URL', 'タイトル', '記事内タイトル', '記事'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['c_getdate', 'c_num', 'c_category', 'c_publish', 'c_URL', 'c_title', 'c_art_title', 'c_article'].join('\t') + '\n', 'utf-8');
            fs.appendFileSync(filepath, ['VARCHAR(8)', 'VARCHAR(6)', 'VARCHAR(50)', 'VARCHAR(8)', 'VARCHAR(200)', 'VARCHAR(1000)', 'VARCHAR(1000)', 'VARCHAR(28000)'].join('\t') + '\n', 'utf-8');

            // write body
            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, [res[i].c_getdate, res[i].min_num, res[i].c_category, res[i].c_publish, res[i].c_URL, res[i].c_title, res[i].c_art_title, res[i].c_article].join('\t') + '\n', 'utf-8');
            }

            resolve(res);
        });
    });
}

// run action
const run = function () {

    co(function* () {
        
        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        console.log(`/// FGS251-vogue scraper.js start /// ${st}`);
        yield db.connect(filePath_db);

        if(runtype == 0 || runtype == 1) {            
            yield scrape_main();
        }

        if(runtype == 0 || runtype == 2) {
            yield setOutFile();
        }

        yield db.close();
        console.log(`/// FGS251-vogue scraper.js   end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();