'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);



// run action
const run = function () {

    co(function* () {
        // 
        switch (cli['run']) {
            case 100: // total-data git
                shell.exec(`cd total-data && git add .`);
                shell.exec(`cd total-data && git add -u .`);
                shell.exec(`cd total-data && git commit -m 'up fgs44'`);
                shell.exec(`cd total-data && git pull origin master`);
                shell.exec(`cd total-data && git push origin master`);
                break;

            case 1: // usual action
            default:
                shell.exec('node src/rakuten/FGS82-similarweb/scraper.js -r 99'); // scrape
                shell.exec('node src/rakuten/FGS82-similarweb/scraper.js -r 1'); // out file
                break;
        }
    });
}

run();