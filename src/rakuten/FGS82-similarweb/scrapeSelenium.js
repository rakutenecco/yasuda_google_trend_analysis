'use strict';

// const
const co = require('co');
const fs = require('fs');
const moment = require('moment');

const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const match = new(require('../../modules/match.js'));
const util = new(require('../../modules/util.js'));
const selenium = new(require('../../modules/selenium.js'));

let browser = '';
let driver = null;
const filePath_db = `${conf.dirPath_db}/cron6.db`;

// scrapeSelenium 関数生成
let scrapeSelenium = function () {

    this.title = 'scrapeSelenium';
};

// init
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            // connect db
            yield db.connect(filePath_db);

            // set selenium with browser
            driver = yield selenium.init(browser, { inVisible: false }); // true

            resolve();
        });
    });
}

// end
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield util.sleep(2000);

            // quit browser
            driver.quit();

            // close db
            yield db.close();

            resolve();
        });
    });
}

// init
scrapeSelenium.prototype.init = function (_browser) {

    return new Promise((resolve, reject) => {
        co(function* () {

            browser = _browser || 'chromium';

            resolve();
        });
    });
}

// delete
scrapeSelenium.prototype.delete = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
/*
            // delete
            yield db.connect(filePath_db);
            const sql1 = `delete from fashion_fgs44_seiko_category where page_id in ('5932-11');`;
            const sql2 = `delete from fashion_fgs44_seiko_product where page_id in ('5932-11');`;
            yield db.do(`run`, sql1, {});
            yield db.do(`run`, sql2, {});
            yield db.close();
*/
            resolve();
        });
    });
}

// show similar web
scrapeSelenium.prototype.showSimilar = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let $ = null;
            let startUrl = 'https://www.google.com/search?q=similarweb';
            let targetUrl = 'https://www.similarweb.com/ja';

            yield init();

            // open url
            driver.get(startUrl);
            yield util.sleep(2000);

            // parse body
            let targetId = -1;
            $ = yield selenium.parseBody(driver);
            $('.r a').each(function (i, e) {
                co(function* () {
                    let url = $(e).attr('href');
                    if (url === targetUrl) {
                        targetId = i;
                    }
                });
            });

            // get elements
            let elements_pre = yield selenium.getElements(driver, '.r');
            let element_pre = elements_pre[targetId];
            let elements = yield selenium.getElements(element_pre, 'a');
            let element = elements[0];
            element.click();

            // yield end();

            resolve();
        });
    });
}


// show similar web
scrapeSelenium.prototype.showSimilarDirect = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let $ = null;
            let startUrl = 'https://www.similarweb.com/ja/website/mixi.jphttps://www.similarweb.com/ja/website/mixi.jp';

            yield init();

            // open url
            driver.get(startUrl);
            yield util.sleep(200000);


            // yield end();

            resolve();
        });
    });
}

module.exports = scrapeSelenium;