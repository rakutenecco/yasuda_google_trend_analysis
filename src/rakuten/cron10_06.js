'use strict';

const shell = require('shelljs');
const cronJob = require('cron').CronJob;
const moment = require('moment');
const fs = require('fs');
const date = moment().format('YYYYMMDD');
const today = moment().format('YYYY-MM-DD hh:mm:ss');
const path_log = `../log/${date}.txt`;
let now = '';
const conf = new (require('/var/app/total/src/conf.js'));

/*
cronJob(`00 00 00 * * 1-5`)
Seconds: 0-59
Minutes: 0-59
Hours: 0-23
Day of Month: 1-31
Months: 0-11
Day of Week: 0-6
*/

//// 01 ////
// FGS239 insta posts daily 0:10
new cronJob(`00 12 00 * * 0-6`, function () {

    // let filepath1 = `${conf.dirPath_share_vb}/targets/targets_fashion_02_01.txt`;
    // if (fs.existsSync(filepath1)) {
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 2 -n cron10_01 -f ${filepath1}`);
    //     shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js`);
    // }

    let filepath = `${conf.dirPath_share_vb}/targets/targets_food_03_03.txt`;
    if (fs.existsSync(filepath)) {
        shell.exec(`node src/rakuten/FGS239-insta-posts/shell.js -r 1 -n cron10_06 -f ${filepath}`);
        shell.exec(`node src/rakuten/FGS239-insta-posts/make_outfile.js -r 1`);
    }

}, function () {
    // error
    console.log(`Error ${today} : FGS239 insta-posts scrape cron10_06`);
},
true, 'Asia/Tokyo');

console.log('cron10_06 setting');
