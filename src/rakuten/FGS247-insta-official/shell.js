'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const shell = require('shelljs');

const util = new(require('../../modules/util.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);


// run action
const run = function () {

    co(function* () {

        shell.exec('node src/rakuten/FGS247-insta-official/scraper.js -r 1');
    });
}

run();