'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const iconv = require('iconv-lite');
const moment = require('moment');
const shell = require('shelljs');
const url = require('url');
const cheerio = new(require('../../modules/cheerio-httpcli.js'));

// new modules
//const targets = require('./targets.js').targets;
const conf = new(require('../../conf.js'));
const db = new(require('../../modules/db.js'));
const util = new(require('../../modules/util.js'));
const insta = new(require('../../modules/instagram.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    //{ name: 'category', alias: 'c', type: String }
]);

// variable
let keywords = [];
//const category = cli['category'] || 'watch';
const filePath_db = `${conf.dirPath_db}/cron0.db`;
const getdate = moment().format('YYYY/MM/DD');
const today = moment().format('YYYYMMDD');

const tname = 'FGS247_insta_official';
const infile = `${conf.dirPath_share_vb}/targets/targets_official.txt`;
//const infile_new = `${conf.dirPath_share_vb}/targets/targets_official_${today}.txt`;

// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            yield db.connect(filePath_db);

            // drop table
            const sql1 = `DROP TABLE IF EXISTS ${tname};`;
            yield db.do(`run`, sql1, {});

            // create table
            const sql2 = `CREATE TABLE IF NOT EXISTS ${tname} ` +
                `(keyword TEXT, category TEXT, getdate TEXT, posts bigint, follower bigint);`;
            //console.log('sql = ' + sql2);
            yield db.do(`run`, sql2, {});

            resolve();
        });
    });
}

// get targets
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            if(keywords.length > 0){
                resolve(true);

            }else{
                // if (fs.existsSync(infile_new)) {
                //     console.log(`  ${infile_new} read start`);
                //     let buf = fs.readFileSync(infile_new);
                //     let buf2 = iconv.decode(buf, "Shift_JIS" );
                //     let arr1 = buf2.split("\r\n");
                //     let arr2 = arr1.map(toKwd).sort();
                //     keywords = arr2.filter(function (x, i, self) {
                //         return self.indexOf(x) === i && x.indexOf('?') == -1;
                //     });
                //     if(keywords[0].length == 0) keywords.shift();

                //     if (fs.existsSync(infile)) {
                //         fs.renameSync(infile, `${infile}.${today}_${cli['run']}.bak`);
                //     }
                //     for (let i = 0; i < keywords.length; i++) {
                //         fs.appendFileSync(infile, keywords[i] + '\n', 'utf8');
                //     }
                //     resolve(true);

                // } else {
                    if (fs.existsSync(infile)) {
                        console.log(`  ${infile} read start`);
                        keywords = fs.readFileSync(infile).toString().split("\n");
                        if(keywords[keywords.length -1].length == 0) keywords.pop();
                        //console.log(keywords[0]+','+keywords[1]);
                        //console.log(keywords[keywords.length-2]+','+keywords[keywords.length-1]);
                        resolve(true);

                    }else{
                        console.log('  not exists in file');
                        resolve(false);

                    }
                //}
            }

        });
    });
}
// # delete
function toKwd(str) {
    let kwd = '';
    if( str.startsWith('#') || str.startsWith('＃') ){
        kwd = str.slice(1);
    }else{
        kwd = str;
    }
    //console.log('kwd = ' + kwd);
    return kwd;
}

// end action
const end = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //driver.quit();
            yield db.close();

            resolve();
        });
    });
}

// scrape google trends
const scrape_insta_posts = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let isFile = yield outFileCheck(tname);

            if (!isFile) {
                let gt = yield get_targets();
                if(gt){

                    yield init();
                    yield cheerio.init(10000);

                    for (let i = 0; i < keywords.length; i++) {

                        //console.log(keywords[i]);
                        let result = yield insta.getInstaCount(keywords[i], 0, 20);
                        //console.log(result);
                        const sql = `INSERT OR REPLACE INTO ${tname} ` +
                            `(keyword, category, getdate, posts, follower) ` +
                            `VALUES (?, ?, ?, ?, ?);`;
                        //console.log('sql = ' + sql);
                        let opt = [keywords[i], '', getdate, result.split(',')[0], result.split(',')[1]];
                        yield db.do(`run`, sql, opt);
                    }

                    yield setOutFile();
                }
            }
            resolve();
        }).catch((e) => {
            console.log(e);
        });
    });
}

// set out file to share folder
const setOutFile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let sql = `select lower(keyword) keyword, lower(category) category, getdate, posts, follower
             from ${tname} order by keyword;`;
            let res = yield db.do(`all`, sql, {});

            // write header
            let filepath = `${conf.dirPath_share_vb}/${tname}_${today}.txt`;

            // append
            fs.appendFileSync(filepath, (['keyword', 'category', '取得日', '投稿数', 'フォロワー'].join('\t') + '\n'), 'utf8');
            fs.appendFileSync(filepath, (['keyword', 'category', 'getdate', 'posts', 'follower'].join('\t') + '\n'), 'utf8');
            fs.appendFileSync(filepath, (['varchar(500)', 'varchar(500)', 'VARCHAR(10)', 'bigint', 'bigint'].join('\t') + '\n'), 'utf8');

            for (let i = 0; i < res.length; i++) {
                fs.appendFileSync(filepath, ([res[i].keyword, res[i].category, res[i].getdate, res[i].posts, res[i].follower].join('\t') + '\n'), 'utf8');
            }

            yield end();

            resolve();
        });
    });
}

// out file exists check
const outFileCheck = function (tname) {

    return new Promise((resolve, reject) => {
        co(function* () {
            let filepath = `${conf.dirPath_share_vb}/${tname}_${today}.txt`;
            if (fs.existsSync(filepath)) {
                console.log('  exists out file');
                resolve(true);
            } else {
                console.log('  not exists out file');
                resolve(false);
            }
        });
    });
}

// out file result
const out_result = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let filepath = `${conf.dirPath_share_vb}/${tname}_${today}.err`;
            // out
            let isFile = yield outFileCheck(tname);
            if (!isFile) {
                fs.appendFileSync(filepath, `no scraping data\n`, 'utf8');
            }
            // in
            if (!fs.existsSync(infile)) {
                fs.appendFileSync(filepath, `infile not exists ${infile}\n`, 'utf8');
            }
            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        // 
        switch (cli['run']) {
            case 1: // scrape data
                yield scrape_insta_posts();
                yield scrape_insta_posts();
                yield scrape_insta_posts();
                yield out_result();
                console.log(`run: ${cli['run']} ${st} - ` + moment().format('YYYY/MM/DD HH:mm:ss'));
                break;

            case 11: // scrape data OutFile
                yield db.connect(filePath_db);
                yield setOutFile();
                break;

            default:
                break;
        }

        console.log('/// end ///');
    });
}

run();