'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const iconv = require('iconv-lite');
const moment = require('moment');
const shell = require('shelljs');

// new modules
const conf = new (require('../../conf.js'));

// variable
let keywords = [];
let category; // food / fashion / watch / life / official
const today = moment().format('YYYYMMDD');
let infile;
let kigou_check_file = `${conf.dirPath_share_vb}/targets/targets_kigoucheck/targets_kigou_check_${today}.txt`;

// get targets
const get_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            infile = `${conf.dirPath_share_vb}/targets/targets_${category}.txt`;
            console.log('infile:',infile);
            console.log('kigou_check_file:',kigou_check_file);

            yield get_new_targets();
            
            resolve();
        });
    });
}

// get new targets
const get_new_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let rt = false;
            let arr = [];

            if (fs.existsSync(infile)) {

                let buf = fs.readFileSync(infile).toString().split("\n");
                console.log(`  ${infile}: ${buf.length}`);
                Array.prototype.push.apply(arr, buf);

                console.log(`  arr: ${arr.length}`);
                //#delete sort

                //Twitter hashtag check!! utf-8
                let twarr = [];
                for(let i = 0; i < arr.length; i++) {
                    
                    let str = arr[i];
                    let testResult = str.match(/[^ー・々〆0-9_a-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                    if(testResult){
                        for(let n = 0; n < testResult.length; n++) {
                            twarr.push(`${category}\t${str}\t${testResult[n]}`);
                        }
                    }
                }
                if(twarr.length > 0){
                    for (let i = 0; i < twarr.length; i++) {
                        fs.appendFileSync(kigou_check_file, twarr[i] + '\n', 'utf8');
                    }
                }
                console.log(kigou_check_file);
            }
            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');

        console.log(`/// targets_kigoucheck start ${st}///`);

        category = 'food';
        yield get_targets();

        category = 'fashion';
        yield get_targets();

        category = 'watch';
        yield get_targets();

        category = 'life';
        yield get_targets();

        // category = 'official';
        // yield get_targets();

        category = 'flower';
        yield get_targets();

        category = 'kitchen';
        yield get_targets();

        // category = 'area';
        // yield get_targets();

        category = 'interior';
        yield get_targets();

        
        console.log(`/// targets_kigoucheck end ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')} ///`);
    });
}

run();