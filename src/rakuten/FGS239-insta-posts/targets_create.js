'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const iconv = require('iconv-lite');
const moment = require('moment');
const shell = require('shelljs');

// new modules
const conf = new (require('../../conf.js'));
const today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
    { name: 'category', alias: 'c', type: String },
]);

//console.log(cli['run']);
let runtype = cli['run'] || 0;//? cli['runtype'] : 1; // 0: all 1: get new only 2:divide only
//console.log(runtype);
let infile_all = `${conf.dirPath_share_vb}/targets/targets_all.txt`;
let newfile_all = `${conf.dirPath_share_vb}/targets/targets_new_${today}.txt`;

const arg_category = cli['category'] ? cli['category'] : ''; // food / fashion / watch / flower / interior / kitchen / life  / cosme  / wine / official / area / 

let tbls = [
    { category: 'food',     div1: '2', div2: '', div3: '', addall: '1', divide: '' },
    { category: 'fashion',  div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'watch',    div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'flower',   div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'interior', div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'kitchen',  div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'life',     div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'cosme',    div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'wine',     div1: '', div2: '', div3: '', addall: '1', divide: ''  },
    { category: 'official', div1: '', div2: '', div3: '', addall: '', divide: ''  },
    { category: 'area',     div1: '', div2: '', div3: '', addall: '', divide: ''  },
];

let tbls_all = [
    { div1: '6', div2: '5', div3: '', divide: '' },
];

// variable
let keywords = [];
let infile;
let infile_new_sjis;
let infile_new_insta;
let infile_new_twitter;

let tgts_add = [];
let tgts_ctgry = [];
let tgts_copy = [];

let tgts_new = [];
let tgts_all = [];

let dv1;
let dv2;
let dv3;
let category;
let addall;

// targets_create
const targets_create = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < tbls.length; i++) {

                category = tbls[i].category;
                addall = tbls[i].addall;
                if(arg_category.length == 0 || arg_category == category){
                    infile = `${conf.dirPath_share_vb}/targets/targets_${category}.txt`;
                    infile_new_sjis = `${conf.dirPath_share_vb}/targets/targets_${category}_add_sjis_${today}.txt`;
                    infile_new_insta = `${conf.dirPath_share_vb}/targets/targets_${category}_add_insta_${today}.txt`;
                    infile_new_twitter = `${conf.dirPath_share_vb}/targets/targets_${category}_add_twitter_${today}.txt`;
                    console.log('  infile:',infile);

                    yield get_new_targets();

                    if(tgts_add.length > 0){
                        yield targets_output(i);
                    }
                }
            }
    
            if(tgts_new.length > 0){
                yield targets_aggregate();
            }

            resolve();
        });
    });
}

// get new targets
const get_new_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            let arr = [];
            tgts_add.length = 0;

            //infile_new_sjis
            if (fs.existsSync(infile_new_sjis)) {
                let buf = fs.readFileSync(infile_new_sjis);
                //decode
                let buf2 = iconv.decode(buf, "Shift_JIS" );
                arr = buf2.split("\r\n");
                if(arr[arr.length -1].length == 0) arr.pop();
                console.log(`  infile_new_sjis: ${infile_new_sjis}: ${arr.length}`);
            }

            //infile_new_insta
            if (fs.existsSync(infile_new_insta)) {
                let buf = fs.readFileSync(infile_new_insta).toString().split("\n");
                if(buf[buf.length -1].length == 0) buf.pop();
                console.log(`  infile_new_insta: ${infile_new_insta}: ${buf.length}`);
                Array.prototype.push.apply(arr, buf);
            }

            //infile_new_twitter
            if (fs.existsSync(infile_new_twitter)) {
                let buf = fs.readFileSync(infile_new_twitter).toString().split("\n");
                if(buf[buf.length -1].length == 0) buf.pop();
                console.log(`  infile_new_twitter: ${infile_new_twitter}: ${buf.length}`);
                Array.prototype.push.apply(arr, buf);
            }

            if(arr.length > 0){
                //normalize
                yield targets_normalize(arr);
            }

            if(tgts_add.length > 0){

                if (fs.existsSync(infile)) {
                    tgts_ctgry.length = 0;
                    let buf = fs.readFileSync(infile).toString().split("\n");
                    if(buf[buf.length -1].length == 0) buf.pop();
                    console.log(`  infile: ${infile}: ${buf.length}`);
                    Array.prototype.push.apply(tgts_ctgry, buf);
                    //console.log(`    tgts_ctgry: ${tgts_ctgry.length}`);

                }else{
                    console.log(` infile: ${infile} not exists`);
                }

                // read tgts_all
                if( addall == '1' && tgts_all.length == 0 ){
                    if (fs.existsSync(infile_all)) {
                        let buf = fs.readFileSync(infile_all).toString().split("\n");
                        if(buf[buf.length -1].length == 0) buf.pop();
                        console.log(`  infile_all: ${infile_all}: ${buf.length}`);
                        Array.prototype.push.apply(tgts_all, buf);
                        //console.log(`    tgts_all: ${tgts_all.length}`);
                        //fs.renameSync(infile_all, `${infile_all}_${today}bak`);
                    } else {
                        console.log(` infile_all: ${infile_all} not exists`);
                    }
                }
            }               
            resolve();
        });
    });
}

// normalize new targets
const targets_normalize = function (arr) {

    return new Promise((resolve, reject) => {
        co(function* () {

            console.log(`  normalize arr: ${arr.length}`);
            //#delete sort
            let arr2 = [];
            for(let i = 0; i < arr.length; i++) {
                //console.log(arr[i]);
                let str = arr[i];
                str = str.normalize('NFKC').toLowerCase();
                console.log('['+ str + ']',str.length);
                let rt2 = true;
                if( str.lastIndexOf('#', 0) === 0 ){
                    str = str.slice(1);
                }

                let slen = str.length;
                for (let j = 0; j < slen; j++) {
                    console.log('['+ str + ']',str.length,str[str.length -1].search(/[ ,\?、〓]/));
                    if( str[str.length -1].search(/[ ,\?、〓]/) == 0 ){
                        str = str.slice(0,-1);
                        console.log('['+ str + ']',str.length);
                    }else{
                        break;
                    }
                }
                if(str.length == 0) rt2 = false;

                if(rt2){
                    if( str.search(/[#\?"]/) >= 0 ){
                        rt2 = false;
                    }
                }

                if (rt2){
                    arr2.push(str);
                }
            }
            console.log(`   arr2: ${arr2.length}`);
            //sort
            arr2.sort();
            //let arr = [0,1,1,2,3,4,4,4,5];
            
            //Duplicate delete
            let set = new Set(arr2);
            //console.log(set); // -> Set { 0, 1, 2, 3, 4, 5 }
            tgts_add = Array.from(set);
            //console.log(keywords); // -> [ 0, 1, 2, 3, 4, 5 ]

            resolve();
        });
    });
}

// new, copy output
const targets_output = function (i_tbls) {

    return new Promise((resolve, reject) => {
        co(function* () {

            //<tgts_add>が<tgts_ctgry>に含まれなかったら<tgts_addfile>
            //<tgts_addfile>が<tgts_all>に含まれていたら<tgts_copy>、含まれなかったら<tgts_new>

            let tgts_addfile = [];
            for (let i = 0; i < tgts_add.length; i++) {
                if(tgts_ctgry.indexOf(tgts_add[i]) == -1){
                    tgts_addfile.push(tgts_add[i]);
                }
            }
            console.log(`   tgts_addfile: ${tgts_addfile.length}`);
            
            if(tgts_addfile.length > 0){
                
                // tgts_addfile output
                yield kigoucheck(tgts_addfile);

                let targets_add_file = `${conf.dirPath_share_vb}/targets/targets_${category}_addspdb_${today}.txt`;
                
                for (let i = 0; i < tgts_addfile.length; i++) {
                    fs.appendFileSync(targets_add_file, tgts_addfile[i] + '\n', 'utf8');
                }
                console.log(`  targets_add_file: ${targets_add_file}: ${tgts_addfile.length}`);

                // infile output
                Array.prototype.push.apply(tgts_ctgry, tgts_addfile);
                tgts_ctgry.sort();

                fs.renameSync(infile, `${infile}_${today}bak`);

                for (let i = 0; i < tgts_ctgry.length; i++) {
                    fs.appendFileSync(infile, tgts_ctgry[i] + '\n', 'utf8');
                }

                tbls[i_tbls].divide = '1';
            

                // addall 
                if( addall == '1' ){

                    tgts_copy.length = 0;

                    for (let i = 0; i < tgts_addfile.length; i++) {
                        if(tgts_all.indexOf(tgts_addfile[i]) == -1){
                            tgts_new.push(tgts_addfile[i]);
                        }else{
                            tgts_copy.push(tgts_addfile[i]);
                        }
                    }
                    console.log(`   tgts_copy: ${tgts_copy.length}`);
                    console.log(`   tgts_new: ${tgts_new.length}`);
                
                    // tgts_copy output
                    if(tgts_copy.length > 0){

                        let targets_copy_file = `${conf.dirPath_share_vb}/targets/targets_${category}_copy_${today}.txt`;

                        for (let i = 0; i < tgts_copy.length; i++) {
                            fs.appendFileSync(targets_copy_file, tgts_copy[i] + '\n', 'utf8');
                        }
                        console.log(`  targets_copy_file: ${targets_copy_file}: ${tgts_copy.length}`);

                    }
                }
            }

            resolve();
        });
    });
}


// targets_all output
const targets_aggregate = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //new file
            tgts_new.sort();            
            //Duplicate delete
            let set = new Set(tgts_new);
            //console.log(set); // -> Set { 0, 1, 2, 3, 4, 5 }
            tgts_new = Array.from(set);
            console.log(`   tgts_new.length: ${tgts_new.length}`);

            //yield kigoucheck(tgts_new);

            for (let i = 0; i < tgts_new.length; i++) {
                fs.appendFileSync(newfile_all, tgts_new[i] + '\n', 'utf8');
            }
            console.log(`  newfile_all: ${newfile_all}: ${tgts_new.length}`);

            //all file
            Array.prototype.push.apply(tgts_all, tgts_new);

            tgts_all.sort();

            fs.renameSync(infile_all, `${infile_all}_${today}bak`);

            for (let i = 0; i < tgts_all.length; i++) {
                fs.appendFileSync(infile_all, tgts_all[i] + '\n', 'utf8');
            }
            console.log(`  infile_all: ${infile_all}: ${tgts_all.length}`);
            tbls_all[0].divide = '1';

            resolve();
        });
    });
}

// kigoucheck
const kigoucheck = function (tgts_new) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let kigou_check_file = `${conf.dirPath_share_vb}/targets/targets_kigoucheck/targets_kigou_check_${today}.txt`;

            //Twitter hashtag check!! utf-8
            let twarr = [];
            for(let i = 0; i < tgts_new.length; i++) {
                
                let str = tgts_new[i];
                let testResult = str.match(/[^ー・々〆0-9_a-zｦ-ﾝ０-９Ａ-Ｚａ-ｚぁ-んァ-ヶ一-龠]/g);
                if(testResult){
                    for(let n = 0; n < testResult.length; n++) {
                        twarr.push(`${category}\t${str}\t${testResult[n]}`);
                    }
                }
            }
            if(twarr.length > 0){
                for (let i = 0; i < twarr.length; i++) {
                    fs.appendFileSync(kigou_check_file, twarr[i] + '\n', 'utf8');
                }
                console.log(`   kigoucheck:${kigou_check_file}`);
            }

            resolve();
        });
    });
}


// get new targets
const targets_divide = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            for (let i = 0; i < tbls.length; i++) {

                category = tbls[i].category;
                //console.log(category);
                if( (runtype == 2 && category == arg_category) || tbls[i].divide == '1' ){

                    if(tbls[i].div1.length > 0) {
                        dv1 = Number(tbls[i].div1);
                    } else {
                        dv1 = 0;
                    }
                    if(tbls[i].div2.length > 0) {
                        dv2 = Number(tbls[i].div2);
                    } else {
                        dv2 = 0;
                    }
                    if(tbls[i].div3.length > 0) {
                        dv3 = Number(tbls[i].div3);
                    } else {
                        dv3 = 0;
                    }
                    if(dv1 + dv2 + dv3 > 0) {
                        infile = `${conf.dirPath_share_vb}/targets/targets_${category}.txt`;
                        if (fs.existsSync(infile)) {
                            yield get_divide();
                        }else{
                            console.log(`  infile:${infile} not exists`);
                        }
                    }
                }            
            }

            //all
            category = 'all';
            if( (runtype == 2 && category == arg_category) || tbls_all[0].divide == '1' ){

                if(tbls_all[0].div1.length > 0) {
                    dv1 = Number(tbls_all[0].div1);
                } else {
                    dv1 = 0;
                }
                if(tbls_all[0].div2.length > 0) {
                    dv2 = Number(tbls_all[0].div2);
                } else {
                    dv2 = 0;
                }
                if(tbls_all[0].div3.length > 0) {
                    dv3 = Number(tbls_all[0].div3);
                } else {
                    dv3 = 0;
                }
                if(dv1 + dv2 + dv3 > 0) {
                    infile = `${conf.dirPath_share_vb}/targets/targets_all.txt`;
                    if (fs.existsSync(infile)) {
                        yield get_divide();
                    }else{
                        console.log(`  infile:${infile} not exists`);
                    }
                }

            }

            resolve();

        }).catch((e) => {
            console.log('    catch targets_divide');
            console.log(e);
            resolve('');
        });
    });
}


// get divide targets
const get_divide = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            console.log(`  ${infile} divide: [${dv1}] [${dv2}] [${dv3}] start ${moment().format('YYYY/MM/DD HH:mm:ss')}`);
            
            //read infile
            keywords.length = 0;
            let buf = fs.readFileSync(infile).toString().split("\n");
            if(buf[buf.length -1].length == 0) buf.pop();
            console.log(`  ${infile}: ${buf.length}`);
            keywords = buf;
            //Array.prototype.push.apply(keywords, buf);
            //console.log(`  keywords[0]: ${keywords[0]}`);
            //console.log(`  keywords[keywords.length-1]: ${keywords[keywords.length-1]}`);

            if( dv1 > 0 ) yield output_divide(dv1);
            if( dv2 > 0 ) yield output_divide(dv2);
            if( dv3 > 0 ) yield output_divide(dv3);            

            resolve();
        
        }).catch((e) => {
            console.log('    catch get_divide');
            console.log(e);
            resolve('');
        });
    });
}

// divide output
const output_divide = function (dnum) {

    return new Promise((resolve, reject) => {
        co(function* () {

            //backup
            let num = ('0' + dnum).slice(-2)
            let ls = shell.ls(`${conf.dirPath_share_vb}/targets/targets_${category}_${num}_[0-9][0-9].txt`);
            console.log(`   ls.length: ${ls.length}`);
            for(let j = 0; j < ls.length; j++ ){
                console.log(`  ${ls[j]} -> ${ls[j]}_${today}bak`);
                fs.renameSync(ls[j], `${ls[j]}_${today}bak`);
            }

            // integer divide
            let LNumber = Math.ceil(keywords.length/dnum);
            console.log(`   LNumber: ${LNumber}`);
            let i = 0;
            let fcnt = 1;
            do{
                let str_fcnt = ('0' + fcnt).slice(-2);
                console.log(`  ${conf.dirPath_share_vb}/targets/targets_${category}_${num}_${str_fcnt}.txt`);

                let filepath = `${conf.dirPath_share_vb}/targets/targets_${category}_${num}_${str_fcnt}.txt`;
                
                let cnt = 0;
                do{
                    fs.appendFileSync(filepath, (keywords[i] + '\n'), 'utf8');
                    cnt++;
                    i++;
                }while(cnt < LNumber && i < keywords.length)
                console.log(`    ${cnt} 件`);
                fcnt++; 
            }while(i < keywords.length)

            resolve();

        }).catch((e) => {
            console.log('    catch output_divide');
            console.log(e);
            resolve('');
        //    return null;
        });
    });
}

// aggregate_targets
const aggregate_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            console.log(` aggregate_targets start //`);
            infile = `${conf.dirPath_share_vb}/targets/targets_food.txt`;
            yield read_targets();

            infile = `${conf.dirPath_share_vb}/targets/targets_fashion.txt`;
            yield read_targets();

            infile = `${conf.dirPath_share_vb}/targets/targets_watch.txt`;
            yield read_targets();

            infile = `${conf.dirPath_share_vb}/targets/targets_life.txt`;
            yield read_targets();

            infile = `${conf.dirPath_share_vb}/targets/targets_flower.txt`;
            yield read_targets();

            infile = `${conf.dirPath_share_vb}/targets/targets_kitchen.txt`;
            yield read_targets();

            infile = `${conf.dirPath_share_vb}/targets/targets_interior.txt`;
            yield read_targets();

            agg_ary.sort();
            //Duplicate delete
            let set = new Set(agg_ary);
            keywords = Array.from(set);
            if(keywords[0].length == 0) keywords.shift();
            console.log(` keywords: ${keywords.length}`);

            let outfile = `${conf.dirPath_share_vb}/targets/targets_all.txt`;
            for (let i = 0; i < keywords.length; i++) {
                fs.appendFileSync(outfile, keywords[i] + '\n', 'utf8');
            }

            console.log(` aggregate_targets end //`);
            resolve();
        });
    });
}

// read targets
const read_targets = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let buf = fs.readFileSync(infile).toString().split("\n");
            if(buf[buf.length -1].length == 0) buf.pop();
            console.log(`  ${infile}: ${buf.length}`);
            Array.prototype.push.apply(agg_ary, buf);
            console.log(`    agg_ary: ${agg_ary.length}`);

            resolve();
        });
    });
}


// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        
        if (runtype == 0 || runtype == 1) {
            //targets_create
            yield targets_create();
        }

        if (runtype == 0 || runtype == 2) {
            //targets_divide
            yield targets_divide();
        }

        console.log(`/// targets_create end /// ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

    });
}

run();