'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const iconv = require('iconv-lite');
const moment = require('moment');
const shell = require('shelljs');

// new modules
const conf = new (require('../../conf.js'));

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: String },
    { name: 'targetdate', alias: 't', type: String },
    { name: 'category', alias: 'c', type: String },
    { name: 'filename', alias: 'f', type: String },
]);

let runtype = cli['run'] ? cli['run'] : 0; // 0/ 1:FGS239 2:FGS258
const today = moment().format('YYYYMMDD');
let yesterday; // = moment().subtract(1, 'days').format('YYYYMMDD');
let targetdate; // = cli['targetdate'] ? cli['targetdate'] : today; // divide count
//console.log('arg targetdate:', cli['targetdate']);
if(cli['targetdate']){
    targetdate = cli['targetdate'];
    yesterday = moment(targetdate.slice(0,4) + '-' + targetdate.slice(4,6) + '-'+targetdate.slice(6)).subtract(1, 'days').format('YYYYMMDD');
    
}else {
    targetdate = today
    yesterday = moment().subtract(1, 'days').format('YYYYMMDD');    
}
//console.log('yesterday:', yesterday);
//console.log('targetdate:', targetdate);
let ag_category = cli['category']? cli['category'] : 'all'; // food / fashion / watch / life
let category;

let infname;
let outfname;
let fnum;
let tmpfnm = cli['filename'];
// ex) FGS286_yahoo_all_20190304_02_01.txt
//     FGS258_twitter_posts_food_20190305_02_01.txt
//     FGS239_insta_posts_food_20190303_05_03.txt
//console.log('tmpfnm', tmpfnm, tmpfnm.split(/[_\.]/g));
if(tmpfnm){
    runtype = 99;
    let tmpf = tmpfnm.split(/[_\.]/g);
    //console.log(tmpf);
    if( tmpf[tmpf.length-3].length == 2 && isFinite(tmpf[tmpf.length-3]) && tmpf[tmpf.length-2].length == 2 && isFinite(tmpf[tmpf.length-2]) && tmpf[tmpf.length-1] == 'txt'){
        runtype = 10;
        infname = `${tmpfnm.substr(0,tmpfnm.length-6)}[0-9][0-9].txt`;
        outfname = `${tmpfnm.substr(0,tmpfnm.length-10)}.txt`;
        console.log(`  infname : ${infname}`);
        console.log(`  outfname: ${outfname}`);
        fnum = Number(tmpf[tmpf.length-3]);
        console.log(`  fnum: ${fnum}`);
    }
}

// variable
let keywords = [];

let infile;
let outfile;
let out_c_file;

let pgname;


// get targets
const make_outfile = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //console.log('infile :',infile);
            //console.log('outfile :',outfile);
            //console.log('out_c_file :',out_c_file);
            //let result = false;
            //pgname = 'FGS239';
            //out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;

            //out_c_file check
            if (fs.existsSync(out_c_file)) {
                console.log('  out_c_file exists :',out_c_file);
            } else {
                console.log('  out_c_file :',out_c_file);
                let in_ls = shell.ls(infile);
                //let in_ls = shell.ls(`${conf.dirPath_share_vb}/targets_${category}_[0-9][0-9].txt`);
                console.log(`   ${infile} ls.length: ${in_ls.length}`);
                //console.log(`  ls.length: ${in_ls[0]}`);
                //console.log(`  ls.length: ${in_ls[1]}`);

                if(in_ls.length > 0 ){
                    let ot_ls = shell.ls(outfile);
                    //let ot_ls = shell.ls(`${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_[0-9][0-9].txt`);
                    console.log(`   ${outfile} ls.length: ${ot_ls.length}`);
                    //console.log(`  ls.length: ${ot_ls[0]}`);
                    //console.log(`  ls.length: ${ot_ls[1]}`);

                    if(ot_ls.length > 0 && in_ls.length == ot_ls.length ){
                        //01
                        let out_lines = fs.readFileSync(ot_ls[0]).toString().split("\n");
                        //console.log(`  out_lines.length ${out_lines.length}`);
                        if(out_lines[out_lines.length -1].length == 0) out_lines.pop();
                        console.log(`    out_lines.length ${out_lines.length}`);

                        //02-
                        for(let j = 1; j < ot_ls.length; j++ ){                        
                                                        
                            let lines = fs.readFileSync(ot_ls[j]).toString().split("\n");
                            //console.log(` ${j} lines.length ${lines.length}`);
                            if(lines[lines.length -1].length == 0) lines.pop();

                            // if(pgname == 'FGS239'){
                            //     lines.splice(0, 3); // 3lines delete
                            // }

                            Array.prototype.push.apply(out_lines, lines);
                            console.log(`   out_lines.length ${out_lines.length}`);

                        }
                        console.log(`   out_lines.length ${out_lines.length}`);
                        for(let i = 0; i < out_lines.length; i++ ){ 
                            fs.appendFileSync(out_c_file, (out_lines[i] + '\n'), 'utf8');
                        }
                        //result = true;
                    }
                }
            }
            resolve();
        // }).catch((e) => {
        //     console.log(e);
        });
    });
}


// Concatenate files
const make_outfile2 = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            //outfname check
            if (fs.existsSync(outfname)) {
                console.log('  outfile exists :',outfname);
            } else {
                let in_ls = shell.ls(infname);
                console.log(`   infname ls.length: ${in_ls.length}`);

                if(in_ls.length == fnum ){                    
                    for(let j = 0; j < in_ls.length; j++ ){
                        yield output(j+1, in_ls[j]);
                    }                    

                }else{
                    console.log(`  Required file does not exist`);
                }
            }
            resolve();
        // }).catch((e) => {
        //     console.log(e);
        });
    });
}

// output file
const output = function (n,f) {

    return new Promise((resolve, reject) => {
        co(function* () {

            let lines = fs.readFileSync(f).toString().split("\n");
            if(lines[lines.length -1].length == 0) lines.pop();
            console.log(`    infile ${n}: ${lines.length}`);

            for(let i = 0; i < lines.length; i++ ){ 
                fs.appendFileSync(outfname, (lines[i] + '\n'), 'utf8');
            }

            resolve();
        // }).catch((e) => {
        //     console.log(e);
        });
    });
}
    
// run action
const run = function () {

    co(function* () {

        let st = moment().format('YYYY/MM/DD HH:mm:ss');
        console.log(` /// FGS239 make_outfile.js start /// runtype:${runtype} ${st}`); 
        
        if(runtype == 0 || runtype == 1){
            pgname = 'FGS239';

            // if(ag_category == 'food' || ag_category == 'all'){
            //     category = 'food';
            //     infile = `${conf.dirPath_share_vb}/targets/targets_${category}_02_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_02_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;
            //     let rt = yield make_outfile();
            // }
            // if(ag_category == 'food' || ag_category == 'all'){
            //     category = 'food';
            //     infile = `${conf.dirPath_share_vb}/targets/targets_${category}_04_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_04_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;
            //     let rt = yield make_outfile();
            // }
            if(ag_category == 'food' || ag_category == 'all'){
                category = 'food';
                infile = `${conf.dirPath_share_vb}/targets/targets_${category}_05_[0-9][0-9].txt`;
                outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_05_[0-9][0-9].txt`;
                out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;
                let rt = yield make_outfile();
            }

            // if(ag_category == 'fashion' || ag_category == 'all'){
            //     category = 'fashion';
            //     infile = `${conf.dirPath_share_vb}/targets/targets_${category}_02_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_02_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;
            //     let rt = yield make_outfile();
            // }

            // if(category == 'skirt' || category == 'all'){

            //     infile = `${conf.dirPath_share_vb}/targets_${category}_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_fahion_${targetdate}_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_fahion_${targetdate}.txt`;
            //     yield make_outfile();

            //     infile = `${conf.dirPath_share_vb}/targets_${category}_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_wearjp_posts_fahion_${targetdate}_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_wearjp_posts_fahion_${targetdate}.txt`;
            //     yield make_outfile();

            // }
            // if(category == 'watch' || category == 'all'){
            //     infile = `${conf.dirPath_share_vb}/targets_${category}_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;
            //     yield make_outfile();

            // }
            // if(category == 'life' || category == 'all'){
            //     infile = `${conf.dirPath_share_vb}/targets_${category}_[0-9][0-9].txt`;
            //     outfile = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}_[0-9][0-9].txt`;
            //     out_c_file = `${conf.dirPath_share_vb}/${pgname}_insta_posts_${category}_${targetdate}.txt`;
            //     yield make_outfile();

            // }

        }
        if(runtype == 0 || runtype == 2){
            pgname = 'FGS258';
            if(ag_category == 'food' || ag_category == 'all'){
                category = 'food';
                //targetdate = yesterday;
                infile = `${conf.dirPath_share_vb}/targets/targets_${category}_02_[0-9][0-9].txt`;
                outfile = `${conf.dirPath_share_vb}/${pgname}_twitter_posts_${category}_${yesterday}_02_[0-9][0-9].txt`;
                out_c_file = `${conf.dirPath_share_vb}/${pgname}_twitter_posts_${category}_${yesterday}.txt`;
                yield make_outfile();
            }
            
        }

        if(runtype == 0 || runtype == 3){
            pgname = 'FGS286';
            if(ag_category == 'food' || ag_category == 'all'){
                category = 'food';
                //targetdate = yesterday;
                infile = `${conf.dirPath_share_vb}/targets/targets_${category}_02_[0-9][0-9].txt`;
                outfile = `${conf.dirPath_share_vb}/${pgname}_yahoo_${category}_${targetdate}_02_[0-9][0-9].txt`;
                out_c_file = `${conf.dirPath_share_vb}/${pgname}_yahoo_${category}_${targetdate}.txt`;
                yield make_outfile();
            }
            
        }

        if(runtype == 10){
            yield make_outfile2();
        } 

        //console.log(`  FGS239 make_outfile run: ${runtype} category: ${category} targetdate: ${targetdate} `);
        
        console.log(` /// FGS239 make_outfile.js end   ///  ${st} - ${moment().format('YYYY/MM/DD HH:mm:ss')}`);

        
    });
}

run();