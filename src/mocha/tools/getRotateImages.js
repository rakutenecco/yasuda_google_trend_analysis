'use strict';

const co = require('co');
const expect = require('chai').expect;
const fs = require('fs');
const shell = require('shelljs');

const conf = new(require('../../conf.js'));
const util = new(require('../../modules/util.js'));



describe('tools', function () {

    this.timeout(50000);
    let mes = util.getMessage(100);

    before(function () {
        co(function* () {
            let dirpath = `./${conf.dirPath_out_test}/img/`;
            if (!util.isInFileSizeAll(dirpath, 4000, null, 12, 12)) {
                mes = util.getMessage(101);
            }
        });
    });

    it('getRotateImages.js', function () {

        // check file size of test/img
        expect(mes).to.equal(util.getMessage(100));
    });
});