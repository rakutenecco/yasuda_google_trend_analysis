'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number },
]);

// check file exist
const isFile = function (filepath) {

    let flag = false;

    try {
        const stats = fs.statSync(filepath);
        flag = true;
    } catch (e) {}

    return flag;
}

// init action
const setMyconf = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            let filepath_myconf_sample = `./myconf-sample.json`;
            let filepath_myconf = `./myconf.json`;

            if (!isFile(filepath_myconf)) {
                let data = fs.readFileSync(filepath_myconf_sample, 'utf-8');
                fs.writeFileSync(filepath_myconf, data, 'utf-8');
            }

            resolve();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 1: // usual action
            default:
                yield setMyconf();
        }
    });
}

run();