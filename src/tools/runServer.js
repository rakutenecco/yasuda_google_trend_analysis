'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');

const conf = new(require('../conf.js'));
const log = new(require('../modules/log.js'));
const util = new(require('../modules/util.js'));
const server = new(require('../modules/server.js'));


// parse args
const cli = commandArgs([
    { name: 'run', alias: 'r', type: Number }
]);


// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            resolve();
        });
    });
}

// get rotate images action
const runServerHtdocs = function () {

    return new Promise((resolve, reject) => {
        co(function* () {

            server.init();
            server.runServerHtdocs();
        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // test action

                break;
            case 1: // usual action
            default:
                yield runServerHtdocs();
        }
    });
}

run();