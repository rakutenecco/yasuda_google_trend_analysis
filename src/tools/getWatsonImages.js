'use strict';

const co = require('co');
const commandArgs = require('command-line-args');
const fs = require('fs');
const jimp = require('jimp');
const moment = require('moment');
const shell = require('shelljs');

const conf = new(require('../conf.js'));
const log = new(require('../modules/log.js'));
const util = new(require('../modules/util.js'));
const image = new(require('../modules/image.js'));

//
const today = moment().format('YYYYMMDD');

// parse args
const cli = commandArgs([
    { name: 'extension', alias: 'e', type: String }, // jpg, png
    { name: 'run', alias: 'r', type: Number },
    { name: 'fname', alias: 'f', type: String },
    { name: 'input', alias: 'i', type: String },
    { name: 'output', alias: 'o', type: String }
]);

const extension = cli['extension'] || 'jpg';
const input = cli['input'] || `./${conf.dirPath_in_img}`;
const output = cli['output'] || `./${conf.dirPath_out}/${today}/img/`;
const fname = cli['fname'] || `image.png`;



// init action
const init = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start init');

            util.mkdir(`${output}/watson-images`);
            resolve();
        });
    });
}


// get rotate images action sync
const getWatsonImagesSync = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start getRotateImages');

            let filenames = fs.readdirSync(input);
            for (let i = 0; i < filenames.length; i++) {
                let { canvas, context, images } = yield image.getCanvasContext(300, 300, 'rgba(255,255,255,1)', [`${input}${filenames[i]}`]);
                // context
                context.drawImage(images[0], 0, 0, 300, 300);
                let filename = fname.replace(`.png`, `_${i}.png`);
                yield image.setCanvasContext(canvas, `${output}/watson-images/${filename}`);
            }
        });
    });
}



// test action
const test = function () {

    return new Promise((resolve, reject) => {
        co(function* () {
            log.info(log.base(__filename), 'start test');


        });
    });
}

// run action
const run = function () {

    co(function* () {

        switch (cli['run']) {
            case 2: // test action
                yield init();
                yield test();
                break;
            case 1: // usual action
            default:
                yield init();
                yield getWatsonImagesSync();
        }
    });
}

run();