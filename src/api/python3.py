# This is api for python3
from flask import Flask, request
import json
import python3_class_util

# init
Util = python3_class_util.Util()
app = Flask(__name__)

@app.route('/')
def hello():
    name = "Hello World"
    return name

@app.route('/mecab')
def mecab():
    text = request.args.get('text')
    print(text)
    if text != None:
        res = Util.mecab(text, 'all')
        resJson = json.dumps(res, ensure_ascii=False)
        return resJson
    else :
        return '{"mecab": []}'

@app.route('/mecab/brands')
def brands():
    text = request.args.get('text')

    if text != None:
        res = Util.mecab(text, 'brands')
        resJson = json.dumps(res, ensure_ascii=False)
        return resJson
    else :
        return '{"brands": []}'

## run api
if __name__ == "__main__":
    app.run(debug=True)





