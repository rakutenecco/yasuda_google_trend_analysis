# This is class for python3
import MeCab

class Util :

    # 初期処理
    def __init__(self) :
        # mecab dictionary
        self.m = MeCab.Tagger("-d /usr/lib/mecab/dic/mecab-ipadic-neologd --node-format %M\\t%f[0],%f[1],%f[2],%f[3],%f[4],%f[5],%f[6],%f[7],%f[8]\\n --unk-format %M\\t%f[0],%f[1],%f[2],%f[3],%f[4],%f[5]\\n")
        print("create class for util")

    # reset mecab data to array
    def _resetMecab(self, data):
        arr = []
        m1 = data.split('\n')

        for row in m1:
            m2 = row.replace('\t', ',').split(',')
            m3 = ['', '', '', '', '', '', '']

            # remove EOS and brank
            if len(m2) < 2:
                continue
            if len(m2) > 9:
                m3 = m2[9].split('<__>') # 辞書追加事項
            if len(m3) < 2: # reset
                m3 = ['', '', '', '', '', '', '']

            # set array
            arr2 = []
            arr2.append(m2)
            arr2.append(m3)
            arr.append(arr2)
        return arr

    def mecab(self, text, type):

        m2 = self.m.parse(text)
        datas = self._resetMecab(m2)
        brands = []
        res = {}

        if type == 'brands' :
            for word in datas:
                if word[1][2] and word[1][0] == 'ブランド':
                    brands.append(word[1][2])

            brands = list(set(brands))
            res = {'brands' : brands}

        elif type == 'all' :
            res = {'mecab' : datas}
        else :
            res = {'mecab' : datas}

        return res

